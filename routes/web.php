<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

//Route::group(['middleware' => ['auth','web']], function () {

Route::get('/home', 'HomeController@index');
#Users
Route::resource('users', 'UserController');
Route::get('searchuser','UserController@searchuser');
Route::get('adminprofile','UserController@adminprofile');
Route::post('updateadminprofile','UserController@updateadminprofile');

#Groups
Route::resource('groups', 'GroupController');
Route::get('searchgroup','GroupController@searchgroup');
Route::get('groupmembers/{id}','GroupController@groupmembers');


#Snowflakes
Route::resource('snowflakes', 'SnowflakeController');
Route::get('confirmed/{id}','SnowflakeController@confirmed');
Route::get('reject/{id}','SnowflakeController@reject');

#Resorts
Route::resource('resorts', 'ResortController');
Route::get('create','ResortController@create');
Route::get('get-state-list','ResortController@getStateList');
Route::get('get-city-list','ResortController@getCityList');
Route::get('searchresort','ResortController@searchresort');
Route::get ('importfile','ResortController@importfile');
Route::post('importExcel','ResortController@importExcel');

#Runs
Route::resource('runs', 'RunController');

#Lifts
Route::resource('lifts', 'LiftController');

#Questions
Route::resource('qAS', 'QAController');

#Guide
Route::resource('guides', 'GuideController');

#Weather
Route::resource('weathers', 'WeatherController');

#Tracking
Route::resource('trackings', 'TrackingController');

#Events
Route::resource('events', 'EventController');

#Business
Route::resource('businesses', 'BusinessController');

#Lunch
Route::resource('lunches', 'LunchController');
Route::get('provisional/{id}', ['as' => 'provisional', 'uses' => 'LunchController@provisional']);
Route::get('live/{id}', ['as' => 'live', 'uses' => 'LunchController@live']);

//});



/*test*/
Route::get('test','RunController@test');
Route::post('insert','RunController@insert');

Route::resource('qS', 'QController');
/*test*/





Route::resource('jobs', 'JobController');