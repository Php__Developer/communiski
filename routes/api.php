<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'auth:api'], function(){

});

/*Users API*/
Route::resource('users', 'UserAPIController'); // constructs default routes for the API
Route::post('login','UserAPIController@login');
Route::post('register','UserAPIController@register');
Route::post('forgotPassword','UserAPIController@forgotPassword');
Route::post('changePassword','UserAPIController@changePassword')->middleware('checkUserToken');
Route::get('getOwnProfile', 'UserAPIController@getOwnProfile');
Route::post('editProfile', 'UserAPIController@editProfile')->middleware('checkUserToken');
Route::post('searchNewBuddies', 'UserAPIController@searchNewBuddies')->middleware('checkUserToken');
Route::post('newBuddyRequest', 'UserAPIController@newBuddyRequest')->middleware('checkUserToken');
Route::post('acceptBuddyRequest', 'UserAPIController@acceptBuddyRequest')->middleware('checkUserToken');
Route::post('rejectBuddyRequest', 'UserAPIController@rejectBuddyRequest')->middleware('checkUserToken');
Route::post('cancelBuddyRequest', 'UserAPIController@cancelBuddyRequest')->middleware('checkUserToken');
Route::post('getBuddies', 'UserAPIController@getBuddies')->middleware('checkUserToken');
Route::post('getUserProfile', 'UserAPIController@getUserProfile')->middleware('checkUserToken');
Route::post('removeBuddy', 'UserAPIController@removeBuddy')->middleware('checkUserToken');
Route::post('deleteProfile', 'UserAPIController@deleteProfile')->middleware('checkUserToken');
Route::post('addPermission','UserAPIController@addPermission')->middleware('checkUserToken');
Route::post('removePermission','UserAPIController@removePermission')->middleware('checkUserToken');
Route::post('getAllPermission','UserAPIController@getAllPermission');
Route::post('getPermission','UserAPIController@getPermission');
Route::post('getBusinessPermission','UserAPIController@getBusinessPermission');
Route::post('cancelBusinessPermission','UserAPIController@cancelBusinessPermission');
Route::post('contactUs','UserAPIController@contactUs');
Route::post('sendMail','UserAPIController@sendMail');

/*Groups API*/
Route::resource('groups', 'GroupAPIController');
Route::post('createGroup','GroupAPIController@createGroup');
Route::post('myGroup','GroupAPIController@myGroup');
Route::post('joinGroup','GroupAPIController@joinGroup');
Route::post('acceptgroupRequest','GroupAPIController@acceptgroupRequest');
Route::post('rejectgroupRequest','GroupAPIController@rejectgroupRequest');
Route::post('openGroup','GroupAPIController@openGroup');
Route::post('searchGroup','GroupAPIController@searchGroup');
Route::post('groupDetails','GroupAPIController@groupDetails');
Route::post('groupInfo','GroupAPIController@groupInfo');
Route::post('cancelgrpRequest','GroupAPIController@cancelgrpRequest');

/*Snowflakes API*/
Route::resource('snowflakes', 'SnowflakeAPIController');
Route::post('snowflakes_history','SnowflakeAPIController@snowflakes_history');

/*Resorts API*/
Route::resource('resorts', 'ResortAPIController');
Route::post('resortDetail','ResortAPIController@resortDetail');
Route::post('addresortDetail','ResortAPIController@addresortDetail');
Route::post('getResorts','ResortAPIController@getResorts');
Route::post('getNote','ResortAPIController@getNote');
Route::post('getHistory','ResortAPIController@getHistory');
Route::post('countrystateResort','ResortAPIController@countrystateResort');
Route::post('getCodes','ResortAPIController@getCodes');
Route::post('regenerateCodes','ResortAPIController@regenerateCodes');
Route::post('regenerateCodes_test','ResortAPIController@regenerateCodes_test');
Route::post('getterrainHistory','ResortAPIController@getterrainHistory');
Route::post('adminSettings','ResortAPIController@adminSettings');
Route::post('getadminSettings','ResortAPIController@getadminSettings');
Route::post('checklifts','ResortAPIController@checklifts');
Route::post('thumbsup','ResortAPIController@thumbsup');
Route::post('getthumbscount','ResortAPIController@getthumbscount');
Route::post('terrainThumbsup','ResortAPIController@terrainThumbsup');
Route::post('getterrainThumbsup','ResortAPIController@getterrainThumbsup');
Route::post('checkAdmin','ResortAPIController@checkAdmin');
Route::post('getRunGrades','ResortAPIController@getRunGrades');
// Route::post('addFavResort','ResortAPIController@addFavResort');
// Route::post('getFavResorts','ResortAPIController@getFavResorts');
Route::post('addTofav','ResortAPIController@addTofav');
Route::post('getFav','ResortAPIController@getFav');
Route::post('getnearbyResorts','ResortAPIController@getnearbyResorts');


/*Runs API*/
Route::resource('runs', 'RunAPIController');
Route::post('addRun','RunAPIController@addRun');
Route::post('getRun','RunAPIController@getRun');
Route::post('editRun','RunAPIController@editRun');
Route::post('runHistory','RunAPIController@runHistory');
Route::post('changeRunStatus','RunAPIController@changeRunStatus');
Route::post('addNotes','RunAPIController@addNotes');
Route::post('getNotes','RunAPIController@getNotes');

/*Testing*/
Route::post('myGroup_new','GroupAPIController@myGroup_new');
Route::post('openGroup_new','GroupAPIController@openGroup_new');

/*Lifts API*/
Route::resource('lifts', 'LiftAPIController');
Route::post('addLift','LiftAPIController@addLift');
Route::post('getLift','LiftAPIController@getLift');
Route::post('editLift','LiftAPIController@editLift');
Route::post('liftHistory','LiftAPIController@liftHistory');
Route::post('changeLiftStatus','LiftAPIController@changeLiftStatus');

/*Questions API*/
Route::resource('q_a_s', 'QAAPIController');
Route::post('askQuestion','QAAPIController@askQuestion');
Route::post('browseQuestions','QAAPIController@browseQuestions');
Route::post('unansweredQuestions','QAAPIController@unansweredQuestions');
Route::post('answerQuestion','QAAPIController@answerQuestion');
Route::post('getuserQA','QAAPIController@getuserQA');
Route::post('markDefault','QAAPIController@markDefault');
Route::post('chooseAnswer','QAAPIController@chooseAnswer');
Route::post('refundRewards','QAAPIController@refundRewards');
Route::post('searchQuestions','QAAPIController@searchQuestions');
Route::post('newAnswer','QAAPIController@newAnswer');
Route::post('archiveUnarchive','QAAPIController@archiveUnarchive');
Route::post('get_archiveQues','QAAPIController@get_archiveQues');
Route::post('editTopics','QAAPIController@editTopics');
Route::post('demo1','QAAPIController@demo1');

/*Guides & Recommendations API*/
Route::resource('guides', 'GuideAPIController');
Route::post('getGuides','GuideAPIController@getGuides');
Route::post('get_guides_desc','GuideAPIController@get_guides_desc');
Route::post('add_like_desc','GuideAPIController@add_like_desc');
Route::post('guidesRating','GuideAPIController@guidesRating');
Route::post('guideDescription','GuideAPIController@guideDescription');
Route::post('guideHistory','GuideAPIController@guideHistory');
Route::post('thumbs_up_rec_desc','GuideAPIController@thumbs_up_rec_desc');
Route::post('get_desc_count','GuideAPIController@get_desc_count');
Route::post('noteHistory','GuideAPIController@noteHistory');
Route::post('makeRecommendations','GuideAPIController@makeRecommendations');
Route::post('recommendedLike','GuideAPIController@recommendedLike');
Route::post('getRecommendedRun','GuideAPIController@getRecommendedRun');
Route::post('mark_archive_unarchive','GuideAPIController@mark_archive_unarchive');

/*Weather API's*/
Route::resource('weathers', 'WeatherAPIController');
Route::post('weatherReport','WeatherAPIController@weatherReport');
Route::post('getSnowHistory','WeatherAPIController@getSnowHistory');
Route::post('getcurrentReport','WeatherAPIController@getcurrentReport');
Route::post('eighthourData','WeatherAPIController@eighthourData');
Route::post('getEightHour','WeatherAPIController@getEightHour');
Route::post('threehourData','WeatherAPIController@threehourData');
Route::post('getthreeHour','WeatherAPIController@getthreeHour');

/*Tracking API's*/
Route::resource('trackings', 'TrackingAPIController');
Route::post('shareStatus','TrackingAPIController@shareStatus');
Route::post('getStatus','TrackingAPIController@getStatus');
Route::post('sharedlocationFriends','TrackingAPIController@sharedlocationFriends');
Route::post('getCurrentLoc','TrackingAPIController@getCurrentLoc');
Route::post('checkmessage','TrackingAPIController@checkmessage');

/*Events API's*/
Route::resource('events', 'EventAPIController');
Route::post('addEvent','EventAPIController@addEvent');
Route::post('getEvents','EventAPIController@getEvents');
Route::post('businessEvents','EventAPIController@businessEvents');
Route::post('eventDetails','EventAPIController@eventDetails');
Route::post('attendEvent','EventAPIController@attendEvent');
Route::post('searchEvent','EventAPIController@searchEvent');
Route::post('myevents','EventAPIController@myevents');
Route::post('buddysEvent','EventAPIController@buddysEvent');
Route::post('businessListing','EventAPIController@businessListing');

/*Business API's*/
Route::resource('businesses', 'BusinessAPIController');
Route::post('addBusiness','BusinessAPIController@addBusiness');
Route::post('checkBusinessLocation','BusinessAPIController@checkBusinessLocation');
Route::post('editBusiness','BusinessAPIController@editBusiness');
Route::post('editBusinessAdminSetting','BusinessAPIController@editBusinessAdminSetting');
Route::post('getbusinessAdminSetting','BusinessAPIController@getbusinessAdminSetting');
Route::post('regenerateBusinessCode','BusinessAPIController@regenerateBusinessCode');
Route::post('serviceHistory','BusinessAPIController@serviceHistory');
Route::post('getListing','BusinessAPIController@getListing');
Route::post('getService','BusinessAPIController@getService');
Route::post('getServices_Activities','BusinessAPIController@getServices_Activities');
Route::post('updateBusiness','BusinessAPIController@updateBusiness');
Route::post('updatebusinessHours','BusinessAPIController@updatebusinessHours');
Route::post('deleteBusinessHour','BusinessAPIController@deleteBusinessHour');
Route::post('getbusinessHours','BusinessAPIController@getbusinessHours');
Route::post('hoursNotes','BusinessAPIController@hoursNotes');
Route::post('searchByArea','BusinessAPIController@searchByArea');
Route::post('searchService','BusinessAPIController@searchService');
Route::post('getActivity','BusinessAPIController@getActivity');
Route::post('businessNotes','BusinessAPIController@businessNotes');
Route::post('getLocationsInRadius','BusinessAPIController@getLocationsInRadius');
Route::post('updatenearbytowns','BusinessAPIController@updatenearbytowns');
Route::post('getNearbyTowns','BusinessAPIController@getNearbyTowns');
Route::post('searchHotels','BusinessAPIController@searchHotels');
Route::post('searchRestaurants','BusinessAPIController@searchRestaurants');
Route::post('searchShops','BusinessAPIController@searchShops');
Route::post('searchActivities','BusinessAPIController@searchActivities');
Route::post('openCageData','BusinessAPIController@openCageData');
Route::post('reverseOpenCage','BusinessAPIController@reverseOpenCage');

/*Lunches APIs*/
Route::resource('lunches', 'LunchAPIController');
Route::post('lunchRequest','LunchAPIController@lunchRequest');
Route::post('updateLunchSettings','LunchAPIController@updateLunchSettings');
Route::get('getLunchSettings','LunchAPIController@getLunchSettings');
Route::post('addLunchItems','LunchAPIController@addLunchItems');
Route::get('getLunchItems','LunchAPIController@getLunchItems');
Route::get('availablelunchItems','LunchAPIController@availablelunchItems');
Route::post('updateStatus','LunchAPIController@updateStatus');
Route::delete('deleteLunchItem','LunchAPIController@deleteLunchItem');
Route::post('countrystateResortLunches','LunchAPIController@countrystateResortLunches');
Route::post('orderLunch','LunchAPIController@orderLunch');
Route::get('preparingLunches','LunchAPIController@preparingLunches');
Route::post('chooseLunch','LunchAPIController@chooseLunch');
Route::get('getOrderedLunch','LunchAPIController@getOrderedLunch');
Route::post('accept_reject_orders','LunchAPIController@accept_reject_orders');
Route::get('upcomming_old_Orders','LunchAPIController@upcomming_old_Orders');
Route::get('commingOrders','LunchAPIController@commingOrders');
Route::delete('cancelOrder','LunchAPIController@cancelOrder');

/*Job APIs*/
Route::resource('jobs', 'JobAPIController');
Route::post('requestJob','JobAPIController@requestJob');
Route::post('postJob','JobAPIController@postJob');
Route::post('getJobs','JobAPIController@getJobs');
Route::post('createResume','JobAPIController@createResume');
Route::post('educationDetails','JobAPIController@educationDetails');
Route::post('workHistory','JobAPIController@workHistory');
Route::get('getResume','JobAPIController@getResume');
Route::post('jobsListing','JobAPIController@jobsListing');
Route::get('adminJobListing','JobAPIController@adminJobListing');
Route::get('closedJobListing','JobAPIController@closedJobListing');
Route::post('updateJobStatus','JobAPIController@updateJobStatus');
Route::post('applyJob','JobAPIController@applyJob');
Route::get('updatingjobs','JobAPIController@updatingjobs');
Route::post('deleteResumeItems','JobAPIController@deleteResumeItems');
Route::post('update_status','JobAPIController@update_status');
Route::get('myJobs','JobAPIController@myJobs');
Route::get('candidatesListing','JobAPIController@candidatesListing');

/*Stripe API*/
// Route::post('create_stripe_user','JobAPIController@create_stripe_user');
// Route::post('create_stripe_account','JobAPIController@create_stripe_account');
Route::post('addCard','JobAPIController@addCard');
Route::post('getCard','JobAPIController@getCard');
#using stripe connect
Route::get('stripeConnect','JobAPIController@stripeConnect');
Route::post('link_stripe','JobAPIController@link_stripe');
Route::post('set_default_card',['as' => 'set_default_card','uses' => 'JobAPIController@set_default_card']);
Route::post('removeCard',['as' => 'removeCard','uses' => 'JobAPIController@removeCard']);
