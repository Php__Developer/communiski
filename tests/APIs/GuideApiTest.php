<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\Traits\MakeGuideTrait;
use Tests\ApiTestTrait;

class GuideApiTest extends TestCase
{
    use MakeGuideTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_guide()
    {
        $guide = $this->fakeGuideData();
        $this->response = $this->json('POST', '/api/guides', $guide);

        $this->assertApiResponse($guide);
    }

    /**
     * @test
     */
    public function test_read_guide()
    {
        $guide = $this->makeGuide();
        $this->response = $this->json('GET', '/api/guides/'.$guide->id);

        $this->assertApiResponse($guide->toArray());
    }

    /**
     * @test
     */
    public function test_update_guide()
    {
        $guide = $this->makeGuide();
        $editedGuide = $this->fakeGuideData();

        $this->response = $this->json('PUT', '/api/guides/'.$guide->id, $editedGuide);

        $this->assertApiResponse($editedGuide);
    }

    /**
     * @test
     */
    public function test_delete_guide()
    {
        $guide = $this->makeGuide();
        $this->response = $this->json('DELETE', '/api/guides/'.$guide->id);

        $this->assertApiSuccess();
        $this->response = $this->json('GET', '/api/guides/'.$guide->id);

        $this->response->assertStatus(404);
    }
}
