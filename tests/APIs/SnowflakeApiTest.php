<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\Traits\MakeSnowflakeTrait;
use Tests\ApiTestTrait;

class SnowflakeApiTest extends TestCase
{
    use MakeSnowflakeTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_snowflake()
    {
        $snowflake = $this->fakeSnowflakeData();
        $this->response = $this->json('POST', '/api/snowflakes', $snowflake);

        $this->assertApiResponse($snowflake);
    }

    /**
     * @test
     */
    public function test_read_snowflake()
    {
        $snowflake = $this->makeSnowflake();
        $this->response = $this->json('GET', '/api/snowflakes/'.$snowflake->id);

        $this->assertApiResponse($snowflake->toArray());
    }

    /**
     * @test
     */
    public function test_update_snowflake()
    {
        $snowflake = $this->makeSnowflake();
        $editedSnowflake = $this->fakeSnowflakeData();

        $this->response = $this->json('PUT', '/api/snowflakes/'.$snowflake->id, $editedSnowflake);

        $this->assertApiResponse($editedSnowflake);
    }

    /**
     * @test
     */
    public function test_delete_snowflake()
    {
        $snowflake = $this->makeSnowflake();
        $this->response = $this->json('DELETE', '/api/snowflakes/'.$snowflake->id);

        $this->assertApiSuccess();
        $this->response = $this->json('GET', '/api/snowflakes/'.$snowflake->id);

        $this->response->assertStatus(404);
    }
}
