<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Tracking;

class TrackingApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_tracking()
    {
        $tracking = factory(Tracking::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/trackings', $tracking
        );

        $this->assertApiResponse($tracking);
    }

    /**
     * @test
     */
    public function test_read_tracking()
    {
        $tracking = factory(Tracking::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/trackings/'.$tracking->id
        );

        $this->assertApiResponse($tracking->toArray());
    }

    /**
     * @test
     */
    public function test_update_tracking()
    {
        $tracking = factory(Tracking::class)->create();
        $editedTracking = factory(Tracking::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/trackings/'.$tracking->id,
            $editedTracking
        );

        $this->assertApiResponse($editedTracking);
    }

    /**
     * @test
     */
    public function test_delete_tracking()
    {
        $tracking = factory(Tracking::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/trackings/'.$tracking->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/trackings/'.$tracking->id
        );

        $this->response->assertStatus(404);
    }
}
