<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\Traits\MakeQATrait;
use Tests\ApiTestTrait;

class QAApiTest extends TestCase
{
    use MakeQATrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_q_a()
    {
        $qA = $this->fakeQAData();
        $this->response = $this->json('POST', '/api/qAS', $qA);

        $this->assertApiResponse($qA);
    }

    /**
     * @test
     */
    public function test_read_q_a()
    {
        $qA = $this->makeQA();
        $this->response = $this->json('GET', '/api/qAS/'.$qA->id);

        $this->assertApiResponse($qA->toArray());
    }

    /**
     * @test
     */
    public function test_update_q_a()
    {
        $qA = $this->makeQA();
        $editedQA = $this->fakeQAData();

        $this->response = $this->json('PUT', '/api/qAS/'.$qA->id, $editedQA);

        $this->assertApiResponse($editedQA);
    }

    /**
     * @test
     */
    public function test_delete_q_a()
    {
        $qA = $this->makeQA();
        $this->response = $this->json('DELETE', '/api/qAS/'.$qA->id);

        $this->assertApiSuccess();
        $this->response = $this->json('GET', '/api/qAS/'.$qA->id);

        $this->response->assertStatus(404);
    }
}
