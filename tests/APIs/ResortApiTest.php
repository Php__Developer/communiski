<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\Traits\MakeResortTrait;
use Tests\ApiTestTrait;

class ResortApiTest extends TestCase
{
    use MakeResortTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_resort()
    {
        $resort = $this->fakeResortData();
        $this->response = $this->json('POST', '/api/resorts', $resort);

        $this->assertApiResponse($resort);
    }

    /**
     * @test
     */
    public function test_read_resort()
    {
        $resort = $this->makeResort();
        $this->response = $this->json('GET', '/api/resorts/'.$resort->id);

        $this->assertApiResponse($resort->toArray());
    }

    /**
     * @test
     */
    public function test_update_resort()
    {
        $resort = $this->makeResort();
        $editedResort = $this->fakeResortData();

        $this->response = $this->json('PUT', '/api/resorts/'.$resort->id, $editedResort);

        $this->assertApiResponse($editedResort);
    }

    /**
     * @test
     */
    public function test_delete_resort()
    {
        $resort = $this->makeResort();
        $this->response = $this->json('DELETE', '/api/resorts/'.$resort->id);

        $this->assertApiSuccess();
        $this->response = $this->json('GET', '/api/resorts/'.$resort->id);

        $this->response->assertStatus(404);
    }
}
