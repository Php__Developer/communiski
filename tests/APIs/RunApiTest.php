<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\Traits\MakeRunTrait;
use Tests\ApiTestTrait;

class RunApiTest extends TestCase
{
    use MakeRunTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_run()
    {
        $run = $this->fakeRunData();
        $this->response = $this->json('POST', '/api/runs', $run);

        $this->assertApiResponse($run);
    }

    /**
     * @test
     */
    public function test_read_run()
    {
        $run = $this->makeRun();
        $this->response = $this->json('GET', '/api/runs/'.$run->id);

        $this->assertApiResponse($run->toArray());
    }

    /**
     * @test
     */
    public function test_update_run()
    {
        $run = $this->makeRun();
        $editedRun = $this->fakeRunData();

        $this->response = $this->json('PUT', '/api/runs/'.$run->id, $editedRun);

        $this->assertApiResponse($editedRun);
    }

    /**
     * @test
     */
    public function test_delete_run()
    {
        $run = $this->makeRun();
        $this->response = $this->json('DELETE', '/api/runs/'.$run->id);

        $this->assertApiSuccess();
        $this->response = $this->json('GET', '/api/runs/'.$run->id);

        $this->response->assertStatus(404);
    }
}
