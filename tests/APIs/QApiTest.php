<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\Traits\MakeQTrait;
use Tests\ApiTestTrait;

class QApiTest extends TestCase
{
    use MakeQTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_q()
    {
        $q = $this->fakeQData();
        $this->response = $this->json('POST', '/api/qS', $q);

        $this->assertApiResponse($q);
    }

    /**
     * @test
     */
    public function test_read_q()
    {
        $q = $this->makeQ();
        $this->response = $this->json('GET', '/api/qS/'.$q->id);

        $this->assertApiResponse($q->toArray());
    }

    /**
     * @test
     */
    public function test_update_q()
    {
        $q = $this->makeQ();
        $editedQ = $this->fakeQData();

        $this->response = $this->json('PUT', '/api/qS/'.$q->id, $editedQ);

        $this->assertApiResponse($editedQ);
    }

    /**
     * @test
     */
    public function test_delete_q()
    {
        $q = $this->makeQ();
        $this->response = $this->json('DELETE', '/api/qS/'.$q->id);

        $this->assertApiSuccess();
        $this->response = $this->json('GET', '/api/qS/'.$q->id);

        $this->response->assertStatus(404);
    }
}
