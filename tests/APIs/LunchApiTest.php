<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Lunch;

class LunchApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_lunch()
    {
        $lunch = factory(Lunch::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/lunches', $lunch
        );

        $this->assertApiResponse($lunch);
    }

    /**
     * @test
     */
    public function test_read_lunch()
    {
        $lunch = factory(Lunch::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/lunches/'.$lunch->id
        );

        $this->assertApiResponse($lunch->toArray());
    }

    /**
     * @test
     */
    public function test_update_lunch()
    {
        $lunch = factory(Lunch::class)->create();
        $editedLunch = factory(Lunch::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/lunches/'.$lunch->id,
            $editedLunch
        );

        $this->assertApiResponse($editedLunch);
    }

    /**
     * @test
     */
    public function test_delete_lunch()
    {
        $lunch = factory(Lunch::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/lunches/'.$lunch->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/lunches/'.$lunch->id
        );

        $this->response->assertStatus(404);
    }
}
