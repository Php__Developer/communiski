<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\Traits\MakeGroupApiControllerTrait;
use Tests\ApiTestTrait;

class GroupApiControllerApiTest extends TestCase
{
    use MakeGroupApiControllerTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_group_api_controller()
    {
        $groupApiController = $this->fakeGroupApiControllerData();
        $this->response = $this->json('POST', '/api/groupApiControllers', $groupApiController);

        $this->assertApiResponse($groupApiController);
    }

    /**
     * @test
     */
    public function test_read_group_api_controller()
    {
        $groupApiController = $this->makeGroupApiController();
        $this->response = $this->json('GET', '/api/groupApiControllers/'.$groupApiController->id);

        $this->assertApiResponse($groupApiController->toArray());
    }

    /**
     * @test
     */
    public function test_update_group_api_controller()
    {
        $groupApiController = $this->makeGroupApiController();
        $editedGroupApiController = $this->fakeGroupApiControllerData();

        $this->response = $this->json('PUT', '/api/groupApiControllers/'.$groupApiController->id, $editedGroupApiController);

        $this->assertApiResponse($editedGroupApiController);
    }

    /**
     * @test
     */
    public function test_delete_group_api_controller()
    {
        $groupApiController = $this->makeGroupApiController();
        $this->response = $this->json('DELETE', '/api/groupApiControllers/'.$groupApiController->id);

        $this->assertApiSuccess();
        $this->response = $this->json('GET', '/api/groupApiControllers/'.$groupApiController->id);

        $this->response->assertStatus(404);
    }
}
