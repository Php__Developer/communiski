<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\Traits\MakeLiftTrait;
use Tests\ApiTestTrait;

class LiftApiTest extends TestCase
{
    use MakeLiftTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_lift()
    {
        $lift = $this->fakeLiftData();
        $this->response = $this->json('POST', '/api/lifts', $lift);

        $this->assertApiResponse($lift);
    }

    /**
     * @test
     */
    public function test_read_lift()
    {
        $lift = $this->makeLift();
        $this->response = $this->json('GET', '/api/lifts/'.$lift->id);

        $this->assertApiResponse($lift->toArray());
    }

    /**
     * @test
     */
    public function test_update_lift()
    {
        $lift = $this->makeLift();
        $editedLift = $this->fakeLiftData();

        $this->response = $this->json('PUT', '/api/lifts/'.$lift->id, $editedLift);

        $this->assertApiResponse($editedLift);
    }

    /**
     * @test
     */
    public function test_delete_lift()
    {
        $lift = $this->makeLift();
        $this->response = $this->json('DELETE', '/api/lifts/'.$lift->id);

        $this->assertApiSuccess();
        $this->response = $this->json('GET', '/api/lifts/'.$lift->id);

        $this->response->assertStatus(404);
    }
}
