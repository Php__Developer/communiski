<?php namespace Tests\Repositories;

use App\Models\Lunch;
use App\Repositories\LunchRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class LunchRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var LunchRepository
     */
    protected $lunchRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->lunchRepo = \App::make(LunchRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_lunch()
    {
        $lunch = factory(Lunch::class)->make()->toArray();

        $createdLunch = $this->lunchRepo->create($lunch);

        $createdLunch = $createdLunch->toArray();
        $this->assertArrayHasKey('id', $createdLunch);
        $this->assertNotNull($createdLunch['id'], 'Created Lunch must have id specified');
        $this->assertNotNull(Lunch::find($createdLunch['id']), 'Lunch with given id must be in DB');
        $this->assertModelData($lunch, $createdLunch);
    }

    /**
     * @test read
     */
    public function test_read_lunch()
    {
        $lunch = factory(Lunch::class)->create();

        $dbLunch = $this->lunchRepo->find($lunch->id);

        $dbLunch = $dbLunch->toArray();
        $this->assertModelData($lunch->toArray(), $dbLunch);
    }

    /**
     * @test update
     */
    public function test_update_lunch()
    {
        $lunch = factory(Lunch::class)->create();
        $fakeLunch = factory(Lunch::class)->make()->toArray();

        $updatedLunch = $this->lunchRepo->update($fakeLunch, $lunch->id);

        $this->assertModelData($fakeLunch, $updatedLunch->toArray());
        $dbLunch = $this->lunchRepo->find($lunch->id);
        $this->assertModelData($fakeLunch, $dbLunch->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_lunch()
    {
        $lunch = factory(Lunch::class)->create();

        $resp = $this->lunchRepo->delete($lunch->id);

        $this->assertTrue($resp);
        $this->assertNull(Lunch::find($lunch->id), 'Lunch should not exist in DB');
    }
}
