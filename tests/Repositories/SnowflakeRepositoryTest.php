<?php namespace Tests\Repositories;

use App\Models\Snowflake;
use App\Repositories\SnowflakeRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\Traits\MakeSnowflakeTrait;
use Tests\ApiTestTrait;

class SnowflakeRepositoryTest extends TestCase
{
    use MakeSnowflakeTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var SnowflakeRepository
     */
    protected $snowflakeRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->snowflakeRepo = \App::make(SnowflakeRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_snowflake()
    {
        $snowflake = $this->fakeSnowflakeData();
        $createdSnowflake = $this->snowflakeRepo->create($snowflake);
        $createdSnowflake = $createdSnowflake->toArray();
        $this->assertArrayHasKey('id', $createdSnowflake);
        $this->assertNotNull($createdSnowflake['id'], 'Created Snowflake must have id specified');
        $this->assertNotNull(Snowflake::find($createdSnowflake['id']), 'Snowflake with given id must be in DB');
        $this->assertModelData($snowflake, $createdSnowflake);
    }

    /**
     * @test read
     */
    public function test_read_snowflake()
    {
        $snowflake = $this->makeSnowflake();
        $dbSnowflake = $this->snowflakeRepo->find($snowflake->id);
        $dbSnowflake = $dbSnowflake->toArray();
        $this->assertModelData($snowflake->toArray(), $dbSnowflake);
    }

    /**
     * @test update
     */
    public function test_update_snowflake()
    {
        $snowflake = $this->makeSnowflake();
        $fakeSnowflake = $this->fakeSnowflakeData();
        $updatedSnowflake = $this->snowflakeRepo->update($fakeSnowflake, $snowflake->id);
        $this->assertModelData($fakeSnowflake, $updatedSnowflake->toArray());
        $dbSnowflake = $this->snowflakeRepo->find($snowflake->id);
        $this->assertModelData($fakeSnowflake, $dbSnowflake->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_snowflake()
    {
        $snowflake = $this->makeSnowflake();
        $resp = $this->snowflakeRepo->delete($snowflake->id);
        $this->assertTrue($resp);
        $this->assertNull(Snowflake::find($snowflake->id), 'Snowflake should not exist in DB');
    }
}
