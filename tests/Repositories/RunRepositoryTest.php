<?php namespace Tests\Repositories;

use App\Models\Run;
use App\Repositories\RunRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\Traits\MakeRunTrait;
use Tests\ApiTestTrait;

class RunRepositoryTest extends TestCase
{
    use MakeRunTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var RunRepository
     */
    protected $runRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->runRepo = \App::make(RunRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_run()
    {
        $run = $this->fakeRunData();
        $createdRun = $this->runRepo->create($run);
        $createdRun = $createdRun->toArray();
        $this->assertArrayHasKey('id', $createdRun);
        $this->assertNotNull($createdRun['id'], 'Created Run must have id specified');
        $this->assertNotNull(Run::find($createdRun['id']), 'Run with given id must be in DB');
        $this->assertModelData($run, $createdRun);
    }

    /**
     * @test read
     */
    public function test_read_run()
    {
        $run = $this->makeRun();
        $dbRun = $this->runRepo->find($run->id);
        $dbRun = $dbRun->toArray();
        $this->assertModelData($run->toArray(), $dbRun);
    }

    /**
     * @test update
     */
    public function test_update_run()
    {
        $run = $this->makeRun();
        $fakeRun = $this->fakeRunData();
        $updatedRun = $this->runRepo->update($fakeRun, $run->id);
        $this->assertModelData($fakeRun, $updatedRun->toArray());
        $dbRun = $this->runRepo->find($run->id);
        $this->assertModelData($fakeRun, $dbRun->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_run()
    {
        $run = $this->makeRun();
        $resp = $this->runRepo->delete($run->id);
        $this->assertTrue($resp);
        $this->assertNull(Run::find($run->id), 'Run should not exist in DB');
    }
}
