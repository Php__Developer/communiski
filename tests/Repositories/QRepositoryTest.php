<?php namespace Tests\Repositories;

use App\Models\Q;
use App\Repositories\QRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\Traits\MakeQTrait;
use Tests\ApiTestTrait;

class QRepositoryTest extends TestCase
{
    use MakeQTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var QRepository
     */
    protected $qRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->qRepo = \App::make(QRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_q()
    {
        $q = $this->fakeQData();
        $createdQ = $this->qRepo->create($q);
        $createdQ = $createdQ->toArray();
        $this->assertArrayHasKey('id', $createdQ);
        $this->assertNotNull($createdQ['id'], 'Created Q must have id specified');
        $this->assertNotNull(Q::find($createdQ['id']), 'Q with given id must be in DB');
        $this->assertModelData($q, $createdQ);
    }

    /**
     * @test read
     */
    public function test_read_q()
    {
        $q = $this->makeQ();
        $dbQ = $this->qRepo->find($q->id);
        $dbQ = $dbQ->toArray();
        $this->assertModelData($q->toArray(), $dbQ);
    }

    /**
     * @test update
     */
    public function test_update_q()
    {
        $q = $this->makeQ();
        $fakeQ = $this->fakeQData();
        $updatedQ = $this->qRepo->update($fakeQ, $q->id);
        $this->assertModelData($fakeQ, $updatedQ->toArray());
        $dbQ = $this->qRepo->find($q->id);
        $this->assertModelData($fakeQ, $dbQ->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_q()
    {
        $q = $this->makeQ();
        $resp = $this->qRepo->delete($q->id);
        $this->assertTrue($resp);
        $this->assertNull(Q::find($q->id), 'Q should not exist in DB');
    }
}
