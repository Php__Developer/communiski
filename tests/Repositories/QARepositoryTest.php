<?php namespace Tests\Repositories;

use App\Models\QA;
use App\Repositories\QARepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\Traits\MakeQATrait;
use Tests\ApiTestTrait;

class QARepositoryTest extends TestCase
{
    use MakeQATrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var QARepository
     */
    protected $qARepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->qARepo = \App::make(QARepository::class);
    }

    /**
     * @test create
     */
    public function test_create_q_a()
    {
        $qA = $this->fakeQAData();
        $createdQA = $this->qARepo->create($qA);
        $createdQA = $createdQA->toArray();
        $this->assertArrayHasKey('id', $createdQA);
        $this->assertNotNull($createdQA['id'], 'Created QA must have id specified');
        $this->assertNotNull(QA::find($createdQA['id']), 'QA with given id must be in DB');
        $this->assertModelData($qA, $createdQA);
    }

    /**
     * @test read
     */
    public function test_read_q_a()
    {
        $qA = $this->makeQA();
        $dbQA = $this->qARepo->find($qA->id);
        $dbQA = $dbQA->toArray();
        $this->assertModelData($qA->toArray(), $dbQA);
    }

    /**
     * @test update
     */
    public function test_update_q_a()
    {
        $qA = $this->makeQA();
        $fakeQA = $this->fakeQAData();
        $updatedQA = $this->qARepo->update($fakeQA, $qA->id);
        $this->assertModelData($fakeQA, $updatedQA->toArray());
        $dbQA = $this->qARepo->find($qA->id);
        $this->assertModelData($fakeQA, $dbQA->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_q_a()
    {
        $qA = $this->makeQA();
        $resp = $this->qARepo->delete($qA->id);
        $this->assertTrue($resp);
        $this->assertNull(QA::find($qA->id), 'QA should not exist in DB');
    }
}
