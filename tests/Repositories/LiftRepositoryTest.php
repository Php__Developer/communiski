<?php namespace Tests\Repositories;

use App\Models\Lift;
use App\Repositories\LiftRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\Traits\MakeLiftTrait;
use Tests\ApiTestTrait;

class LiftRepositoryTest extends TestCase
{
    use MakeLiftTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var LiftRepository
     */
    protected $liftRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->liftRepo = \App::make(LiftRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_lift()
    {
        $lift = $this->fakeLiftData();
        $createdLift = $this->liftRepo->create($lift);
        $createdLift = $createdLift->toArray();
        $this->assertArrayHasKey('id', $createdLift);
        $this->assertNotNull($createdLift['id'], 'Created Lift must have id specified');
        $this->assertNotNull(Lift::find($createdLift['id']), 'Lift with given id must be in DB');
        $this->assertModelData($lift, $createdLift);
    }

    /**
     * @test read
     */
    public function test_read_lift()
    {
        $lift = $this->makeLift();
        $dbLift = $this->liftRepo->find($lift->id);
        $dbLift = $dbLift->toArray();
        $this->assertModelData($lift->toArray(), $dbLift);
    }

    /**
     * @test update
     */
    public function test_update_lift()
    {
        $lift = $this->makeLift();
        $fakeLift = $this->fakeLiftData();
        $updatedLift = $this->liftRepo->update($fakeLift, $lift->id);
        $this->assertModelData($fakeLift, $updatedLift->toArray());
        $dbLift = $this->liftRepo->find($lift->id);
        $this->assertModelData($fakeLift, $dbLift->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_lift()
    {
        $lift = $this->makeLift();
        $resp = $this->liftRepo->delete($lift->id);
        $this->assertTrue($resp);
        $this->assertNull(Lift::find($lift->id), 'Lift should not exist in DB');
    }
}
