<?php namespace Tests\Repositories;

use App\Models\GroupApiController;
use App\Repositories\GroupApiControllerRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\Traits\MakeGroupApiControllerTrait;
use Tests\ApiTestTrait;

class GroupApiControllerRepositoryTest extends TestCase
{
    use MakeGroupApiControllerTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var GroupApiControllerRepository
     */
    protected $groupApiControllerRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->groupApiControllerRepo = \App::make(GroupApiControllerRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_group_api_controller()
    {
        $groupApiController = $this->fakeGroupApiControllerData();
        $createdGroupApiController = $this->groupApiControllerRepo->create($groupApiController);
        $createdGroupApiController = $createdGroupApiController->toArray();
        $this->assertArrayHasKey('id', $createdGroupApiController);
        $this->assertNotNull($createdGroupApiController['id'], 'Created GroupApiController must have id specified');
        $this->assertNotNull(GroupApiController::find($createdGroupApiController['id']), 'GroupApiController with given id must be in DB');
        $this->assertModelData($groupApiController, $createdGroupApiController);
    }

    /**
     * @test read
     */
    public function test_read_group_api_controller()
    {
        $groupApiController = $this->makeGroupApiController();
        $dbGroupApiController = $this->groupApiControllerRepo->find($groupApiController->id);
        $dbGroupApiController = $dbGroupApiController->toArray();
        $this->assertModelData($groupApiController->toArray(), $dbGroupApiController);
    }

    /**
     * @test update
     */
    public function test_update_group_api_controller()
    {
        $groupApiController = $this->makeGroupApiController();
        $fakeGroupApiController = $this->fakeGroupApiControllerData();
        $updatedGroupApiController = $this->groupApiControllerRepo->update($fakeGroupApiController, $groupApiController->id);
        $this->assertModelData($fakeGroupApiController, $updatedGroupApiController->toArray());
        $dbGroupApiController = $this->groupApiControllerRepo->find($groupApiController->id);
        $this->assertModelData($fakeGroupApiController, $dbGroupApiController->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_group_api_controller()
    {
        $groupApiController = $this->makeGroupApiController();
        $resp = $this->groupApiControllerRepo->delete($groupApiController->id);
        $this->assertTrue($resp);
        $this->assertNull(GroupApiController::find($groupApiController->id), 'GroupApiController should not exist in DB');
    }
}
