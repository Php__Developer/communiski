<?php namespace Tests\Repositories;

use App\Models\Guide;
use App\Repositories\GuideRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\Traits\MakeGuideTrait;
use Tests\ApiTestTrait;

class GuideRepositoryTest extends TestCase
{
    use MakeGuideTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var GuideRepository
     */
    protected $guideRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->guideRepo = \App::make(GuideRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_guide()
    {
        $guide = $this->fakeGuideData();
        $createdGuide = $this->guideRepo->create($guide);
        $createdGuide = $createdGuide->toArray();
        $this->assertArrayHasKey('id', $createdGuide);
        $this->assertNotNull($createdGuide['id'], 'Created Guide must have id specified');
        $this->assertNotNull(Guide::find($createdGuide['id']), 'Guide with given id must be in DB');
        $this->assertModelData($guide, $createdGuide);
    }

    /**
     * @test read
     */
    public function test_read_guide()
    {
        $guide = $this->makeGuide();
        $dbGuide = $this->guideRepo->find($guide->id);
        $dbGuide = $dbGuide->toArray();
        $this->assertModelData($guide->toArray(), $dbGuide);
    }

    /**
     * @test update
     */
    public function test_update_guide()
    {
        $guide = $this->makeGuide();
        $fakeGuide = $this->fakeGuideData();
        $updatedGuide = $this->guideRepo->update($fakeGuide, $guide->id);
        $this->assertModelData($fakeGuide, $updatedGuide->toArray());
        $dbGuide = $this->guideRepo->find($guide->id);
        $this->assertModelData($fakeGuide, $dbGuide->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_guide()
    {
        $guide = $this->makeGuide();
        $resp = $this->guideRepo->delete($guide->id);
        $this->assertTrue($resp);
        $this->assertNull(Guide::find($guide->id), 'Guide should not exist in DB');
    }
}
