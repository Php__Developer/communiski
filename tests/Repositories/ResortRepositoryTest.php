<?php namespace Tests\Repositories;

use App\Models\Resort;
use App\Repositories\ResortRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\Traits\MakeResortTrait;
use Tests\ApiTestTrait;

class ResortRepositoryTest extends TestCase
{
    use MakeResortTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var ResortRepository
     */
    protected $resortRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->resortRepo = \App::make(ResortRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_resort()
    {
        $resort = $this->fakeResortData();
        $createdResort = $this->resortRepo->create($resort);
        $createdResort = $createdResort->toArray();
        $this->assertArrayHasKey('id', $createdResort);
        $this->assertNotNull($createdResort['id'], 'Created Resort must have id specified');
        $this->assertNotNull(Resort::find($createdResort['id']), 'Resort with given id must be in DB');
        $this->assertModelData($resort, $createdResort);
    }

    /**
     * @test read
     */
    public function test_read_resort()
    {
        $resort = $this->makeResort();
        $dbResort = $this->resortRepo->find($resort->id);
        $dbResort = $dbResort->toArray();
        $this->assertModelData($resort->toArray(), $dbResort);
    }

    /**
     * @test update
     */
    public function test_update_resort()
    {
        $resort = $this->makeResort();
        $fakeResort = $this->fakeResortData();
        $updatedResort = $this->resortRepo->update($fakeResort, $resort->id);
        $this->assertModelData($fakeResort, $updatedResort->toArray());
        $dbResort = $this->resortRepo->find($resort->id);
        $this->assertModelData($fakeResort, $dbResort->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_resort()
    {
        $resort = $this->makeResort();
        $resp = $this->resortRepo->delete($resort->id);
        $this->assertTrue($resp);
        $this->assertNull(Resort::find($resort->id), 'Resort should not exist in DB');
    }
}
