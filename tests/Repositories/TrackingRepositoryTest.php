<?php namespace Tests\Repositories;

use App\Models\Tracking;
use App\Repositories\TrackingRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class TrackingRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var TrackingRepository
     */
    protected $trackingRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->trackingRepo = \App::make(TrackingRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_tracking()
    {
        $tracking = factory(Tracking::class)->make()->toArray();

        $createdTracking = $this->trackingRepo->create($tracking);

        $createdTracking = $createdTracking->toArray();
        $this->assertArrayHasKey('id', $createdTracking);
        $this->assertNotNull($createdTracking['id'], 'Created Tracking must have id specified');
        $this->assertNotNull(Tracking::find($createdTracking['id']), 'Tracking with given id must be in DB');
        $this->assertModelData($tracking, $createdTracking);
    }

    /**
     * @test read
     */
    public function test_read_tracking()
    {
        $tracking = factory(Tracking::class)->create();

        $dbTracking = $this->trackingRepo->find($tracking->id);

        $dbTracking = $dbTracking->toArray();
        $this->assertModelData($tracking->toArray(), $dbTracking);
    }

    /**
     * @test update
     */
    public function test_update_tracking()
    {
        $tracking = factory(Tracking::class)->create();
        $fakeTracking = factory(Tracking::class)->make()->toArray();

        $updatedTracking = $this->trackingRepo->update($fakeTracking, $tracking->id);

        $this->assertModelData($fakeTracking, $updatedTracking->toArray());
        $dbTracking = $this->trackingRepo->find($tracking->id);
        $this->assertModelData($fakeTracking, $dbTracking->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_tracking()
    {
        $tracking = factory(Tracking::class)->create();

        $resp = $this->trackingRepo->delete($tracking->id);

        $this->assertTrue($resp);
        $this->assertNull(Tracking::find($tracking->id), 'Tracking should not exist in DB');
    }
}
