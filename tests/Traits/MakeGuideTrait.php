<?php namespace Tests\Traits;

use Faker\Factory as Faker;
use App\Models\Guide;
use App\Repositories\GuideRepository;

trait MakeGuideTrait
{
    /**
     * Create fake instance of Guide and save it in database
     *
     * @param array $guideFields
     * @return Guide
     */
    public function makeGuide($guideFields = [])
    {
        /** @var GuideRepository $guideRepo */
        $guideRepo = \App::make(GuideRepository::class);
        $theme = $this->fakeGuideData($guideFields);
        return $guideRepo->create($theme);
    }

    /**
     * Get fake instance of Guide
     *
     * @param array $guideFields
     * @return Guide
     */
    public function fakeGuide($guideFields = [])
    {
        return new Guide($this->fakeGuideData($guideFields));
    }

    /**
     * Get fake data of Guide
     *
     * @param array $guideFields
     * @return array
     */
    public function fakeGuideData($guideFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'guide_title' => $fake->word,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'deleted_at' => $fake->date('Y-m-d H:i:s')
        ], $guideFields);
    }
}
