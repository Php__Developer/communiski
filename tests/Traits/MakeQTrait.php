<?php namespace Tests\Traits;

use Faker\Factory as Faker;
use App\Models\Q;
use App\Repositories\QRepository;

trait MakeQTrait
{
    /**
     * Create fake instance of Q and save it in database
     *
     * @param array $qFields
     * @return Q
     */
    public function makeQ($qFields = [])
    {
        /** @var QRepository $qRepo */
        $qRepo = \App::make(QRepository::class);
        $theme = $this->fakeQData($qFields);
        return $qRepo->create($theme);
    }

    /**
     * Get fake instance of Q
     *
     * @param array $qFields
     * @return Q
     */
    public function fakeQ($qFields = [])
    {
        return new Q($this->fakeQData($qFields));
    }

    /**
     * Get fake data of Q
     *
     * @param array $qFields
     * @return array
     */
    public function fakeQData($qFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s')
        ], $qFields);
    }
}
