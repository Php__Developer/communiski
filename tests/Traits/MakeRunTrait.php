<?php namespace Tests\Traits;

use Faker\Factory as Faker;
use App\Models\Run;
use App\Repositories\RunRepository;

trait MakeRunTrait
{
    /**
     * Create fake instance of Run and save it in database
     *
     * @param array $runFields
     * @return Run
     */
    public function makeRun($runFields = [])
    {
        /** @var RunRepository $runRepo */
        $runRepo = \App::make(RunRepository::class);
        $theme = $this->fakeRunData($runFields);
        return $runRepo->create($theme);
    }

    /**
     * Get fake instance of Run
     *
     * @param array $runFields
     * @return Run
     */
    public function fakeRun($runFields = [])
    {
        return new Run($this->fakeRunData($runFields));
    }

    /**
     * Get fake data of Run
     *
     * @param array $runFields
     * @return array
     */
    public function fakeRunData($runFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'run_name' => $fake->word,
            'run_difficulty' => $fake->word,
            'run_length' => $fake->word,
            'measure_unit' => $fake->word,
            'max_slope' => $fake->word,
            'average_slope' => $fake->word,
            'direction_run_faces' => $fake->word,
            'accessed_from' => $fake->word,
            'run_description' => $fake->word,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'deleted_at' => $fake->date('Y-m-d H:i:s')
        ], $runFields);
    }
}
