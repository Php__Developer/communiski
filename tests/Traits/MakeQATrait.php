<?php namespace Tests\Traits;

use Faker\Factory as Faker;
use App\Models\QA;
use App\Repositories\QARepository;

trait MakeQATrait
{
    /**
     * Create fake instance of QA and save it in database
     *
     * @param array $qAFields
     * @return QA
     */
    public function makeQA($qAFields = [])
    {
        /** @var QARepository $qARepo */
        $qARepo = \App::make(QARepository::class);
        $theme = $this->fakeQAData($qAFields);
        return $qARepo->create($theme);
    }

    /**
     * Get fake instance of QA
     *
     * @param array $qAFields
     * @return QA
     */
    public function fakeQA($qAFields = [])
    {
        return new QA($this->fakeQAData($qAFields));
    }

    /**
     * Get fake data of QA
     *
     * @param array $qAFields
     * @return array
     */
    public function fakeQAData($qAFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'user_id' => $fake->word,
            'question' => $fake->word,
            'answer' => $fake->word,
            'question_type' => $fake->word,
            'topics' => $fake->word,
            'archive_status' => $fake->word,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'deleted_at' => $fake->date('Y-m-d H:i:s')
        ], $qAFields);
    }
}
