<?php namespace Tests\Traits;

use Faker\Factory as Faker;
use App\Models\GroupApiController;
use App\Repositories\GroupApiControllerRepository;

trait MakeGroupApiControllerTrait
{
    /**
     * Create fake instance of GroupApiController and save it in database
     *
     * @param array $groupApiControllerFields
     * @return GroupApiController
     */
    public function makeGroupApiController($groupApiControllerFields = [])
    {
        /** @var GroupApiControllerRepository $groupApiControllerRepo */
        $groupApiControllerRepo = \App::make(GroupApiControllerRepository::class);
        $theme = $this->fakeGroupApiControllerData($groupApiControllerFields);
        return $groupApiControllerRepo->create($theme);
    }

    /**
     * Get fake instance of GroupApiController
     *
     * @param array $groupApiControllerFields
     * @return GroupApiController
     */
    public function fakeGroupApiController($groupApiControllerFields = [])
    {
        return new GroupApiController($this->fakeGroupApiControllerData($groupApiControllerFields));
    }

    /**
     * Get fake data of GroupApiController
     *
     * @param array $groupApiControllerFields
     * @return array
     */
    public function fakeGroupApiControllerData($groupApiControllerFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'group_name' => $fake->word,
            'group_image' => $fake->word,
            'group_owner' => $fake->word,
            'total_users' => $fake->word,
            'privacy_status' => $fake->word,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'deleted_at' => $fake->date('Y-m-d H:i:s')
        ], $groupApiControllerFields);
    }
}
