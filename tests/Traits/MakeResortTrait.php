<?php namespace Tests\Traits;

use Faker\Factory as Faker;
use App\Models\Resort;
use App\Repositories\ResortRepository;

trait MakeResortTrait
{
    /**
     * Create fake instance of Resort and save it in database
     *
     * @param array $resortFields
     * @return Resort
     */
    public function makeResort($resortFields = [])
    {
        /** @var ResortRepository $resortRepo */
        $resortRepo = \App::make(ResortRepository::class);
        $theme = $this->fakeResortData($resortFields);
        return $resortRepo->create($theme);
    }

    /**
     * Get fake instance of Resort
     *
     * @param array $resortFields
     * @return Resort
     */
    public function fakeResort($resortFields = [])
    {
        return new Resort($this->fakeResortData($resortFields));
    }

    /**
     * Get fake data of Resort
     *
     * @param array $resortFields
     * @return array
     */
    public function fakeResortData($resortFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'id' => $fake->randomDigitNotNull,
            'resort_name' => $fake->word,
            'country' => $fake->word,
            'state' => $fake->word,
            'image' => $fake->word,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'deleted_at' => $fake->date('Y-m-d H:i:s')
        ], $resortFields);
    }
}
