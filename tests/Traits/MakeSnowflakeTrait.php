<?php namespace Tests\Traits;

use Faker\Factory as Faker;
use App\Models\Snowflake;
use App\Repositories\SnowflakeRepository;

trait MakeSnowflakeTrait
{
    /**
     * Create fake instance of Snowflake and save it in database
     *
     * @param array $snowflakeFields
     * @return Snowflake
     */
    public function makeSnowflake($snowflakeFields = [])
    {
        /** @var SnowflakeRepository $snowflakeRepo */
        $snowflakeRepo = \App::make(SnowflakeRepository::class);
        $theme = $this->fakeSnowflakeData($snowflakeFields);
        return $snowflakeRepo->create($theme);
    }

    /**
     * Get fake instance of Snowflake
     *
     * @param array $snowflakeFields
     * @return Snowflake
     */
    public function fakeSnowflake($snowflakeFields = [])
    {
        return new Snowflake($this->fakeSnowflakeData($snowflakeFields));
    }

    /**
     * Get fake data of Snowflake
     *
     * @param array $snowflakeFields
     * @return array
     */
    public function fakeSnowflakeData($snowflakeFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'user_id' => $fake->word,
            'snowflake_rewards' => $fake->word,
            'reason' => $fake->word,
            'reward_status' => $fake->word,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'deleted_at' => $fake->date('Y-m-d H:i:s')
        ], $snowflakeFields);
    }
}
