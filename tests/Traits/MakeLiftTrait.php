<?php namespace Tests\Traits;

use Faker\Factory as Faker;
use App\Models\Lift;
use App\Repositories\LiftRepository;

trait MakeLiftTrait
{
    /**
     * Create fake instance of Lift and save it in database
     *
     * @param array $liftFields
     * @return Lift
     */
    public function makeLift($liftFields = [])
    {
        /** @var LiftRepository $liftRepo */
        $liftRepo = \App::make(LiftRepository::class);
        $theme = $this->fakeLiftData($liftFields);
        return $liftRepo->create($theme);
    }

    /**
     * Get fake instance of Lift
     *
     * @param array $liftFields
     * @return Lift
     */
    public function fakeLift($liftFields = [])
    {
        return new Lift($this->fakeLiftData($liftFields));
    }

    /**
     * Get fake data of Lift
     *
     * @param array $liftFields
     * @return array
     */
    public function fakeLiftData($liftFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'user_id' => $fake->word,
            'lift_name' => $fake->word,
            'lift_number' => $fake->word,
            'description' => $fake->word,
            'lift_type' => $fake->word,
            'capacity' => $fake->word,
            'manufacturer' => $fake->word,
            'installed_year' => $fake->word,
            'hourly_capacity' => $fake->word,
            'base_elevation' => $fake->word,
            'top_elevation' => $fake->word,
            'vertical_rise' => $fake->word,
            'length' => $fake->word,
            'ride_time' => $fake->word,
            'opening_time' => $fake->word,
            'closing_time' => $fake->word,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'deleted_at' => $fake->date('Y-m-d H:i:s')
        ], $liftFields);
    }
}
