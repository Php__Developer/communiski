-- phpMyAdmin SQL Dump
-- version 4.8.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 22, 2019 at 01:17 PM
-- Server version: 10.2.10-MariaDB
-- PHP Version: 7.2.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `communiski`
--

-- --------------------------------------------------------

--
-- Table structure for table `access_codes`
--

CREATE TABLE `access_codes` (
  `id` int(11) NOT NULL,
  `resort_id` varchar(100) NOT NULL,
  `resort_administrator` varchar(255) NOT NULL,
  `resort_editor` varchar(255) NOT NULL,
  `patroller` varchar(255) NOT NULL,
  `lifty` varchar(255) NOT NULL,
  `ambassador` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `access_codes`
--

INSERT INTO `access_codes` (`id`, `resort_id`, `resort_administrator`, `resort_editor`, `patroller`, `lifty`, `ambassador`, `created_at`, `updated_at`, `deleted_at`) VALUES
(42, '107', 'ADM5cc22MJ', 'EDIVAOlfy7', 'PATfCsI6Lp', 'LIF7X0EglP', NULL, '2019-11-22 04:34:45', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'USA', '2019-08-27 06:47:30', NULL, NULL),
(2, 'Canada', '2019-08-27 06:47:30', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `fav_resorts`
--

CREATE TABLE `fav_resorts` (
  `id` int(11) NOT NULL,
  `resort_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `favourite` varchar(100) NOT NULL COMMENT '1=yes,2=no',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `friends`
--

CREATE TABLE `friends` (
  `id` int(11) NOT NULL,
  `sender_id` varchar(100) NOT NULL,
  `reciever_id` varchar(100) NOT NULL,
  `status` enum('sent','accepted','rejected','canceled','removed') NOT NULL COMMENT '1=sent,2=accepted,3=rejected,4=cancel,5=removed',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` int(11) NOT NULL,
  `group_name` varchar(255) NOT NULL DEFAULT '',
  `group_location` varchar(255) NOT NULL DEFAULT '',
  `group_info` varchar(255) NOT NULL DEFAULT '',
  `group_image` varchar(255) NOT NULL DEFAULT 'groupdefault.jpg',
  `group_owner_id` varchar(255) NOT NULL DEFAULT '',
  `group_date` date NOT NULL,
  `total_users` varchar(100) NOT NULL DEFAULT '',
  `privacy_status` enum('BuddiesOnly','MembersBuddies','Everyone') NOT NULL COMMENT '1=BuddiesOnly,2=MembersOnly,3=Everyone',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `group_members`
--

CREATE TABLE `group_members` (
  `id` int(11) NOT NULL,
  `group_id` varchar(100) NOT NULL,
  `user_id` varchar(100) NOT NULL,
  `message` text NOT NULL,
  `request_status` enum('sent','accepted','rejected','cancelled','banned') NOT NULL DEFAULT 'sent' COMMENT '''1=sent'',''2=accepted'',''3=rejected'',''4=cancelled'',''4=banned''',
  `group_location` varchar(255) NOT NULL DEFAULT '',
  `created_at` date NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `lifts`
--

CREATE TABLE `lifts` (
  `id` int(11) NOT NULL,
  `status` varchar(255) DEFAULT NULL,
  `country_id` varchar(255) NOT NULL,
  `state_id` varchar(255) NOT NULL,
  `resort_id` varchar(255) NOT NULL,
  `user_id` varchar(255) NOT NULL,
  `lift_name` varchar(255) DEFAULT NULL,
  `lift_number` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `lift_type` varchar(255) DEFAULT NULL,
  `capacity` varchar(255) DEFAULT NULL,
  `manufacturer` varchar(255) DEFAULT NULL,
  `installed_year` year(4) DEFAULT NULL,
  `hourly_capacity` varchar(255) DEFAULT NULL,
  `base_elevation` varchar(255) DEFAULT NULL,
  `base_elevation_unit` varchar(255) DEFAULT NULL,
  `top_elevation` varchar(255) DEFAULT NULL,
  `top_elevation_unit` varchar(255) DEFAULT NULL,
  `vertical_rise` varchar(255) DEFAULT NULL,
  `vertical_rise_unit` varchar(255) DEFAULT NULL,
  `length` varchar(255) DEFAULT NULL,
  `length_unit` varchar(255) DEFAULT NULL,
  `ride_time` time DEFAULT NULL,
  `opening_time` time DEFAULT NULL,
  `closing_time` time DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `lift_history`
--

CREATE TABLE `lift_history` (
  `id` int(11) NOT NULL,
  `post_anonymous` varchar(100) NOT NULL DEFAULT '' COMMENT '1=Yes,'' ''=No',
  `lift_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `note_for` varchar(255) DEFAULT NULL,
  `added_note` varchar(255) DEFAULT NULL,
  `lift_name` varchar(255) DEFAULT NULL,
  `lift_number` varchar(255) DEFAULT NULL,
  `lift_type` varchar(255) DEFAULT NULL,
  `capacity` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `manufacturer` varchar(255) DEFAULT NULL,
  `installed_year` year(4) DEFAULT NULL,
  `hourly_capacity` varchar(255) DEFAULT NULL,
  `base_elevation` varchar(255) DEFAULT NULL,
  `base_elevation_unit` varchar(255) DEFAULT NULL,
  `top_elevation` varchar(255) DEFAULT NULL,
  `top_elevation_unit` varchar(255) DEFAULT NULL,
  `vertical_rise_unit` varchar(255) DEFAULT NULL,
  `vertical_rise` varchar(255) DEFAULT NULL,
  `length` int(255) DEFAULT NULL,
  `length_unit` varchar(255) DEFAULT NULL,
  `ride_time` time DEFAULT NULL,
  `opening_time` time DEFAULT NULL,
  `closing_time` time DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `likes`
--

CREATE TABLE `likes` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `resort_id` int(11) DEFAULT NULL,
  `run_id` varchar(100) DEFAULT NULL,
  `lift_id` varchar(100) DEFAULT NULL,
  `event_type` varchar(255) NOT NULL,
  `like_for` varchar(255) NOT NULL,
  `like_for_value` varchar(255) NOT NULL,
  `is_like` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_23_115546_create_country_state_city_tables', 2),
(4, '2019_08_23_115758_create_country_state_city_tables', 3);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('Sumit@gmail.com', '$2y$10$T5OrG4jGRw7Q.Ny0AVZ8tu21dnVYMiLor45UNcplj7yMrYaT/VxBy', '2019-06-27 08:27:28'),
('milanrana090@gmail.com', '$2y$10$lqXh0SM5.2cU2ip0KyTjkev04WtAJ7UFWwMX/UhpZxgmfTzY2cF5K', '2019-07-05 06:14:06'),
('vineeta@gmail.com', '$2y$10$Qi7Glb1Bnlo3yD7nJ8w7vOu4XH2vx2ma7U7VDaByFyDqP.QDAXIX6', '2019-07-05 15:25:10'),
('user@gmail.com', '$2y$10$FJvXIOZxvDWcPx.UbFe03eMv.C2dsaSaxTf2/s4qoSayhJYrt3Et.', '2019-07-06 10:28:29');

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `resort_id` int(11) NOT NULL,
  `role` varchar(255) NOT NULL,
  `role_type` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `question_answers`
--

CREATE TABLE `question_answers` (
  `id` int(11) NOT NULL,
  `resort_id` int(11) NOT NULL,
  `user_id` varchar(255) NOT NULL COMMENT 'asked_by_user_id',
  `question` varchar(255) NOT NULL,
  `answer` varchar(255) DEFAULT NULL,
  `question_type` varchar(255) NOT NULL,
  `topic` varchar(255) NOT NULL,
  `mark_default` enum('2','1') NOT NULL DEFAULT '2' COMMENT '2=No,1=Yes',
  `choosed_by_user` enum('1','2') NOT NULL DEFAULT '2' COMMENT '1=Yes,2=No',
  `archive_status` enum('1','2') NOT NULL DEFAULT '1' COMMENT '1=No,2=Yes',
  `refund_status` enum('1','2') NOT NULL DEFAULT '2' COMMENT '1=Yes,2=No',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `question_history`
--

CREATE TABLE `question_history` (
  `id` int(11) NOT NULL COMMENT 'answer_id',
  `resort_id` int(11) NOT NULL,
  `question_id` varchar(100) NOT NULL,
  `answer_by_user_id` varchar(100) NOT NULL,
  `answer` varchar(255) NOT NULL DEFAULT '',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `resorts`
--

CREATE TABLE `resorts` (
  `id` int(11) NOT NULL,
  `resort_name` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL DEFAULT '',
  `lat` varchar(255) NOT NULL DEFAULT '',
  `long` varchar(255) NOT NULL DEFAULT '',
  `country` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL DEFAULT 'defaultResort.jpg',
  `website` varchar(255) DEFAULT NULL,
  `run_grades` varchar(255) DEFAULT NULL,
  `bcolor` varchar(100) DEFAULT NULL,
  `fcolor` varchar(100) DEFAULT NULL,
  `lift_names` enum('2','1') NOT NULL DEFAULT '2' COMMENT '2=No,1=Yes',
  `lift_numbers` enum('2','1') NOT NULL DEFAULT '2' COMMENT '2=No,1=Yes',
  `run_names` enum('2','1') NOT NULL DEFAULT '2' COMMENT '2=No,1=Yes',
  `run_numbers` enum('2','1') NOT NULL DEFAULT '2' COMMENT '2=No,1=Yes',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `resorts`
--

INSERT INTO `resorts` (`id`, `resort_name`, `address`, `lat`, `long`, `country`, `state`, `image`, `website`, `run_grades`, `bcolor`, `fcolor`, `lift_names`, `lift_numbers`, `run_names`, `run_numbers`, `created_at`, `updated_at`, `deleted_at`) VALUES
(107, 'Lake Louise', 'Lake Louise, AB, Canada', '51.42537050000001', '-116.17725519999999', '2', '1', '1574417085_downloadfiles_wallpapers_1920_1200_japan_digital_nature_6618.jpg', '', 'Green,Blue,Black,Double Black', '#10aec4', '#ffa710', '1', '1', '1', '1', '2019-11-22 04:34:45', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `resort_details`
--

CREATE TABLE `resort_details` (
  `id` int(11) NOT NULL,
  `resort_id` varchar(100) DEFAULT NULL,
  `resort_info` varchar(255) DEFAULT NULL,
  `vertical_rise` varchar(255) DEFAULT NULL,
  `vertical_rise_unit` varchar(255) DEFAULT NULL,
  `top_elevation` varchar(255) DEFAULT NULL,
  `top_elevation_unit` varchar(255) DEFAULT NULL,
  `bottom_elevation` varchar(255) DEFAULT NULL,
  `bottom_elevation_unit` varchar(255) DEFAULT NULL,
  `skiable_area` varchar(100) DEFAULT NULL,
  `skiable_area_unit` varchar(255) DEFAULT NULL,
  `runs` varchar(100) DEFAULT NULL,
  `lifts` varchar(100) DEFAULT NULL,
  `longest_run` varchar(100) DEFAULT NULL,
  `longest_run_unit` varchar(255) DEFAULT NULL,
  `terrain_parks` varchar(100) DEFAULT NULL,
  `half_pipes` varchar(100) DEFAULT NULL,
  `annual_snowfall` varchar(255) DEFAULT NULL,
  `annual_snowfall_unit` varchar(255) DEFAULT NULL,
  `snow_making` varchar(255) DEFAULT NULL,
  `snow_making_unit` varchar(255) DEFAULT NULL,
  `uphill_capacity` varchar(255) DEFAULT NULL,
  `terrain_difficulty_beginner` varchar(255) NOT NULL DEFAULT '0',
  `terrain_difficulty_intermediate` varchar(255) NOT NULL DEFAULT '0',
  `terrain_difficulty_advance` varchar(255) NOT NULL DEFAULT '0',
  `terrain_difficulty_expert` varchar(255) NOT NULL DEFAULT '0',
  `opening_date` date DEFAULT NULL,
  `closing_date` date DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `resort_details`
--

INSERT INTO `resort_details` (`id`, `resort_id`, `resort_info`, `vertical_rise`, `vertical_rise_unit`, `top_elevation`, `top_elevation_unit`, `bottom_elevation`, `bottom_elevation_unit`, `skiable_area`, `skiable_area_unit`, `runs`, `lifts`, `longest_run`, `longest_run_unit`, `terrain_parks`, `half_pipes`, `annual_snowfall`, `annual_snowfall_unit`, `snow_making`, `snow_making_unit`, `uphill_capacity`, `terrain_difficulty_beginner`, `terrain_difficulty_intermediate`, `terrain_difficulty_advance`, `terrain_difficulty_expert`, `opening_date`, `closing_date`, `created_at`, `updated_at`, `deleted_at`) VALUES
(42, '107', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', '0', NULL, NULL, '2019-11-22 04:34:45', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `resort_history`
--

CREATE TABLE `resort_history` (
  `id` int(11) NOT NULL,
  `post_anonymous` varchar(100) NOT NULL DEFAULT '' COMMENT '1=Yes,2=No',
  `resort_id` varchar(100) NOT NULL,
  `user_id` int(11) NOT NULL,
  `note_for` varchar(255) DEFAULT NULL,
  `added_note` varchar(255) DEFAULT NULL,
  `resort_info` varchar(255) DEFAULT NULL,
  `vertical_rise` varchar(255) DEFAULT NULL,
  `vertical_rise_unit` varchar(255) DEFAULT NULL,
  `top_elevation` varchar(255) DEFAULT NULL,
  `top_elevation_unit` varchar(255) DEFAULT NULL,
  `bottom_elevation` varchar(255) DEFAULT NULL,
  `bottom_elevation_unit` varchar(255) DEFAULT NULL,
  `skiable_area` varchar(100) DEFAULT NULL,
  `skiable_area_unit` varchar(255) DEFAULT NULL,
  `runs` varchar(100) DEFAULT NULL,
  `lifts` varchar(100) DEFAULT NULL,
  `longest_run` varchar(100) DEFAULT NULL,
  `longest_run_unit` varchar(255) DEFAULT NULL,
  `terrain_parks` varchar(100) DEFAULT NULL,
  `half_pipes` varchar(100) DEFAULT NULL,
  `annual_snowfall` varchar(255) DEFAULT NULL,
  `annual_snowfall_unit` varchar(255) DEFAULT NULL,
  `snow_making` varchar(255) DEFAULT NULL,
  `snow_making_unit` varchar(255) DEFAULT NULL,
  `uphill_capacity` varchar(255) DEFAULT NULL,
  `terrain_difficulty_beginner` varchar(255) DEFAULT NULL,
  `terrain_difficulty_intermediate` varchar(255) DEFAULT NULL,
  `terrain_difficulty_advance` varchar(255) DEFAULT NULL,
  `terrain_difficulty_expert` varchar(255) DEFAULT NULL,
  `opening_date` date DEFAULT NULL,
  `closing_date` date DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `resort_history`
--

INSERT INTO `resort_history` (`id`, `post_anonymous`, `resort_id`, `user_id`, `note_for`, `added_note`, `resort_info`, `vertical_rise`, `vertical_rise_unit`, `top_elevation`, `top_elevation_unit`, `bottom_elevation`, `bottom_elevation_unit`, `skiable_area`, `skiable_area_unit`, `runs`, `lifts`, `longest_run`, `longest_run_unit`, `terrain_parks`, `half_pipes`, `annual_snowfall`, `annual_snowfall_unit`, `snow_making`, `snow_making_unit`, `uphill_capacity`, `terrain_difficulty_beginner`, `terrain_difficulty_intermediate`, `terrain_difficulty_advance`, `terrain_difficulty_expert`, `opening_date`, `closing_date`, `created_at`, `updated_at`, `deleted_at`) VALUES
(203, '', '107', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-22 04:34:45', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `rewards_points`
--

CREATE TABLE `rewards_points` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `total` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rewards_points`
--

INSERT INTO `rewards_points` (`id`, `user_id`, `total`, `created_at`, `updated_at`, `deleted_at`) VALUES
(17, 137, '102', '2019-11-01 05:10:35', '2019-11-21 05:37:58', NULL),
(18, 138, '122', '2019-11-01 05:21:43', '2019-11-21 05:37:58', NULL),
(19, 139, '100', '2019-11-01 05:30:26', '2019-11-21 05:37:58', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `runs`
--

CREATE TABLE `runs` (
  `id` int(11) NOT NULL,
  `status` varchar(255) DEFAULT 'NULL',
  `country_id` varchar(255) NOT NULL,
  `state_id` varchar(255) NOT NULL,
  `resort_id` varchar(255) NOT NULL,
  `user_id` varchar(255) NOT NULL,
  `run_name` varchar(255) DEFAULT NULL,
  `run_number` varchar(255) DEFAULT NULL,
  `run_difficulty` varchar(255) DEFAULT NULL,
  `accessed_from` varchar(255) DEFAULT NULL,
  `run_length_value` varchar(255) DEFAULT NULL,
  `measure_length_unit` enum('Imperial','Metric') DEFAULT NULL,
  `max_slope` varchar(255) DEFAULT NULL,
  `average_slope` varchar(255) DEFAULT NULL,
  `direction_run_faces` enum('North','North East','East','South East','South','South West','West','North West') DEFAULT NULL,
  `run_description` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `run_history`
--

CREATE TABLE `run_history` (
  `id` int(11) NOT NULL,
  `post_anonymous` varchar(100) NOT NULL DEFAULT '' COMMENT '1=Yes,'' ''=No',
  `run_id` varchar(100) DEFAULT NULL,
  `user_id` varchar(100) NOT NULL,
  `note_for` varchar(255) DEFAULT NULL,
  `added_note` varchar(255) DEFAULT NULL,
  `run_name` varchar(255) DEFAULT 'NULL',
  `run_number` varchar(255) DEFAULT 'NULL',
  `run_difficulty` varchar(255) DEFAULT 'NULL',
  `run_description` varchar(255) DEFAULT NULL,
  `accessed_from` varchar(255) DEFAULT NULL,
  `direction_run_faces` enum('North','North East','East','South East','South','South West','West','North West') DEFAULT NULL,
  `max_slope` varchar(255) DEFAULT NULL,
  `average_slope` varchar(255) DEFAULT NULL,
  `run_length_value` varchar(255) DEFAULT NULL,
  `measure_length_unit` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `skiers`
--

CREATE TABLE `skiers` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `skiers`
--

INSERT INTO `skiers` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Beginner', '2019-07-01 09:39:12', '2019-07-01 09:39:12'),
(2, 'Intermediate', '2019-07-01 09:39:12', '2019-07-01 09:39:12'),
(3, 'Advanced', '2019-07-01 09:39:28', '2019-07-01 09:39:28'),
(4, 'Expert', '2019-07-01 09:39:28', '2019-07-01 09:39:28');

-- --------------------------------------------------------

--
-- Table structure for table `snowboarders`
--

CREATE TABLE `snowboarders` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `snowboarders`
--

INSERT INTO `snowboarders` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Groomers', '2019-07-01 09:40:01', '2019-07-01 09:40:01'),
(2, 'Moguls', '2019-07-01 09:40:01', '2019-07-01 09:40:01'),
(3, 'Trees', '2019-07-01 09:40:32', '2019-07-01 09:40:32'),
(4, 'Park', '2019-07-01 09:40:32', '2019-07-01 09:40:32'),
(5, 'Sleep & Deep', '2019-07-01 09:40:53', '2019-07-01 09:40:53'),
(6, 'Backcountry', '2019-07-01 09:40:53', '2019-07-01 09:40:53');

-- --------------------------------------------------------

--
-- Table structure for table `snowflakes`
--

CREATE TABLE `snowflakes` (
  `id` int(11) NOT NULL,
  `user_id` varchar(100) NOT NULL,
  `resort_id` varchar(100) DEFAULT NULL,
  `run_id` varchar(100) DEFAULT NULL,
  `lift_id` varchar(100) DEFAULT NULL,
  `reward_for` varchar(255) DEFAULT NULL,
  `snowflake_rewards` varchar(255) NOT NULL,
  `reason` varchar(255) NOT NULL,
  `reward_status` enum('0','1','2') NOT NULL COMMENT '''0=Pending'',''1=Confirmed'',''2=Rejected''',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `spend_snowflakes`
--

CREATE TABLE `spend_snowflakes` (
  `id` int(11) NOT NULL,
  `user_id` varchar(100) NOT NULL,
  `spend_snowflake` varchar(255) NOT NULL,
  `reason` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `spend_snowflakes`
--

INSERT INTO `spend_snowflakes` (`id`, `user_id`, `spend_snowflake`, `reason`, `created_at`, `updated_at`, `deleted_at`) VALUES
(57, '137', '50', 'Asked a Question', '2019-11-01 05:19:11', NULL, NULL),
(58, '139', '50', 'Asked a Question', '2019-11-02 02:43:04', NULL, NULL),
(59, '139', '50', 'Asked a Question', '2019-11-02 04:05:59', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `states`
--

CREATE TABLE `states` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `country_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `states`
--

INSERT INTO `states` (`id`, `name`, `country_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Alberta', 2, '2019-08-27 06:49:37', NULL, NULL),
(2, 'Labrador', 2, '2019-08-27 06:49:37', NULL, NULL),
(3, 'New Brunswick', 2, '2019-08-27 06:50:00', NULL, NULL),
(4, 'Nova Scotia', 2, '2019-08-27 06:50:00', NULL, NULL),
(5, 'North West Terr.', 2, '2019-08-27 06:50:23', NULL, NULL),
(6, 'Prince Edward Is.', 2, '2019-08-27 06:50:23', NULL, NULL),
(7, 'Saskatchewen', 2, '2019-08-27 06:50:42', NULL, NULL),
(8, 'British Columbia', 2, '2019-08-27 06:50:42', NULL, NULL),
(9, 'Manitoba', 2, '2019-08-27 06:50:58', NULL, NULL),
(10, 'Newfoundland', 2, '2019-08-27 06:50:58', NULL, NULL),
(11, 'Nunavut', 2, '2019-08-27 06:51:14', NULL, NULL),
(12, 'Ontario', 2, '2019-08-27 06:51:14', NULL, NULL),
(13, 'Quebec', 2, '2019-08-27 06:51:30', NULL, NULL),
(14, 'Yukon', 2, '2019-08-27 06:51:30', NULL, NULL),
(15, 'Alabama', 1, '2019-08-27 06:52:08', NULL, NULL),
(16, 'Arkansas', 1, '2019-08-27 06:52:08', NULL, NULL),
(17, 'Alaska', 1, '2019-08-27 06:52:25', NULL, NULL),
(18, 'Arizona', 1, '2019-08-27 06:52:25', NULL, NULL),
(19, 'California', 1, '2019-08-27 06:52:39', NULL, NULL),
(20, 'Colorado', 1, '2019-08-27 06:52:39', NULL, NULL),
(21, 'Connecticut', 1, '2019-08-27 06:52:55', NULL, NULL),
(22, 'Dist. Columbia', 1, '2019-08-27 06:52:55', NULL, NULL),
(23, 'Delaware', 1, '2019-08-27 06:53:11', NULL, NULL),
(24, 'Florida', 1, '2019-08-27 06:53:11', NULL, NULL),
(25, 'Georgia', 1, '2019-08-27 06:54:38', NULL, NULL),
(26, 'Hawaii', 1, '2019-08-27 06:54:38', NULL, NULL),
(27, 'Idaho', 1, '2019-08-27 06:58:15', NULL, NULL),
(28, 'Illinois', 1, '2019-08-27 06:58:15', NULL, NULL),
(29, 'Indiana', 1, '2019-08-27 06:58:34', NULL, NULL),
(30, 'Iowa', 1, '2019-08-27 06:58:34', NULL, NULL),
(31, 'Kansas', 1, '2019-08-27 06:58:44', NULL, NULL),
(32, 'Kentucky', 1, '2019-08-27 06:58:44', NULL, NULL),
(33, 'Louisiana', 1, '2019-08-27 06:59:03', NULL, NULL),
(34, 'Maine', 1, '2019-08-27 06:59:03', NULL, NULL),
(35, 'Maryland', 1, '2019-08-27 06:59:17', NULL, NULL),
(36, 'Massachusetts', 1, '2019-08-27 06:59:17', NULL, NULL),
(37, 'Michigan', 1, '2019-08-27 06:59:30', NULL, NULL),
(38, 'Minnesota', 1, '2019-08-27 06:59:30', NULL, NULL),
(39, 'Mississippi', 1, '2019-08-27 06:59:44', NULL, NULL),
(40, 'Missouri', 1, '2019-08-27 06:59:44', NULL, NULL),
(41, 'Montana', 1, '2019-08-27 06:59:58', NULL, NULL),
(42, 'Nebraska', 1, '2019-08-27 06:59:58', NULL, NULL),
(43, 'Nevada', 1, '2019-08-27 07:00:11', NULL, NULL),
(44, 'New Hampshire', 1, '2019-08-27 07:00:11', NULL, NULL),
(45, 'New Jersey', 1, '2019-08-27 07:00:26', NULL, NULL),
(46, 'New Mexico', 1, '2019-08-27 07:00:26', NULL, NULL),
(47, 'New York', 1, '2019-08-27 07:00:50', NULL, NULL),
(48, 'North Carolina', 1, '2019-08-27 07:00:50', NULL, NULL),
(49, 'North Dakota', 1, '2019-08-27 07:01:02', NULL, NULL),
(50, 'Ohio', 1, '2019-08-27 07:01:02', NULL, NULL),
(51, 'Oklahoma', 1, '2019-08-27 07:01:16', NULL, NULL),
(52, 'Oregon', 1, '2019-08-27 07:01:16', NULL, NULL),
(53, 'Pennsylvania', 1, '2019-08-27 07:01:30', NULL, NULL),
(54, 'Rhode Island', 1, '2019-08-27 07:01:30', NULL, NULL),
(55, 'South Carolina', 1, '2019-08-27 07:01:45', NULL, NULL),
(56, 'South Dakota', 1, '2019-08-27 07:01:45', NULL, NULL),
(57, 'Tennessee', 1, '2019-08-27 07:02:00', NULL, NULL),
(58, 'Texas', 1, '2019-08-27 07:02:00', NULL, NULL),
(59, 'Utah', 1, '2019-08-27 07:02:14', NULL, NULL),
(60, 'Vermont', 1, '2019-08-27 07:02:14', NULL, NULL),
(61, 'Virginia', 1, '2019-08-27 07:02:27', NULL, NULL),
(62, 'Washington', 1, '2019-08-27 07:02:27', NULL, NULL),
(63, 'West Virginia', 1, '2019-08-27 07:02:42', NULL, NULL),
(64, 'Wisconsin', 1, '2019-08-27 07:02:42', NULL, NULL),
(65, 'Wyoming', 1, '2019-08-27 07:02:50', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `test`
--

CREATE TABLE `test` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `item1` varchar(255) NOT NULL DEFAULT '',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `test`
--

INSERT INTO `test` (`id`, `name`, `item1`, `created_at`) VALUES
(7, '1,2,3,4', 'test', '2019-09-07 07:59:15'),
(8, 'Amit', '1,2,3', '2019-09-07 08:03:34'),
(9, '', '', '2019-09-07 08:16:42'),
(10, '0', '0', '2019-09-07 08:17:08'),
(11, 'ghghg', 'ghghgh,hghghg,ghghgh,', '2019-09-07 10:57:52'),
(12, 'fdfd', 'fdfd,fdfdf,fdff', '2019-09-07 11:14:35');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `device_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `device_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'NULL',
  `referral_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `referral_by` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '''''',
  `DOB` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '''''',
  `country` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '''''',
  `gender` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `phone_no` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'default.jpg',
  `unit` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `skier` enum('1','2') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '2' COMMENT '2=No,1=Yes',
  `skier_sky_level` enum('1','2','3','4') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1' COMMENT '1=Beginner,2=Intermediate,3=Advance,4=Expert',
  `skier_fav_tarrian` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1' COMMENT '1=Groomers,2=Moguls,3=Trees,4=Park,5=Sleep&Deep,6=Backcountry',
  `snowboarder` enum('1','2') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '2' COMMENT '2=No,1=Yes',
  `snowboarder_sky_level` enum('1','2','3','4') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1' COMMENT '1=Beginner,2=Intermediate,3=Advance,4=Expert',
  `snowboarder_fav_tarrian` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1' COMMENT '1=Groomers,2=Moguls,3=Trees,4=Park,5=Sleep&Deep,6=Backcountry',
  `login_status` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0' COMMENT 'First time Login 0=No,1=Yes',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `device_id`, `device_type`, `remember_token`, `referral_code`, `referral_by`, `name`, `email`, `password`, `DOB`, `country`, `gender`, `phone_no`, `image`, `unit`, `skier`, `skier_sky_level`, `skier_fav_tarrian`, `snowboarder`, `snowboarder_sky_level`, `snowboarder_fav_tarrian`, `login_status`, `email_verified_at`, `created_at`, `updated_at`, `deleted_at`) VALUES
(2, 'AAAAYBmHz5g:APA91bHGee6_av7cgW_nA9eNQF_J0SEWlqcS_arCLAdnMStD69eFyjdDx5iDdCV5obWP_9sYD_D3eMHn06hHSKNSD0vSVkTP4-JTl7kOyjPSd8ubu4xgtpy_1nh_9mtBvWvyi6fMd-j4', 'A', 'F9GU0bDwBzQen4upn6uf0gGjJHAt8bOL7FSJxq6G3QiJZ1SG9c8jamfb1r6s', '', NULL, 'Charlie', 'admin@gmail.com', '$2y$10$sFESc6Ip56WsB4nUi3U0KuW.rcUxVyHLljD6uDrTdzRZBU8g7HazW', '11-01-2000', 'India', 'Male', '7018915635', '1571031887download.jpg', '', '1', '1', '1', '2', '1', '1', '0', NULL, '2019-06-26 06:49:03', '2014-10-19 00:14:47', NULL),
(137, 'AAAAYBmHz5g:APA91bHGee6_av7cgW_nA9eNQF_J0SEWlqcS_arCLAdnMStD69eFyjdDx5iDdCV5obWP_9sYD_D3eMHn06hHSKNSD0vSVkTP4-JTl7kOyjPSd8ubu4xgtpy_1nh_9mtBvWvyi6fMd-j4', 'A', 'eyJpdiI6IkQ5TXFcL2tobnNtY1pPWHYybVkwcld3PT0iLCJ2YWx1ZSI6Im5icXhJMFdMMmRsMmdpNDB1YXBsd3c9PSIsIm1hYyI6ImE4MTk5YzEzMjJmODgyODI3NTZjNmU3NTQwNWUzNjM2MzI1YWFiOTg5YmEyMTY0OTQ1NWU5ODA0Y2EwYWJjNjMifQ==', 'AMI1WRZe', '139', 'Amit Sankhyan', 'sankhyan.amit@gmail.com', '$2y$10$MGZhs6.zRjxvTQYgP83FDe9PjJg4CjS9DAa8qM9/s3LppLgebC8Yq', '11-01-2000', 'Canada', '', '7018915635', 'default.jpg', 'Metric', '2', '1', '1', '2', '1', '1', '0', NULL, '2019-11-01 05:10:35', '2019-11-01 05:17:51', NULL),
(138, 'AAAAYBmHz5g:APA91bHGee6_av7cgW_nA9eNQF_J0SEWlqcS_arCLAdnMStD69eFyjdDx5iDdCV5obWP_9sYD_D3eMHn06hHSKNSD0vSVkTP4-JTl7kOyjPSd8ubu4xgtpy_1nh_9mtBvWvyi6fMd-j4', 'A', 'eyJpdiI6ImxKWUdlMnRYWXl1VjZFTE9aRkEyRGc9PSIsInZhbHVlIjoiSVUzdVhhTUVnOWNzQ0JQNGo1WnF5QT09IiwibWFjIjoiZmU1YTJmYzIwMmE5NWFiNTYwMzAyZDI5ZTY4NDRiZGU3MjMwMzE0ZGQyYjA2YWJlZjVhMjc5N2E1YjhhOWM0ZSJ9', 'SUMCkmoT', NULL, 'Sumit Sankhyan', 'sumit@gmail.com', '$2y$10$EhVAh0v7t6EuGhZNfr4xU.VFJsavl0NnRqBKevRCvnNt02OyPFi.e', '11-01-2000', 'Canada', '', '7018915635', 'default.jpg', 'Metric', '2', '1', '1', '2', '1', '1', '0', NULL, '2019-11-01 05:21:43', '2019-11-01 05:21:55', NULL),
(139, 'AAAAYBmHz5g:APA91bHGee6_av7cgW_nA9eNQF_J0SEWlqcS_arCLAdnMStD69eFyjdDx5iDdCV5obWP_9sYD_D3eMHn06hHSKNSD0vSVkTP4-JTl7kOyjPSd8ubu4xgtpy_1nh_9mtBvWvyi6fMd-j4', 'A', 'eyJpdiI6IjNVamUweHZFM1JZVWhGeFwveFBHT3ZRPT0iLCJ2YWx1ZSI6Ijd2T1hkOEcwRXJ0UWh0U0d0MGJmY0E9PSIsIm1hYyI6IjBjZDI3OTA1NmM0NDZiNjQ5MTAzMDhiZTM5MTk1YzU0Yzc2ZDZkZTJhYmJjZTFmN2JlMWYwNzRiZWI1MWFlNGUifQ==', 'ROHGKM0I', '139', 'Rohit Sharma', 'rohit@gmail.com', '$2y$10$TiUFLmaSeP6Q3e3G8sZXsuWISvnbZjW7DwgB.TOOFq9sNddpEXh/e', '11-01-2000', 'Canada', '', '7018915635', 'default.jpg', 'Metric', '2', '1', '1', '2', '1', '1', '0', NULL, '2019-11-01 05:30:26', '2019-11-01 05:30:32', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `access_codes`
--
ALTER TABLE `access_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fav_resorts`
--
ALTER TABLE `fav_resorts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `friends`
--
ALTER TABLE `friends`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `group_members`
--
ALTER TABLE `group_members`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lifts`
--
ALTER TABLE `lifts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lift_history`
--
ALTER TABLE `lift_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `likes`
--
ALTER TABLE `likes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `question_answers`
--
ALTER TABLE `question_answers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `question_history`
--
ALTER TABLE `question_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `resorts`
--
ALTER TABLE `resorts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `resort_details`
--
ALTER TABLE `resort_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `resort_history`
--
ALTER TABLE `resort_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rewards_points`
--
ALTER TABLE `rewards_points`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `runs`
--
ALTER TABLE `runs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `run_history`
--
ALTER TABLE `run_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `skiers`
--
ALTER TABLE `skiers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `snowboarders`
--
ALTER TABLE `snowboarders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `snowflakes`
--
ALTER TABLE `snowflakes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `spend_snowflakes`
--
ALTER TABLE `spend_snowflakes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `states`
--
ALTER TABLE `states`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `test`
--
ALTER TABLE `test`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `access_codes`
--
ALTER TABLE `access_codes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `fav_resorts`
--
ALTER TABLE `fav_resorts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `friends`
--
ALTER TABLE `friends`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=165;

--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=184;

--
-- AUTO_INCREMENT for table `group_members`
--
ALTER TABLE `group_members`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=321;

--
-- AUTO_INCREMENT for table `lifts`
--
ALTER TABLE `lifts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `lift_history`
--
ALTER TABLE `lift_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT for table `likes`
--
ALTER TABLE `likes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `question_answers`
--
ALTER TABLE `question_answers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT for table `question_history`
--
ALTER TABLE `question_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'answer_id', AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT for table `resorts`
--
ALTER TABLE `resorts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=108;

--
-- AUTO_INCREMENT for table `resort_details`
--
ALTER TABLE `resort_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `resort_history`
--
ALTER TABLE `resort_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=204;

--
-- AUTO_INCREMENT for table `rewards_points`
--
ALTER TABLE `rewards_points`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `runs`
--
ALTER TABLE `runs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;

--
-- AUTO_INCREMENT for table `run_history`
--
ALTER TABLE `run_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;

--
-- AUTO_INCREMENT for table `skiers`
--
ALTER TABLE `skiers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `snowboarders`
--
ALTER TABLE `snowboarders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `snowflakes`
--
ALTER TABLE `snowflakes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=263;

--
-- AUTO_INCREMENT for table `spend_snowflakes`
--
ALTER TABLE `spend_snowflakes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;

--
-- AUTO_INCREMENT for table `states`
--
ALTER TABLE `states`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;

--
-- AUTO_INCREMENT for table `test`
--
ALTER TABLE `test`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=140;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
