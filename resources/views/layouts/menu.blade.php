<li>
<a href="{!! url('/home') !!}"><i class="fas fa-tachometer-alt"></i>
	<span>Dashboard</span>
</a>
</li>
<li class="{{ Request::is('users*') ? 'active' : '' }}">
    <a href="{!! route('users.index') !!}"><i class="fa fa-user">
    </i><span>Users</span></a>
</li>

<li class="{{ Request::is('groups*') ? 'active' : '' }}">
    <a href="{!! route('groups.index') !!}"><i class="fa fa-users">    
    </i><span> Groups</span></a>
</li>

<li class="{{ Request::is('snowflakes*') ? 'active' : '' }}">
    <a href="{!! route('snowflakes.index') !!}"><i class="fa fa-snowflake-o"></i><span>Snowflakes</span></a>
</li>
<li class="{{ Request::is('resorts*') ? 'active' : '' }}">
    <a href="{!! route('resorts.index') !!}"><i class="fa fa-location-arrow" aria-hidden="true"></i><span>Resorts</span></a>
</li>

<li class="{{ Request::is('runs*') ? 'active' : '' }}">
    <a href="{!! route('runs.index') !!}"><i class="fas fa-skiing"></i>
    	<span>Runs</span>
    </a>
</li>

<li class="{{ Request::is('lifts*') ? 'active' : '' }}">
    <a href="{!! route('lifts.index') !!}">
        <i class="fas fa-tram"></i>
    	<span>Lifts</span>
    </a>
</li>
<!--
<li class="{{ Request::is('qAS*') ? 'active' : '' }}">
    <a href="{!! route('qAS.index') !!}">
        <i class="fas fa-question-circle"></i>
        <span>Q & A</span></a>
</li>

<li class="{{ Request::is('guides*') ? 'active' : '' }}">
    <a href="{!! route('guides.index') !!}"><i class="fa fa-thumbs-up"></i><span>Guides</span></a>
</li>

<li class="{{ Request::is('weathers*') ? 'active' : '' }}">
    <a href="{{ route('weathers.index') }}"><i class="fas fa-cloud-moon-rain"></i><span> Weathers</span></a>
</li>

<li class="{{ Request::is('trackings*') ? 'active' : '' }}">
    <a href="{{ route('trackings.index') }}"><i class="fa fa-edit"></i><span>Trackings</span></a>
</li>-->

<li class="{{ Request::is('events*') ? 'active' : '' }}">
    <a href="{{ route('events.index') }}"><i class="fa fa-calendar"></i><span>Events</span></a>
</li>

<li class="{{ Request::is('businesses*') ? 'active' : '' }}">
    <a href="{{ route('businesses.index') }}"><i class="fas fa-business-time"></i><span>  Businesses</span></a>
</li>

<li class="{{ Request::is('lunches*') ? 'active' : '' }}">
    <a href="{{ route('lunches.index') }}"><i class="fas fa-utensils"></i><span> Lunches</span></a>
</li>

<li class="{{ Request::is('jobs*') ? 'active' : '' }}">
    <a href="{{ route('jobs.index') }}"><i class="fa fa-edit"></i><span>Jobs</span></a>
</li>

