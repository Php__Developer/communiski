<!-- User Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('user_id', 'User Id:') !!}
    {!! Form::text('user_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Snowflake Rewards Field -->
<div class="form-group col-sm-6">
    {!! Form::label('snowflake_rewards', 'Snowflake Rewards:') !!}
    {!! Form::text('snowflake_rewards', null, ['class' => 'form-control']) !!}
</div>

<!-- Reason Field -->
<div class="form-group col-sm-6">
    {!! Form::label('reason', 'Reason:') !!}
    {!! Form::text('reason', null, ['class' => 'form-control']) !!}
</div>

<!-- Reward Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('reward_status', 'Reward Status:') !!}
    {!! Form::text('reward_status', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('snowflakes.index') !!}" class="btn btn-default">Cancel</a>
</div>
