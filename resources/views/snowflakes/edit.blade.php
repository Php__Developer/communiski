@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Snowflake
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($snowflake, ['route' => ['snowflakes.update', $snowflake->id], 'method' => 'patch']) !!}

                        @include('snowflakes.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection