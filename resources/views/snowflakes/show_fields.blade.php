<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $snowflake->id !!}</p>
</div>

<!-- User Id Field -->
<div class="form-group">
    {!! Form::label('user_id', 'User Id:') !!}
    <p>{!! $snowflake->user_name !!}</p>
</div>

<!-- Snowflake Rewards Field -->
<div class="form-group">
    {!! Form::label('snowflake_rewards', 'Snowflake Rewards:') !!}
    <p>{!! $snowflake->snowflake_rewards !!}</p>
</div>

<!-- Reason Field -->
<div class="form-group">
    {!! Form::label('reason', 'Reason:') !!}
    <p>{!! $snowflake->reason !!}</p>
</div>

<!-- Reward Status Field -->
<div class="form-group">
    {!! Form::label('reward_status', 'Reward Status:') !!}
    <p>@if($snowflake->reward_status == 1)
        {{ "Confirmed" }}
        @elseif($snowflake->reward_status == 0)
        {{ "Pending" }}
        @elseif($snowflake->reward_status == 2)
        {{ "Rejected" }}
       @endif
    </p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $snowflake->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $snowflake->updated_at !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $snowflake->deleted_at !!}</p>
</div>

