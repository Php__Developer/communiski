<div class="table-responsive">
    <table class="table" id="snowflakes-table">
        <thead>
            <tr>
                <th scope="col">#ID</th>
                <th>User Name</th>
                <th>Snowflake Rewards</th>
                <th>Reason</th>
                <th>Reward Status</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
            <?php $i = ($snowflakes->currentPage() - 1) * $snowflakes->perPage() + 1; ?>
        @foreach($snowflakes as $snowflake)
            <tr>
                <td scope="row">{!! $i++ !!}</td>
                <td>{!! $snowflake->user_name !!}</td>
                <td><i class="fa fa-snowflake-o"></i> {!! $snowflake->snowflake_rewards !!}</td>
                <td>{!! $snowflake->reason !!}</td>
                <td>
                    @if($snowflake->reward_status == 1)
                    {{ "Confirmed" }}
                    @elseif($snowflake->reward_status == 0)
                    {{ "Pending" }}
                    @elseif($snowflake->reward_status == 2)
                    {{ "Rejected" }}
                    @endif
                </td>
                <td>
                    {!! Form::open(['route' => ['snowflakes.destroy', $snowflake->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('snowflakes.show', [$snowflake->id]) !!}" class='btn btn-default btn-md'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <?php $snow = $snowflake->reward_status; ?>
                        @if($snow == 0)
                        
                        <a href="{!! url('/confirmed/' .$snowflake->id) !!}" class='btn btn-info btn-md'><i class="fa fa-snowflake-o" aria-hidden="true"></i></a>

                        <a href="{!! url('/reject/' .$snowflake->id) !!}" class='btn btn-danger btn-md'><i class="fa fa-times" aria-hidden="true"></i></i></a>
                        @endif
                        <!--<a href="{!! route('snowflakes.edit', [$snowflake->id]) !!}" class='btn btn-default btn-md'><i class="glyphicon glyphicon-edit"></i></a>

                        {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-md', 'onclick' => "return confirm('Are you sure?')"]) !!}-->
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <div class="col-5">
        <div class="float-right">
            {!! $snowflakes->render() !!}
        </div>
    </div>
</div>
