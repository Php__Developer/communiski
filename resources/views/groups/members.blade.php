@extends('layouts.app') @section('content')
<?php
use App\Models\Group_member;
use App\Models\Group;
use App\Models\User;
?>
<style>
    .col-sm-2 {
    padding-left: 40px;
    width: 16.66666667%;
}
</style>
    <section class="content-header">
        <h1><center>
    Group Members
</center></h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    <?php
$groupid = $groupid;
$groupDetail = Group::where(['id' => $groupid])->select('id', 'group_location', 'group_owner_id', 'privacy_status')
    ->first();
$ownerimage = User::where(['id' => $groupDetail
    ->group_owner_id])
    ->select('image')
    ->first();
?>
                        <div>
                            <div class="row">
                                <div class="user_info">
                                    <div class="col-sm-3"></div>
                                    <div class="col-sm-2">
                                        <div class="user_image">
                                            <a href="http://93.188.167.68/projects/communiski/public/images/<?=$ownerimage->image; ?>" target="_blank"><img src="{!! url('/images/'. $ownerimage->image) !!}" width="auto;" height="125"></a>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <h1 style="margin-top: 42px;">
                                    <b>{!! ucfirst($groupDetail->group_location) !!}</b>
                                </h1>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <h3 style="text-align: center;">Members</h3>
                        <div>
                            <div class="row">
                                <div class="members">
                                        @foreach($users as $user)
                                            <div class="col-sm-2">
                                            <a href="http://93.188.167.68/projects/communiski/public/images/<?=$user->image; ?>" target="_blank"><img style="border-radius: 10%;" src="{!! url('/images/'. $user->image) !!}" width="72" height="76"></a>
                                            <h4>{{ ucfirst($user->name) }}</h4>
                                            </div>
                                        @endforeach
                                    
                                </div>
                            </div>
                        </div>
                        <a href="{!! route('groups.index') !!}" class="btn btn-default" style="margin-top: 25px;">Back</a>
                </div>
            </div>
        </div>
    </div>
    @endsection