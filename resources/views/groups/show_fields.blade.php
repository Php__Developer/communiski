<?php
use App\Models\User;
use App\Models\Group;
?>
<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">

      <style>
         h3 {
         margin-top: 5%;
         padding-left: 24px;
         }
         .user_info {
         display: -webkit-box;
         }
         .activate_button {
         float: right;
         }
         h5 {
         margin-bottom: 5px;
         }
         .icon {
         font-size: xx-large;
         padding-right: 15px;
         }
      </style>
   </head>
   <body>
      <div class="container">
         <div class="row">
            <div class="col-sm-4">
               <div class="row">
                  <table>
                     <tr>
                        <td>
                           <div class="icon">
                              <i class="fa fa-users"></i>
                           </div>
                        </td>
                        <td>
                           <h5>Name:
                           </h5>
                           {!! $group->group_name !!}
                        </td>
                     </tr>
                  </table>
               </div>
            </div>
            <div class="col-sm-4">
               <div class="row">
                  <table>
                     <tr>
                        <td>
                           <div class="icon">
                              <i class="fa fa-user"></i>
                           </div>
                        </td>
                        <td>
                           <h5>Group Owner:
                           </h5>
                           <?php
                           $grpowner = User::where(['id'=> $group['group_owner_id']])->select('name')->first();
                           ?>
                           {!! ucfirst($grpowner->name) !!}
                        </td>
                     </tr>
                  </table>
               </div>
            </div>

            <div class="col-sm-4">
               <div class="row">
                  <table>
                     <tr>
                        <td>
                           <div class="icon">
                              <i class="fa fa-calendar"></i>
                           </div>
                        </td>
                        <td>
                           <h5>Group Date:
                           </h5>
                           {!! $group->group_date !!}
                        </td>
                     </tr>
                  </table>
               </div>
            </div>
         </div>
         <br>
         <div class="row">
            <div class="col-sm-4">
               <div class="row">
                  <table>
                     <tr>
                        <td>
                           <div class="icon">
                              <i class="fa fa-user-secret"></i>
                           </div>
                        </td>
                        <td>
                           <h5>Group Privacy:
                           </h5>
                           {!! $group->privacy_status !!}
                        </td>
                     </tr>
                  </table>
               </div>
            </div>
            <div class="col-sm-4">
               <div class="row">
                  <table>
                     <tr>
                        <td>
                           <div class="icon">
                              <i class="fa fa-calendar-plus-o"></i>
                           </div>
                        </td>
                        <td>
                           <h5>Created At:
                           </h5>
                           {!! $group->created_at !!}
                        </td>
                     </tr>
                  </table>
               </div>
            </div>
            
         <div class="row">
            <div class="col-sm-4">
               <div class="row">
                  <table>
                     <tr>
                        <td>
                           <div class="icon">
                              <i class="fa fa-calendar-plus-o"></i>
                           </div>
                        </td>
                        <td>
                           <h5>Updated At:
                           </h5>
                           {!! $group->updated_at !!}
                        </td>
                     </tr>
                  </table>
               </div>
            </div>
            <div class="col-sm-4">
            </div>
         </div>
         </div>
      </div>
   </body>
</html>