<!-- Group Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('group_name', 'Group Name:') !!}
    {!! Form::text('group_name', null, ['class' => 'form-control']) !!}
</div>

<!-- Group Image Field -->
<!--<div class="form-group col-sm-6">
    {!! Form::label('group_image', 'Group Image:') !!}
    {!! Form::text('group_image', null, ['class' => 'form-control']) !!}
</div>-->

<!-- Group Owner Field -->
<div class="form-group col-sm-6">
    {!! Form::label('group_owner_id', 'Group Owner:') !!}
    {!! Form::text('group_owner_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Group Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('group_date', 'Group Date:') !!}
    {!! Form::text('group_date', null, ['class' => 'form-control']) !!}
</div>

<!-- Total Users Field -->
<div class="form-group col-sm-6">
    {!! Form::label('group_info', 'Group Info:') !!}
    {!! Form::text('group_info', null, ['class' => 'form-control']) !!}
</div>

<!-- Privacy Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('privacy_status', 'Privacy Status:') !!}
    {!! Form::text('privacy_status', null, ['class' => 'form-control' ,'placeholder' => '1 For Buddies 2 for members 3 for everyone','readonly'=>'true']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('groups.index') !!}" class="btn btn-default">Cancel</a>
</div>
