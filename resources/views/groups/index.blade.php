@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">All Groups</h1>
        <h1 class="pull-right">
            <form action="searchgroup" method="get">
           <input type="search" style="height: 35px;" class="search" name="search" placeholder="Search..">
           <!--<button type="submit"><i class="fa fa-search button"></i></button>-->
           <button type="submit" class="btn btn-success button">Search</button>
       </form>
        </h1>
        <!--<h1 class="pull-right">
           <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{!! route('groups.create') !!}">Add New</a>
        </h1>-->
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                    @include('groups.table')
            </div>
        </div>
        <div class="text-center">
        
        </div>
    </div>
<style type="text/css">
    input.search {
        height: 35px;
        border-radius: 10px;
        margin-bottom: 10px;
        width: 196px;
        -webkit-appearance: media-sliderthumb;
    }
</style>
@endsection

