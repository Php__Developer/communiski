<?php
use App\Models\Group_member;
use App\Models\Group;
?>

<div class="table-responsive">
    <table class="table" id="groups-table">
        <thead>
            <tr>
                <th scope="col">#ID</th>
                <th>Group Date</th>
                <th>Location</th>
                <th>Group Owner</th>
                <th>Group Information</th>
                <th>Group Members</th>
                <th>Privacy Status</th>
                <th colspan="3">Action</th>
            </tr>
            </thead>
            <tbody>
                <?php $i = ($groups->currentPage() - 1) * $groups->perPage() + 1; ?>
                @foreach($groups as $group)
                <tr>
                    <td scope="row">{!! $i++ !!}</td>
                    <td>{!! date('d-m-Y', strtotime($group->group_date)); !!}</td>
                    <td><b>{!! ucfirst($group->group_location) !!}</b></td>
                    <!--<td><a href="http://93.188.167.68/projects/communiski/public/images/<?= $group->group_image; ?>" target="_blank"><img src="{!! url('/images/'. $group->group_image) !!}" width="72" height="76"></a></td>-->
                    <td>{!! ucfirst($group->user_name) !!}</td>
                    <td>{!! $group->group_info !!}</td>

                    <td>
                    <a href="{!! url('/groupmembers/' .$group->id) !!}" title="View Members">
                    <?php
                    $groupid = $group->id;
                    $groupDetail = Group_member::where(['group_id'=> $groupid])->where(['request_status'=>'accepted'])->select('user_id')->get();
                    print_r(json_encode(count($groupDetail)));
                    ?>
                    Member
                    </a>
                    </td>

                    <td>{!! $group->privacy_status !!}</td>
                    <td>
                        {!! Form::open(['route' => ['groups.destroy', $group->id], 'method' => 'delete']) !!}
                        <div class='btn-group'>
                            <a href="{!! route('groups.show', [$group->id]) !!}" class='btn btn-default btn-md'><i class="glyphicon glyphicon-eye-open"></i></a>
                            <!--<a href="{!! route('groups.edit', [$group->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>-->
                            {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-md', 'onclick' => "return confirm('Are you sure?')"]) !!}
                        </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
    </tbody>
</table>
<div class="col-5">
    <div class="float-right">
     {!! $groups->render() !!}
 </div>
</div>
</div>
