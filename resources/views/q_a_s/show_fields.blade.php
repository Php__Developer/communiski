<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $qA->id !!}</p>
</div>

<!-- User Id Field -->
<div class="form-group">
    {!! Form::label('user_id', 'User Id:') !!}
    <p>{!! $qA->user_id !!}</p>
</div>

<!-- Question Field -->
<div class="form-group">
    {!! Form::label('question', 'Question:') !!}
    <p>{!! $qA->question !!}</p>
</div>

<!-- Answer Field -->
<div class="form-group">
    {!! Form::label('answer', 'Answer:') !!}
    <p>{!! $qA->answer !!}</p>
</div>

<!-- Question Type Field -->
<div class="form-group">
    {!! Form::label('question_type', 'Question Type:') !!}
    <p>{!! $qA->question_type !!}</p>
</div>

<!-- Topics Field -->
<div class="form-group">
    {!! Form::label('topic', 'Topic:') !!}
    <p>{!! $qA->topic !!}</p>
</div>

<!-- Archive Status Field -->
<div class="form-group">
    {!! Form::label('archive_status', 'Archive Status:') !!}
    <p>{!! $qA->archive_status !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $qA->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $qA->updated_at !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $qA->deleted_at !!}</p>
</div>

