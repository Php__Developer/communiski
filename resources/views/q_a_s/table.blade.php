<div class="table-responsive">
    <table class="table" id="qAS-table">
        <thead>
            <tr>
                <th>User Id</th>
                <th>Question</th>
                <th>Answer</th>
                <th>Question Type</th>
                <th>Topic</th>
                <th>Archive Status</th>
                <!--<th colspan="3">Action</th>-->
            </tr>
        </thead>
        <tbody>
        @foreach($qAS as $qA)
            <tr>
            <td>{!! $qA->user_id !!}</td>
            <td>{!! $qA->question !!}</td>
            <td>{!! $qA->answer !!}</td>
            <td>{!! $qA->question_type !!}</td>
            <td>{!! $qA->topic !!}</td>
            <td>{!! $qA->archive_status !!}</td>
                <td>
                    {!! Form::open(['route' => ['qAS.destroy', $qA->id], 'method' => 'delete']) !!}
                    <!--<div class='btn-group'>
                        <a href="{!! route('qAS.show', [$qA->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{!! route('qAS.edit', [$qA->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>-->
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
