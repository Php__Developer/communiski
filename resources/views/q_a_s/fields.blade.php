<!-- User Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('user_id', 'User Id:') !!}
    {!! Form::text('user_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Question Field -->
<div class="form-group col-sm-6">
    {!! Form::label('question', 'Question:') !!}
    {!! Form::text('question', null, ['class' => 'form-control']) !!}
</div>

<!-- Answer Field -->
<div class="form-group col-sm-6">
    {!! Form::label('answer', 'Answer:') !!}
    {!! Form::text('answer', null, ['class' => 'form-control']) !!}
</div>

<!-- Question Type Field -->
<div class="form-group col-sm-6">
    {!! Form::label('question_type', 'Question Type:') !!}
    {!! Form::text('question_type', null, ['class' => 'form-control']) !!}
</div>

<!-- Topics Field -->
<div class="form-group col-sm-6">
    {!! Form::label('topic', 'Topics:') !!}
    {!! Form::text('topic', null, ['class' => 'form-control']) !!}
</div>

<!-- Archive Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('archive_status', 'Archive Status:') !!}
    {!! Form::text('archive_status', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('qAS.index') !!}" class="btn btn-default">Cancel</a>
</div>
