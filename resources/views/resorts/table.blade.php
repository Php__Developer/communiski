<div class="table-responsive">
    <table class="table" id="resorts-table">
        <thead>
            <tr>
                <th>#ID</th>
                <th>Resort Name</th>
                <th>Country</th>
                <th>State</th>
                <th>Image</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
            <?php $i = ($resorts->currentPage() - 1) * $resorts->perPage() + 1; ?>
            @foreach($resorts as $resort)
            <tr>
                <td scope="row">{!! $i++ !!}</td>
                <td>{!! $resort->resort_name !!}</td>
                <td>{!! $resort->country_name !!}</td>
                <td>{!! $resort->state_name !!}</td>
                <td><a href="http://93.188.167.68/projects/communiski/public/resort_images/<?= $resort->image; ?>" target="_blank"><img src="{!! url('/resort_images/'. $resort->image) !!}" width="72" height="76"></a></td>
                <td>
                    {!! Form::open(['route' => ['resorts.destroy', $resort->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('resorts.show', [$resort->id]) !!}" class='btn btn-default btn-md'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{!! route('resorts.edit', [$resort->id]) !!}" class='btn btn-default btn-md'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-md', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <div class="col-5">
        <div class="float-right">
            {!! $resorts->render() !!}
        </div>
    </div>
</div>
