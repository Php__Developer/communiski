<style type="text/css">
    img 
    {
        width: 100%;
        height: 260px;
        border-radius: 10px;
    }
    .container 
    {
        padding: 0px 20px;
        }
        .highlight 
        {
            font-size: 17px;
        }
</style>
<div class="container">
    <div class="row">
        <div class="col-sm-7">
            <div class="form-group">
                <td href="http://93.188.167.68/projects/communiski/public/resort_images/<?= $resort->image; ?>"><img src="{!! url('/resort_images/'. $resort->image) !!}" width="72" height="76"></td>
            </div>
        </div>
        <div class="col-sm-5">
            <div class="highlight">
                <div class="form-group">
                    {!! Form::label('id', '#ID:') !!}
                    <p>{!! $resort->id !!}</p>
                </div>

                <div class="form-group">
                    {!! Form::label('resort_name', 'Resort Name:') !!}
                    <p>{!! $resort->resort_name !!}</p>
                </div>

                <div class="form-group">
                    {!! Form::label('address', 'Resort Address:') !!}
                    <p>{!! $resort->address !!}</p>
                </div>

                <div class="form-group">
                    {!! Form::label('country', 'Country:') !!}
                    <p>{!! $resort->country_name !!}</p>
                </div>

                <div class="form-group">
                    {!! Form::label('state', 'State:') !!}
                    <p>{!! $resort->state_name !!}</p>
                </div>

                <div class="form-group">
                    {!! Form::label('phone_no', 'Phone No:') !!}
                    <p>{!! $resort->phone_no !!}</p>
                </div>

                <div class="form-group">
                    {!! Form::label('top_latitude', 'Top Latitude:') !!}
                    <p>{!! $resort->top_latitude !!}</p>
                </div>

                <div class="form-group">
                    {!! Form::label('top_longitude', 'Top Longitude:') !!}
                    <p>{!! $resort->top_longitude !!}</p>
                </div>

                <div class="form-group">
                    {!! Form::label('latitude', 'Base Latitude:') !!}
                    <p>{!! $resort->latitude !!}</p>
                </div>

                <div class="form-group">
                    {!! Form::label('longitude', 'Base Longitude:') !!}
                    <p>{!! $resort->longitude !!}</p>
                </div>
            </div>
        </div>
    </div>

    <div>
        <h3><b>Access Codes and Services:</b></h3>
    </div>

    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                {!! Form::label('resort_administrator', 'Resort Administrator:') !!}
                <p>{!! $accessCode->resort_administrator !!}</p>
            </div>

            <div class="form-group">
                {!! Form::label('resort_editor', 'Resort Editor:') !!}
                <p>{!! $accessCode->resort_editor !!}</p>
            </div>

            <div class="form-group">
                {!! Form::label('patroller', 'Patroller:') !!}
                <p>{!! $accessCode->patroller !!}</p>
            </div>

            <div class="form-group">
                {!! Form::label('lifty', 'Lifty:') !!}
                <p>{!! $accessCode->lifty !!}</p>
            </div>

            <div class="form-group">
                {!! Form::label('created_at', 'Created At:') !!}
                <p>{!! $resort->created_at !!}</p>
            </div>
        </div>

        <div class="col-sm-6">
            <div class="form-group">
                {!! Form::label('lift_names', 'Does the resort use Lift Names?:') !!}
                <p>
                    @if($resort->lift_names == 1)
                    {{ "Yes" }}
                    @elseif($resort->lift_names == 2)
                    {{ "No" }}
                   @endif
               </p>
           </div>

           <div class="form-group">
            {!! Form::label('lift_numbers', 'Does the resort use Lift Numbers?:') !!}
            <p>
                @if($resort->lift_numbers == 1)
                {{ "Yes" }}
                @elseif($resort->lift_numbers == 2)
                {{ "No" }}
               @endif
           </p>
           </div>

           <div class="form-group">
            {!! Form::label('run_names', 'Does the resort use Run Names?:') !!}
            <p>
                @if($resort->run_names == 1)
                {{ "Yes" }}
                @elseif($resort->run_names == 2)
                {{ "No" }}
               @endif
           </p>
          </div>

          <div class="form-group">
            {!! Form::label('run_numbers', 'Does the resort use Run Numbers?:') !!}
            <p>
                @if($resort->run_numbers == 1)
                {{ "Yes" }}
                @elseif($resort->run_numbers == 2)
                {{ "No" }}
                @endif
            </p>
          </div>

          <div class="form-group">
            {!! Form::label('updated_at', 'Updated At:') !!}
            <p>{!! $resort->updated_at !!}</p>
          </div>
<!--
          <div class="form-group">
              {!! Form::label('deleted_at', 'Deleted At:') !!}
              <p>{!! $resort->deleted_at !!}</p>
          </div>
-->
    </div>
  </div>
</div>