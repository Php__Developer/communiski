@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Resort
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">

                   {!! Form::model($resort, ['route' => ['resorts.update', $resort->id], 'method' => 'patch', 'files'=>'true' ]) !!}

                        @include('resorts.editfields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection