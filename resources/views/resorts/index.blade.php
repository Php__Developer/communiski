@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">Resorts</h1>
        <h1 class="pull-right">
           <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{!! route('resorts.create') !!}">Add Resort</a>
        </h1>
        <form action="searchresort" method="get">
           <input type="search" style="height: 35px;" class="search" name="search" placeholder="Search..">
           <button type="submit" class="btn btn-success button">Search</button>
       </form>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                    @include('resorts.table')
            </div>
        </div>
        <div class="text-center">
        
        </div>
    </div>
    <style type="text/css">
    input.search {
        height: 35px;
        border-radius: 10px;
        margin-bottom: 10px;
        width: 196px;
        -webkit-appearance: media-sliderthumb;
    }
</style>
@endsection

