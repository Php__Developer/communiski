<!-- Resort Name Field -->
<style>
/* The container */
.container {
  display: block;
  position: relative;
  padding-left: 35px;
  margin-bottom: 12px;
  cursor: pointer;
  font-size: 22px;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
}

/* Hide the browser's default radio button */
.container input {
  position: absolute;
  opacity: 0;
  cursor: pointer;
}

/* Create a custom radio button */
.checkmark {
  position: absolute;
  top: 0;
  left: 0;
  height: 25px;
  width: 25px;
  background-color: #eee;
  border-radius: 50%;
}

/* On mouse-over, add a grey background color */
.container:hover input ~ .checkmark {
  background-color: #ccc;
}

/* When the radio button is checked, add a blue background */
.container input:checked ~ .checkmark {
  background-color: #2196F3;
}

/* Create the indicator (the dot/circle - hidden when not checked) */
.checkmark:after {
  content: "";
  position: absolute;
  display: none;
}

/* Show the indicator (dot/circle) when checked */
.container input:checked ~ .checkmark:after {
  display: block;
}

/* Style the indicator (dot/circle) */
.container .checkmark:after {
    top: 9px;
    left: 9px;
    width: 8px;
    height: 8px;
    border-radius: 50%;
    background: white;
}
</style>

<!-- Image Field -->
<div class="form-group col-sm-6">
  <label for="image" class="col-sm-2 col-form-label">Resort Image</label>
    <img src="{!! url('/resort_images/'. $resort->image) !!}" width="350" height="173">
    <input type="file" class="form-control" value="{!! $resort->image !!}" id="image" name="image">
</div>

<div class="form-group col-sm-6">
    {!! Form::label('resort_name', 'Resort Name:') !!}
    {!! Form::text('resort_name', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('address', 'Resort Address:') !!}
    {!! Form::text('address',null,['class'=>'form-control','id' => 'autocomplete','onFocus' =>'geolocate()','placeholder' => 'Address']) !!}
    {!! Form::hidden('lat',null,['class'=>'form-control','id' => 'lat']) !!}
    {!! Form::hidden('long',null,['class'=>'form-control','id' => 'long']) !!}
</div>

<!-- Country Field -->
<div class="form-group col-sm-6">
    <label for="title">Select Country:</label>
    <select id="country" name="country" class="form-control">
      @foreach($countries as $key => $value)
      <option value="{{$key}}" {{ ( $key == $resort->country) ? 'selected' : '' }}>{{$value}}</option>
      @endforeach
        <!-- <option value="" selected disabled>Select</option> -->
        <!-- @foreach($countries as $key => $country)
        <option value="{{$key}}"> {{$country}}</option>
        @endforeach -->
    </select>
</div>

<div class="form-group col-sm-6">
    <label for="title">Select State:</label>
    <select name="state" id="state" class="form-control" >
      @foreach($states as $key => $value)
      <option value="{{$key}}" {{ ( $key == $resort->state) ? 'selected' : '' }}>{{$value}}</option>
      @endforeach
    </select>
</div>


<!--<div class="form-group col-sm-6">
    {!! Form::label('image', 'Image:') !!}
    <input type="file" class="form-control" name="image"/>
</div>-->

<div class="form-group col-sm-6">
    {!! Form::label('phone_no', 'Phone No:') !!}
    {!! Form::text('phone_no', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('top_latitude', 'Top Latitude:') !!}
    {!! Form::text('top_latitude', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('top_longitude', 'Top Longitude:') !!}
    {!! Form::text('top_longitude', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('latitude', 'Latitude:') !!}
    {!! Form::text('latitude', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('longitude', 'Longitude:') !!}
    {!! Form::text('longitude', null, ['class' => 'form-control']) !!}
</div>
<!-- lift and run Field -->

<div class="form-group col-sm-6">
    {!! Form::label('liftnames', 'Does the resort use Lift Names?:') !!}
    <label class="container">Yes
  <input type="radio" checked="checked" name="lift_names" value="1" <?php if ($resort['lift_names'] == '1') {echo ' checked ';} ?>>
  <span class="checkmark"></span>
</label>
<label class="container">No
  <input type="radio" name="lift_names" value="2" <?php if ($resort['lift_names'] == '2') {echo ' checked ';} ?>>
  <span class="checkmark"></span>
</label>
</div>

<div class="form-group col-sm-6">
    {!! Form::label('liftnames', 'Does the resort use Lift Numbers?:') !!}
    <label class="container">Yes
  <input type="radio" checked="checked" name="lift_numbers" value="1" <?php if ($resort['lift_numbers'] == '1') {echo ' checked ';} ?>>
  <span class="checkmark"></span>
</label>
<label class="container">No
  <input type="radio" name="lift_numbers" value="2" <?php if ($resort['lift_numbers'] == '2') {echo ' checked ';} ?>>
  <span class="checkmark"></span>
</label>
</div>

<div class="form-group col-sm-6">
    {!! Form::label('liftnames', 'Does the resort use Run Names?:') !!}
    <label class="container">Yes
  <input type="radio" checked="checked" name="run_names" value="1" <?php if ($resort['run_names'] == '1') {echo ' checked ';} ?>>
  <span class="checkmark"></span>
</label>
<label class="container">No
  <input type="radio" name="run_names" value="2" <?php if ($resort['run_names'] == '2') {echo ' checked ';} ?>>
  <span class="checkmark"></span>
</label>
</div>

<div class="form-group col-sm-6">
    {!! Form::label('liftnames', 'Does the resort use Run Numbers?:') !!}
    <label class="container">Yes
  <input type="radio" checked="checked" name="run_numbers" value="1" <?php if ($resort['run_numbers'] == '1') {echo ' checked ';} ?>>
  <span class="checkmark"></span>
</label>
<label class="container">No
  <input type="radio" name="run_numbers" value="2" <?php if ($resort['run_numbers'] == '2') {echo ' checked ';} ?>>
  <span class="checkmark"></span>
</label>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('resorts.index') !!}" class="btn btn-default">Cancel</a>
</div>

<script src="http://www.codermen.com/js/jquery.js"></script>
<script type="text/javascript">
    $('#country').change(function(){
        var countryID = $(this).val();    
        if(countryID){
            $.ajax({
                type:"GET",
                url:"{{url('get-state-list')}}?country_id="+countryID,
                success:function(res){               
                    if(res){
                        $("#state").empty();
                        $("#state").append('<option>Select</option>');
                        $.each(res,function(key,value){
                            $("#state").append('<option value="'+key+'">'+value+'</option>');
                        });
                    }else{
                        $("#state").empty();
                    }
                }
            });
        }else{
            $("#state").empty();
            $("#city").empty();
        }      
    });
    $('#state').on('change',function(){
        var stateID = $(this).val();
        if(stateID){
            $.ajax({
                type:"GET",
                url:"{{url('get-city-list')}}?state_id="+stateID,
                success:function(res){               
                    if(res){
                        $("#city").empty();
                        $.each(res,function(key,value){
                            $("#city").append('<option value="'+key+'">'+value+'</option>');
                        });
                    }else{
                        $("#city").empty();
                    }
                }
            });
        }else{
            $("#city").empty();
        }
    });
</script>

<script>
  var placeSearch, autocomplete;
  var componentForm = {
    street_number: 'short_name',
    route: 'long_name',
    locality: 'long_name',
    administrative_area_level_1: 'short_name',
    country: 'long_name',
    postal_code: 'short_name'
  };

  function initAutocomplete() {
        // Create the autocomplete object, restricting the search to geographical
        // location types.
        autocomplete = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
            {types: ['geocode']});

        // When the user selects an address from the dropdown, populate the address
        // fields in the form.
        autocomplete.addListener('place_changed', fillInAddress);
      }

      function fillInAddress() {
        // Get the place details from the autocomplete object.
        var place = autocomplete.getPlace();

        document.getElementById('lat').value = place.geometry.location.lat();
        document.getElementById('long').value = place.geometry.location.lng();

        for (var component in componentForm) {
          document.getElementById(component).value = '';
          document.getElementById(component).disabled = false;
        }

        // Get each component of the address from the place details
        // and fill the corresponding field on the form.
        for (var i = 0; i < place.address_components.length; i++) {
          var addressType = place.address_components[i].types[0];
          if (componentForm[addressType]) {
            var val = place.address_components[i][componentForm[addressType]];
            document.getElementById(addressType).value = val;
          }
        }
      }

      // Bias the autocomplete object to the user's geographical location,
      // as supplied by the browser's 'navigator.geolocation' object.
      function geolocate() {
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var geolocation = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };
            var circle = new google.maps.Circle({
              center: geolocation,
              radius: position.coords.accuracy
            });
            autocomplete.setBounds(circle.getBounds());
          });
        }
      }

    </script>
    <script>
     $(function() {
        $('#opentime').timepicker({
          'minTime': '12:00am',
  'maxTime': '11:30pm',
  'showDuration': true
        });
      });

      $(function() {
        $('#closedtime').timepicker({'minTime': '12:00am',
  'maxTime': '11:30pm',
  'showDuration': true});
      });
      </script>

      <script>
        $(function(){
            $('#delivery').change(function(){
                var delivery = $('#delivery').val();
                if(delivery == "Yes"){
                  $('.deliveryfee').show();
                }
                else{
                  $('.deliveryfee').hide();
                  $('#deliveryfee').val("");
                }
            });
        });
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD_5P0pxn1q9hvvTeCr3YCsDhLJoHwxs2c&libraries=places&callback=initAutocomplete"async defer>
</script>
