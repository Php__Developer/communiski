<div class="table-responsive">
    <table class="table table-hover" id="users-table">
        <thead>
            <tr>
                <th scope="col">#ID</th>
                <th>Name</th>
                <th>Image</th>
                <th>Email</th>
                <th>Phone</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
            <?php $i = ($users->currentPage() - 1) * $users->perPage() + 1; ?>
        @foreach($users as $user)
            <tr>
                <td scope="row">{!! $i++ !!}</td>
                <td>{!! ucfirst($user->name) !!}</td>
                <td><a href="http://93.188.167.68/projects/communiski/public/images/<?= $user->image; ?>" target="_blank"><img src="{!! url('/images/'. $user->image) !!}" width="72" height="76"></a></td>
                <td>{!! $user->email !!}</td>
                <td>{!! $user->phone_no !!}</td>
                <td>
                    {!! Form::open(['route' => ['users.destroy', $user->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('users.show', [$user->id]) !!}" class='btn btn-default btn-md'>
                            <i class="glyphicon glyphicon-eye-open"></i>
                        </a>
                        <a href="{!! route('users.edit', [$user->id]) !!}" class='btn btn-default btn-md'>
                            <i class="glyphicon glyphicon-edit"></i>
                        </a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-md', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <div class="col-5">
            <div class="float-right">
                {!! $users->render() !!}
            </div>
        </div>
</div>
