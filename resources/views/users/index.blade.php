@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">All Users</h1>
        <h1 class="pull-right">
            <form action="searchuser" method="get">
           <input type="search" style="height: 35px;" class="search" name="search" placeholder="Search..">
           <button type="submit" class="btn btn-success button">Search</button>
       </form>
        </h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                    @include('users.table')
            </div>
        </div>
        <div class="text-center">
        
        </div>
    </div>
<style type="text/css">
    input.search {
        height: 35px;
        border-radius: 10px;
        margin-bottom: 10px;
        width: 196px;
        -webkit-appearance: media-sliderthumb;
    }
</style>
@endsection

