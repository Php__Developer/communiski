@extends('layouts.app')

@section('content')
<style>
         h3 {
         margin-top: 5%;
         padding-left: 24px;
         }
         .user_info {
         display: -webkit-box;
         }
         .activate_button {
         float: right;
         }
         h5 {
         margin-bottom: 5px;
         }
         .icon {
         font-size: xx-large;
         padding-right: 15px;
         }
         table {
            width: 80%;
         }
         input[type="text"] {
        width: 100%;
         }
         .container {
    padding-left: 30px;
}
.form-group.row {
        padding-bottom: 10px;
    padding-left: 16px;
}
.content-wrapper {
    height: max-content;
}
      </style>
    <section class="content-header">
        <h1>
            Profile
        </h1>
    </section>
    <form action="{!! url('updateadminprofile') !!}" method="post" enctype="multipart/form-data">
        @csrf
      <div class="container">
         <div class="row">
            <div class="col-sm-6">
               <div class="user_info">
                  <div class="image">
                    <a href="http://93.188.167.68/projects/communiski/public/images/<?= $users->image; ?>" target="_blank"><img src="{!! url('/images/'. $users->image) !!}" width="300" height="200"></a>
                    <input type="file" class="form-control" value="{!! $users->image !!}" id="image" name="image">
                  </div>
               </div>
            </div>
         </div>
         <hr>
         <div class="row">
            <div class="col-sm-4">
               <div class="row">
                  <table>
                     <tr>
                        <td>
                           <div class="icon">
                              <i class="fa fa-user-circle"></i>
                           </div>
                        </td>
                        <td>
                           <h5>Name:
                           </h5>
                           <input type="text" name="name" value=" {!! $users->name !!} ">
                        </td>
                     </tr>
                  </table>
               </div>
            </div>
            <div class="col-sm-4">
               <div class="row">
                  <table>
                     <tr>
                        <td>
                           <div class="icon">
                              <i class="fa fa-calendar"></i>
                           </div>
                        </td>
                        <td>
                           <h5>Email:
                           </h5>
                           <input type="text" name="email" value=" {!! $users->email !!} ">
                        </td>
                     </tr>
                  </table>
               </div>
            </div>
            <div class="col-sm-4">
               <div class="row">
                  <table>
                     <tr>
                        <td>
                           <div class="icon">
                              <i class="fa fa-phone"></i>
                           </div>
                        </td>
                        <td>
                           <h5>Phone Number:
                           </h5>
                          <input type="text" name="phone_no" value=" {!! $users->phone_no !!} ">
                        </td>
                     </tr>
                  </table>
               </div>
            </div>
         </div>
         <br>
         <div class="row">
            <div class="col-sm-4">
            </div>
            <div class="col-sm-4">
            </div>
            <div class="col-sm-4">
         </div>
      </div>
         <br>
         <div class="row">
            <div class="col-sm-4">
            </div>
            <div class="col-sm-4">
            </div>
            <div class="col-sm-4">
            </div>
         </div>
      </div>
      <div class="form-group row">
               <div class="col-sm-10">
                  <button type="submit" class="btn btn-primary">Update</button>
               </div>
            </div>
      </form>
@endsection