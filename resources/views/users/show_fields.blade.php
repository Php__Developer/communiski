<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">

      <style>
         h3 {
         margin-top: 5%;
         padding-left: 24px;
         }
         .user_info {
         display: -webkit-box;
         }
         .activate_button {
         float: right;
         }
         h5 {
         margin-bottom: 5px;
         }
         .icon {
         font-size: xx-large;
         padding-right: 15px;
         }
      </style>
   </head>
   <body>
      <div class="container">
         <div class="row">
            <div class="col-sm-6">
               <div class="user_info">
                  <div class="user_image">
                     <center><a href="http://93.188.167.68/projects/communiski/public/images/<?= $user->image; ?>" target="_blank"><img src="{!! url('/images/'. $user->image) !!}" width="auto;" height="200"></a></center>
                  </div>
                  <h3>
                     <b>{!! ucfirst($user->name) !!}</b>
                     <table>
                     <tr>
                        <td>
                           <div class="icon">
                              <i class="fas fa-share-square"></i>
                           </div>
                        </td>
                        <td>
                           <button class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">
                           <h5>
                              {!! $space = " "; !!}
                              @if($refferalcount <= '1')
                              <b>{{ $refferalcount . $space . 'User referral by ' . ucfirst($user->name) }}</b>        
                              @else
                              <b><i>{{ $refferalcount . $space . 'Users referral by ' . ucfirst($user->name) }}</i></b>    
                              @endif
                           </h5>
                           </button>
                        </td>
                     </tr>
                  </table>
                  </h3>

               </div>
            </div>
         </div>
         <hr>
         <div class="row">
            <div class="col-sm-4">
               <div class="row">
                  <table>
                     <tr>
                        <td>
                           <div class="icon">
                              <i class="fa fa-user-circle"></i>
                           </div>
                        </td>
                        <td><h5>User ID:</h5><i>#{!! $user->id !!}</i></td>
                     </tr>
                  </table>
               </div>
            </div>
            <div class="col-sm-4">
               <div class="row">
                  <table>
                     <tr>
                        <td>
                           <div class="icon">
                              <i class="fas fa-envelope-open"></i>
                           </div>
                        </td>
                        <td>
                           <h5>Email:
                           </h5>
                           {!! $user->email !!}
                        </td>
                     </tr>
                  </table>
               </div>
            </div>

            <div class="col-sm-4">
               <div class="row">
                  <table>
                     <tr>
                        <td>
                           <div class="icon">
                              <i class="fa fa-calendar"></i>
                           </div>
                        </td>
                        <td>
                           <h5>Date of Birth:
                           </h5>
                           {!! $user->DOB !!}
                        </td>
                     </tr>
                  </table>
               </div>
            </div>
         </div>
         <br>
         <div class="row">
            <div class="col-sm-4">
               <div class="row">
                  <table>
                     <tr>
                        <td>
                           <div class="icon">
                              <i class="fa fa-globe"></i>
                           </div>
                        </td>
                        <td>
                           <h5>Country:
                           </h5>
                           {!! $user->country !!}
                        </td>
                     </tr>
                  </table>
               </div>
            </div>
            <div class="col-sm-4">
               <div class="row">
                  <table>
                     <tr>
                        <td>
                           <div class="icon">
                              <i class="fa fa-phone"></i>
                           </div>
                        </td>
                        <td>
                           <h5>Phone Number:
                           </h5>
                           {!! $user->phone_no !!}
                        </td>
                     </tr>
                  </table>
               </div>
            </div>
            
            <div class="col-sm-4">
               <div class="row">
                  <table>
                     <tr>
                        <td>
                           <div class="icon">
                              <i class="fa fa-calendar-check-o"></i>
                           </div>
                        </td>
                        <td>
                           <h5>Created At:
                           </h5>
                           {!! $user->created_at !!}
                        </td>
                     </tr>
                  </table>
               </div>
            </div>
         </div>

         <br>
         <div class="row">
            <div class="col-sm-4">
               <div class="row">
                  <table>
                     <tr>
                        <td>
                           <div class="icon">
                              <i class="fa fa-calendar-plus-o"></i>
                           </div>
                        </td>
                        <td>
                           <h5>Updated At:
                           </h5>
                           {!! $user->updated_at !!}
                        </td>
                     </tr>
                  </table>
               </div>
            </div>
            <div class="col-sm-4">
            </div>
         </div>
      </div> 
      <!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Referral Users Listing</h4>
      </div>
      <div class="modal-body">
        <table class="table no-margin text-center">
          <thead>
            <tr>
              <th>ID</th>
              <th>User Name</th>
              <th>Image</th>
            </tr>
          </thead>
          <tbody>
            @foreach($referralusers as $sd)
            <tr>
              <td>{{ $sd->id }}</td>
              <td>{{ $sd->name }}</td>
              <td><a href="http://93.188.167.68/projects/communiski/public/images/<?= $sd->image; ?>" target="_blank"><img src="{!! url('/images/'. $sd->image) !!}" width="auto;" height="40"></a></td>
            </tr>
            @endforeach
          </tbody>
        </table> 
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-info" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
   </body>
</html>