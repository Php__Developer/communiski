<!-- Business Service Id Field -->
<div class="form-group">
    {!! Form::label('business_service_id', 'Business Service Id:') !!}
    <p>{{ $job->business_service_id }}</p>
</div>

<!-- Resort Id Field -->
<div class="form-group">
    {!! Form::label('resort_id', 'Resort Id:') !!}
    <p>{{ $job->resort_id }}</p>
</div>

<!-- Job Request Field -->
<div class="form-group">
    {!! Form::label('job_request', 'Job Request:') !!}
    <p>{{ $job->job_request }}</p>
</div>

