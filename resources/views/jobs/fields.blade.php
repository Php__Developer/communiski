<!-- Business Service Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('business_service_id', 'Business Service Id:') !!}
    {!! Form::number('business_service_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Resort Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('resort_id', 'Resort Id:') !!}
    {!! Form::number('resort_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Job Request Field -->
<div class="form-group col-sm-6">
    {!! Form::label('job_request', 'Job Request:') !!}
    {!! Form::number('job_request', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('jobs.index') }}" class="btn btn-default">Cancel</a>
</div>
