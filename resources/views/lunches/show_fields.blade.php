<!-- Business Service Id Field -->
<div class="form-group">
    {!! Form::label('business_service_id', 'Business Service Id:') !!}
    <p>{{ $lunch->business_service_id }}</p>
</div>

<!-- Request Status Field -->
<div class="form-group">
    {!! Form::label('request_status', 'Request Status:') !!}
    <p>{{ $lunch->request_status }}</p>
</div>

<!-- Active Status Field -->
<div class="form-group">
    {!! Form::label('active_status', 'Active Status:') !!}
    <p>{{ $lunch->active_status }}</p>
</div>

