<!-- Business Service Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('business_service_id', 'Business Service Id:') !!}
    {!! Form::number('business_service_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Request Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('request_status', 'Request Status:') !!}
    {!! Form::text('request_status', null, ['class' => 'form-control']) !!}
</div>

<!-- Active Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('active_status', 'Active Status:') !!}
    {!! Form::text('active_status', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('lunches.index') }}" class="btn btn-default">Cancel</a>
</div>
