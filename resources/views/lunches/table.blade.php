<div class="table-responsive">
    <table class="table" id="lunches-table">
        <thead>
            <tr>
                <th scope="col">#ID</th>
                <th>Business Location/Detail</th>
                <th>Lunch Status</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
            <?php $i = ($lunches->currentPage() - 1) * $lunches->perPage() + 1; ?>
            @foreach($lunches as $lunch)
            <tr>
                <td scope="row">{!! $i++ !!}</td>
                <td>{{ $lunch->business_name }}</td>
                <td>
                    @if ($lunch->request_status == 2)
                    <button class="btn btn-danger">Requested</button>
                    @elseif ($lunch->request_status == 3)
                    <button class="btn btn-info">Provisional</button>
                    @elseif ($lunch->request_status == 4)
                    <button class="btn btn-success">Live</button>
                    @endif
                </td>
                <td>
                    <div class='btn-group'>
                        <button type="button" class="btn btn-info" aria-pressed="false">
                            <a href="{{ route('provisional', $lunch->id) }}">Provisional</a>
                        </button>
                    </div>

                    <div class='btn-group'>
                        <button type="button" class="btn btn-success" aria-pressed="false">
                            <a href="{{ route('live', $lunch->id) }}">Live</a>
                        </button>
                    </div>

                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
        <div class="col-5">
            <div class="float-right">
                {!! $lunches->render() !!}
            </div>
        </div>
</div>

<style type="text/css">
    a {
    color: #ffffff;
}
</style>
