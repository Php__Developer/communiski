<!-- Event Name Field -->
<div class="form-group">
    {!! Form::label('event_name', 'Event Name:') !!}
    <p>{{ $event->event_name }}</p>
</div>

<!-- Start Date Field -->
<div class="form-group">
    {!! Form::label('start_date', 'Start Date:') !!}
    <p>{{ $event->start_date }}</p>
</div>

<!-- End Date Field -->
<div class="form-group">
    {!! Form::label('end_date', 'End Date:') !!}
    <p>{{ $event->end_date }}</p>
</div>

<!-- Event Location Field -->
<div class="form-group">
    {!! Form::label('event_location', 'Event Location:') !!}
    <p>{{ $event->event_location }}</p>
</div>

<!-- Event Description Field -->
<div class="form-group">
    {!! Form::label('event_description', 'Event Description:') !!}
    <p>{{ $event->event_description }}</p>
</div>

<!-- Tags Field -->
<div class="form-group">
    {!! Form::label('tags', 'Tags:') !!}
    <p>{{ $event->tags }}</p>
</div>

<!-- Event Image Field -->
<div class="form-group">
    {!! Form::label('event_image', 'Event Image:') !!}
    <p>{{ $event->event_image }}</p>
</div>

<!-- Event Host Id Field -->
<div class="form-group">
    {!! Form::label('event_host_id', 'Event Host Id:') !!}
    <p>{{ $event->event_host_id }}</p>
</div>

<!-- Event Link Field -->
<div class="form-group">
    {!! Form::label('event_link', 'Event Link:') !!}
    <p>{{ $event->event_link }}</p>
</div>

<!-- Event Contact Email Field -->
<div class="form-group">
    {!! Form::label('event_contact_email', 'Event Contact Email:') !!}
    <p>{{ $event->event_contact_email }}</p>
</div>

<!-- Event Contact Number Field -->
<div class="form-group">
    {!! Form::label('event_contact_number', 'Event Contact Number:') !!}
    <p>{{ $event->event_contact_number }}</p>
</div>

<!-- Event Price Field -->
<div class="form-group">
    {!! Form::label('event_price', 'Event Price:') !!}
    <p>{{ $event->event_price }}</p>
</div>

<!-- Declaration Field -->
<div class="form-group">
    {!! Form::label('declaration', 'Declaration:') !!}
    <p>{{ $event->declaration }}</p>
</div>

