<!-- Event Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('event_name', 'Event Name:') !!}
    {!! Form::text('event_name', null, ['class' => 'form-control']) !!}
</div>

<!-- Start Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('start_date', 'Start Date:') !!}
    {!! Form::date('start_date', null, ['class' => 'form-control','id'=>'start_date']) !!}
</div>

@push('scripts')
    <script type="text/javascript">
        $('#start_date').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endpush

<!-- End Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('end_date', 'End Date:') !!}
    {!! Form::date('end_date', null, ['class' => 'form-control','id'=>'end_date']) !!}
</div>

@push('scripts')
    <script type="text/javascript">
        $('#end_date').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endpush

<!-- Event Location Field -->
<div class="form-group col-sm-6">
    {!! Form::label('event_location', 'Event Location:') !!}
    {!! Form::text('event_location', null, ['class' => 'form-control']) !!}
</div>

<!-- Event Description Field -->
<div class="form-group col-sm-6">
    {!! Form::label('event_description', 'Event Description:') !!}
    {!! Form::text('event_description', null, ['class' => 'form-control']) !!}
</div>

<!-- Tags Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tags', 'Tags:') !!}
    {!! Form::text('tags', null, ['class' => 'form-control']) !!}
</div>

<!-- Event Image Field -->
<div class="form-group col-sm-6">
    {!! Form::label('event_image', 'Event Image:') !!}
    {!! Form::text('event_image', null, ['class' => 'form-control']) !!}
</div>

<!-- Event Host Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('event_host_id', 'Event Host Id:') !!}
    {!! Form::number('event_host_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Event Link Field -->
<div class="form-group col-sm-6">
    {!! Form::label('event_link', 'Event Link:') !!}
    {!! Form::text('event_link', null, ['class' => 'form-control']) !!}
</div>

<!-- Event Contact Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('event_contact_email', 'Event Contact Email:') !!}
    {!! Form::text('event_contact_email', null, ['class' => 'form-control']) !!}
</div>

<!-- Event Contact Number Field -->
<div class="form-group col-sm-6">
    {!! Form::label('event_contact_number', 'Event Contact Number:') !!}
    {!! Form::text('event_contact_number', null, ['class' => 'form-control']) !!}
</div>

<!-- Event Price Field -->
<div class="form-group col-sm-6">
    {!! Form::label('event_price', 'Event Price:') !!}
    {!! Form::number('event_price', null, ['class' => 'form-control']) !!}
</div>

<!-- Declaration Field -->
<div class="form-group col-sm-6">
    {!! Form::label('declaration', 'Declaration:') !!}
    {!! Form::text('declaration', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('events.index') }}" class="btn btn-default">Cancel</a>
</div>
