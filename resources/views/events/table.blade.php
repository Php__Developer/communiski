<div class="table-responsive">
    <table class="table" id="events-table">
        <thead>
            <tr>
                <th scope="col">#ID</th>
                <th>Event Name</th>
        <th>Start Date</th>
        <th>End Date</th>
        <th>Location</th>
        <th>Description</th>
        <!-- <th>Tags</th> -->
        <th>Image</th>
        <!-- <th>Event Host Id</th> -->
        <!-- <th>URL</th> -->
        <!-- <th>Email</th> -->
        <!-- <th>Number</th> -->
        <!-- <th>Price</th> -->
        <!-- <th>Declaration</th> -->
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
            <?php $i = ($events->currentPage() - 1) * $events->perPage() + 1; ?>
        @foreach($events as $event)
            <tr>
                <td scope="row">{!! $i++ !!}</td>
            <td>{{ $event->event_name }}</td>
            <td>{{ $event->start_date }}</td>
            <td>{{ $event->end_date }}</td>
            <td>{{ $event->event_location }}</td>
            <td>{{ $event->event_description }}</td>
            <!-- <td>{{ $event->tags }}</td> -->
            <td>
                <img src="{!! url('/images/'. $event->event_image) !!}" width="72" height="76">
            </td>
            <!-- <td>{{ $event->event_host_id }}</td> -->
            <!-- <td>{{ $event->event_link }}</td> -->
            <!-- <td>{{ $event->event_contact_email }}</td> -->
            <!-- <td>{{ $event->event_contact_number }}</td> -->
            <!-- <td>{{ $event->event_price }}</td> -->
            <!-- <td>{{ $event->declaration }}</td> -->
                <td>
                    {!! Form::open(['route' => ['events.destroy', $event->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('events.show', [$event->id]) }}" class='btn btn-default btn-md'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <!--<a href="{{ route('events.edit', [$event->id]) }}" class='btn btn-default btn-md'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-md', 'onclick' => "return confirm('Are you sure?')"]) !!}-->
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
        <div class="col-5">
            <div class="float-right">
                {!! $events->render() !!}
            </div>
        </div>
</div>
