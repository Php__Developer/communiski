@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Guide
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($guide, ['route' => ['guides.update', $guide->id], 'method' => 'patch']) !!}

                        @include('guides.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection