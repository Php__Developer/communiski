<div class="table-responsive">
    <table class="table" id="guides-table">
        <thead>
            <tr>
                <th scope="col">#ID</th>
                <th>Guide Title</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
            <?php $i = ($guides->currentPage() - 1) * $guides->perPage() + 1; ?>
        @foreach($guides as $guide)
            <tr>
                <td scope="row">{!! $i++ !!}</td>
                <td>{!! $guide->guide_title !!}</td>
                <td>
                    {!! Form::open(['route' => ['guides.destroy', $guide->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('guides.show', [$guide->id]) !!}" class='btn btn-default btn-md'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{!! route('guides.edit', [$guide->id]) !!}" class='btn btn-default btn-md'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-md', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
        <div class="col-5">
            <div class="float-right">
                {!! $guides->render() !!}
            </div>
        </div>
</div>
