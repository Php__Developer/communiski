@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Weather
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($weather, ['route' => ['weathers.update', $weather->id], 'method' => 'patch']) !!}

                        @include('weathers.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection