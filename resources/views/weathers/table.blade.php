<div class="table-responsive">
    <table class="table" id="weathers-table">
        <thead>
            <tr>
                <th scope="col">#ID</th>
                <th>Resort</th>
                <th>Report Date</th>
                <th>New Snow Top</th>
                <th>New Snow Bottom</th>
                <th>Base Depth Top</th>
                <th>Base Depth Bottom</th>
                <th>Unit</th>
                <th>On Piste</th>
                <th>Off Piste</th>
                <th>Created At</th>
                <!--<th colspan="3">Action</th>-->
            </tr>
        </thead>
        <tbody>
            <?php $i = ($weathers->currentPage() - 1) * $weathers->perPage() + 1; ?>
        @foreach($weathers as $weather)
            <tr>
                <td scope="row">{!! $i++ !!}</td>
                <td>{{ $weather->resort_name }}</td>
                <td>{{ $weather->report_date->format('d/m/Y') }}</td>
                <td>{{ $weather->new_snow_top }}</td>
                <td>{{ $weather->new_snow_bottom }}</td>
                <td>{{ $weather->base_depth_top }}</td>
                <td>{{ $weather->base_depth_bottom }}</td>
                <td>{{ $weather->unit }}</td>
                <td>{{ $weather->on_piste }}</td>
                <td>{{ $weather->off_piste }}</td>
                <td>{{ $weather->created_at->format('d/m/Y') }}</td>
                <td>
                    {!! Form::open(['route' => ['weathers.destroy', $weather->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <!--<a href="{{ route('weathers.show', [$weather->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{{ route('weathers.edit', [$weather->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}-->
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
        <div class="col-5">
            <div class="float-right">
                {!! $weathers->render() !!}
            </div>
        </div>
</div>
