<!-- Resort Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('resort_id', 'Resort Id:') !!}
    {!! Form::number('resort_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Report Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('report_date', 'Report Date:') !!}
    {!! Form::number('report_date', null, ['class' => 'form-control']) !!}
</div>

<!-- New Snow Top Field -->
<div class="form-group col-sm-6">
    {!! Form::label('new_snow_top', 'New Snow Top:') !!}
    {!! Form::number('new_snow_top', null, ['class' => 'form-control']) !!}
</div>

<!-- New Snow Bottom Field -->
<div class="form-group col-sm-6">
    {!! Form::label('new_snow_bottom', 'New Snow Bottom:') !!}
    {!! Form::number('new_snow_bottom', null, ['class' => 'form-control']) !!}
</div>

<!-- Base Depth Top Field -->
<div class="form-group col-sm-6">
    {!! Form::label('base_depth_top', 'Base Depth Top:') !!}
    {!! Form::number('base_depth_top', null, ['class' => 'form-control']) !!}
</div>

<!-- Base Depth Bottom Field -->
<div class="form-group col-sm-6">
    {!! Form::label('base_depth_bottom', 'Base Depth Bottom:') !!}
    {!! Form::number('base_depth_bottom', null, ['class' => 'form-control']) !!}
</div>

<!-- On Piste Field -->
<div class="form-group col-sm-6">
    {!! Form::label('on_piste', 'On Piste:') !!}
    {!! Form::number('on_piste', null, ['class' => 'form-control']) !!}
</div>

<!-- Off Piste Field -->
<div class="form-group col-sm-6">
    {!! Form::label('off_piste', 'Off Piste:') !!}
    {!! Form::number('off_piste', null, ['class' => 'form-control']) !!}
</div>

<!-- Update At Field -->
<div class="form-group col-sm-6">
    {!! Form::label('update_at', 'Update At:') !!}
    {!! Form::date('update_at', null, ['class' => 'form-control','id'=>'update_at']) !!}
</div>

@push('scripts')
    <script type="text/javascript">
        $('#update_at').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endpush

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('weathers.index') }}" class="btn btn-default">Cancel</a>
</div>
