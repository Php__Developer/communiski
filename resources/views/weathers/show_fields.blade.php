<!-- Resort Id Field -->
<div class="form-group">
    {!! Form::label('resort_id', 'Resort Id:') !!}
    <p>{{ $weather->resort_id }}</p>
</div>

<!-- Report Date Field -->
<div class="form-group">
    {!! Form::label('report_date', 'Report Date:') !!}
    <p>{{ $weather->report_date }}</p>
</div>

<!-- New Snow Top Field -->
<div class="form-group">
    {!! Form::label('new_snow_top', 'New Snow Top:') !!}
    <p>{{ $weather->new_snow_top }}</p>
</div>

<!-- New Snow Bottom Field -->
<div class="form-group">
    {!! Form::label('new_snow_bottom', 'New Snow Bottom:') !!}
    <p>{{ $weather->new_snow_bottom }}</p>
</div>

<!-- Base Depth Top Field -->
<div class="form-group">
    {!! Form::label('base_depth_top', 'Base Depth Top:') !!}
    <p>{{ $weather->base_depth_top }}</p>
</div>

<!-- Base Depth Bottom Field -->
<div class="form-group">
    {!! Form::label('base_depth_bottom', 'Base Depth Bottom:') !!}
    <p>{{ $weather->base_depth_bottom }}</p>
</div>

<!-- On Piste Field -->
<div class="form-group">
    {!! Form::label('on_piste', 'On Piste:') !!}
    <p>{{ $weather->on_piste }}</p>
</div>

<!-- Off Piste Field -->
<div class="form-group">
    {!! Form::label('off_piste', 'Off Piste:') !!}
    <p>{{ $weather->off_piste }}</p>
</div>

<!-- Update At Field -->
<div class="form-group">
    {!! Form::label('update_at', 'Update At:') !!}
    <p>{{ $weather->update_at }}</p>
</div>

