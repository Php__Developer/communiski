<div class="table-responsive">
    <table class="table" id="runs-table">
        <thead>
            <tr>
                <th scope="col">#ID</th>
                <th>Country</th>
                <th>State</th>
                <th>Resort Location</th>
                <th>Run Name</th>
                <!--<th>Run Difficulty</th>
                <th>Run Length</th>
                <th>Measure Unit</th>
                <th>Max Slope</th>
                <th>Average Slope</th>
                <th>Direction Run Faces</th>-->
                <th>Accessed From</th>
                <th>Run Description</th>
                <th>Run Created By</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
            <?php $i = ($runs->currentPage() - 1) * $runs->perPage() + 1; ?>
            @foreach($runs as $run)
            <tr>
                <td scope="row">{!! $i++ !!}</td>
                <td>{!! $run->country_name !!}</td>
                <td>{!! $run->state_name !!}</td>  
                <td>{!! $run->resort_name !!}</td> 
                <td>{!! $run->run_name !!}</td>
                <!--<td>{!! $run->run_difficulty !!}</td>
                <td>{!! $run->run_length_value !!}</td>
                <td>{!! $run->measure_length_unit !!}</td>
                <td>{!! $run->max_slope !!}</td>
                <td>{!! $run->average_slope !!}</td>
                <td>{!! $run->direction_run_faces !!}</td>-->
                <td>{!! $run->accessed_from !!}</td>
                <td>{!! $run->run_description !!}</td>
                <td>{!! $run->user_name !!}</td>
                    <td>
                        {!! Form::open(['route' => ['runs.destroy', $run->id], 'method' => 'delete']) !!}
                        <div class='btn-group'>
                            <a href="{!! route('runs.show', [$run->id]) !!}" class='btn btn-default btn-md'><i class="glyphicon glyphicon-eye-open"></i></a>
                            <!--<a href="{!! route('runs.edit', [$run->id]) !!}" class='btn btn-default btn-md'><i class="glyphicon glyphicon-edit"></i></a>-->
                            {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-md', 'onclick' => "return confirm('Are you sure?')"]) !!}
                        </div>
                        {!! Form::close() !!}
                    </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <div class="col-5">
    <div class="float-right">
     {!! $runs->render() !!}
 </div>
</div>
</div>
