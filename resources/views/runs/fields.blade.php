<!-- Run Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('resort_id', 'Resort Name:') !!}
    {!! Form::text('resort_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Run Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('country_id', 'Country:') !!}
    {!! Form::text('country_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Run Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('state_id', 'State:') !!}
    {!! Form::text('state_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Run Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('run_name', 'Run Name:') !!}
    {!! Form::text('run_name', null, ['class' => 'form-control']) !!}
</div>

<!-- Run Difficulty Field -->
<div class="form-group col-sm-6">
    {!! Form::label('run_difficulty', 'Run Difficulty:') !!}
    {!! Form::text('run_difficulty', null, ['class' => 'form-control']) !!}
</div>

<!-- Run Length Field -->
<div class="form-group col-sm-6">
    {!! Form::label('run_length_value', 'Run Length:') !!}
    {!! Form::text('run_length_value', null, ['class' => 'form-control']) !!}
</div>

<!-- Measure Unit Field -->
<div class="form-group col-sm-6">
    {!! Form::label('measure_length_unit', 'Measure Unit:') !!}
    {!! Form::text('measure_length_unit', null, ['class' => 'form-control']) !!}
</div>

<!-- Max Slope Field -->
<div class="form-group col-sm-6">
    {!! Form::label('max_slope', 'Max Slope:') !!}
    {!! Form::text('max_slope', null, ['class' => 'form-control']) !!}
</div>

<!-- Average Slope Field -->
<div class="form-group col-sm-6">
    {!! Form::label('average_slope', 'Average Slope:') !!}
    {!! Form::text('average_slope', null, ['class' => 'form-control']) !!}
</div>

<!-- Direction Run Faces Field -->
<div class="form-group col-sm-6">
    {!! Form::label('direction_run_faces', 'Direction Run Faces:') !!}
    {!! Form::text('direction_run_faces', null, ['class' => 'form-control']) !!}
</div>

<!-- Accessed From Field -->
<div class="form-group col-sm-6">
    {!! Form::label('accessed_from', 'Accessed From:') !!}
    {!! Form::text('accessed_from', null, ['class' => 'form-control']) !!}
</div>

<!-- Run Description Field -->
<div class="form-group col-sm-6">
    {!! Form::label('run_description', 'Run Description:') !!}
    {!! Form::text('run_description', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('runs.index') !!}" class="btn btn-default">Cancel</a>
</div>
