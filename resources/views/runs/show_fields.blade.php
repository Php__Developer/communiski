<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $run->id !!}</p>
</div>

<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Resort:') !!}
    <p>{!! $run->resort_name !!}</p>
</div>

<!-- Id Field -->
<div class="form-group">
    {!! Form::label('country_id', 'Country:') !!}
    <p>{!! $run->country_name !!}</p>
</div>

<!-- Id Field -->
<div class="form-group">
    {!! Form::label('state_id', 'State:') !!}
    <p>{!! $run->state_name !!}</p>
</div>

<!-- Run Name Field -->
<div class="form-group">
    {!! Form::label('run_name', 'Run Name:') !!}
    <p>{!! $run->run_name !!}</p>
</div>

<!-- Run Name Field -->
<div class="form-group">
    {!! Form::label('run_number', 'Run Number:') !!}
    <p>{!! $run->run_number !!}</p>
</div>

<!-- Run Difficulty Field -->
<div class="form-group">
    {!! Form::label('run_difficulty', 'Run Difficulty:') !!}
    <p>{!! $run->run_difficulty !!}</p>
</div>

<!-- Run Length Field -->
<div class="form-group">
    {!! Form::label('run_length_value', 'Run Length:') !!}
    <p>{!! $run->run_length_value !!}</p>
</div>

<!-- Measure Unit Field -->
<div class="form-group">
    {!! Form::label('measure_length_unit', 'Measure Unit:') !!}
    <p>{!! $run->measure_length_unit !!}</p>
</div>

<!-- Max Slope Field -->
<div class="form-group">
    {!! Form::label('max_slope', 'Max Slope:') !!}
    <p>{!! $run->max_slope !!}</p>
</div>

<!-- Average Slope Field -->
<div class="form-group">
    {!! Form::label('average_slope', 'Average Slope:') !!}
    <p>{!! $run->average_slope !!}</p>
</div>

<!-- Direction Run Faces Field -->
<div class="form-group">
    {!! Form::label('direction_run_faces', 'Direction Run Faces:') !!}
    <p>{!! $run->direction_run_faces !!}</p>
</div>

<!-- Accessed From Field -->
<div class="form-group">
    {!! Form::label('accessed_from', 'Accessed From:') !!}
    <p>{!! $run->accessed_from !!}</p>
</div>

<!-- Run Description Field -->
<div class="form-group">
    {!! Form::label('run_description', 'Run Description:') !!}
    <p>{!! $run->run_description !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $run->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $run->updated_at !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $run->deleted_at !!}</p>
</div>

