<html>
<head>
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>Dynamically Add or Remove input fields in PHP with JQuery</title>  
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />  
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>  
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
</head>
      <body>
        <div class="container">
          <br />
          <br /> 
          <h2 align="center">Add</h2>
          <div>
              <select>
                  <option>Choose Color</option>
                  <option value="red">Red</option>
              </select>
          </div>
          <div class="form-group">
            <form name="add_name" id="add_name">
              <div class="table-responsive">
                <div class="box">
                <table class="table table-bordered" id="dynamic_field">
                  <tr>
                    <td>
                      <label for="male">1st value</label>
                      <input type="text" name="name1" placeholder="1st value" class="form-control name_list" />
                    </td>
                    <td>
                      <label for="male">Multiple value</label>
                      <input type="text" name="name[]" placeholder="multiple values" class="form-control name_list" />
                    </td>
                    <td>
                      <button type="button" name="add" id="add" class="btn btn-success">+ Add More</button>
                    </td>
                  </tr>
                </table>
              </div>
                <input type="button" name="submit" id="submit" class="btn btn-info" value="Submit" />  
              </div>
            </form>
          </div>
        </div>  
      </body>  
 </html>
<script type="text/javascript">
$(document).ready(function(){
    $("select").change(function(){
        $(this).find("option:selected").each(function(){
            var optionValue = $(this).attr("value");
            if(optionValue){
                $(".box").not("." + optionValue).hide();
                $("." + optionValue).show();
            } else{
                $(".box").hide();
            }
        });
    }).change();
});
</script>
 <script>  
  $(document).ready(function(){
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });

    var i=1;  
    $('#add').click(function(){
      i++;
      $('#dynamic_field').append('<tr id="row'+i+'"><td><input type="text" name="name[]" placeholder="Enter your Name" class="form-control name_list" /></td><td><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove">X</button></td></tr>');
    });
    $(document).on('click', '.btn_remove', function(){
      var button_id = $(this).attr("id");
      $('#row'+button_id+'').remove();
    });
    $('#submit').click(function(){
      $.ajax({
        url:"/insert",
        method:"POST",
        data:$('#add_name').serialize(),
        success:function(data)
        {
          console.log(data);
          $('#add_name')[0].reset();
        }
      });  
    });  
 });  
 </script>



<!-- Routes -->
<!--
Route::get('test','RunController@test');
Route::post('insert','RunController@insert');

-->
<!-- Controller -->
<!--
public function test(Request $request)
    {
        return view('runs.test');
    }

    public function insert(Request $request)
    {
        $name = $request->input('name1');
        $items = $request->input('name');
        $item1 = implode(",",$items);
        if (empty($item1)) {
            $allitem = 0;
        }
        else{
            $allitem = $item1;
        }
        if (empty($name)) {
            $nam = 0;
        }
        else{
            $nam = $name;
        }
        $cartsave = array(
                'name' => $nam,
                'item1' => $allitem
            );
            DB::table('test')->insert($cartsave);
            if ($cartsave) {
                echo "saved";
            }
            else{
                echo "error";
            }
    }
-->