@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Run
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($run, ['route' => ['runs.update', $run->id], 'method' => 'patch']) !!}

                        @include('runs.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection