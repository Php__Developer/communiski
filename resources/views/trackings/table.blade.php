<div class="table-responsive">
    <table class="table" id="trackings-table">
        <thead>
            <tr>
                <th>Resort Id</th>
        <th>Track Id</th>
        <th>User Id</th>
        <th>Date</th>
        <th>Current Track</th>
        <th>Future Track</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($trackings as $tracking)
            <tr>
                <td>{{ $tracking->resort_id }}</td>
            <td>{{ $tracking->track_id }}</td>
            <td>{{ $tracking->user_id }}</td>
            <td>{{ $tracking->date }}</td>
            <td>{{ $tracking->current_track }}</td>
            <td>{{ $tracking->future_track }}</td>
                <td>
                    {!! Form::open(['route' => ['trackings.destroy', $tracking->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('trackings.show', [$tracking->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{{ route('trackings.edit', [$tracking->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
