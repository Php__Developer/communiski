<!-- Resort Id Field -->
<div class="form-group">
    {!! Form::label('resort_id', 'Resort Id:') !!}
    <p>{{ $tracking->resort_id }}</p>
</div>

<!-- Track Id Field -->
<div class="form-group">
    {!! Form::label('track_id', 'Track Id:') !!}
    <p>{{ $tracking->track_id }}</p>
</div>

<!-- User Id Field -->
<div class="form-group">
    {!! Form::label('user_id', 'User Id:') !!}
    <p>{{ $tracking->user_id }}</p>
</div>

<!-- Date Field -->
<div class="form-group">
    {!! Form::label('date', 'Date:') !!}
    <p>{{ $tracking->date }}</p>
</div>

<!-- Current Track Field -->
<div class="form-group">
    {!! Form::label('current_track', 'Current Track:') !!}
    <p>{{ $tracking->current_track }}</p>
</div>

<!-- Future Track Field -->
<div class="form-group">
    {!! Form::label('future_track', 'Future Track:') !!}
    <p>{{ $tracking->future_track }}</p>
</div>

