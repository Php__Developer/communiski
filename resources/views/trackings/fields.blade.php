<!-- Resort Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('resort_id', 'Resort Id:') !!}
    {!! Form::number('resort_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Track Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('track_id', 'Track Id:') !!}
    {!! Form::text('track_id', null, ['class' => 'form-control']) !!}
</div>

<!-- User Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('user_id', 'User Id:') !!}
    {!! Form::number('user_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('date', 'Date:') !!}
    {!! Form::date('date', null, ['class' => 'form-control','id'=>'date']) !!}
</div>

@push('scripts')
    <script type="text/javascript">
        $('#date').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endpush

<!-- Current Track Field -->
<div class="form-group col-sm-6">
    {!! Form::label('current_track', 'Current Track:') !!}
    {!! Form::text('current_track', null, ['class' => 'form-control']) !!}
</div>

<!-- Future Track Field -->
<div class="form-group col-sm-6">
    {!! Form::label('future_track', 'Future Track:') !!}
    {!! Form::text('future_track', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('trackings.index') }}" class="btn btn-default">Cancel</a>
</div>
