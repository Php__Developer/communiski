<!-- Owner Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('added_by', 'Owner Id:') !!}
    {!! Form::number('added_by', null, ['class' => 'form-control']) !!}
</div>

<!-- Business Type Field -->
<div class="form-group col-sm-6">
    {!! Form::label('business_type', 'Business Type:') !!}
    {!! Form::text('business_type', null, ['class' => 'form-control']) !!}
</div>

<!-- Business Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('business_address', 'Business Name:') !!}
    {!! Form::text('business_address', null, ['class' => 'form-control']) !!}
</div>

<!-- Cuisines Services Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cuisines_services', 'Cuisines Services:') !!}
    {!! Form::text('cuisines_services', null, ['class' => 'form-control']) !!}
</div>

<!-- Price Star Rating Field -->
<div class="form-group col-sm-6">
    {!! Form::label('price_star_rating', 'Price Star Rating:') !!}
    {!! Form::text('price_star_rating', null, ['class' => 'form-control']) !!}
</div>

<!-- Facilities Activities Field -->
<div class="form-group col-sm-6">
    {!! Form::label('facilities_activities', 'Facilities Activities:') !!}
    {!! Form::text('facilities_activities', null, ['class' => 'form-control']) !!}
</div>

<!-- Service Image Field -->
<div class="form-group col-sm-6">
    {!! Form::label('service_image', 'Service Image:') !!}
    {!! Form::text('service_image', null, ['class' => 'form-control']) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('description', 'Description:') !!}
    {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
</div>

<!-- Website Field -->
<div class="form-group col-sm-6">
    {!! Form::label('website', 'Website:') !!}
    {!! Form::text('website', null, ['class' => 'form-control']) !!}
</div>

<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email', 'Email:') !!}
    {!! Form::email('email', null, ['class' => 'form-control']) !!}
</div>

<!-- Phone Number Field -->
<div class="form-group col-sm-6">
    {!! Form::label('phone_number', 'Phone Number:') !!}
    {!! Form::number('phone_number', null, ['class' => 'form-control']) !!}
</div>

<!-- Own Declaration Field -->
<div class="form-group col-sm-6">
    {!! Form::label('own_work', 'Own Declaration:') !!}
    {!! Form::text('own_work', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('businesses.index') }}" class="btn btn-default">Cancel</a>
</div>
