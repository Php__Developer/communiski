<!-- Owner Id Field -->
<div class="form-group">
    {!! Form::label('owner_id', 'Owner Id:') !!}
    <p>{{ $business->owner_id }}</p>
</div>

<!-- Business Type Field -->
<div class="form-group">
    {!! Form::label('business_type', 'Business Type:') !!}
    <p>{{ $business->business_type }}</p>
</div>

<!-- Business Name Field -->
<div class="form-group">
    {!! Form::label('business_address', 'Business Name:') !!}
    <p>{{ $business->business_address }}</p>
</div>

<!-- Cuisines Services Field -->
<div class="form-group">
    {!! Form::label('cuisines_services', 'Cuisines Services:') !!}
    <p>{{ $business->cuisines_services }}</p>
</div>

<!-- Price Star Rating Field -->
<div class="form-group">
    {!! Form::label('price_star_rating', 'Price Star Rating:') !!}
    <p>{{ $business->price_star_rating }}</p>
</div>

<!-- Facilities Activities Field -->
<div class="form-group">
    {!! Form::label('facilities_activities', 'Facilities Activities:') !!}
    <p>{{ $business->facilities_activities }}</p>
</div>

<!-- Service Image Field -->
<div class="form-group">
    {!! Form::label('service_image', 'Service Image:') !!}
    <p>{{ $business->service_image }}</p>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', 'Description:') !!}
    <p>{{ $business->description }}</p>
</div>

<!-- Website Field -->
<div class="form-group">
    {!! Form::label('website', 'Website:') !!}
    <p>{{ $business->website }}</p>
</div>

<!-- Email Field -->
<div class="form-group">
    {!! Form::label('email', 'Email:') !!}
    <p>{{ $business->email }}</p>
</div>

<!-- Phone Number Field -->
<div class="form-group">
    {!! Form::label('phone_number', 'Phone Number:') !!}
    <p>{{ $business->phone_number }}</p>
</div>

<!-- Own Declaration Field -->
<div class="form-group">
    {!! Form::label('own_declaration', 'Own Declaration:') !!}
    <p>{{ $business->own_declaration }}</p>
</div>

