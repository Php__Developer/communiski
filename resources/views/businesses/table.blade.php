<div class="table-responsive">
    <table class="table" id="businesses-table">
        <thead>
            <tr>
                <th scope="col">#ID</th>
                <th>Owner</th>
                <th>Business Type</th>
                <th>Code</th>
                <th>Business Name/Location</th>
                <th>Picture</th>
                <th>Phone Number</th>
                <!-- <th>Own Declaration</th> -->
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
            <?php $i = ($businesses->currentPage() - 1) * $businesses->perPage() + 1; ?>
        @foreach($businesses as $business)
            <tr>
                <td scope="row">{!! $i++ !!}</td>
                <td>{{ $business->user_name }}</td>
                <td>{{ $business->business_type }}</td>
                <td>{{ $business->business_code }}</td>
                <td>{{ $business->business_address }}</td>
                <td><a href="http://93.188.167.68/projects/communiski/public/service_images/<?= $business->service_image; ?>" target="_blank"><img src="{!! url('/service_images/'. $business->service_image) !!}" width="72" height="76"></a></td>
            <td>{{ $business->phone_number }}</td>
                <td>
                    {!! Form::open(['route' => ['businesses.destroy', $business->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('businesses.show', [$business->id]) }}" class='btn btn-default btn-md'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <!-- <a href="{{ route('businesses.edit', [$business->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!} -->
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <div class="col-5">
            <div class="float-right">
                {!! $businesses->render() !!}
            </div>
        </div>
</div>
