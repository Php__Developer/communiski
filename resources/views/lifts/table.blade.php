<div class="table-responsive">
    <table class="table" id="lifts-table">
        <thead>
            <tr>
                <th scope="col">#ID</th>
                <th>Country</th>
                <th>State</th>
                <th>Resort Location</th>
                <th>Lift Creator</th>
                <th>Lift Number</th>
                <!--<th>Lift Name</th>
                <th>Description</th>
                <th>Lift Type</th>
                <th>Capacity</th>
                <th>Manufacturer</th>
                <th>Installed Year</th>
                <th>Hourly Capacity</th>
                <th>Base Elevation</th>
                <th>Top Elevation</th>
                <th>Vertical Rise</th>-->
                <th>Length</th>
                <th>Ride Time</th>
                <th>Opening Time</th>
                <th>Closing Time</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
            <?php $i = ($lifts->currentPage() - 1) * $lifts->perPage() + 1; ?>
            @foreach($lifts as $lift)
                <tr>
                    <td scope="row">{!! $i++ !!}</td>
                    <td>{!! $lift->country_name !!}</td>
                    <td>{!! $lift->state_name !!}</td>
                    <td>{!! $lift->resort_name !!}</td>
                    <td>{!! $lift->user_name !!}</td>
                    <td>{!! $lift->lift_number !!}</td>
                    <!--<td>{!! $lift->lift_name !!}</td>
                    <td>{!! $lift->description !!}</td>
                    <td>{!! $lift->lift_type !!}</td>
                    <td>{!! $lift->capacity !!}</td>
                    <td>{!! $lift->user_name !!}</td>
                    <td>{!! $lift->installed_year !!}</td>
                    <td>{!! $lift->hourly_capacity !!}</td>
                    <td>{!! $lift->base_elevation !!}</td>
                    <td>{!! $lift->top_elevation !!}</td>
                    <td>{!! $lift->vertical_rise !!}</td>-->
                    <td>{!! $lift->length !!}</td>
                    <td>{!! $lift->ride_time !!}</td>
                    <td>{!! $lift->opening_time !!}</td>
                    <td>{!! $lift->closing_time !!}</td>
                        <td>
                            {!! Form::open(['route' => ['lifts.destroy', $lift->id], 'method' => 'delete']) !!}
                            <div class='btn-group'>
                                <a href="{!! route('lifts.show', [$lift->id]) !!}" class='btn btn-default btn-md'><i class="glyphicon glyphicon-eye-open"></i></a>
                                <!--<a href="{!! route('lifts.edit', [$lift->id]) !!}" class='btn btn-default btn-md'><i class="glyphicon glyphicon-edit"></i></a>-->
                                {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-md', 'onclick' => "return confirm('Are you sure?')"]) !!}
                            </div>
                            {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <div class="col-5">
    <div class="float-right">
     {!! $lifts->render() !!}
 </div>
</div>
</div>
