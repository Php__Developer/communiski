<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $lift->id !!}</p>
</div>

<!-- Country Id Field -->
<div class="form-group">
    {!! Form::label('country_id', 'Country:') !!}
    <p>{!! $lift->country_name !!}</p>
</div>

<!-- State Id Field -->
<div class="form-group">
    {!! Form::label('state_id', 'State:') !!}
    <p>{!! $lift->state_name !!}</p>
</div>


<!-- Resort Id Field -->
<div class="form-group">
    {!! Form::label('resort_id', 'Resort:') !!}
    <p>{!! $lift->resort_name !!}</p>
</div>


<!-- User Id Field -->
<div class="form-group">
    {!! Form::label('user_id', 'User Id:') !!}
    <p>{!! $lift->user_name !!}</p>
</div>

<!-- Lift Name Field -->
<div class="form-group">
    {!! Form::label('lift_name', 'Lift Name:') !!}
    <p>{!! $lift->lift_name !!}</p>
</div>

<!-- Lift Number Field -->
<div class="form-group">
    {!! Form::label('lift_number', 'Lift Number:') !!}
    <p>{!! $lift->lift_number !!}</p>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', 'Description:') !!}
    <p>{!! $lift->description !!}</p>
</div>

<!-- Lift Type Field -->
<div class="form-group">
    {!! Form::label('lift_type', 'Lift Type:') !!}
    <p>{!! $lift->lift_type !!}</p>
</div>

<!-- Capacity Field -->
<div class="form-group">
    {!! Form::label('capacity', 'Capacity:') !!}
    <p>{!! $lift->capacity !!}</p>
</div>

<!-- Manufacturer Field -->
<div class="form-group">
    {!! Form::label('manufacturer', 'Manufacturer:') !!}
    <p>{!! $lift->manufacturer !!}</p>
</div>

<!-- Installed Year Field -->
<div class="form-group">
    {!! Form::label('installed_year', 'Installed Year:') !!}
    <p>{!! $lift->installed_year !!}</p>
</div>

<!-- Hourly Capacity Field -->
<div class="form-group">
    {!! Form::label('hourly_capacity', 'Hourly Capacity:') !!}
    <p>{!! $lift->hourly_capacity !!}</p>
</div>

<!-- Base Elevation Field -->
<div class="form-group">
    {!! Form::label('base_elevation', 'Base Elevation:') !!}
    <p>{!! $lift->base_elevation !!}</p>
</div>

<!-- Top Elevation Field -->
<div class="form-group">
    {!! Form::label('top_elevation', 'Top Elevation:') !!}
    <p>{!! $lift->top_elevation !!}</p>
</div>

<!-- Vertical Rise Field -->
<div class="form-group">
    {!! Form::label('vertical_rise', 'Vertical Rise:') !!}
    <p>{!! $lift->vertical_rise !!}</p>
</div>

<!-- Length Field -->
<div class="form-group">
    {!! Form::label('length', 'Length:') !!}
    <p>{!! $lift->length !!}</p>
</div>

<!-- Ride Time Field -->
<div class="form-group">
    {!! Form::label('ride_time', 'Ride Time:') !!}
    <p>{!! $lift->ride_time !!}</p>
</div>

<!-- Opening Time Field -->
<div class="form-group">
    {!! Form::label('opening_time', 'Opening Time:') !!}
    <p>{!! $lift->opening_time !!}</p>
</div>

<!-- Closing Time Field -->
<div class="form-group">
    {!! Form::label('closing_time', 'Closing Time:') !!}
    <p>{!! $lift->closing_time !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $lift->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $lift->updated_at !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $lift->deleted_at !!}</p>
</div>

