<!-- User Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('user_id', 'User Id:') !!}
    {!! Form::text('user_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Lift Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('lift_name', 'Lift Name:') !!}
    {!! Form::text('lift_name', null, ['class' => 'form-control']) !!}
</div>

<!-- Lift Number Field -->
<div class="form-group col-sm-6">
    {!! Form::label('lift_number', 'Lift Number:') !!}
    {!! Form::text('lift_number', null, ['class' => 'form-control']) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-6">
    {!! Form::label('description', 'Description:') !!}
    {!! Form::text('description', null, ['class' => 'form-control']) !!}
</div>

<!-- Lift Type Field -->
<div class="form-group col-sm-6">
    {!! Form::label('lift_type', 'Lift Type:') !!}
    {!! Form::text('lift_type', null, ['class' => 'form-control']) !!}
</div>

<!-- Capacity Field -->
<div class="form-group col-sm-6">
    {!! Form::label('capacity', 'Capacity:') !!}
    {!! Form::text('capacity', null, ['class' => 'form-control']) !!}
</div>

<!-- Manufacturer Field -->
<div class="form-group col-sm-6">
    {!! Form::label('manufacturer', 'Manufacturer:') !!}
    {!! Form::text('manufacturer', null, ['class' => 'form-control']) !!}
</div>

<!-- Installed Year Field -->
<div class="form-group col-sm-6">
    {!! Form::label('installed_year', 'Installed Year:') !!}
    {!! Form::date('installed_year', null, ['class' => 'form-control','id'=>'installed_year']) !!}
</div>

@section('scripts')
    <script type="text/javascript">
        $('#installed_year').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endsection

<!-- Hourly Capacity Field -->
<div class="form-group col-sm-6">
    {!! Form::label('hourly_capacity', 'Hourly Capacity:') !!}
    {!! Form::text('hourly_capacity', null, ['class' => 'form-control']) !!}
</div>

<!-- Base Elevation Field -->
<div class="form-group col-sm-6">
    {!! Form::label('base_elevation', 'Base Elevation:') !!}
    {!! Form::text('base_elevation', null, ['class' => 'form-control']) !!}
</div>

<!-- Top Elevation Field -->
<div class="form-group col-sm-6">
    {!! Form::label('top_elevation', 'Top Elevation:') !!}
    {!! Form::text('top_elevation', null, ['class' => 'form-control']) !!}
</div>

<!-- Vertical Rise Field -->
<div class="form-group col-sm-6">
    {!! Form::label('vertical_rise', 'Vertical Rise:') !!}
    {!! Form::text('vertical_rise', null, ['class' => 'form-control']) !!}
</div>

<!-- Length Field -->
<div class="form-group col-sm-6">
    {!! Form::label('length', 'Length:') !!}
    {!! Form::text('length', null, ['class' => 'form-control']) !!}
</div>

<!-- Ride Time Field -->
<div class="form-group col-sm-6">
    {!! Form::label('ride_time', 'Ride Time:') !!}
    {!! Form::text('ride_time', null, ['class' => 'form-control']) !!}
</div>

<!-- Opening Time Field -->
<div class="form-group col-sm-6">
    {!! Form::label('opening_time', 'Opening Time:') !!}
    {!! Form::text('opening_time', null, ['class' => 'form-control']) !!}
</div>

<!-- Closing Time Field -->
<div class="form-group col-sm-6">
    {!! Form::label('closing_time', 'Closing Time:') !!}
    {!! Form::text('closing_time', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('lifts.index') !!}" class="btn btn-default">Cancel</a>
</div>
