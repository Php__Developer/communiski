@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Lift
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('lifts.show_fields')
                    <a href="{!! route('lifts.index') !!}" class="btn btn-default">Back</a>
                </div>
            </div>
        </div>
    </div>
@endsection
