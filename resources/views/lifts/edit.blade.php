@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Lift
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($lift, ['route' => ['lifts.update', $lift->id], 'method' => 'patch']) !!}

                        @include('lifts.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection