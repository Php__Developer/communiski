<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBusinessesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('businesses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->biginteger('uder_id')->comment('added by');
            $table->string('name');
            $table->string('city');
            $table->string('state')->nullable();
            $table->string('country');
            $table->string('street');
            $table->string('unit_number')->nullable();
            $table->point('long_lat');
            $table->smallInteger('type_id');
            $table->tinyInteger('price_rating')->nullable();
            $table->tinyInteger('star_rating')->nullable();
            $table->string('image')->nullable();
            $table->string('description')->nullable();
            $table->string('website')->nullable();
            $table->string('email')->nullable();
            $table->string('phone_no')->nullable();
            $table->string('bcolor')->nullable();
            $table->string('fcolor')->nullable();
            $table->json('opening_hours')->nullable();
            $table->tinyInteger('features');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('businesses');
    }
}
