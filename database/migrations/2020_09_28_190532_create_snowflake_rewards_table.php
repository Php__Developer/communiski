<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSnowflakeRewardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('snowflake_rewards', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id');
            $table->bigInteger('user_edited_history_id')->nullable();
            $table->integer('amount'); 
            $table->enum('rewarded_spent', ['rewarded', 'spent']);
            $table->enum('status', ['pending', 'confirmed', 'rejected']);
            $table->string('reward_for');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('snowflake_rewards');
    }
}
