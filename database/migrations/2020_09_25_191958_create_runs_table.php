<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRunsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('runs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->smallInteger('resort_id');
            $table->string('status')->nullable();
            $table->string('name')->nullable();
            $table->string('number')->nullable();
            $table->string('difficulty');
            $table->string('accessed_from')->nullable();
            $table->integer('length')->nullable();
            $table->integer('max_slope')->nullable()->comment('in percentage');
            $table->integer('avg_slope')->nullable()->comment('in percentage');
            $table->string('facing')->nullable();
            $table->string('description');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('runs');
    }
}
