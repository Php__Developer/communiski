<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserEditedHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_edited_histories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id'); 
            $table->integer('resort_id')->nullable(); 
            $table->integer('run_id')->nullable();
            $table->integer('lift_id')->nullable();
            $table->integer('business_details_id')->nullable();
            $table->string('changed_column'); 
            $table->string('new_value');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_edited_histories');
    }
}
