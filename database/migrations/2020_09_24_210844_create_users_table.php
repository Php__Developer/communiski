<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('referral_code');
            $table->bigInteger('referrer_user_id');
            $table->string('DOB');
            $table->string('country');
            $table->string('units');
            $table->string('phone_no');
            $table->string('image')->default('default.jpg');
            $table->string('description')->nullable();
            $table->boolean('share_location')->default(0);
            $table->string('type')->default('U');
            $table->bigInteger('activity_id')->comment('the sport')->nullable();
            $table->integer('snowflakes')->default(0);
            $table->string('fav_resorts')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
