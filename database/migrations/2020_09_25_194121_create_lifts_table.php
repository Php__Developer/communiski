<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLiftsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lifts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->nullable();
            $table->string('number')->nulable();
            $table->string('description')->nullable();
            $table->string('type');
            $table->tinyInteger('capacity')->nullable();
            $table->smallInteger('hourly_capacity')->nullable();
            $table->string('manufacturer')->nullable();
            $table->integer('installed_year')->nullable()->comment('full year');
            $table->integer('base_elevation')->nullable();
            $table->integer('top_elevation')->nullable();
            $table->tinyInteger('vertical_rise')->comment('In percentage');
            $table->smallInteger('length')->nullable()->comment('in metres');
            $table->string('ride_time')->nullable();
            $table->time('opening_time')->nullable();
            $table->time('closing_time')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lifts');
    }
}
