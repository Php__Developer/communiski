<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->dateTime('start_date');
            $table->dateTime('end_date')->nullable();
            $table->string('location');
            $table->string('description');
            $table->point('long_lat')->nullable();
            $table->string('price')->nullable();
            $table->string('host_name')->nullable();
            $table->string('host_id')->nullable();
            $table->string('host_type')->nullable();
            $table->string('contact_number');
            $table->string('contact_email');
            $table->string('website');
            $table->string('image')->default('eventDefault.jpg');
            $table->string('tags');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
