<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResortsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('resorts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->point('long_lat');
            $table->point('toplong_toplat');
            $table->string('country');
            $table->string('state')->nullable();
            $table->string('image')->nullable();
            $table->string('website')->nullable();
            $table->string('phone_no')->nullable();
            $table->string('patrol_no')->nullable();
            $table->string('run_grades')->nullable();
            $table->string('bcolor')->nullable();
            $table->string('fcolor')->nullable();
            $table->enum('lift_names', ['yes', 'no']);
            $table->enum('lift_numbers', ['yes', 'no']);
            $table->enum('run_names', ['yes', 'no']);
            $table->enum('run_numbers', ['yes', 'no']);
            $table->string('vertical_rise', 100)->nullable();
            $table->string('top_elevation', 100)->nullable();
            $table->string('bottom_elevation', 100)->nullable();
            $table->string('skiable_area', 100)->nullable();
            $table->smallInteger('runs')->nullable();
            $table->smallInteger('lifts')->nullable();
            $table->string('longest_run')->nullable();
            $table->smallInteger('terrain_parks')->nullable();
            $table->smallInteger('half_pipes')->nullable();
            $table->smallInteger('annual_snowfall')->nullable()->comment('in centimetres');
            $table->smallInteger('snow_making')->nullable()->comment('in square kilometres');
            $table->integer('uphill_capacity')->nullable();
            $table->smallInteger('terrain_diff_beginner')->nullable()->comment('in square kilometres');
            $table->smallInteger('terrain_diff_inter')->nullable()->comment('in square kilometres');
            $table->smallInteger('terrain_diff_advanced')->nullable()->comment('in square kilometres');
            $table->smallInteger('terrain_diff_expert')->nullable()->comment('in square kilometres');
            $table->string('opening_date')->nullable();
            $table->string('closing_date')->nullable();
            $table->smallInteger('current_weather_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('resorts');
    }
}
