<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserSnowReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_snow_reports', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id');
            $table->integer('report_id');
            $table->date('date');
            $table->string('new_snow_top')->nullable();
            $table->string('new_snow_bottom')->nullable();
            $table->string('base_depth_top')->nullable();
            $table->string('base_depth_bottom')->nullable();
            $table->string('on_piste')->nullable();
            $table->string('off_piste')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_snow_reports');
    }
}
