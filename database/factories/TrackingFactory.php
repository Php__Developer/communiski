<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Tracking;
use Faker\Generator as Faker;

$factory->define(Tracking::class, function (Faker $faker) {

    return [
        'resort_id' => $faker->randomDigitNotNull,
        'track_id' => $faker->word,
        'user_id' => $faker->randomDigitNotNull,
        'date' => $faker->word,
        'current_track' => $faker->word,
        'future_track' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
