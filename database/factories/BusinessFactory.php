<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Business;
use Faker\Generator as Faker;

$factory->define(Business::class, function (Faker $faker) {

    return [
        'owner_id' => $faker->randomDigitNotNull,
        'business_type' => $faker->word,
        'business_name' => $faker->word,
        'cuisines_services' => $faker->word,
        'price_star_rating' => $faker->word,
        'facilities_activities' => $faker->word,
        'service_image' => $faker->word,
        'description' => $faker->text,
        'website' => $faker->word,
        'email' => $faker->word,
        'phone_number' => $faker->randomDigitNotNull,
        'own_declaration' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
