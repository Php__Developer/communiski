<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Event;
use Faker\Generator as Faker;

$factory->define(Event::class, function (Faker $faker) {

    return [
        'event_name' => $faker->word,
        'start_date' => $faker->date('Y-m-d H:i:s'),
        'end_date' => $faker->date('Y-m-d H:i:s'),
        'event_location' => $faker->word,
        'event_description' => $faker->word,
        'tags' => $faker->word,
        'event_image' => $faker->word,
        'event_host_id' => $faker->randomDigitNotNull,
        'event_link' => $faker->word,
        'event_contact_email' => $faker->word,
        'event_contact_number' => $faker->word,
        'event_price' => $faker->randomDigitNotNull,
        'declaration' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
