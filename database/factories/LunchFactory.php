<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Lunch;
use Faker\Generator as Faker;

$factory->define(Lunch::class, function (Faker $faker) {

    return [
        'business_service_id' => $faker->randomDigitNotNull,
        'request_status' => $faker->word,
        'active_status' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
