<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Weather;
use Faker\Generator as Faker;

$factory->define(Weather::class, function (Faker $faker) {

    return [
        'resort_id' => $faker->randomDigitNotNull,
        'report_date' => $faker->randomDigitNotNull,
        'new_snow_top' => $faker->randomDigitNotNull,
        'new_snow_bottom' => $faker->randomDigitNotNull,
        'base_depth_top' => $faker->randomDigitNotNull,
        'base_depth_bottom' => $faker->randomDigitNotNull,
        'on_piste' => $faker->randomDigitNotNull,
        'off_piste' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'update_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
