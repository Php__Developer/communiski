<h2><strong>Terms and Conditions</strong></h2>

<p>Updated 29 July 2019</p>
<p>Welcome to Communiski!</p>

<p>These terms and conditions outline the rules and regulations for the use of CommuniSki's Website, located at <b><a href="http://www.CommuniSki.com">CommuniSki</a></b> and CommuniSki’s App</p>

<p>By accessing this website or installing our apps we assume you accept these terms and conditions. Do not continue to use CommuniSki if you do not agree to take all of the terms and conditions stated on this page. </a>.</p>

<p>The following terminology applies to these Terms and Conditions: "User", "You" and "Your" refers to you, the person using our Services. "The Company", "Ourselves", "We", "Our" and "Us", refers to CommuniSki. "Party", "Parties", or "Us", refers to both the User and ourselves.“Services” refers to any product or service offered by Us, including our websites and mobile applications (“Apps”). Any use of the above terminology or other words in the singular, plural, capitalization and/or he/she or they, are taken as interchangeable and therefore as referring to same.</p>

<h3><strong>License</strong></h3>

<p>Unless otherwise stated, CommuniSki and/or its licensors own the intellectual property rights for all material on CommuniSki. All intellectual property rights are reserved. You may access this from CommuniSki for your own personal use subjected to restrictions set in these terms and conditions.</p>

<p>You must not:</p>
<ul>
    <li>Republish material from Communiski</li>
    <li>Sell, rent or sub-license material from Communiski</li>
    <li>Reproduce, duplicate or copy material from Communiski</li>
    <li>Redistribute content from Communiski</li>
    <li>Reverse engineer our Services</li>
    <li>Knowingly or negligently use our Services in a way that abuses or disrupts our networks, user accounts or the Services</li>
    <li>Engage in any activity that stops other users from enjoying or using our Services</li>
    <li>Use the Services in violation of any applicable laws or regulations</li>
    <li>Use the Services to send unauthorised advertising or spam</li>
    <li>Harvest, collect or gather user data without their consent</li>
</ul>

<p>Our Servicesallow users to post and exchange opinions and information, including but not limited to text, links, graphics, photos, videos and other materials, (“Content”), including Content created with, or submitted to the services by you or through your account (“Your Content”).</p>

<p>CommuniSki does not filter, edit, publish or review Content prior to its presence on the Services. Content does not reflect the views and opinions of CommuniSki,its agents and/or affiliates. Content reflects the views and opinions of the person who post their views and opinions. To the extent permitted by applicable laws, CommuniSki shall not be liable for the Content or for any liability, damages or expenses caused and/or suffered as a result of any use of and/or posting of and/or appearance of the Content on this website.</p>

<p>You warrant and represent that:</p>

<ul>
    <li>You are entitled to post the Content on our website and have all necessary licenses and consents to do so;</li>
    <li>The Content do not invade any intellectual property right, including without limitation copyright, patent or trademark of any third party;</li>
    <li>The Content do not contain any defamatory, libellous, offensive, indecent or otherwise unlawful material which is an invasion of privacy;</li>
    <li>The Content will not be used to solicit or promote business or custom or present commercial activities, except for areas of CommuniSki explicitly designed for the purpose of listing businesses (e.g. our restaurant / shopping / services directories);</li>
    <li>The Content will not be used to solicit or promote unlawful activity.</li>
</ul>

<p>CommuniSki reserves the right to monitor all Content and to remove any Content which can be considered inappropriate, offensive or causes breach of these Terms and Conditions.</p>

<p>You retain any ownership rights you have in Your Content, but you hereby grant CommuniSki aworldwide, perpetual,royalty free, non-exclusive, transferable and sublicensable license to use, reproduce, modify, adapt, prepare derivative works from, distribute, and displayYour Content in any and all forms, formats or media. This license includes the right for Us to make Your Content available to third parties under the same terms. You also agree that we may remove any metadata associated with Your Content and you irrevocably waive any claims or assertions of moral rights or attribution with respect to Your Content.</p>

<p>Any ideas, suggestions or feedback about CommuniSki or our Services that you provide are entirely voluntary and you agree that we may use such ideas, suggestions and feedback without.</p>

<h3><strong>Limitation of Liability</strong></h3>

<p>YOU AGREE THAT COMMUNISKI SHALL IN NO EVENT BE LIABLE FOR ANY CONSEQUENTIONAL, INCIDENTAL, INDIRECT, SPECIAL, PUNITIVE OR OTHER LOSS OR DAMAGE WHATSOEVER OR FOR LOSS OF BUSINESS PROFITS, BUSINESS INTERRUPTION, COMPUTER FAILURE, LOSS OF BUSINESS INFORMATION OR OTHER LOSS ARISING OUT OF OR CAUSED BY YOUR USE OF OR INABILITY TO USE THE SERVICES, EVEN IF COMMUNISKI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. IN NO EVENT SHALL COMMUNISKI’S ENTIRE LIABILITY TO YOU IN ANY RESPECT OF ANY SERVICE, WHETHER DIRECT OR INDIRECT, EXCEED THE FEES PAID BY YOU FOR SUCH SERVICE.</p>

<h3><strong>Termination of Accounts</strong></h3>

<p>If you fail, or we suspect that you have failed, to comply with the provisions of this Agreement, CommuniSki may, without notice to you, (i) terminate this Agreement; and/or (ii) delete your account; and/or (iii) restrict your access to the Services.</p>

<p>CommuniSki reserves the right to modify, suspend or discontinue the Services (or any part or Content thereof) at any time, with or without notice and will not be liable to you or any third party in the event these rights are exercised.</p>

<p>These organizations may link to our home page, to publications or to other Website information so long as the link: (a) is not in any way deceptive; (b) does not falsely imply sponsorship, endorsement or approval of the linking party and its products and/or services; and (c) fits within the context of the linking party’s site.</p>

<h3><strong>Governing Law and Jurisdiction</strong></h3>

<p>These Terms and Conditions are governed by the laws of England and Wales and all parties submitto the non-exclusive jurisdiction of the English courts.</p>