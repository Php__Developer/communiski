<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;

class Event_attendee extends Pivot
{
    protected $table = "event_attendees";
    public $incrementing = true;
}
