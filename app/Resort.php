<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Resort extends Model
{
    protected $table = "resorts";
    use SoftDeletes;
    
    public function groups()
    {
        return $this->hasMany('App\Group');
    }
    public function runs()
    {
        return $this->hasMany('App\Run');
    }
    public function lifts()
    {
        return $this->hasMany('App\Lift');
    }
    public function weather()
    {
        return $this->hasOne('App\Weather');
    }
    public function user_snow_reports()
    {
        return $this->hasMany('App\User_snow_report');
    }
}
