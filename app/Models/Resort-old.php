<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Resort
 * @package App\Models
 * @version August 23, 2019, 11:18 am UTC
 *
 * @property integer id
 * @property string resort_name
 * @property string country
 * @property string state
 * @property string image
 */
class Resort extends Model
{
    use SoftDeletes;

    public $table = 'resorts';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'resort_name',
        // 'address',
        'country',
        'state',
        'image',
        'lift_names',
        'lift_numbers',
        'run_names',
        'favourite',
        'run_numbers',
        // 'latitude',
        // 'longitude',
        // 'top_latitude',
        // 'top_longitude'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'resort_name' => 'string',
        // 'address' => 'string',
        'country' => 'string',
        'state' => 'string',
        'image' => 'string',
        'lift_names' => 'string',
        'lift_numbers' => 'string',
        'run_names' => 'string',
        'favourite' => 'string',
        'run_numbers' => 'string',
        // 'latitude' => 'string',
        // 'longitude' => 'string',
        // 'top_latitude' => 'string',
        // 'top_longitude' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'resort_name' => 'required',
        // 'address' => 'required',
        'country' => 'required',
        'state' => 'required',
        //'image' => 'required',
        'lift_names' => 'required',
        'lift_numbers' => 'required',
        'run_names' => 'required',
        //'favourite' => 'required',
        'run_numbers' => 'required',
        // 'latitude' => 'required',
        // 'longitude' => 'required',
        // 'top_latitude' => 'required',
        // 'top_longitude' => 'required'
    ];

}
