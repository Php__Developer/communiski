<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Snowflake
 * @package App\Models
 * @version August 21, 2019, 6:21 am UTC
 *
 * @property string user_id
 * @property string snowflake_rewards
 * @property string reason
 * @property string reward_status
 */
class Access_code extends Model
{
    use SoftDeletes;

    public $table = 'access_codes';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'resort_id',
        'resort_administrator',
        'resort_editor',
        'patroller',
        'lifty',
        'ambassador'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'resort_id' => 'string',
        'resort_administrator' => 'string',
        'resort_editor' => 'string',
        'patroller' => 'string',
        'lifty' => 'string',
        'ambassador' => 'string'

    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'id' => 'required',
        'resort_id' => 'required',
        'resort_administrator' => 'required',
        'resort_editor' => 'required',
        'patroller' => 'required',
        'lifty' => 'required',
        'ambassador' => 'required',
        'created_at' => 'required'
    ];

    
}