<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Business
 * @package App\Models
 * @version April 9, 2020, 6:58 am UTC
 *
 * @property integer owner_id
 * @property string business_type
 * @property string business_name
 * @property string cuisines_services
 * @property string price_star_rating
 * @property string facilities_activities
 * @property string service_image
 * @property string description
 * @property string website
 * @property string email
 * @property integer phone_number
 * @property string own_declaration
 */
class Business_address extends Model
{
    use SoftDeletes;

    public $table = 'business_addresses';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'street_address1',
        'street_address2',
        'town_city',
        'state',
        'country'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'street_address1' => 'string',
        'street_address2' => 'string',
        'town_city  ' => 'string',
        'state' => 'string',
        'country' => 'string'
    ];
}
