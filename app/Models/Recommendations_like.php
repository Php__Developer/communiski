<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Guide
 * @package App\Models
 * @version November 25, 2019, 7:37 am UTC
 *
 * @property string guide_title
 */
class Recommendations_like extends Model
{
    use SoftDeletes;

    public $table = 'recommendation_likes';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'user_id',
        'resort_id',
        'recommendation_id',
        'event_type',
        'like_for',
        'like_for_value',
        'is_like'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'user_id' => 'integer',
        'resort_id' => 'integer',
        'recommendation_id' => 'integer',
        'event_type' => 'string',
        'like_for' => 'string',
        'like_for_value' => 'string',
        'is_like' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'user_id' => 'required',
        'resort_id' => 'required',
        'recommendation_id' => 'required',
        'event_type' => 'required',
        'like_for' => 'required',
        'like_for_value' => 'required',
        'is_like' => 'required',
        'created_at' => 'required'
    ];
    
}