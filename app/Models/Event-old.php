<?php

namespace App\Models;

use Eloquent as Model;
use DB;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Event
 * @package App\Models
 * @version April 6, 2020, 1:40 pm UTC
 *
 * @property string event_name
 * @property string|\Carbon\Carbon start_date
 * @property string|\Carbon\Carbon end_date
 * @property string event_location
 * @property string event_description
 * @property string tags
 * @property string event_image
 * @property integer event_host_id
 * @property string event_link
 * @property string event_contact_email
 * @property string event_contact_number
 * @property integer event_price
 * @property string declaration
 */
class Event extends Model
{
    use SoftDeletes;

    public $table = 'events';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'event_name',
        'start_date',
        'end_date',
        'event_location',
        // 'resort_id',
        'event_description',
        'tags',
        'event_image',
        'event_host',
        'event_host_id',
        'event_link',
        'event_contact_email',
        'event_contact_number',
        'event_price',
        'declaration'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        // 'resort_id' => 'integer',
        'id' => 'integer',
        'event_name' => 'string',
        'start_date' => 'datetime',
        'end_date' => 'datetime',
        'event_location' => 'string',
        'event_description' => 'string',
        'tags' => 'string',
        'event_image' => 'string',
        'event_host' => 'string',
        'event_host_id' => 'integer',
        'event_link' => 'string',
        'event_contact_email' => 'string',
        'event_contact_number' => 'string',
        'event_price' => 'string',
        'declaration' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'event_name' => 'required',
        // 'resort_id' => 'required',
        'start_date' => 'required',
        'end_date' => 'required',
        'event_location' => 'required',
        // 'event_host' => 'required',
        // 'event_description' => 'required',
        // 'tags' => 'required',
        // 'event_image' => 'required',
        // 'event_host_id' => 'required',
        // 'event_link' => 'required',
        // 'event_contact_email' => 'required',
        // 'event_contact_number' => 'required',
        // 'event_price' => 'required',
        // 'declaration' => 'required',
        'created_at' => 'required'
    ];


    public static function getByDistance($lat, $lng, $radius = 5)
    {
      $results = DB::select(DB::raw('SELECT *, ( 3959 * acos( cos( radians(' . $lat . ') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians(' . $lng . ') ) + sin( radians(' . $lat .') ) * sin( radians(latitude) ) ) ) AS distance FROM events HAVING distance < ' . $radius . ' ORDER BY distance') );

      return $results;
    }

    
}
