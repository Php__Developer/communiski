<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Snowflake
 * @package App\Models
 * @version August 21, 2019, 6:21 am UTC
 *
 * @property string user_id
 * @property string snowflake_rewards
 * @property string reason
 * @property string reward_status
 */
class Lunch_order_detail extends Model
{
    use SoftDeletes;

    public $table = 'business_lunch_order_details';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'orderID',
        'child_users',
        'parent_user',
        'main',
        'snack',
        'drink',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'orderID' => 'integer',
        'child_users' => 'string',
        'parent_user' => 'string',
        'main' => 'string',
        'snack' => 'string',
        'drink' => 'string',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'orderID' => 'required',
        'user_id' => 'required',
        'ordering_status' => 'required',
        'main' => 'required',
        'snack' => 'required',
        'drink' => 'required'
    ];

}