<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Lift
 * @package App\Models
 * @version August 30, 2019, 10:36 am UTC
 *
 * @property string user_id
 * @property string lift_name
 * @property string lift_number
 * @property string description
 * @property string lift_type
 * @property string capacity
 * @property string manufacturer
 * @property string installed_year
 * @property string hourly_capacity
 * @property string base_elevation
 * @property string top_elevation
 * @property string vertical_rise
 * @property string length
 * @property time ride_time
 * @property time opening_time
 * @property time closing_time
 */
class Lift extends Model
{
    use SoftDeletes;

    public $table = 'lifts';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'country_id',
        'state_id',
        'state_id',
        'user_id',
        'lift_name',
        'lift_number',
        'description',
        'lift_type',
        'capacity',
        'manufacturer',
        'installed_year',
        'hourly_capacity',
        'base_elevation',
        'top_elevation',
        'vertical_rise',
        'length',
        'ride_time',
        'opening_time',
        'closing_time'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'country_id' => 'string',
        'state_id' => 'string',
        'resort_id' => 'string',
        'user_id' => 'string',
        'lift_name' => 'string',
        'lift_number' => 'string',
        'description' => 'string',
        'lift_type' => 'string',
        'capacity' => 'string',
        'manufacturer' => 'string',
        //'installed_year' => 'year',
        'hourly_capacity' => 'string',
        'base_elevation' => 'string',
        'top_elevation' => 'string',
        'vertical_rise' => 'string',
        'length' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        //'id' => 'required',
        'country_id' => 'required',
        'state_id' => 'required',
        'resort_id' => 'required',
        'user_id' => 'required',
        'lift_name' => 'required',
        'lift_number' => 'required',
        'description' => 'required',
        'lift_type' => 'required',
        'capacity' => 'required',
        'manufacturer' => 'required',
        'installed_year' => 'required',
        'hourly_capacity' => 'required',
        'base_elevation' => 'required',
        'top_elevation' => 'required',
        'vertical_rise' => 'required',
        'length' => 'required',
        'ride_time' => 'required',
        'opening_time' => 'required',
        'closing_time' => 'required',
        'created_at' => 'required'
    ];

    
}
