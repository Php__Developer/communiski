<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Weather
 * @package App\Models
 * @version February 29, 2020, 8:23 am UTC
 *
 * @property integer resort_id
 * @property integer report_date
 * @property integer new_snow_top
 * @property integer new_snow_bottom
 * @property integer base_depth_top
 * @property integer base_depth_bottom
 * @property integer on_piste
 * @property integer off_piste
 * @property string|\Carbon\Carbon update_at
 */
class Weather_history extends Model
{
    use SoftDeletes;

    public $table = 'weather_history';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'official_report',
        'notes',
        'resort_id',
        'user_id',
        'report_date',
        'month',
        'year',
        'new_snow_top',
        'new_snow_bottom',
        'base_depth_top',
        'base_depth_bottom',
        'unit',
        'on_piste',
        'off_piste',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'official_report' => 'integer',
        'notes' => 'string',
        'resort_id' => 'integer',
        'user_id' => 'integer',
        'month' => 'integer',
        'year' => 'year',
        'report_date' => 'date',
        'new_snow_top' => 'integer',
        'new_snow_bottom' => 'integer',
        'base_depth_top' => 'integer',
        'base_depth_bottom' => 'integer',
        'unit' => 'string',
        'on_piste' => 'string',
        'off_piste' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'resort_id' => 'required',
        'official_report' => 'required',
        'user_id' => 'required',
        'month' => 'required',
        'year' => 'required',
        'report_date' => 'required',
        'new_snow_top' => 'required',
        'new_snow_bottom' => 'required',
        'base_depth_top' => 'required',
        'base_depth_bottom' => 'required',
        'unit' => 'required',
        'on_piste' => 'required',
        'off_piste' => 'required'
    ];

    
}
