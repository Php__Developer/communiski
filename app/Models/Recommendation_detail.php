<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Guide
 * @package App\Models
 * @version November 25, 2019, 7:37 am UTC
 *
 * @property string guide_title
 */
class Recommendation_detail extends Model
{
    use SoftDeletes;

    public $table = 'recommendation_details';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'resort_id',
        'recommendation_id',
        'user_id',
        'description'

    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'resort_id' => 'string',
        'recommendation_id' => 'string',
        'user_id' => 'string',
        'description' => 'string',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'resort_id' => 'required',
        'recommendation_id' => 'required',
        'user_id' => 'required',
        'description' => 'required'
    ];

    
}