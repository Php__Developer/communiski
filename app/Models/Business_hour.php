<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Business
 * @package App\Models
 * @version April 9, 2020, 6:58 am UTC
 *
 * @property integer owner_id
 * @property string business_type
 * @property string business_name
 * @property string cuisines_services
 * @property string price_star_rating
 * @property string facilities_activities
 * @property string service_image
 * @property string description
 * @property string website
 * @property string email
 * @property integer phone_number
 * @property string own_declaration
 */
class Business_hour extends Model
{
    use SoftDeletes;

    public $table = 'business_hours';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'business_service_id',
        'day',
        'open_time',
        'close_time'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'business_service_id' => 'integer',
        'day' => 'string',
        'open_time' => 'time',
        'close_time' => 'time'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'business_service_id' => 'required',
        'day' => 'required',
        'open_time' => 'required',
        'close_time' => 'required',
        'created_at' => 'required'
    ];

    
}
