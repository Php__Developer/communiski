<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class QA
 * @package App\Models
 * @version September 16, 2019, 10:47 am UTC
 *
 * @property string user_id
 * @property string question
 * @property string answer
 * @property string question_type
 * @property string topics
 * @property string archive_status
 */
class Fav_resort extends Model
{
    use SoftDeletes;

    public $table = 'fav_resorts';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'user_id',
        'resort_id',
        'favourite'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    
    protected $casts = [
        'id' => 'integer',
        'user_id' => 'integer',
        'resort_id' => 'integer',
        'favourite' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */

    public static $rules = [
        'user_id' => 'required',
        'resort_id' => 'required',
        'favourite' => 'required',
        'created_at' => 'required'
    ];
    
}