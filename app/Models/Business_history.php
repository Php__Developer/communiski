<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Business
 * @package App\Models
 * @version April 9, 2020, 6:58 am UTC
 *
 * @property integer owner_id
 * @property string business_type
 * @property string business_name
 * @property string cuisines_services
 * @property string price_star_rating
 * @property string facilities_activities
 * @property string service_image
 * @property string description
 * @property string website
 * @property string email
 * @property integer phone_number
 * @property string own_declaration
 */
class Business_history extends Model
{
    use SoftDeletes;

    public $table = 'business_services_history';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'business_service_id',
        'business_service_name',
        'user_id',
        'post_anonymously',
        'cuisines_services',
        'facilities_activities',
        'star_rating',
        'description',
        'website',
        'email',
        'phone_number',
        'note_for',
        'added_note'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'user_id' => 'string',
        'post_anonymously' => 'string',
        'business_service_id' => 'string',
        'business_service_name' => 'string',
        'cuisines_services' => 'string',
        'star_rating' => 'string',
        'facilities_activities' => 'string',
        'description' => 'string',
        'website' => 'string',
        'email' => 'string',
        'phone_number' => 'string',
        'added_note' => 'string',
        'note_for' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'resort_id' => 'required',
        'business_service_id' => 'required',
        'created_at' => 'required'
    ];

    
}
