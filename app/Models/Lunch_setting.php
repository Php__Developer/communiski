<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Snowflake
 * @package App\Models
 * @version August 21, 2019, 6:21 am UTC
 *
 * @property string user_id
 * @property string snowflake_rewards
 * @property string reason
 * @property string reward_status
 */
class Lunch_setting extends Model
{
    use SoftDeletes;

    public $table = 'business_lunch_details';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'lunch_id',
        'business_service_id',
        'resort_id',
        'state',
        'days_available',
        'order_cut_off_time',
        'pick_up_location',
        'pick_up_start_time',
        'pick_up_end_time',
        'phone',
        'instructions',
        'business_license',
        'business_insurance',
        'food_hygiene_certificate',
        'tax',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'lunch_id' => 'integer',
        'business_service_id' => 'integer',
        'resort_id' => 'integer',
        'state' => 'integer',

    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'id' => 'required',
        'lunch_id' => 'required',
        'business_service_id' => 'required',
        'resort_id' => 'required',
        'state' => 'required'
    ];

}