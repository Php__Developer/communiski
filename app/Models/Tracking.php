<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Tracking
 * @package App\Models
 * @version March 5, 2020, 9:39 am UTC
 *
 * @property integer resort_id
 * @property string track_id
 * @property integer user_id
 * @property string date
 * @property string current_track
 * @property string future_track
 */
class Tracking extends Model
{
    use SoftDeletes;

    public $table = 'tracking_details';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'resort_id',
        'track_id',
        'user_id',
        'date',
        'current_track',
        'future_track'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'resort_id' => 'integer',
        'track_id' => 'string',
        'user_id' => 'integer',
        'date' => 'string',
        'current_track' => 'string',
        'future_track' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'resort_id' => 'required',
        'user_id' => 'required',
        'date' => 'required',
        'current_track' => 'required',
        'future_track' => 'required',
        'created_at' => 'required'
    ];

    
}
