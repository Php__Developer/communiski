<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Business
 * @package App\Models
 * @version April 9, 2020, 6:58 am UTC
 *
 * @property integer owner_id
 * @property string business_type
 * @property string business_name
 * @property string cuisines_services
 * @property string price_star_rating
 * @property string facilities_activities
 * @property string service_image
 * @property string description
 * @property string website
 * @property string email
 * @property integer phone_number
 * @property string own_declaration
 */
class Business extends Model
{
    use SoftDeletes;

    public $table = 'business_services';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'added_by',
        'resort_id',
        'category',
        'business_type',
        'business_address',
        'cuisines_services',
        'price_star_rating',
        'facilities_activities',
        'service_image',
        'description',
        'website',
        'email',
        'phone_number',
        'own_work'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'added_by' => 'integer',
        'resort_id' => 'integer',
        'category' => 'string',
        'business_type' => 'string',
        'business_address' => 'string',
        'cuisines_services' => 'string',
        'price_star_rating' => 'string',
        'facilities_activities' => 'string',
        'service_image' => 'string',
        'description' => 'string',
        'website' => 'string',
        'email' => 'string',
        'phone_number' => 'string',
        'own_work' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'added_by' => 'required',
        'resort_id' => 'required',
        // 'business_type' => 'required',
        // 'business_name' => 'required',
        // 'cuisines_services' => 'required',
        // 'price_star_rating' => 'required',
        // 'facilities_activities' => 'required',
        // 'service_image' => 'required',
        // 'description' => 'required',
        'own_work' => 'required',
        'created_at' => 'required'
    ];

    
}
