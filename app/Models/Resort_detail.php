<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Snowflake
 * @package App\Models
 * @version August 21, 2019, 6:21 am UTC
 *
 * @property string user_id
 * @property string snowflake_rewards
 * @property string reason
 * @property string reward_status
 */
class Resort_detail extends Model
{
    use SoftDeletes;

    public $table = 'resort_details';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


   protected $dates = ['deleted_at'];


    public $fillable = [
        'resort_id',
        'resort_info',
        'skiable_area',
        'runs',
        'lifts',
        'longest_run',
        'terrain_parks',
        'half_pipes',
        'annual_snowfall',
        'snow_making',
        'uphill_capacity',
        'terrain_difficulty_beginner',
        'terrain_difficulty_intermediate',
        'terrain_difficulty_advance',
        'terrain_difficulty_expert',
        'opening_date',
        'closing_date'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'resort_id' => 'string',
        'resort_info' => 'string',
        'skiable_area' => 'string',
        'runs' => 'string',
        'lifts' => 'string',
        'longest_run' => 'string',
        'terrain_parks' => 'string',
        'half_pipes' => 'string',
        'annual_snowfall' => 'string',
        'snow_making' => 'string',
        'uphill_capacity' => 'string',
        'terrain_difficulty_beginner' => 'string',
        'terrain_difficulty_intermediate' => 'string',
        'terrain_difficulty_advance' => 'string',
        'terrain_difficulty_expert' => 'string',
        'opening_date' => 'string',
        'closing_date' => 'string'

    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        //'id' => 'required',
        'resort_id' => 'required',
        'resort_info' => 'required',
        'skiable_area' => 'required',
        'runs' => 'required',
        'lifts' => 'required',
        'longest_run' => 'required',
        'terrain_parks' => 'required',
        'half_pipes' => 'required',
        'annual_snowfall' => 'required',
        'snow_making' => 'required',
        'uphill_capacity' => 'required',
        'terrain_difficulty_beginner' => 'required',
        'terrain_difficulty_intermediate' => 'required',
        'terrain_difficulty_advance' => 'required',
        'terrain_difficulty_expert' => 'required',
        'opening_date' => 'required',
        'closing_date' => 'required'
    ];

    
}