<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Snowflake
 * @package App\Models
 * @version August 21, 2019, 6:21 am UTC
 *
 * @property string user_id
 * @property string snowflake_rewards
 * @property string reason
 * @property string reward_status
 */
class Rewards_point extends Model
{
    use SoftDeletes;

    public $table = 'rewards_points';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'user_id',
        'total'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'user_id' => 'string',
        'total' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'id' => 'required',
        'user_id' => 'required',
        'total' => 'required'
    ];

    
}