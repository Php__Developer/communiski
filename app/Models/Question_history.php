<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class QA
 * @package App\Models
 * @version September 16, 2019, 10:47 am UTC
 *
 * @property string user_id
 * @property string question
 * @property string answer
 * @property string question_type
 * @property string topics
 * @property string archive_status
 */
class Question_history extends Model
{
    use SoftDeletes;

    public $table = 'question_history';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'question_id',
        'answer_by_user_id',
        'answer',
        'confirmed_by_admin'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'question_id' => 'string',
        'answer_by_user_id' => 'string',
        'answer' => 'string',
        'confirmed_by_admin' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'question_id' => 'required',
        'answer_by_user_id' => 'required',
        'confirmed_by_admin' => 'required',
        'answer' => 'required',
        'created_at' => 'required'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
    
}