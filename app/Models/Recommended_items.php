<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Guide
 * @package App\Models
 * @version November 25, 2019, 7:37 am UTC
 *
 * @property string guide_title
 */
class Recommended_items extends Model
{
    use SoftDeletes;

    public $table = 'recommended_items';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'resort_id',
        'recommendation_id',
        'run_id',
        'user_id'

    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'resort_id' => 'string',
        'recommendation_id' => 'string',
        'user_id' => 'string',
        'run_id' => 'string',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'resort_id' => 'required',
        'recommendation_id' => 'required',
        'user_id' => 'required',
        'run_id' => 'required'
    ];
}