<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Job
 * @package App\Models
 * @version July 14, 2020, 9:53 am UTC
 *
 * @property integer business_service_id
 * @property integer resort_id
 * @property integer job_request
 */
class Resume_education extends Model
{
    use SoftDeletes;

    public $table = 'resume_educations';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'user_id',
        'qualification_title',
        'grade',
        'institution',
        'town_city',
        'country',
        'from_year',
        'to_year'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'user_id' => 'string',
        'qualification_title' => 'string',
        'institution' => 'string',
        'grade' => 'string',
        'town_city' => 'string',
        'country' => 'string',
        'from_year' => 'string',
        'to_year' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'created_at' => 'required'
    ];

    
}
