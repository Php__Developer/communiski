<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Job
 * @package App\Models
 * @version July 14, 2020, 9:53 am UTC
 *
 * @property integer business_service_id
 * @property integer resort_id
 * @property integer job_request
 */
class Resume extends Model
{
    use SoftDeletes;

    public $table = 'resumes';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'user_id',
        'name',
        'dob',
        'phone_no',
        'personal_statement',
        'qualification',
        'photo'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'user_id' => 'integer',
        'id' => 'integer',
        'name' => 'string',
        'dob' => 'date',
        'phone_no' => 'integer',
        'personal_statement' => 'string',
        'qualification' => 'string',
        'photo' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'created_at' => 'required'
    ];

    
}
