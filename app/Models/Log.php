<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Snowflake
 * @package App\Models
 * @version August 21, 2019, 6:21 am UTC
 *
 * @property string user_id
 * @property string snowflake_rewards
 * @property string reason
 * @property string reward_status
 */
class Log extends Model
{
    use SoftDeletes;

    public $table = 'email_logs';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'email',
        'subject',
        'content'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'email' => 'string',
        'subject' => 'string',
        'content' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'id' => 'required',
        'name' => 'required',
        'email' => 'required',
        'subject' => 'required',
        'content' => 'required'
    ];

    
}