<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Snowflake
 * @package App\Models
 * @version August 21, 2019, 6:21 am UTC
 *
 * @property string user_id
 * @property string snowflake_rewards
 * @property string reason
 * @property string reward_status
 */
class Lunch_item extends Model
{
    use SoftDeletes;

    public $table = 'business_lunch_items';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'lunch_id',
        'business_service_id',
        'resort_id',
        'state',
        'lunch_option_type',
        'item_status',
        'name',
        'item_picture',
        'item_description',
        'dietary_prefernces',
        'allergy_info',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'lunch_id' => 'integer',
        'business_service_id' => 'integer',
        'resort_id' => 'integer',
        'state' => 'integer',
        'name' => 'string',
        'item_picture' => 'string',
        'item_description' => 'string',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'id' => 'required',
        'lunch_id' => 'required',
        'business_service_id' => 'required',
        'resort_id' => 'required',
        'state' => 'required'
    ];

}