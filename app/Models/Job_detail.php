<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Job
 * @package App\Models
 * @version July 14, 2020, 9:53 am UTC
 *
 * @property integer business_service_id
 * @property integer resort_id
 * @property integer job_request
 */
class Job_detail extends Model
{
    use SoftDeletes;

    public $table = 'business_job_details';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'job_id',
        'job_title',
        'category',
        'description',
        'hours',
        'duration',
        'qualification',
        'benefits',
        'other_benefits',
        'advert_size',
        'pause_date',
        'applicant_limit',
        'save_later'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'job_id' => 'integer',
        'job_title' => 'string',
        'description' => 'string',
        'category' => 'string',
        'hours'=> 'string',
        'duration'=> 'string',
        'qualification'=> 'string',
        'benefits'=> 'string',
        'other_benefits'=> 'string',
        'advert_size'=> 'string',
        'pause_date'=> 'date',
        'applicant_limit'=> 'integer',
        'save_later'=> 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'job_id' => 'required',
        'job_title' => 'required',
        'category' => 'required',
        'description' => 'required',
        'hours'=> 'required',
        'duration'=> 'required',
        'qualification'=> 'required',
        'benefits'=> 'required',
        'other_benefits'=> 'required',
        'advert_size'=> 'required',
        'pause_date'=> 'required',
        'applicant_limit'=> 'required',
        'save_later'=> 'required',
        'created_at' => 'required'
    ];

    
}
