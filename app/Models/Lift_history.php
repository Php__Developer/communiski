<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Run
 * @package App\Models
 * @version August 30, 2019, 6:10 am UTC
 *
 * @property string run_name
 * @property string run_difficulty
 * @property string run_length
 * @property string measure_unit
 * @property string max_slope
 * @property string average_slope
 * @property string direction_run_faces
 * @property string accessed_from
 * @property string run_description
 */
class Lift_history extends Model
{
    use SoftDeletes;

    public $table = 'lift_history';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    public $fillable = [
        'lift_id',
        'post_anonymous',
        'user_id',
        'description',
        'manufacturer',
        'installed_year',
        'hourly_capacity',
        'base_elevation',
        'base_elevation_unit',
        'top_elevation',
        'top_elevation_unit',
        'vertical_rise',
        'vertical_rise_unit',
        'length',
        'length_unit',
        'ride_time',
        'opening_time',
        'closing_time'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'lift_id' => 'integer',
        'post_anonymous' => 'string',
        'user_id' => 'string',
        'description' => 'string',
        'manufacturer' => 'string',
        'installed_year' => 'string',
        'hourly_capacity' => 'string',
        'base_elevation' => 'string',
        'base_elevation_unit' => 'string',
        'top_elevation' => 'string',
        'top_elevation_unit' => 'string',
        'vertical_rise' => 'string',
        'vertical_rise_unit' => 'string',
        'length' => 'string',
        'length_unit' => 'string',
        'ride_time' => 'string',
        'opening_time' => 'string',
        'closing_time' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'lift_id' => 'required',
        'post_anonymous' => 'required',
        'user_id' => 'required',
        'description' => 'required',
        'manufacturer' => 'required',
        'installed_year' => 'required',
        'hourly_capacity' => 'required',
        'base_elevation' => 'required',
        'base_elevation_unit' => 'required',
        'top_elevation' => 'required',
        'top_elevation_unit' => 'required',
        'vertical_rise' => 'required',
        'vertical_rise_unit' => 'required',
        'length' => 'required',
        'length_unit' => 'required',
        'ride_time' => 'required',
        'opening_time' => 'required',
        'closing_time' => 'required'
    ];

    
}
