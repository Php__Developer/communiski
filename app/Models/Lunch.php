<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Lunch
 * @package App\Models
 * @version May 29, 2020, 9:58 am UTC
 *
 * @property integer business_service_id
 * @property string request_status
 * @property string active_status
 */
class Lunch extends Model
{
    use SoftDeletes;

    public $table = 'business_lunches';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'business_service_id',
        'request_status',
        'active_status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'business_service_id' => 'integer',
        'request_status' => 'string',
        'active_status' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'business_service_id' => 'required',
        'request_status' => 'required',
        'active_status' => 'required',
        'created_at' => 'required'
    ];

    
}
