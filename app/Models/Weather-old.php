<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Weather
 * @package App\Models
 * @version February 29, 2020, 8:23 am UTC
 *
 * @property integer resort_id
 * @property integer report_date
 * @property integer new_snow_top
 * @property integer new_snow_bottom
 * @property integer base_depth_top
 * @property integer base_depth_bottom
 * @property integer on_piste
 * @property integer off_piste
 * @property string|\Carbon\Carbon update_at
 */
class Weather extends Model
{
    use SoftDeletes;

    public $table = 'weather_reports';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $dates = ['deleted_at'];


    public $fillable = [
        'resort_id',
        'report_date',
        'new_snow_top',
        'new_snow_bottom',
        'base_depth_top',
        'base_depth_bottom',
        'unit',
        'on_piste',
        'off_piste',
        'update_at'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        //'id' => 'integer',
        'resort_id' => 'integer',
        'report_date' => 'date',
        'new_snow_top' => 'integer',
        'new_snow_bottom' => 'integer',
        'base_depth_top' => 'integer',
        'base_depth_bottom' => 'integer',
        'unit' => 'string',
        'on_piste' => 'string',
        'off_piste' => 'string',
        'update_at' => 'datetime'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'resort_id' => 'required',
        'report_date' => 'required',
        'new_snow_top' => 'required',
        'new_snow_bottom' => 'required',
        'base_depth_top' => 'required',
        'base_depth_bottom' => 'required',
        'unit' => 'required',
        'on_piste' => 'required',
        'off_piste' => 'required',
        'created_at' => 'required'
    ];

    
}
