<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Events
 * @package App\Models
 * @version April 2, 2020, 12:59 pm UTC
 *
 * @property string event_name
 * @property string|\Carbon\Carbon start_date
 * @property string|\Carbon\Carbon end_date
 * @property string event_description
 * @property string tags
 * @property integer event_host_id
 * @property string event_link
 * @property string event_contact_email
 * @property string event_contact_number
 * @property integer event_price
 * @property string declaration
 */
class Event_attendee extends Model
{
    use SoftDeletes;

    public $table = 'event_attendees';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'event_id',
        'user_id',
        'attend_status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'event_id' => 'integer',
        'user_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'event_id' => 'required',
        'user_id' => 'required',
        'attend_status' => 'required',
    ];

    
}
