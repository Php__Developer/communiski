<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class QA
 * @package App\Models
 * @version September 16, 2019, 10:47 am UTC
 *
 * @property string user_id
 * @property string question
 * @property string answer
 * @property string question_type
 * @property string topics
 * @property string archive_status
 */
class QA extends Model
{
    use SoftDeletes;

    public $table = 'question_answers';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'user_id',
        'question',
        'answer',
        'question_type',
        'topic',
        'archive_status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'user_id' => 'string',
        'question' => 'string',
        'answer' => 'string',
        'question_type' => 'string',
        'topic' => 'string',
        'archive_status' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        //'id' => 'required',
        'user_id' => 'required',
        'question' => 'required',
        'question_type' => 'required',
        'topic' => 'required',
        'archive_status' => 'required',
        'created_at' => 'required'
    ];

    
}
