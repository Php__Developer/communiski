<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Group
 * @package App\Models
 * @version July 4, 2019, 10:28 am UTC
 *
 * @property string group_name
 * @property string group_image
 * @property string group_owner_id
 * @property string total_users
 * @property string privacy_status
 */
class Group extends Model
{
    use SoftDeletes;

    public $table = 'groups';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'group_name',
        //'group_image',
        //'group_owner_id',
        'group_date',
        'group_info',
        'privacy_status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'group_id' => 'integer',
        'group_name' => 'string',
        'group_image' => 'string',
        //'group_owner_id' => 'string',
        'group_date' => 'string',
        'group_info' => 'string',
        'privacy_status' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        //'group_id' => 'required',
        'group_name' => 'required',
        //'group_image' => 'required',
        //'group_owner_id' => 'required',
        'group_date' => 'required',
        'group_info' => 'required',
        'privacy_status' => 'required',
        //'created_at' => 'required'
    ];

    
}
