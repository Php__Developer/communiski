<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Snowflake
 * @package App\Models
 * @version August 21, 2019, 6:21 am UTC
 *
 * @property string user_id
 * @property string snowflake_rewards
 * @property string reason
 * @property string reward_status
 */
class Lunch_order extends Model
{
    use SoftDeletes;

    public $table = 'business_lunch_orders';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'resort_id',
        'orderID',
        'business_service_id',
        'user_id',
        'order_date',
        'child_ids',
        'phone_no',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'resort_id' => 'integer',
        'orderID' => 'string',
        'business_service_id' => 'integer',
        'user_id' => 'integer',
        'order_date' => 'date',
        'child_ids' => 'string',
        'phone_no' => 'string',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'id' => 'required',
        'resort_id' => 'required',
        'orderID' => 'required',
        'business_service_id' => 'required',
        'user_id' => 'required',
        'order_date' => 'required',
        'child_ids' => 'required',
        'phone_no' => 'required'
    ];

}