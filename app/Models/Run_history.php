<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Run
 * @package App\Models
 * @version August 30, 2019, 6:10 am UTC
 *
 * @property string run_name
 * @property string run_difficulty
 * @property string run_length
 * @property string measure_unit
 * @property string max_slope
 * @property string average_slope
 * @property string direction_run_faces
 * @property string accessed_from
 * @property string run_description
 */
class Run_history extends Model
{
    use SoftDeletes;

    public $table = 'run_history';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    public $fillable = [
        'run_id',
        'user_id',
        'post_anonymous',
        'run_name',
        'run_difficulty',
        'run_length_value',
        'measure_length_unit',
        'max_slope',
        'average_slope',
        'direction_run_faces',
        'accessed_from',
        'run_description'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'run_id' => 'integer',
        'post_anonymous' => 'string',
        'user_id' => 'string',
        'run_name' => 'string',
        'run_difficulty' => 'string',
        'run_length_value' => 'string',
        'measure_length_unit' => 'string',
        'max_slope' => 'string',
        'average_slope' => 'string',
        'direction_run_faces' => 'string',
        'accessed_from' => 'string',
        'run_description' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'run_id' => 'required',
        'user_id' => 'required',
        'post_anonymous' => 'required',
        'run_difficulty' => 'required',
        'run_length_value' => 'required',
        'measure_length_unit' => 'required',
        'max_slope' => 'required',
        'average_slope' => 'required',
        'direction_run_faces' => 'required',
        'accessed_from' => 'required',
        'run_description' => 'required',
        'created_at' => 'required'
    ];

    
}
