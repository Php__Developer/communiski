<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Snowflake
 * @package App\Models
 * @version August 21, 2019, 6:21 am UTC
 *
 * @property string user_id
 * @property string snowflake_rewards
 * @property string reason
 * @property string reward_status
 */
class Permission extends Model
{
    use SoftDeletes;

    public $table = 'permissions';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'user_id',
        'resort_id',
        'role',
        'role_type'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'user_id' => 'string',
        'resort_id' => 'string',
        'role' => 'string',
        'role_type' => 'string'

    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        //'id' => 'required',
        'resort_id' => 'required',
        'user_id' => 'required',
        'role' => 'required',
        'role_type' => 'required'
    ];
    
}