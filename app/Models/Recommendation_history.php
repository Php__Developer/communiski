<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Guide
 * @package App\Models
 * @version November 25, 2019, 7:37 am UTC
 *
 * @property string guide_title
 */
class Recommendation_history extends Model
{
    use SoftDeletes;

    public $table = 'recommendation_history';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'post_anonymously',
        'added_note',
        'resort_id',
        'recommendation_id',
        'user_id',
        'description',
        'archive_unarchive'

    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'post_anonymously' => 'string',
        'added_note' => 'string',
        'resort_id' => 'string',
        'recommendation_id' => 'string',
        'user_id' => 'string',
        'description' => 'string',
        'archive_unarchive' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'resort_id' => 'required',
        'post_anonymously' => 'required',
        'recommendation_id' => 'required',
        'user_id' => 'required',
        'description' => 'required',
        'added_note' => 'required'
    ];

    
}