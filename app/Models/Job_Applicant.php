<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Job
 * @package App\Models
 * @version July 14, 2020, 9:53 am UTC
 *
 * @property integer business_service_id
 * @property integer resort_id
 * @property integer job_request
 */
class Job_Applicant extends Model
{
    use SoftDeletes;

    public $table = 'business_job_applicants';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'job_id',
        'user_id',
        'hiring_reason',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'job_id' => 'integer',
        'user_id' => 'integer',
        'status' => 'string',
        'hiring_reason' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'job_id' => 'required',
        // 'user_id' => 'required',
        // 'status' => 'required',
        'created_at' => 'required'
    ];

    
}
