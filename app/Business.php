<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Business extends Model
{
    protected $table = 'users';
    use SoftDeletes;

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function businessType()
    {
        return $this->belongsTo('App\Business_type');
    }

    public function businessDetails()
    {
        return $this->belongsToMany('App\Business_detail', 'business_detailed');
    }

}