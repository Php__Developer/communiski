<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Lift extends Model
{
    protected $table = "lifts";
    use SoftDeletes;

    public function resort()
    {
        return $this->belongsTo('App\Resort');
    }
}
