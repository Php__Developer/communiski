<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Weather extends Model
{
    protected $table = 'weathers';

    public function resort()
    {
        return $this->hasOne('App\Models\Resort');
    }
}
