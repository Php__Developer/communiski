<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Job_detail;
use Carbon\Carbon;

class ClosingUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'closing:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command is for check deadline of a job post after 3 months';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $jobStatus =[
            'job_status'=>'closed'
        ];
        $update = Job_detail::where('job_status','live')
        ->where('created_at', '<=', Carbon::now()->subDays(90)->toDateTimeString())->Update($jobStatus);
    }
}
