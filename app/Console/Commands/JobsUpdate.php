<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;

class JobsUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'daily:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'check closing status of each job and update status after due date';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::table('business_job_details')->whereDate('pause_date', now()->toDateString())
                ->update(['job_status' => 'pause']);
    }
}
