<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    protected $table = 'activities';

    public function users()
    {
        return $this->belongsTo('App\User');
    }
}
