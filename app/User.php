<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    protected $table = 'users';
    protected $with = 'accessCodes';

    use SoftDeletes;
    use Notifiable;

    public function activities()
    {
        return $this->hasMany('App\Activity');
    }

    public function devices()
    {
        return $this->hasMany('App\Device');
    }

    public function buddies()
    {
        return $this->hasMany('App\Buddy');
    }

    public function groups()
    {
        //defining a custom joining table
        return $this->belongsToMany('App\Group', 'group_members')->using('App\GroupMember')->orderBy('date', 'asc');
    }

    public function groupAdmin()
    {
        return $this->hasMany('\App\Group');
    }

    public function accessCodes()
    {
        //defining a custom name for the joining table
        return $this->belongsToMany('App\Access_code', 'permissions');
    }

    public function businesses()
    {
        return $this->hasMany('App\Business');
    }

    public function userSnowReports()
    {
        return $this->hasMany('App\User_snow_report');
    }

    public function events()
    {
        return $this->belongsToMany('App\Event')->using('App\Event_attendee');
    }

    public function lastKnownLocation()
    {
        return $this->hasOne('App\Last_known_location');
    }

    public function likes()
    {
        return $this->hasMany('App\Like');
    }

    public function flags()
    {
        return $this->hasMany('App\Flag');
    }


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'country',
        'phone_no',
        'DOB',
        'email_verified_at',
    ];

    /**
     * The attributes that should be guarded from HTML injection
     *
     * @var array
     */
    protected $guarded = [
        'password', 
        'remember_token', 
        'snowflakes', 
        'type', 
        'referral_code', 
        ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

}
