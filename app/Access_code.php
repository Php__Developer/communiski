<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Access_code extends Model
{
    protected $table = 'access_codes';
    use SoftDeletes;

    public function users()
    {
        //defining a custom name for the joining table
        return $this->belongsToMany('App\User', 'permissions');
    }

}
