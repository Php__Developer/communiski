<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Event extends Model
{
    protected $table = "events";
    use SoftDeletes;

    public function users()
    {
        return $this->belongsToMany('App\User')->using('App\Event_attendee');
    }
}
