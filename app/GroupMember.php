<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\Pivot;

class GroupMember extends Pivot
{
    protected $table = 'group_members';
    public $incrementing = true;

    public function user(){
        return $this->belongsTo('\App\User');
    }
    public function group(){
        return $this->belongsTo('\App\Group');
    }
    
}
