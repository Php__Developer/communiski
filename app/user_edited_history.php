<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class user_edited_history extends Model
{
    protected $table = 'user_edited_histories'; 
    use SoftDeletes;

    public function likes()
    {
        return $this->hasMany('App\Like');
    }
}
