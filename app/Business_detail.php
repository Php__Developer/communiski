<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Business_detail extends Model
{
    protected $table = 'business_details';
    use SoftDeletes;
    
    public function businesses()
    {
        return $this->belongsToMany('App\Business', 'business_detailed');
    }
}
