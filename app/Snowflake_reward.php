<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Snowflake_reward extends Model
{

    protected $table = 'snowflake_rewards'; 
    use SoftDeletes;
}
