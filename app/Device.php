<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Device extends Model
{
    use SoftDeletes;
    protected $table = 'devices';

    public function user(){
        return $this->belongsTo('\App\User');
    }

    protected $fillable = [
        'updated_at',
    ];
}
