<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Group extends Model
{
    use SoftDeletes;

    protected $table = 'groups';
    protected $with = 'resort:id,name';

    public function users()
    {
        //defining a custom joining table
        return $this->belongsToMany('App\User', 'group_members')->using('App\GroupMember');
    }
    public function resort()
    {
        return $this->belongsTo('App\Resort');
    }
    public function userAdmin()
    {
        return $this->belongsTo('App\User', 'user_id')->select('id', 'name', 'image');
    }

}
