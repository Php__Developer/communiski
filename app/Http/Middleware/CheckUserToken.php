<?php

namespace App\Http\Middleware;

use Closure;
use Crypt;

class CheckUserToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $existingUser = \App\User::where(['remember_token'=> Crypt::decrypt($request->header('token'))])->first();
        if($existingUser){
            $request->merge(compact('existingUser'));
            return $next($request);
        }else{
            return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }
}
