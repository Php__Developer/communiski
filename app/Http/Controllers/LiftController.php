<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateLiftRequest;
use App\Http\Requests\UpdateLiftRequest;
use App\Repositories\LiftRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use App\Models\Resort;
use App\Models\Lift;
use App\Models\Country;
use App\Models\State;
use Flash;
use Response;

class LiftController extends AppBaseController
{
    /** @var  LiftRepository */
    private $liftRepository;

    public function __construct(LiftRepository $liftRepo)
    {
        $this->liftRepository = $liftRepo;
    }

    /**
     * Display a listing of the Lift.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        //$lifts = $this->liftRepository->paginate(5);
        $lifts = Lift::join('resorts', 'resorts.id', '=', 'lifts.resort_id')
        ->join('states', 'states.id', '=', 'lifts.state_id')
        ->join('countries', 'countries.id', '=', 'lifts.country_id')
        ->join('users', 'users.id', '=', 'lifts.user_id')
        ->orderBy('id', 'DESC')
        ->select('resorts.resort_name as resort_name','countries.name as country_name','states.name as state_name','users.name as user_name','lifts.*')
        ->paginate(5);

        return view('lifts.index')
            ->with('lifts', $lifts);
    }

    /**
     * Show the form for creating a new Lift.
     *
     * @return Response
     */
    public function create()
    {
        return view('lifts.create');
    }

    /**
     * Store a newly created Lift in storage.
     *
     * @param CreateLiftRequest $request
     *
     * @return Response
     */
    public function store(CreateLiftRequest $request)
    {
        $input = $request->all();

        $lift = $this->liftRepository->create($input);

        Flash::success('Lift saved successfully.');

        return redirect(route('lifts.index'));
    }

    /**
     * Display the specified Lift.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        //$lift = $this->liftRepository->find($id);
        $lift = Lift::join('resorts', 'resorts.id', '=', 'lifts.resort_id')
        ->join('states', 'states.id', '=', 'lifts.state_id')
        ->join('countries', 'countries.id', '=', 'lifts.country_id')
        ->join('users', 'users.id', '=', 'lifts.user_id')
        ->where('lifts.id', '=', $id)
        ->orderBy('id', 'DESC')
        ->select('resorts.resort_name as resort_name','countries.name as country_name','states.name as state_name','users.name as user_name','lifts.*')
        ->first();

        if (empty($lift)) {
            Flash::error('Lift not found');

            return redirect(route('lifts.index'));
        }

        return view('lifts.show')->with('lift', $lift);
    }

    /**
     * Show the form for editing the specified Lift.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $lift = $this->liftRepository->find($id);

        if (empty($lift)) {
            Flash::error('Lift not found');

            return redirect(route('lifts.index'));
        }

        return view('lifts.edit')->with('lift', $lift);
    }

    /**
     * Update the specified Lift in storage.
     *
     * @param int $id
     * @param UpdateLiftRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateLiftRequest $request)
    {
        $lift = $this->liftRepository->find($id);

        if (empty($lift)) {
            Flash::error('Lift not found');

            return redirect(route('lifts.index'));
        }

        $lift = $this->liftRepository->update($request->all(), $id);

        Flash::success('Lift updated successfully.');

        return redirect(route('lifts.index'));
    }

    /**
     * Remove the specified Lift from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $lift = $this->liftRepository->find($id);

        if (empty($lift)) {
            Flash::error('Lift not found');

            return redirect(route('lifts.index'));
        }

        $this->liftRepository->delete($id);

        Flash::success('Lift deleted successfully.');

        return redirect(route('lifts.index'));
    }
}
