<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateLunchRequest;
use App\Http\Requests\UpdateLunchRequest;
use App\Repositories\LunchRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use App\Models\Lunch;
use Flash;
use Response;

class LunchController extends AppBaseController
{
    /** @var  LunchRepository */
    private $lunchRepository;

    public function __construct(LunchRepository $lunchRepo)
    {
        $this->lunchRepository = $lunchRepo;
    }

    /**
     * Display a listing of the Lunch.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $lunches = Lunch::join('business_services', 'business_services.id', '=', 'business_lunches.business_service_id')
        ->orderBy('id', 'ASC')
        ->select('business_services.business_address as business_name','business_lunches.*')
        ->paginate(5);
        return view('lunches.index')
            ->with('lunches', $lunches);
    }

    //update status on admin side

    public function provisional(Request $request,$id)
    {
        $checkId = Lunch::where('id',$id)->first();
        if ($checkId) {
            $updateData = [
                'request_status'=>'3',
                'updated_at'=>date('Y-m-d h:i:s'),
            ];
            $update = Lunch::where('id',$id)->update($updateData);
            Flash::success('Lunch request provisional successfully.');

            return redirect(route('lunches.index'));
        }
    }

    public function live(Request $request,$id)
    {
        $checkId = Lunch::where('id',$id)->first();
        if ($checkId) {
            $updateData = [
                'request_status'=>'4',
                'active_status'=>'1',
                'updated_at'=>date('Y-m-d h:i:s'),
            ];
            $update = Lunch::where('id',$id)->update($updateData);
            Flash::success('Lunch request live successfully.');

            return redirect(route('lunches.index'));
        }
    }

    /**
     * Show the form for creating a new Lunch.
     *
     * @return Response
     */

    public function create()
    {
        return view('lunches.create');
    }

    /**
     * Store a newly created Lunch in storage.
     *
     * @param CreateLunchRequest $request
     *
     * @return Response
     */

    public function store(CreateLunchRequest $request)
    {
        $input = $request->all();

        $lunch = $this->lunchRepository->create($input);

        Flash::success('Lunch saved successfully.');

        return redirect(route('lunches.index'));
    }

    /**
     * Display the specified Lunch.
     *
     * @param int $id
     *
     * @return Response
     */

    public function show($id)
    {
        $lunch = $this->lunchRepository->find($id);

        if (empty($lunch)) {
            Flash::error('Lunch not found');

            return redirect(route('lunches.index'));
        }

        return view('lunches.show')->with('lunch', $lunch);
    }

    /**
     * Show the form for editing the specified Lunch.
     *
     * @param int $id
     *
     * @return Response
     */

    public function edit($id)
    {
        $lunch = $this->lunchRepository->find($id);

        if (empty($lunch)) {
            Flash::error('Lunch not found');

            return redirect(route('lunches.index'));
        }

        return view('lunches.edit')->with('lunch', $lunch);
    }

    /**
     * Update the specified Lunch in storage.
     *
     * @param int $id
     * @param UpdateLunchRequest $request
     *
     * @return Response
     */

    public function update($id, UpdateLunchRequest $request)
    {
        $lunch = $this->lunchRepository->find($id);

        if (empty($lunch)) {
            Flash::error('Lunch not found');

            return redirect(route('lunches.index'));
        }

        $lunch = $this->lunchRepository->update($request->all(), $id);

        Flash::success('Lunch updated successfully.');

        return redirect(route('lunches.index'));
    }

    /**
     * Remove the specified Lunch from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    
    public function destroy($id)
    {
        $lunch = $this->lunchRepository->find($id);

        if (empty($lunch)) {
            Flash::error('Lunch not found');

            return redirect(route('lunches.index'));
        }

        $this->lunchRepository->delete($id);

        Flash::success('Lunch deleted successfully.');

        return redirect(route('lunches.index'));
    }
}
