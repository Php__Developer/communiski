<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateWeatherRequest;
use App\Http\Requests\UpdateWeatherRequest;
use App\Repositories\WeatherRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use App\Models\Weather;
use Flash;
use Response;

class WeatherController extends AppBaseController
{
    /** @var  WeatherRepository */
    private $weatherRepository;

    public function __construct(WeatherRepository $weatherRepo)
    {
        $this->weatherRepository = $weatherRepo;
    }

    /**
     * Display a listing of the Weather.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        // $weathers = $this->weatherRepository->all();
        $weathers = Weather::join('resorts', 'resorts.id', '=', 'weather_reports.resort_id')
        ->orderBy('id', 'DESC')
        ->select('resorts.resort_name as resort_name','weather_reports.*')
        ->paginate(15);

        // $weathers = Weather::orderBy('id', 'DESC')->paginate(10); 

        return view('weathers.index')
            ->with('weathers', $weathers);
    }

    /**
     * Show the form for creating a new Weather.
     *
     * @return Response
     */
    public function create()
    {
        return view('weathers.create');
    }

    /**
     * Store a newly created Weather in storage.
     *
     * @param CreateWeatherRequest $request
     *
     * @return Response
     */
    public function store(CreateWeatherRequest $request)
    {
        $input = $request->all();

        $weather = $this->weatherRepository->create($input);

        Flash::success('Weather saved successfully.');

        return redirect(route('weathers.index'));
    }

    /**
     * Display the specified Weather.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $weather = $this->weatherRepository->find($id);

        if (empty($weather)) {
            Flash::error('Weather not found');

            return redirect(route('weathers.index'));
        }

        return view('weathers.show')->with('weather', $weather);
    }

    /**
     * Show the form for editing the specified Weather.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $weather = $this->weatherRepository->find($id);

        if (empty($weather)) {
            Flash::error('Weather not found');

            return redirect(route('weathers.index'));
        }

        return view('weathers.edit')->with('weather', $weather);
    }

    /**
     * Update the specified Weather in storage.
     *
     * @param int $id
     * @param UpdateWeatherRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateWeatherRequest $request)
    {
        $weather = $this->weatherRepository->find($id);

        if (empty($weather)) {
            Flash::error('Weather not found');

            return redirect(route('weathers.index'));
        }

        $weather = $this->weatherRepository->update($request->all(), $id);

        Flash::success('Weather updated successfully.');

        return redirect(route('weathers.index'));
    }

    /**
     * Remove the specified Weather from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $weather = $this->weatherRepository->find($id);

        if (empty($weather)) {
            Flash::error('Weather not found');

            return redirect(route('weathers.index'));
        }

        $this->weatherRepository->delete($id);

        Flash::success('Weather deleted successfully.');

        return redirect(route('weathers.index'));
    }
}
