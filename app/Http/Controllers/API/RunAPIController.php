<?php

namespace App\Http\Controllers\API;


use App\Http\Requests\API\CreateRunAPIRequest;
use App\Http\Requests\API\UpdateRunAPIRequest;
use App\Models\Run;
use App\Models\Permission;
use App\Models\Lift_history;
use App\Models\Run_history;
use App\Models\User;
use App\Models\Resort;
use App\Models\Country;
use App\Models\State;
use App\Models\Snowflake;
use App\Models\Rewards_point;
use App\Repositories\RunRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Validator;
use Hash;
use Crypt;
use Response;

/**
 * Class RunController
 * @package App\Http\Controllers\API
 */

class RunAPIController extends AppBaseController
{
    /** @var  RunRepository */
    private $runRepository;
    public $unauthorised = 401;
    public $successStatus = 200;
    public $badrequest = 400;

    public function __construct(RunRepository $runRepo)
    {
        $this->runRepository = $runRepo;
    }

    /**
     * Display a listing of the Run.
     * GET|HEAD /runs
     *
     * @param Request $request
     * @return Response
     */

    public function addRun(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
            $validator = Validator::make($request->all(), [
                'country_id' => 'required',
                'state_id' => 'required',
                'resort_id' => 'required',
                'run_name' => 'required_without_all:run_number', 
                'run_number' => 'required_without_all:run_name', 
                'run_difficulty' => 'required',
                // 'run_length_value' => 'required',
                // 'measure_length_unit' => 'required',
                // 'max_slope' => 'required', 
                // 'average_slope' => 'required',
                // 'direction_run_faces' => 'required',
                // 'accessed_from' => 'required',
                // 'run_description' => 'required',
            ]);
            if ($validator->fails()) {
                return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
            }
            else{
                $country_id = $saveArray['country_id'];
                $state_id = $saveArray['state_id'];
                $resort_id = $saveArray['resort_id'];
                $user_id = $userExist['id'];
                $run_name = $saveArray['run_name'];
                $run_number = $saveArray['run_number'];
                $run_difficulty = $saveArray['run_difficulty'];
                $run_length_value = $saveArray['run_length_value'];
                $measure_length_unit = $saveArray['measure_length_unit'];
                $max_slope = $saveArray['max_slope'];
                $average_slope = $saveArray['average_slope'];
                $direction_run_faces = $saveArray['direction_run_faces'];
                $accesse_from = $saveArray['accessed_from'];
                $run_description = $saveArray['run_description'];

                $accessfrom = array($saveArray['accessed_from']);
                $accessed_from = implode(",", $accessfrom);
                // print_r($accessfrom);die("dd");
                $insertData = [
                    'country_id'=>$country_id,
                    'state_id'=>$state_id,
                    'resort_id'=>$resort_id,
                    'user_id'=>$user_id,
                    'run_name'=>$run_name,
                    'run_number'=>$run_number,
                    'run_difficulty'=>$run_difficulty,
                    'run_length_value'=>$run_length_value,
                    'measure_length_unit'=>$measure_length_unit,
                    'max_slope'=> $max_slope,
                    'average_slope'=> $average_slope,
                    'direction_run_faces'=> $direction_run_faces,
                    'accessed_from'=> $accessed_from,
                    'run_description'=> $run_description,
                    'created_at'=>date('Y-m-d h:i:s'),
                ];
                $run_id = Run::insertGetId($insertData);
                /*Run History*/
                $insertHistory = [
                    'run_id'=>$run_id,
                    'user_id'=>$user_id,
                    'note_for'=>'NULL',
                    'added_note'=>'NULL',
                    'run_name'=>$run_name,
                    'run_number'=>$run_number,
                    'run_difficulty'=>$run_difficulty,
                    'run_length_value'=>$run_length_value,
                    'measure_length_unit'=>$measure_length_unit,
                    'max_slope'=> $max_slope,
                    'average_slope'=> $average_slope,
                    'direction_run_faces'=> $direction_run_faces,
                    'accessed_from'=> $accessed_from,
                    'run_description'=> $run_description,
                    'created_at'=>date('Y-m-d h:i:s'),
                ];
                $insert = Run_history::insert($insertHistory);

                /*snowflakes rewards and total points*/

                $getRun = Run::where('id',$run_id)->first();
                if ($saveArray['run_name'] != '') {
                    $run_name = $saveArray['run_name'];
                }
                elseif ($saveArray['run_name'] != '' && $saveArray['run_number'] != '') {
                    $run_name = $saveArray['run_name'];
                }
                elseif ($saveArray['run_number'] != '') {
                    $run_name = $saveArray['run_number'];
                }
                $resortName = Resort::where('id',$saveArray['resort_id'])->select('resort_name')->first(); 
                $concate_reason1 = 'Added a Run at ';
                $concate_reason2 = $resortName['resort_name'];
                $space = ' : ';
                $concate_reason3 = $run_name?$run_name:'';
                $concate_reason = $concate_reason1.$concate_reason2.$space.$concate_reason3;
                if( ($getRun['run_name'] !== '' || $getRun['run_number'] !== '') && $getRun['run_difficulty'] !== '' && empty($getRun['run_description']) )
                {
                  $insertData = [
                    'run_id' => $run_id,
                    'user_id' => $userExist['id'],
                    'snowflake_rewards' => '10',
                    'reward_status' => '1',
                    'reason' => $concate_reason,
                    'created_at' => date('Y-m-d H:i:s')
                  ];
                  $insert = Snowflake::insert($insertData);
                  $getUser = Rewards_point::where('user_id',$userExist['id'])->select('total')->first();
                  $rewardedPoint = $getUser['total'] + 10;
                  $updateData = [
                        'user_id'=> $userExist['id'],
                        'total'=> $rewardedPoint,
                        'updated_at'=>date('Y-m-d h:i:s'),
                    ];
                    $update = Rewards_point::where(['user_id'=>$userExist['id']])->update($updateData);
                }
                elseif ( ($getRun['run_name'] !== '' || $getRun['run_number'] !== '') && $getRun['run_description'] !== '' && $getRun['run_difficulty'] !== '' && empty($getRun['run_length_value']) ) 
                {
                  $insertData = [
                    'run_id' => $run_id,
                    'user_id' => $userExist['id'],
                    'snowflake_rewards' => '20',
                    'reward_status' => '1',
                    'reason' => $concate_reason,
                    'created_at' => date('Y-m-d H:i:s')
                  ];
                  $insert = Snowflake::insert($insertData);
                  $getUser = Rewards_point::where('user_id',$userExist['id'])->select('total')->first();
                  $rewardedPoint = $getUser['total'] + 20;
                  $updateData = [
                        'user_id'=> $userExist['id'],
                        'total'=> $rewardedPoint,
                        'updated_at'=>date('Y-m-d h:i:s'),
                    ];
                    $update = Rewards_point::where(['user_id'=>$userExist['id']])->update($updateData);
                }
                elseif ( ($getRun['run_name'] !== '' || $getRun['run_number'] !== '') && $getRun['run_description'] !== '' && $getRun['run_difficulty'] !== '' && ( $getRun['run_length_value'] !== '' || $getRun['measure_length_unit'] !== '' || $getRun['max_slope'] !== '' || $getRun['average_slope'] !== '' || $getRun['direction_run_faces'] !== '' || $getRun['accessed_from'] !== '' ) )
                {
                  $insertData = [
                    'run_id' => $run_id,
                    'user_id' => $userExist['id'],
                    'snowflake_rewards' => '40',
                    'reward_status' => '1',
                    'reason' => $concate_reason,
                    'created_at' => date('Y-m-d H:i:s')
                  ];
                  $insert = Snowflake::insert($insertData);
                  $getUser = Rewards_point::where('user_id',$userExist['id'])->select('total')->first();
                  $rewardedPoint = $getUser['total'] + 40;
                  $updateData = [
                        'user_id'=> $userExist['id'],
                        'total'=> $rewardedPoint,
                        'updated_at'=>date('Y-m-d h:i:s'),
                    ];
                    $update = Rewards_point::where(['user_id'=>$userExist['id']])->update($updateData);
                }

                /*snowflakes rewards and total points*/

                if($run_id){
                    return response()->json([
                     'status'=>'1',
                     'message'=>'Run Created Successfully',
                     'run_id'=>(String)$run_id
                 ], $this->successStatus);
                }
                else{
                    return response()->json([
                        'status'=>'0',
                        'message'=>'Run Creation Failed'
                    ], $this->badrequest);
                }
            }
        }
        else{
            return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }

  public function getRun(Request $request)
  {
    $saveArray = $request->all();
    $token = $request->header('token');
    $userExist = User::where(['remember_token'=>$token])->first();
    if($userExist){
        $validator = Validator::make($request->all(), [
            'resort_id' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
        }
        else{
            //$image = url('/images',$userExist['image']);
            $runDetails = Run::where(['resort_id'=>$saveArray['resort_id']])
            ->orderBy('created_at','DESC')
            ->get();
            $runArray = [];
            if(count($runDetails)){
              foreach($runDetails as $run){
                $userDetails = User::where(['id'=>$userExist['id']])->first();
                $resortDetails = Resort::where(['id'=>$run['resort_id']])->first();
                $countryName = Country::where(['id'=>$run['country_id']])->first();
                $stateName = State::where(['id'=>$run['state_id']])->first();
                $listing['status'] = $run['status']?$run['status']:'';
                $listing['resort_id'] = (String)$run['resort_id'];
                $listing['resort_location'] = $resortDetails['resort_name'];
                $listing['bcolor'] = $resortDetails['bcolor'];
                $listing['fcolor'] = $resortDetails['fcolor'];
                $listing['country_id'] = (String)$run['country_id'];
                $listing['country_name'] = $countryName['name'];
                $listing['state_id'] = (String)$run['state_id'];
                $listing['state_name'] = $stateName['name'];
                $listing['user_id'] = (String)$userExist['id'];
                $listing['user_name'] = $userDetails['name'];
                $listing['run_id'] = (String)$run['id'];
                $listing['run_name'] = $run['run_name']?$run['run_name']:'';
                $listing['run_number'] = $run['run_number']?$run['run_number']:'';
                $listing['run_difficulty'] = $run['run_difficulty']?$run['run_difficulty']:'';
                $listing['run_length_value'] = $run['run_length_value']?$run['run_length_value']:'';
                $listing['measure_length_unit'] = $run['measure_length_unit']?$run['measure_length_unit']:'';
                $listing['max_slope'] = $run['max_slope']?$run['max_slope']:'';
                $listing['average_slope'] = $run['average_slope']?$run['average_slope']:'';
                $listing['direction_run_faces'] = $run['direction_run_faces']?$run['direction_run_faces']:'';
                $listing['accessed_from'] = $run['accessed_from']?$run['accessed_from']:'';
                $listing['run_description'] = $run['run_description']?$run['run_description']:'';
                $listing['created_at'] = $run['created_at']->format('d-m-Y');
                $runArray[] = $listing;
            }
              return response()->json([
                'status'=>'1',
                'message'=>'Runs fetched successfully',
                'run_listing'=>$runArray
              ], $this->successStatus);
            }
            else{
              return response()->json([
                'message'=>'No Run Found',
                'status'=>'0',
              ], $this->successStatus);
            }
        }
    }
    else{
      return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
    }
  }

  public function editRun(Request $request)
  {
    $saveArray = $request->all();
    $token = $request->header('token');
    $userExist = User::where(['remember_token'=>$token])->first();
    if($userExist){
      $validator = Validator::make($request->all(), [
        'run_id' => 'required',
        'post_anonymous' => 'required'
      ]);
      if ($validator->fails()) {
        return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
      }
      else{
        $runDetail = Run::where(['id'=>$saveArray['run_id']])->first();
        $accessfrom = array($saveArray['accessed_from']);
        $accessed_from = implode(",", $accessfrom);
        $updateRun = [
          'run_name'=>$saveArray['run_name']?$saveArray['run_name']:$runDetail['run_name'],
          'run_number'=>$saveArray['run_number']?$saveArray['run_number']:$runDetail['run_number'],
          'run_difficulty'=>$saveArray['run_difficulty']?$saveArray['run_difficulty']:$runDetail['run_difficulty'],
          'run_description'=>$saveArray['run_description']?$saveArray['run_description']:$runDetail['run_description'],
          'accessed_from'=>$accessed_from?$accessed_from:$runDetail['accessed_from'],
          'direction_run_faces'=>$saveArray['direction_run_faces']?$saveArray['direction_run_faces']:$runDetail['direction_run_faces'],
          'max_slope'=>$saveArray['max_slope']?$saveArray['max_slope']:$runDetail['max_slope'],
          'average_slope'=>$saveArray['average_slope']?$saveArray['average_slope']:$runDetail['average_slope'],
          'run_length_value'=>$saveArray['run_length_value']?$saveArray['run_length_value']:$runDetail['run_length_value'],
          'measure_length_unit'=>$saveArray['measure_length_unit']?$saveArray['measure_length_unit']:$runDetail['measure_length_unit'],
          'updated_at'=>date('Y-m-d h:i:s')
        ];

        /*update rewards on behalf first time and 2nd time insert update*/
        $givingReward = $this->giveRewards($saveArray,$userExist);
        /*update rewards on behalf first time and 2nd time insert update*/

        /*filtered existing records*/
        /*
        $existingRecord = Run::where('id',$saveArray['run_id'])->first()->toArray();
        $alreadyExistValue = (array_filter($existingRecord));
        $getUpdatedValue = (array_filter($updateRun));
        $currentValue = array_diff($getUpdatedValue, $alreadyExistValue);
        $cols = array_except($currentValue, ['measure_length_unit']);
        foreach($cols as $key=>$value)
        {
          $defaultValue =  $key;
        }
        $getField = Run::where(['id'=>$saveArray['run_id']])->select($defaultValue)->first();
        if (!empty($getField->$defaultValue)) {
            $concateReason = 'Added data in Runs for ';
            $getReason = $concateReason.$defaultValue;
            $insertData = [
                'user_id' => $userExist['id'],
                'run_id' => $saveArray['run_id'],
                'reward_for' => $defaultValue,
                'snowflake_rewards' => '5',
                'reward_status' => '0',
                'reason' => $getReason,
                'created_at' => date('Y-m-d H:i:s')
            ];
            $insert = Snowflake::insert($insertData);
        }
        else{
            $concateReason = 'Added data in Runs for ';
            $getReason = $concateReason.$defaultValue;
            $insertData = [
                'user_id' => $userExist['id'],
                'run_id' => $saveArray['run_id'],
                'reward_for' => $defaultValue,
                'snowflake_rewards' => '10',
                'reward_status' => '0',
                'reason' => $getReason,
                'created_at' => date('Y-m-d H:i:s')
            ];
            $insert = Snowflake::insert($insertData);
        }
        */
        /*filtered existing records*/
        $update = Run::where(['id'=>$saveArray['run_id']])->update($updateRun);

        $insertHistroy = [
          'run_id'=>$saveArray['run_id'],
          'user_id'=>$userExist['id'],
          'run_name'=>$saveArray['run_name'],
          'run_number'=>$saveArray['run_number'],
          'run_difficulty'=>$saveArray['run_difficulty'],
          'run_description'=> $saveArray['run_description'],
          'accessed_from'=>$accessed_from,
          'direction_run_faces'=>$saveArray['direction_run_faces'],
          'max_slope'=>$saveArray['max_slope'],
          'average_slope'=>$saveArray['average_slope'],
          'run_length_value'=>$saveArray['run_length_value'],
          'measure_length_unit'=>$saveArray['measure_length_unit'],
          'post_anonymous'=>$saveArray['post_anonymous'],
          'created_at'=>date('Y-m-d h:i:s'),
        ];
        $history = Run_history::insert($insertHistroy);

        if($update){
          return response()->json([
            'status'=>'1',
            'message'=>'Run Updated Successfully'
          ], $this->successStatus);
        }
      }
    }
    else{
      return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
    }
  }

    public function giveRewards ($request,$userExist)
    {
        $saveArray = $request;
        $getUpdatedValue = (array_filter($saveArray));
        $cols = array_except($getUpdatedValue, ['run_id','post_anonymous','measure_length_unit']);
        $unit = '';
        if ($saveArray['run_length_value'] != '') {
            $unit = $saveArray['measure_length_unit'];
        }
        foreach($cols as $key=>$value)
        {
            $defaultValue =  $key;
            $defaultValue1 = $value;
        }
        $output = str_replace('_', ' ', $defaultValue);

        $getField = Run::where(['id'=>$saveArray['run_id']])->select($defaultValue,'resort_id','run_name')->first();
        $resortName = Resort::where('id',$getField['resort_id'])->select('resort_name')->first();
        if (!empty($getField->$defaultValue)) {
            $concateReason = 'Updated '.$output.' for the Run '.$getField->run_name.' at '.$resortName->resort_name.' : '.$defaultValue1.''.$unit.'';
            // print_r($concateReason);die();
            // $concateReason = 'Added data in Runs for ';
            // $getReason = $concateReason.$defaultValue;
            $insertData = [
                'user_id' => $userExist['id'],
                'run_id' => $saveArray['run_id'],
                'reward_for' => $defaultValue,
                'snowflake_rewards' => '5',
                'reward_status' => '0',
                'reason' => $concateReason,
                'created_at' => date('Y-m-d H:i:s')
            ];
            $insert = Snowflake::insert($insertData);
        }
        else{
            $concateReason = 'Added '.$output.' for the Run '.$getField->run_name.' at '.$resortName->resort_name.' : '.$defaultValue1.''.$unit.'';
            // print_r($concateReason);die();
            // $concateReason = 'Added data in Runs for ';
            // $getReason = $concateReason.$defaultValue;
            $insertData = [
                'user_id' => $userExist['id'],
                'run_id' => $saveArray['run_id'],
                'reward_for' => $defaultValue,
                'snowflake_rewards' => '10',
                'reward_status' => '0',
                'reason' => $concateReason,
                'created_at' => date('Y-m-d H:i:s')
            ];
            $insert = Snowflake::insert($insertData);
        }
    }

  /*Run status changed by this api*/
    public function changeRunStatus(Request $request)
    {
      $saveArray = $request->all();
      $token = $request->header('token');
      $userExist = User::where(['remember_token'=>$token])->first();
      if($userExist){
        $validator = Validator::make($request->all(), [
                'run_id' => 'required',
                'status' => 'required',
            ]);
            if ($validator->fails()) {
                return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
            }
            else{
              $checkPermission = Permission::where(['user_id'=>$userExist['id']])
              ->where('role_type','!=','LIF')->first();
              if($checkPermission){
                $updateData = [
                    'status'=>$saveArray['status'],
                    'updated_at'=>date('Y-m-d h:i:s'),
                ];
                $update = Run::where(['id'=>$saveArray['run_id']])->update($updateData);
                if($update){
                    $getStatus = Run::where(['id'=>$saveArray['run_id']])->select('status')->first();
                    return response()->json([
                        'status'=>'1',
                        'message'=>'Status Updated Successfully',
                        'run_status'=>$getStatus
                    ], $this->successStatus);
                }else{
                    return response()->json([
                        'status'=>'0',
                      'message'=>'error'
                    ], $this->successStatus);
                  }
              }
              else{
                return response()->json([
                  'status'=>'1',
                  'message'=>'You dont have permissions to change status'
                ], $this->successStatus);
              }
              /**/
            }
          }
          else{
              return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
          }
      }

    public function runHistory(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
            $validator = Validator::make($request->all(), [
                'run_id' => 'required',
                'type' => 'required',
            ]);
            if ($validator->fails()) {
                return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
            }
            else{
                $dynamicField = $saveArray['type'];
                $gethistory = Run_history::where(['run_id'=>$saveArray['run_id']])
                    ->select('id','user_id','post_anonymous',$dynamicField)
                    ->where($dynamicField, '!=', 'Null')
                    ->orderBy('id','DESC')
                    ->get();
                    $detailArray = [];
                    if(count($gethistory)){
                        foreach($gethistory as $ls){
                            $username = User::where(['id'=>$ls['user_id']])->first();
                            $info['id'] = $ls['id'];
                            $info['post_anonymous'] = $ls['post_anonymous']?$ls['post_anonymous']:'';
                            $info['user_id'] = $ls['user_id'];
                            $info['user_name'] = $username['name'];
                            $info['type'] = $dynamicField;
                            $info['type_value'] = $ls->$dynamicField;
                            $info['created_at'] = date('Y-m-d h:i:s');
                            $detailArray[] = $info;
                        }
                        return response()->json([
                            'status'=>'1',
                            'message'=>'data fetched successfully',
                            'resort_history'=>$detailArray
                        ], $this->successStatus);
                    }
                    else{
                        return response()->json([
                            'message'=>'No data',
                            'status'=>'0'
                        ], $this->successStatus);
                    }
            }
        }
        else{
            return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }

    /* add notes for run and lift */

    public function addNotes(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
            $validator = Validator::make($request->all(), [
                'note_type' => 'required',
                'id' => 'required',
                'note_for' => 'required',
                'added_note' => 'required',
            ]);
            if ($validator->fails()) {
                return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
            }
            else{
              if ($saveArray['note_type'] == 'run') {
                $insertData = [
                  'user_id'=>$userExist['id'],
                  'run_id'=>$saveArray['id'],
                  'note_for'=>$saveArray['note_for'],
                  'added_note'=>$saveArray['added_note'],
                  'created_at'=>date('Y-m-d h:i:s')
                ];
                $insert = Run_history::insertGetId($insertData);
                if($insert){
                        return response()->json([
                          'status'=>'1',
                          'message'=>'Note Added Successfully',
                        ], $this->successStatus);
                      }
                      else{
                        return response()->json([
                          'message'=>'Error',
                          'status'=>'0'
                        ], $this->badrequest);
                      }
              }elseif($saveArray['note_type'] == 'lift'){
                $insertData = [
                  'user_id'=>$userExist['id'],
                  'lift_id'=>$saveArray['id'],
                  'note_for'=>$saveArray['note_for'],
                  'added_note'=>$saveArray['added_note'],
                  'created_at'=>date('Y-m-d h:i:s')
                ];
                $insert = Lift_history::insertGetId($insertData);
                if($insert){
                        return response()->json([
                          'status'=>'1',
                          'message'=>'Note Added Successfully',
                        ], $this->successStatus);
                      }
                      else{
                        return response()->json([
                          'message'=>'Error',
                          'status'=>'0'
                        ], $this->badrequest);
                      }
              }
            }
        }
        else{
            return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }

    /* add notes for run and lift */

    /* get notes for run and lift */

    public function getNotes(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
            $validator = Validator::make($request->all(), [
                'note_type' => 'required',
                'id' => 'required',
                'note_for' => 'required',
            ]);
            if ($validator->fails()) {
              return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
            }
            else{
              if ($saveArray['note_type'] == 'run') {
                $notedetail = Run_history::where(['run_id'=>$saveArray['id']])
                ->select('id','user_id','run_id','note_for','added_note')
                ->where('note_for', '!=', 'Null')
                ->orderBy('id','DESC')
                ->get();
                $noteArray = [];
                if(count($notedetail)){
                  foreach($notedetail as $ls){
                    $username = User::where(['id'=>$ls['user_id']])->first();
                    $note['id'] = $ls['id'];
                    $note['user_id'] = $ls['user_id'];
                    $note['user_name'] = $username['name'];
                    $note['run_id'] = $ls['run_id']?$ls['run_id']:'';
                    $note['note_for'] = $ls['note_for']?$ls['note_for']:'';
                    $note['added_note'] = $ls['added_note']?$ls['added_note']:'';
                    $note['created_at'] = date('Y-m-d h:i:s');
                    $noteArray[] = $note;
                  }
                  return response()->json([
                    'status'=>'1',
                    'message'=>'data fetched successfully',
                    'noteHistory'=>$noteArray
                  ], $this->successStatus);
                }
                else{
                  return response()->json([
                    'message'=>'No Notes',
                    'status'=>'0'
                  ], $this->successStatus);
                }
              }elseif ($saveArray['note_type'] == 'lift') {
                $notedetail = Lift_history::where(['lift_id'=>$saveArray['id']])
                ->where('note_for', '!=', 'Null')
                ->select('id','user_id','lift_id','note_for','added_note')
                ->orderBy('id','DESC')
                ->get();
                $noteArray = [];
                if(count($notedetail)){
                  foreach($notedetail as $ls){
                    $username = User::where(['id'=>$ls['user_id']])->first();
                    $note['id'] = $ls['id'];
                    $note['user_id'] = $ls['user_id'];
                    $note['user_name'] = $username['name'];
                    $note['lift_id'] = $ls['lift_id']?$ls['lift_id']:'';
                    $note['note_for'] = $ls['note_for']?$ls['note_for']:'';
                    $note['added_note'] = $ls['added_note']?$ls['added_note']:'';
                    $note['created_at'] = date('Y-m-d h:i:s');
                    $noteArray[] = $note;
                  }
                  return response()->json([
                    'status'=>'1',
                    'message'=>'data fetched successfully',
                    'noteHistory'=>$noteArray
                  ], $this->successStatus);
                }
                else{
                  return response()->json([
                    'message'=>'No Notes',
                    'status'=>'0'
                  ], $this->successStatus);
                }
              }
            }
          }
          else{
            return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
          }
        }

    /* add notes for run and lift */

    public function index(Request $request)
    {
        $runs = $this->runRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($runs->toArray(), 'Runs retrieved successfully');
    }

    /**
     * Store a newly created Run in storage.
     * POST /runs
     *
     * @param CreateRunAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateRunAPIRequest $request)
    {
        $input = $request->all();

        $run = $this->runRepository->create($input);

        return $this->sendResponse($run->toArray(), 'Run saved successfully');
    }

    /**
     * Display the specified Run.
     * GET|HEAD /runs/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Run $run */
        $run = $this->runRepository->find($id);

        if (empty($run)) {
            return $this->sendError('Run not found');
        }

        return $this->sendResponse($run->toArray(), 'Run retrieved successfully');
    }

    /**
     * Update the specified Run in storage.
     * PUT/PATCH /runs/{id}
     *
     * @param int $id
     * @param UpdateRunAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRunAPIRequest $request)
    {
        $input = $request->all();

        /** @var Run $run */
        $run = $this->runRepository->find($id);

        if (empty($run)) {
            return $this->sendError('Run not found');
        }

        $run = $this->runRepository->update($input, $id);

        return $this->sendResponse($run->toArray(), 'Run updated successfully');
    }

    /**
     * Remove the specified Run from storage.
     * DELETE /runs/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Run $run */
        $run = $this->runRepository->find($id);

        if (empty($run)) {
            return $this->sendError('Run not found');
        }

        $run->delete();

        return $this->sendResponse($id, 'Run deleted successfully');
    }
}
