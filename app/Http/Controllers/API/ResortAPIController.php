<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateResortAPIRequest;
use App\Http\Requests\API\UpdateResortAPIRequest;
use App\Models\Like;
use App\Models\Run;
use App\Models\Run_history;
use App\Models\Lift;
use App\Models\Lift_history;
use App\Models\Permission;
use App\Models\Access_code;
use App\Models\Resort;
use App\Models\Resort_detail;
use App\Models\Resort_history;
use App\Models\User;
use App\Models\State;
use App\Models\Country;
use App\Models\Fav_resort;
use App\Models\Snowflake;
use App\Models\Rewards_point;
use App\Repositories\ResortRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use DB;
use Validator;
use Response;

/**
 * Class ResortController
 * @package App\Http\Controllers\API
 */

class ResortAPIController extends AppBaseController
{
    /** @var  ResortRepository */
    private $resortRepository;
    public $unauthorised = 401;
    public $successStatus = 200;
    public $badrequest = 400;

    public function __construct(ResortRepository $resortRepo)
    {
        $this->resortRepository = $resortRepo;
    }

    /**
     * Display a listing of the Resort.
     * GET|HEAD /resorts
     *
     * @param Request $request
     * @return Response
     */

    public function addresortDetail(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist) {
            $validator = Validator::make($request->all(), [
                'resort_id' => 'required',
                'post_anonymous' => 'required'
            ]);
            if ($validator->fails()) {
                return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
            }else{
                $resortDetail = Resort_detail::where(['resort_id'=>$saveArray['resort_id']])->first();
                if ($resortDetail) {
                    $topElevation = $saveArray['top_elevation']?$saveArray['top_elevation']:$resortDetail['top_elevation'];

                    $baseElevation = $saveArray['bottom_elevation']?$saveArray['bottom_elevation']:$resortDetail['bottom_elevation'];
                    if ($saveArray['top_elevation'] == '') {
                        $topEle = $topElevation;
                        $topEleUnit = $saveArray['bottom_elevation_unit']?$saveArray['bottom_elevation_unit']:$resortDetail['bottom_elevation_unit'];

                        $baseEle = $baseElevation;
                        $baseEleUnit = $saveArray['bottom_elevation_unit']?$saveArray['bottom_elevation_unit']:$resortDetail['bottom_elevation_unit'];

                        $vertRise = $topEle - $baseEle;
                        $topEleUnit = $saveArray['bottom_elevation_unit'];
                    }
                    else{
                        $topEle = $topElevation;
                        $topEleUnit = $saveArray['top_elevation_unit']?$saveArray['top_elevation_unit']:$resortDetail['top_elevation_unit'];

                        $baseEle = $baseElevation;
                        $baseEleUnit = $saveArray['top_elevation_unit']?$saveArray['top_elevation_unit']:$resortDetail['top_elevation_unit'];

                        $vertRise = $topEle - $baseEle;
                        $topEleUnit = $saveArray['top_elevation_unit'];
                    }
                    $updateData = [
                        /**/

                        'vertical_rise'=>$vertRise,
                        'vertical_rise_unit'=>$topEleUnit,

                        'top_elevation'=>$topEle,
                        'top_elevation_unit'=>$topEleUnit,

                        'bottom_elevation'=>$baseEle,
                        'bottom_elevation_unit'=>$baseEleUnit,

                        /**/
                        'resort_info'=>$saveArray['resort_info']?$saveArray['resort_info']:$resortDetail['resort_info'],
                        'skiable_area'=>$saveArray['skiable_area']?$saveArray['skiable_area']:$resortDetail['skiable_area'],
                        'skiable_area_unit'=>$saveArray['skiable_area_unit']?$saveArray['skiable_area_unit']:$resortDetail['skiable_area_unit'],
                        'runs'=> $saveArray['runs']?$saveArray['runs']:$resortDetail['runs'],
                        'lifts'=> $saveArray['lifts']?$saveArray['lifts']:$resortDetail['lifts'],
                        'longest_run'=>$saveArray['longest_run']?$saveArray['longest_run']:$resortDetail['longest_run'],
                        'longest_run_unit'=>$saveArray['longest_run_unit']?$saveArray['longest_run_unit']:$resortDetail['longest_run_unit'],
                        'terrain_parks'=>$saveArray['terrain_parks']?$saveArray['terrain_parks']:$resortDetail['terrain_parks'],
                        'half_pipes'=>$saveArray['half_pipes']?$saveArray['half_pipes']:$resortDetail['half_pipes'],
                        'annual_snowfall'=>$saveArray['annual_snowfall']?$saveArray['annual_snowfall']:$resortDetail['annual_snowfall'],
                        'annual_snowfall_unit'=>$saveArray['annual_snowfall_unit']?$saveArray['annual_snowfall_unit']:$resortDetail['annual_snowfall_unit'],
                        'snow_making'=>$saveArray['snow_making']?$saveArray['snow_making']:$resortDetail['snow_making'],
                        'snow_making_unit'=>$saveArray['snow_making_unit']?$saveArray['snow_making_unit']:$resortDetail['snow_making_unit'],
                        'uphill_capacity'=>$saveArray['uphill_capacity']?$saveArray['uphill_capacity']:$resortDetail['uphill_capacity'],
                        'terrain_difficulty_beginner'=>$saveArray['terrain_difficulty_beginner']?$saveArray['terrain_difficulty_beginner']:$resortDetail['terrain_difficulty_beginner'],
                        'terrain_difficulty_intermediate'=>$saveArray['terrain_difficulty_intermediate']?$saveArray['terrain_difficulty_intermediate']:$resortDetail['terrain_difficulty_intermediate'],
                        'terrain_difficulty_advance'=>$saveArray['terrain_difficulty_advance']?$saveArray['terrain_difficulty_advance']:$resortDetail['terrain_difficulty_advance'],
                        'terrain_difficulty_expert'=>$saveArray['terrain_difficulty_expert']?$saveArray['terrain_difficulty_expert']:$resortDetail['terrain_difficulty_expert'],
                        'opening_date'=>$saveArray['opening_date']?$saveArray['opening_date']:$resortDetail['opening_date'],
                        'closing_date'=>$saveArray['closing_date']?$saveArray['closing_date']:$resortDetail['closing_date'],
                        'updated_at'=>date('Y-m-d h:i:s')
                    ];

                    /*24Dec2019*/
                    /*update rewards on behalf first time and 2nd time insert update*/
                    $givingReward = $this->giveRewards($saveArray,$userExist);
                    /*update rewards on behalf first time and 2nd time insert update*/
                    /*24Dec2019*/

                    $update = Resort_detail::where(['resort_id'=>$saveArray['resort_id']])->update($updateData);
                    $insertData = [
                        'user_id'=>$userExist['id'],
                        'resort_id'=>$saveArray['resort_id'],
                        'vertical_rise'=>$saveArray['vertical_rise'],
                        'vertical_rise_unit'=>$saveArray['vertical_rise_unit'],
                        'top_elevation'=>$saveArray['top_elevation'],
                        'top_elevation_unit'=>$saveArray['top_elevation_unit'],
                        'bottom_elevation'=>$saveArray['bottom_elevation'],
                        'bottom_elevation_unit'=>$saveArray['bottom_elevation_unit'],
                        'resort_info'=>$saveArray['resort_info'],
                        'skiable_area'=>$saveArray['skiable_area'],
                        'skiable_area_unit'=>$saveArray['skiable_area_unit'],
                        'runs'=>$saveArray['runs'],
                        'lifts'=>$saveArray['lifts'],
                        'longest_run'=>$saveArray['longest_run'],
                        'longest_run_unit'=>$saveArray['longest_run_unit'],
                        'terrain_parks'=>$saveArray['terrain_parks'],
                        'half_pipes'=>$saveArray['half_pipes'],
                        'annual_snowfall'=>$saveArray['annual_snowfall'],
                        'annual_snowfall_unit'=>$saveArray['annual_snowfall_unit'],
                        'snow_making'=>$saveArray['snow_making'],
                        'snow_making_unit'=>$saveArray['snow_making_unit'],
                        'uphill_capacity'=>$saveArray['uphill_capacity'],
                        'terrain_difficulty_beginner'=>$saveArray['terrain_difficulty_beginner'],
                        'terrain_difficulty_intermediate'=>$saveArray['terrain_difficulty_intermediate'],
                        'terrain_difficulty_advance'=>$saveArray['terrain_difficulty_advance'],
                        'terrain_difficulty_expert'=>$saveArray['terrain_difficulty_expert'],
                        'opening_date'=>$saveArray['opening_date'],
                        'closing_date'=>$saveArray['closing_date'],
                        'note_for'=>$saveArray['note_for'],
                        'added_note'=>$saveArray['added_note'],
                        'post_anonymous'=>$saveArray['post_anonymous'],
                        'created_at'=>date('Y-m-d h:i:s')
                    ];
                    $insertedID = Resort_history::insertGetId($insertData);
                    if($insertedID){
                        return response()->json([
                          'status'=>'1',
                          'message'=>'Resort Updated Successfully',
                        ], $this->successStatus);
                      }
                      else{
                        return response()->json([
                          'message'=>'Resort does not Update',
                          'status'=>'0'
                        ], $this->badrequest);
                      }
                }
                else{
                    return response()->json([
                          'message'=>'Resort not found',
                          'status'=>'0'
                        ], $this->badrequest);
                }
            }
        }
        else{
            return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }

    #giving rewards to every user behalf of adding and updating data IF their data is confirmed

    // public function giveRewards ($request,$userExist)
    // {
    //     /*update rewards on behalf first time and 2nd time insert update*/
    //     $saveArray = $request;
    //     $getUpdatedValue = (array_filter($saveArray));
    //     $cols = array_except($getUpdatedValue, ['resort_id','post_anonymous','vertical_rise_unit','top_elevation_unit','bottom_elevation_unit','skiable_area_unit','longest_run_unit','annual_snowfall_unit','snow_making_unit']);
    //     // print_r($cols);die();
    //     #23Dec2019
    //     /*$getUpdatedValue = (array_filter($updateData));
    //     $cols = array_except($getUpdatedValue, ['vertical_rise_unit','top_elevation_unit','bottom_elevation_unit','skiable_area_unit','longest_run_unit','annual_snowfall_unit','snow_making_unit']);*/
    //     #23Dec2019
    //     foreach($cols as $key=>$value){
    //         $defaultValue =  $key;
    //     }
    //     $getField = Resort_detail::where(['resort_id'=>$saveArray['resort_id']])
    //     ->select($defaultValue)->first();
    //     if (!empty($getField->$defaultValue)) {
    //         $concateReason = 'Update data in Resort Database for ';
    //         $getReason = $concateReason.$defaultValue;
    //         $insertData = [
    //             'user_id' => $userExist['id'],
    //             'resort_id' => $saveArray['resort_id'],
    //             'reward_for' => $defaultValue,
    //             'snowflake_rewards' => '5',
    //             'reward_status' => '0',
    //             'reason' => $getReason,
    //             'created_at' => date('Y-m-d H:i:s')
    //         ];
    //         $insert = Snowflake::insert($insertData);
    //     }
    //     else{
    //         $concateReason = 'Add data in Resort Database for ';
    //         $getReason = $concateReason.$defaultValue;
    //         $insertData = [
    //             'user_id' => $userExist['id'],
    //             'resort_id' => $saveArray['resort_id'],
    //             'reward_for' => $defaultValue,
    //             'snowflake_rewards' => '10',
    //             'reward_status' => '0',
    //             'reason' => $getReason,
    //             'created_at' => date('Y-m-d H:i:s')
    //         ];
    //         $insert = Snowflake::insert($insertData);
    //     }
    //     /*update rewards on behalf first time and 2nd time insert update*/
    // }

    public function giveRewards ($request,$userExist)
    {
        $saveArray = $request;
        $getUpdatedValue = (array_filter($saveArray));
        $cols = array_except($getUpdatedValue, ['resort_id','post_anonymous','vertical_rise_unit','top_elevation_unit','bottom_elevation_unit','skiable_area_unit','longest_run_unit','annual_snowfall_unit','snow_making_unit']);
        $unit = '';
        if ($saveArray['top_elevation'] != '') {
            $unit = $saveArray['top_elevation_unit'];
        }
        if ($saveArray['bottom_elevation'] != '') {
            $unit = $saveArray['bottom_elevation_unit'];
        }
        if ($saveArray['skiable_area'] != '') {
            $unit = $saveArray['skiable_area_unit'];
        }
        if ($saveArray['longest_run'] != '') {
            $unit = $saveArray['longest_run_unit'];
        }
        if ($saveArray['annual_snowfall'] != '') {
            $unit = $saveArray['annual_snowfall_unit'];
        }
        if ($saveArray['snow_making'] != '') {
            $unit = $saveArray['snow_making_unit'];
        }
        foreach($cols as $key=>$value){
            $defaultValue =  $key;
            $defaultValue1 = $value;
        }
        $output = str_replace('_', ' ', $defaultValue);

        $getField = Resort_detail::where(['resort_id'=>$saveArray['resort_id']])
        ->select($defaultValue)->first();

        $resortName = Resort::where('id',$saveArray['resort_id'])->select('resort_name')->first();

        if (!empty($getField->$defaultValue)) {
            $concateReason = 'Updated '.$output.' for '.$resortName->resort_name.' : '.$defaultValue1.''.$unit.'';
            $insertData = [
                'user_id' => $userExist['id'],
                'resort_id' => $saveArray['resort_id'],
                'reward_for' => $defaultValue,
                'snowflake_rewards' => '5',
                'reward_status' => '0',
                'reason' => $concateReason,
                'created_at' => date('Y-m-d H:i:s')
            ];
            $insert = Snowflake::insert($insertData);
        }
        else{
            $concateReason = 'Updated '.$output.' for '.$resortName->resort_name.' : '.$defaultValue1.'';
            $insertData = [
                'user_id' => $userExist['id'],
                'resort_id' => $saveArray['resort_id'],
                'reward_for' => $defaultValue,
                'snowflake_rewards' => '10',
                'reward_status' => '0',
                'reason' => $concateReason,
                'created_at' => date('Y-m-d H:i:s')
            ];
            $insert = Snowflake::insert($insertData);
        }
    }
    #giving rewards to every user behalf of adding and updating data IF their data is confirmed

    public function resortDetail(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
            $validator = Validator::make($request->all(), [
            'resort_id' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
        }
        else{
            $resortDetail = Resort_detail::where(['resort_id'=>$saveArray['resort_id']])->first();
            $resort = Resort::where('id',$saveArray['resort_id'])->first();
            // print_r(json_encode($resort));die();
            $checkIsFav = Fav_resort::where('resort_id',$saveArray['resort_id'])
            ->where('user_id',$userExist['id'])
            ->where('favourite','=','1')
            ->first();
            if ($checkIsFav['favourite'] == 1) {
                $isFav = 'Yes';
            }else{
                $isFav = 'No';
            }
            $resortLogo = Resort::where('id',$saveArray['resort_id'])->select('image')->first();
            $image = url('/resort_images',$resortLogo['image']);
            $resort_details = array(
                'is_favourite' => $isFav,
                'resort_logo' => $image,
                'resort_id'=>(String)$resortDetail['resort_id'],
                'resort_info'=>$resortDetail['resort_info']?$resortDetail['resort_info']:'',
                'bcolor'=>$resort['bcolor'],
                'fcolor'=>$resort['fcolor'],
                'vertical_rise'=>$resortDetail['vertical_rise']?$resortDetail['vertical_rise']:'',
                'vertical_rise_unit'=>$resortDetail['vertical_rise_unit']?$resortDetail['vertical_rise_unit']:'',
                'top_elevation'=>$resortDetail['top_elevation']?$resortDetail['top_elevation']:'',
                'top_elevation_unit'=>$resortDetail['top_elevation_unit']?$resortDetail['top_elevation_unit']:'',
                'bottom_elevation'=>$resortDetail['bottom_elevation']?$resortDetail['bottom_elevation']:'',
                'bottom_elevation_unit'=>$resortDetail['bottom_elevation_unit']?$resortDetail['bottom_elevation_unit']:'',
                'skiable_area'=>$resortDetail['skiable_area']?$resortDetail['skiable_area']:'',
                'skiable_area_unit'=>$resortDetail['skiable_area_unit']?$resortDetail['skiable_area_unit']:'',
                'runs'=>$resortDetail['runs']?$resortDetail['runs']:'',
                'lifts'=>$resortDetail['lifts']?$resortDetail['lifts']:'',
                'longest_run'=>$resortDetail['longest_run']?$resortDetail['longest_run']:'',
                'longest_run_unit'=>$resortDetail['longest_run_unit']?$resortDetail['longest_run_unit']:'',
                'terrain_parks'=>$resortDetail['terrain_parks']?$resortDetail['terrain_parks']:'',
                'half_pipes'=>$resortDetail['half_pipes']?$resortDetail['half_pipes']:'',
                'annual_snowfall'=>$resortDetail['annual_snowfall']?$resortDetail['annual_snowfall']:'',
                'annual_snowfall_unit'=>$resortDetail['annual_snowfall_unit']?$resortDetail['annual_snowfall_unit']:'',
                'snow_making'=>$resortDetail['snow_making']?$resortDetail['snow_making']:'',
                'snow_making_unit'=>$resortDetail['snow_making_unit']?$resortDetail['snow_making_unit']:'',
                'uphill_capacity'=>$resortDetail['uphill_capacity']?$resortDetail['uphill_capacity']:'',
                'terrain_difficulty_beginner'=>$resortDetail['terrain_difficulty_beginner']?$resortDetail['terrain_difficulty_beginner']:'',
                'terrain_difficulty_intermediate'=>$resortDetail['terrain_difficulty_intermediate']?$resortDetail['terrain_difficulty_intermediate']:'',
                'terrain_difficulty_advance'=>$resortDetail['terrain_difficulty_advance']?$resortDetail['terrain_difficulty_advance']:'',
                'terrain_difficulty_expert'=>$resortDetail['terrain_difficulty_expert']?$resortDetail['terrain_difficulty_expert']:'',
                'opening_date'=>$resortDetail['opening_date']?$resortDetail['opening_date']:'',
                'closing_date'=>$resortDetail['closing_date']?$resortDetail['closing_date']:'',
                'created_at'=>$resortDetail['created_at']->format('d-m-Y'),
                      );
            return response()->json([
                'status'=>'1',
                'message'=>'Details fetched successfully',
                'resort_details'=>$resort_details
            ], $this->successStatus);
        }
    }
    else{
        return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }


     /*
    * Get All Permission of a Resort
  */  

  public function getAllPermission(Request $request)
  {
    $saveArray = $request->all();
    $token = $request->header('token');
    $userExist = User::where(['remember_token'=>$token])->first();
    if($userExist){
      $validator = Validator::make($request->all(), [
        'resort_id' => 'required',
      ]);
      if ($validator->fails()) {
        return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
      }
      else{
        $checkResort = Resort::where(['id'=>$saveArray['resort_id']])->first();
        if ($checkResort) {
          $adminArray = [];
          $editorArray = [];
          $patrollerArray = [];
          $liftyArray = [];
          $getCodes = Access_code::where(['resort_id'=>$saveArray['resort_id']])->first();
          $resortDetails = Resort::where(['id'=>$saveArray['resort_id']])->select('id','resort_name')->first();
          $resortdetail = array(
            'resort_id'=>$resortDetails['id'],
            'resort_location'=>$resortDetails['resort_name']
          );
          $getAdmin = Permission::where('permission_type','ADM')->where('entity_id',$saveArray['resort_id'])->select('id','user_id','entity_id','code','permission_type','created_at')->get();
          foreach($getAdmin as $admin){
            $userDetails = User::where(['id'=>$admin['user_id']])->select('name')->first();
            // $resortDetails = Resort::where(['id'=>$admin['entity_id']])->select('resort_name')->first();
            $adm['user_id'] = (String)$admin['user_id'];
            $adm['user_name'] = $userDetails['name'];
            // $admin['resort_id'] = (String)$admin['entity_id'];
            // $admin['resort_location'] = $resortDetails['resort_name'];
            $adm['role_type'] = $admin['permission_type'];
            $adm['created_at'] = $admin['created_at']->format('Y-m-d');
            $adminArray[] = $adm;
          }
          $getEditor = Permission::where('permission_type','EDI')->where('entity_id',$saveArray['resort_id'])->select('id','user_id','entity_id','code','permission_type','created_at')->get();
          foreach($getEditor as $editor){
            $userDetails1 = User::where(['id'=>$editor['user_id']])->select('name')->first();
            // $resortDetails = Resort::where(['id'=>$editor['entity_id']])->first();
            $edit['id'] = (String)$editor['id'];
            $edit['user_id'] = (String)$editor['user_id'];
            $edit['user_name'] = (String)$userDetails1['name'];
            // $edit['resort_id'] = (String)$editor['entity_id'];
            // $edit['resort_location'] = (String)$resortDetails['resort_name'];
            $edit['role_type'] = $editor['permission_type'];
            $edit['created_at'] = $editor['created_at']->format('Y-m-d');
            $editorArray[] = $edit;
          }
          $getPatroller = Permission::where('permission_type','PAT')->where('entity_id',$saveArray['resort_id'])->select('id','user_id','entity_id','code','permission_type','created_at')->get();
          foreach($getPatroller as $patroller){
            $userDetails2 = User::where(['id'=>$patroller['user_id']])->first();
            // $resortDetails2 = Resort::where(['id'=>$patroller['entity_id']])->first();
            $pat['user_id'] = (String)$patroller['user_id'];
            $pat['user_name'] = (String)$userDetails2['name'];
            // $pat['resort_id'] = (String)$patroller['entity_id'];
            // $pat['resort_location'] = (String)$resortDetails2['resort_name'];
            $pat['role_type'] = $patroller['permission_type'];
            $pat['created_at'] = $patroller['created_at']->format('Y-m-d');
            $patrollerArray[] = $pat;
          }
          $getLifty = Permission::where('permission_type','LIF')->where('entity_id',$saveArray['resort_id'])->select('id','user_id','entity_id','code','permission_type','created_at')->get();
          foreach($getLifty as $lifty){
            $userDetails3 = User::where(['id'=>$lifty['user_id']])->first();
            $resortDetails3 = Resort::where(['id'=>$lifty['entity_id']])->first();
            $lif['user_id'] = (String)$lifty['user_id'];
            $lif['user_name'] = (String)$userDetails3['name'];
            // $lif['resort_id'] = (String)$lifty['entity_id'];
            // $lif['resort_location'] = (String)$resortDetails3['resort_name'];
            // $lif['role_type'] = $lifty['permission_type'];
            $lif['created_at'] = $lifty['created_at']->format('Y-m-d');
            $liftyArray[] = $lif;
          }
          return response()->json([
            'status'=>'1',
            'message'=>'data fetched successfully',
            'adminCode' =>$getCodes['resort_administrator'],
            'editorCode' =>$getCodes['resort_editor'],
            'patrollerCode' =>$getCodes['patroller'],
            'liftyCode' =>$getCodes['lifty'],
            'resortdetail'=>$resortdetail,
            'adminArray'=>$adminArray,
            'editorArray'=>$editorArray,
            'patrollerArray'=>$patrollerArray,
            'liftyArray'=>$liftyArray
          ], $this->successStatus);
        }
        else{
          return response()->json([
            'status'=>'0',
            'message'=>'Resort does not exists',
          ], $this->successStatus);
        }
      }
    }else{
      return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
    }
  }

    /*getRun Grades API*/

    public function getRunGrades(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
            $validator = Validator::make($request->all(), [
                'resort_id' => 'required',
            ]);
            if ($validator->fails()) {
                return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
            }
            else{
            $runGrades = Resort::where(['id'=>$saveArray['resort_id']])->first();
            if ($runGrades) {
                $rungrades_arr = explode (",", $runGrades['run_grades']);
                return response()->json([
                    'status'=>'1',
                    'message'=>'Run Grades fetched successfully',
                    'run_grades'=>$rungrades_arr
                ], $this->successStatus);
                }
                else{
                    return response()->json([
                        'message'=>'No Data',
                        'status'=>'0'
                    ], $this->successStatus);
                }
            }
        }
        else{
            return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }

    /*Get Notes of all details*/

    public function getNote(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
            $validator = Validator::make($request->all(), [
                'resort_id' => 'required',
                'note_for' => 'required',
            ]);
            if ($validator->fails()) {
                return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
            }
            else{
            $notes = Resort_history::where(['resort_id'=>$saveArray['resort_id']])
            ->where(['note_for'=>$saveArray['note_for']])
            ->orderBy('id','DESC')
            ->select('id','user_id','resort_id','note_for','added_note')->get();
            $noteArray = [];
            if(count($notes)){
                foreach($notes as $ls){
                    $username = User::where(['id'=>$ls['user_id']])->first();
                    $note['id'] = $ls['id'];
                    $note['resort_id'] = $ls['resort_id'];
                    $note['user_id'] = $ls['user_id'];
                    $note['user_name'] = $username['name'];
                    $note['note_for'] = $ls['note_for'];
                    $note['added_note'] = $ls['added_note'];
                    $note['created_at'] = date('Y-m-d h:i:s');
                    $noteArray[] = $note;
                }
                return response()->json([
                    'status'=>'1',
                    'message'=>'Notes fetched successfully',
                    'listing'=>$noteArray
                ], $this->successStatus);
            }
            else{
                return response()->json([
                    'message'=>'No Notes',
                    'status'=>'0'
                ], $this->successStatus);
            }
        }
    }
    else{
        return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }

    /*tarrain history*/

    public function getterrainHistory(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
            $validator = Validator::make($request->all(), [
                'resort_id' => 'required'
            ]);
            if ($validator->fails()) {
                return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
            }
            else{
                $terrain_difficulty = Resort_history::where(['resort_id'=>$saveArray['resort_id']])
                ->where('terrain_difficulty_beginner','!=','')
                ->where('terrain_difficulty_intermediate','!=','')
                ->where('terrain_difficulty_advance','!=','')
                ->where('terrain_difficulty_expert','!=','')
                ->select('id','user_id','resort_id','terrain_difficulty_beginner','terrain_difficulty_intermediate','terrain_difficulty_advance','terrain_difficulty_expert')
                ->orderBy('id','DESC')
                ->get();
                $terrainArray = [];
                if(count($terrain_difficulty)){
                    foreach($terrain_difficulty as $ls){
                        $username = User::where(['id'=>$ls['user_id']])->first();
                        $note['id'] = $ls['id'];
                        $note['resort_id'] = $ls['resort_id'];
                        $note['user_id'] = $ls['user_id'];
                        $note['user_name'] = $username['name'];
                        $note['terrain_difficulty_beginner'] = $ls['terrain_difficulty_beginner']?$ls['terrain_difficulty_beginner']:'';
                        $note['terrain_difficulty_intermediate'] = $ls['terrain_difficulty_intermediate']?$ls['terrain_difficulty_intermediate']:'';
                        $note['terrain_difficulty_advance'] = $ls['terrain_difficulty_advance']?$ls['terrain_difficulty_advance']:'';
                        $note['terrain_difficulty_expert'] = $ls['terrain_difficulty_expert']?$ls['terrain_difficulty_expert']:'';
                        $note['created_at'] = date('Y-m-d h:i:s');
                        $terrainArray[] = $note;
                    }
                    return response()->json([
                        'status'=>'1',
                        'message'=>'data fetched successfully',
                        'terrainHistory'=>$terrainArray
                    ], $this->successStatus);
                }
                else{
                    return response()->json([
                        'message'=>'No Notes',
                        'status'=>'0'
                    ], $this->successStatus);
                }
            }
        }
        else{
            return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }

    /*resort history*/

    public function getHistory(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
            $validator = Validator::make($request->all(), [
                'resort_id' => 'required',
                'type' => 'required',
            ]);
            if ($validator->fails()) {
                return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
            }
            else{
                $dynamicField = $saveArray['type'];
                $gethistory =Resort_history::where(['resort_id'=>$saveArray['resort_id']])
                    ->select('id','user_id','post_anonymous',$dynamicField)
                    ->where($dynamicField, '!=', 'Null')
                    //->orwhere($dynamicField, '=', '0')
                    ->orderBy('id','DESC')
                    ->get();
                    // print_r(json_encode($gethistory));die();
                    $detailArray = [];
                    if(count($gethistory)){
                        foreach($gethistory as $ls){
                            $username = User::where(['id'=>$ls['user_id']])->first();
                            $info['id'] = $ls['id'];
                            $info['post_anonymous'] = $ls['post_anonymous']?$ls['post_anonymous']:'';
                            $info['user_id'] = $ls['user_id'];
                            $info['user_name'] = $username['name'];
                            $info['type'] = $dynamicField;
                            $info['type_value'] = $ls->$dynamicField;
                            $info['created_at'] = date('Y-m-d h:i:s');
                            $detailArray[] = $info;
                        }
                        return response()->json([
                            'status'=>'1',
                            'message'=>'data fetched successfully',
                            'resort_history'=>$detailArray
                        ], $this->successStatus);
                    }
                    else{
                        return response()->json([
                            'message'=>'No data',
                            'status'=>'1'
                        ], $this->successStatus);
                    }
            }
        }
        else{
            return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }

    

    public function getnearbyResorts(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
            $validator = Validator::make($request->all(), [
                'lat' => 'required',
                'lon' => 'required',
            ]);
            if ($validator->fails()) {
                return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
            }
            else{
                $lat = $saveArray['lat'];
                $long = $saveArray['lon'];
                // $resort = DB::select("SELECT * , (3956 * 2 * ASIN(SQRT( POWER(SIN(( $lat - latitude) *  pi()/180 / 2), 2) +COS( $lat * pi()/180) * COS(latitude * pi()/180) * POWER(SIN(( $long - longitude) * pi()/180 / 2), 2) ))) as distance from resorts having  distance <= 10 order by distance");
                $resorts = DB::table('resorts')->select('*', DB::raw(sprintf('(6371 * acos(cos(radians(%1$.7f)) * cos(radians(latitude)) * cos(radians(longitude) - radians(%2$.7f)) + sin(radians(%1$.7f)) * sin(radians(latitude)))) AS distance',
                    $request->input('lat'),$request->input('lon'))))
                ->having('distance', '<', 50)
                ->orderBy('distance', 'asc')
                ->get();
                $resortArray = [];
                if(count($resorts)){
                    foreach($resorts as $ls){
                        $image = url('/resort_images',$ls->image);
                           $countryName = Country::where(['id'=>$ls->country])->first();
                           $stateName = State::where(['id'=>$ls->state])->first();
                           $listing['resort_id'] = (String)$ls->id;
                           $listing['resort_name'] = $ls->resort_name;
                           $listing['country_id'] = $ls->country;
                           $listing['country'] = $countryName->name;
                           $listing['state_id'] = $ls->state;
                           $listing['state'] = $stateName->name;
                           $listing['image'] = $image;
                           $listing['latitude'] = $ls->latitude;
                           $listing['longitude'] = $ls->longitude;
                           $listing['lift_names'] = $ls->lift_names;
                           $listing['lift_numbers'] = $ls->lift_numbers;
                           $listing['run_names'] = $ls->run_names;
                           $listing['run_numbers'] = $ls->run_numbers;
                           $listing['created_at'] = date('Y-m-d h:i:s');
                           $resortArray[] = $listing;
                       }
                       return response()->json([
                        'status'=>'1',
                        'message'=>'Resorts fetched successfully',
                        'listing'=>$resortArray
                    ], $this->successStatus);
                   }
                   else{
                    return response()->json([
                        'message'=>'No resorts',
                        'status'=>'0'
                    ], $this->successStatus);
                }
            }
        }
        else{
            return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }


    /*Get all Resort*/
    public function getResorts(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
            $resorts = Resort::all();
            $resortArray = [];
            if(count($resorts)){
                foreach($resorts as $ls){
                    $image = url('/resort_images',$ls['image']);
                    $countryName = Country::where(['id'=>$ls['country']])->first();
                    $stateName = State::where(['id'=>$ls['state']])->first();
                    $listing['resort_id'] = (String)$ls['id'];
                    $listing['resort_name'] = $ls['resort_name'];
                    $listing['country_id'] = $ls['country'];
                    $listing['country'] = $countryName['name'];
                    $listing['state_id'] = $ls['state'];
                    $listing['state'] = $stateName['name'];
                    $listing['image'] = $image;
                    $listing['lift_names'] = $ls['lift_names'];
                    $listing['lift_numbers'] = $ls['lift_numbers'];
                    $listing['run_names'] = $ls['run_names'];
                    $listing['run_numbers'] = $ls['run_numbers'];
                    $listing['created_at'] = date('Y-m-d h:i:s');
                    $resortArray[] = $listing;
                }
                return response()->json([
                    'status'=>'1',
                    'message'=>'Resorts fetched successfully',
                    'listing'=>$resortArray
                ], $this->successStatus);
            }
            else{
                return response()->json([
                    'message'=>'No resorts',
                    'status'=>'0'
                ], $this->badrequest);
            }
        }
        else{
            return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }

    /*Country States and Resorts*/

    public function countrystateResort(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
            $rules = array(
                'user_id' => 'required_without_all:state,country',
                'country' => 'required_without_all:user_id,state',
                'state' => 'required_without_all:user_id,country'
            );
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
            }
            else{
                if($saveArray['user_id'] == $userExist['id']){
                    $countries = Country::all();
                    $countryArray = [];
                    if(count($countries)){
                        foreach($countries as $ls){
                            $listing['country_id'] = (String)$ls['id'];
                            $listing['country_name'] = $ls['name'];
                            $listing['created_at'] = date('Y-m-d h:i:s');
                            $countryArray[] = $listing;
                        }
                        return response()->json([
                            'status'=>'1',
                            'message'=>'Countries fetched successfully',
                            'countryListing'=>$countryArray
                        ], $this->successStatus);
                    }
                    else{
                        return response()->json([
                            'message'=>'No Country Found',
                            'status'=>'0'
                        ], $this->successStatus);
                    }
                }
                elseif(in_array($saveArray['country'], ['USA','Canada'], true ) ){
                    $country_id = Country::where(['name'=>$saveArray['country']])->select('id')->first();
                    $resorts = Resort::select('state')->groupBy('state')->get();
                    $state_id = $resorts;
                    $states = State::where(['country_id'=>$country_id['id']])
                    ->whereIn('id', $state_id)
                    ->select('id','name')->get();
                    $stateArray = [];
                    if(count($states)){
                        foreach($states as $state){
                            $listing['state_id'] = (String)$state['id'];
                            $listing['state_name'] = $state['name'];
                            $listing['created_at'] = date('Y-m-d h:i:s');
                            $stateArray[] = $listing;
                        }
                        return response()->json([
                            'status'=>'1',
                            'message'=>'States fetched successfully',
                            'stateListing'=>$stateArray
                        ], $this->successStatus);
                    }
                    else{
                        return response()->json([
                            'message'=>'No State Found',
                            'status'=>'0'
                        ], $this->successStatus);
                    }
                }
                elseif($saveArray['state']){
                    $checkState = State::where(['name'=>$saveArray['state']])->select('id','name')->first();
                    if ($checkState) {
                        $getResort = Resort::where(['state'=>$checkState['id']])->get();
                        $resortArray = [];
                        if(count($getResort)){
                            foreach($getResort as $ls){
                                $checkIsAdmin = Permission::where(['resort_id'=>$ls['id']])->where(['user_id'=>$userExist['id']])->where(['role_type'=>'ADM'])->first();
                                if ($checkIsAdmin) {
                                    $isAdmin = 'true';
                                }else{
                                    $isAdmin = 'false';
                                }
                                $checkIsFav = Fav_resort::where('resort_id',$ls['id'])
                                ->where('user_id',$userExist['id'])
                                ->where('favourite','=','1')
                                ->first();
                                if ($checkIsFav['favourite'] == 1) {
                                    $isFav = 'Yes';
                                }else{
                                    $isFav = 'No';
                                }
                                $image = url('/resort_images',$ls['image']);
                                $countryName = Country::where(['id'=>$ls['country']])->first();
                                $stateName = State::where(['id'=>$ls['state']])->first();
                                $listing['is_favourite'] = $isFav;
                                $listing['isAdmin'] = $isAdmin;
                                $listing['resort_id'] = (String)$ls['id'];
                                $listing['resort_name'] = $ls['resort_name'];
                                $listing['address'] = $ls['address'];
                                $listing['phone_no'] = $ls['phone_no'];
                                $listing['toplat'] = $ls['top_latitude']?$ls['top_latitude']:'';
                                $listing['toplong'] = $ls['top_longitude']?$ls['top_longitude']:'';
                                $listing['lat'] = $ls['latitude'];
                                $listing['long'] = $ls['longitude'];
                                $listing['country_id'] = $ls['country'];
                                $listing['country'] = $countryName['name'];
                                $listing['state_id'] = $ls['state'];
                                $listing['state'] = $stateName['name'];
                                $listing['bcolor'] = $ls['bcolor'];
                                $listing['fcolor'] = $ls['fcolor'];
                                $listing['image'] = $image;
                                $listing['lift_names'] = $ls['lift_names'];
                                $listing['lift_numbers'] = $ls['lift_numbers'];
                                $listing['run_names'] = $ls['run_names'];
                                $listing['run_numbers'] = $ls['run_numbers'];
                                $listing['created_at'] = date('Y-m-d h:i:s');
                                $resortArray[] = $listing;
                            }
                            return response()->json([
                                'status'=>'1',
                                'message'=>'Resorts fetched successfully',
                                'resortListing'=>$resortArray
                            ], $this->successStatus);
                        }
                        else{
                            return response()->json([
                                'message'=>'No resorts',
                                'status'=>'1',
                                'resortListing'=>$resortArray
                            ], $this->successStatus);
                        }
                    }
                    else{
                        return response()->json([
                                'message'=>'Error',
                                'status'=>'0'
                            ], $this->badrequest);
                    }
                }
                else{
                    return response()->json([
                            'message'=>'Error',
                            'status'=>'0'
                        ], $this->badrequest);
                }
            }
        }
        else{
            return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }

    public function addTofav(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
            $validator = Validator::make($request->all(), [
                'resort_id' => 'required',
            ]);
            if ($validator->fails()) {
                return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
            }
            else{
                $checkResort = Resort::where('id',$saveArray['resort_id'])->first();
                if ($checkResort) {
                    $checkfavResort = Fav_resort::where('resort_id',$saveArray['resort_id'])
                    ->where('user_id',$userExist['id'])
                    ->where('favourite','=','1')
                    ->first();
                    if ($checkfavResort) {
                        $unfavorite_resort = Fav_resort::where('resort_id',$saveArray['resort_id'])
                        ->where('user_id',$userExist['id'])
                        ->where('favourite','=','1')
                        ->delete();
                        return response()->json([
                            'status'=>'1',
                            'message'=>'Resort removed from favourite'
                        ], $this->successStatus);
                    }
                    else {
                        $insertData = [
                            'user_id' => $userExist['id'],
                            'resort_id' => $saveArray['resort_id'],
                            'favourite' => '1',
                            'created_at' => date('Y-m-d H:i:s')
                        ];
                        $insert = Fav_resort::insert($insertData);
                        return response()->json([
                            'status'=>'1',
                            'message'=>'Resort added to favourite'
                        ], $this->successStatus);
                    }
                }else {
                    return response()->json([
                        'status'=>'1',
                        'message'=>'No resort found'
                    ], $this->successStatus);
                } 
            }
        }
        else{
            return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }

    public function getFav(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
            $fav_resorts = Fav_resort::where('user_id',$userExist['id'])
            ->where('favourite','=','1')
            ->select('resort_id')
            ->get();
            // print_r(json_decode($fav_resorts));die();
            $resorts = Resort::whereIn('id', $fav_resorts)->get();
            // print_r(json_decode($resorts));die();
            $resortArray = [];
            if(count($resorts)){
                foreach($resorts as $ls){
                    $checkIsAdmin = Permission::where(['resort_id'=>$ls['id']])
                    ->where(['user_id'=>$userExist['id']])
                    ->first();
                    // print_r($checkIsAdmin['role_type']);die();
                    if ($checkIsAdmin['role_type'] == 'ADM') {
                        $isAdmin = 'true';
                        
                    }else{
                        $isAdmin = 'false';
                        
                    }
                    if ($checkIsAdmin['role_type'] == 'PAT') {
                        $isPat = 'true';
                        
                    }else{
                        $isPat = 'false';
                        
                    }
                    if ($checkIsAdmin['role_type'] == 'EDI') {
                        $isEdi = 'true';
                        
                    }else{
                        $isEdi = 'false';
                        
                    }
                    if ($checkIsAdmin['role_type'] == 'LIF') {
                        $isLif = 'true';
                        
                    }else{
                        $isLif = 'false';
                        
                    }
                    $image = url('/resort_images',$ls['image']);
                    $countryName = Country::where(['id'=>$ls['country']])->first();
                    $stateName = State::where(['id'=>$ls['state']])->first();
                    $listing['isAdmin'] = $isAdmin;
                    $listing['isPatroller'] = $isPat;
                    $listing['isEditor'] = $isEdi;
                    $listing['isLifty'] = $isLif;
                    $listing['resort_id'] = (String)$ls['id'];
                    $listing['resort_name'] = $ls['resort_name'];
                    $listing['phone_no'] = $ls['phone_no'];
                    $listing['country_id'] = $ls['country'];
                    $listing['country'] = $countryName['name'];
                    $listing['state_id'] = $ls['state'];
                    $listing['state'] = $stateName['name'];
                    $listing['bcolor'] = $ls['bcolor'];
                    $listing['fcolor'] = $ls['fcolor'];
                    $listing['image'] = $image;
                    $listing['latitude'] = $ls['latitude'];
                    $listing['longitude'] = $ls['longitude'];
                    $listing['top_latitude'] = $ls['top_latitude'];
                    $listing['top_longitude'] = $ls['top_longitude'];
                    $listing['lift_names'] = $ls['lift_names'];
                    $listing['lift_numbers'] = $ls['lift_numbers'];
                    $listing['run_names'] = $ls['run_names'];
                    $listing['run_numbers'] = $ls['run_numbers'];
                    $listing['created_at'] = date('Y-m-d h:i:s');
                    $resortArray[] = $listing;
                }
                return response()->json([
                    'status'=>'1',
                    'message'=>'Resorts fetched successfully',
                    'fav_resortListing'=>$resortArray
                ], $this->successStatus);
            }
            else{
                return response()->json([
                    'message'=>'No resorts',
                    'status'=>'1',
                ], $this->successStatus);
            }
        }
        else{
            return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }

    public function getCodes(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
            $validator = Validator::make($request->all(), [
                'resort_id' => 'required',
            ]);
            if ($validator->fails()) {
                return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
            }
            else{
                $checkResort = Resort::where(['id'=>$saveArray['resort_id']])->first();
                if ($checkResort) {
                    $getCodes = Access_code::where(['resort_id'=>$saveArray['resort_id']])->first();
                $code_details = array(
                    'id'=>(String)$getCodes['id'],
                    'resort_administrator'=>$getCodes['resort_administrator'],
                    'resort_editor'=>$getCodes['resort_editor'],
                    'patroller'=>$getCodes['patroller'],
                    'lifty'=>$getCodes['lifty'],
                    'created_at'=>$getCodes['created_at']->format('Y-m-d'),
                );
                return response()->json([
                    'status'=>'1',
                    'message'=>'Details fetched successfully',
                    'access_codes'=>$code_details
                  ], $this->successStatus);
                }else{
                    return response()->json([
                    'status'=>'0',
                    'message'=>'Resort does not exists'
                  ], $this->successStatus);
                }
            }
        }
        else{
            return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }



    // public function regenerateCodes_test(Request $request)
    // {
    //     $saveArray = $request->all();
    //     $token = $request->header('token');
    //     $userExist = User::where(['remember_token'=>$token])->first();
    //     if($userExist){
    //         $validator = Validator::make($request->all(), [
    //             'resort_id' => 'required',
    //             'refreshfor' => 'required',
    //         ]);
    //         if ($validator->fails()) {
    //             return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
    //         }

    //         else{
    //             // $checkAdmin = Permission::where(['resort_id'=>$saveArray['resort_id']])
    //             // ->where(['user_id'=>$userExist['id']])
    //             // ->where(['role_type'=>'ADM'])
    //             // ->first();
    //             $checkAdmin = Permission::where(['user_id'=>$userExist['id']])
    //             ->first();
    //             if ($checkAdmin) {
    //                 if ($saveArray['refreshfor'] == 'ADM') {
    //                 $ADMrandomname = 'ADM';
    //                 $ADMgetRstring = ResortAPIController::ADMgenerateRandomString();
    //                 $adminCode = $ADMrandomname.$ADMgetRstring;
    //                 $updateData = [
    //                     'resort_administrator'=> $adminCode,
    //                     'ambassador'=>Null,
    //                     'updated_at'=>date('Y-m-d h:i:s'),
    //                 ];
    //                 $update = Access_code::where(['resort_id'=>$saveArray['resort_id']])->update($updateData);
    //                 /*remove all permission for current resort id and type*/

    //                 /*$removePermission = Permission::where(['resort_id'=>$saveArray['resort_id']])->where(['role_type'=>$saveArray['refreshfor']])->delete();*/

    //                 /*remove all permission for current resort id and type*/
    //                 if($update){
    //                     return response()->json([
    //                         'status'=>'1',
    //                         'message'=>'refreshed successfully '
    //                     ], $this->successStatus);
    //                   }
    //                   else{
    //                     return response()->json([
    //                         'status'=>'0',
    //                         'message'=>'error'
    //                     ], $this->badrequest);
    //                 }
    //             }elseif ($saveArray['refreshfor'] == 'EDI') {
    //                 $EDIrandomname = 'EDI';
    //                 $EDIgetRstring = ResortAPIController::EDIgenerateRandomString();
    //                 $editorCode = $EDIrandomname.$EDIgetRstring;
    //                 $updateData = [
    //                     'resort_editor'=>$editorCode,
    //                     'ambassador'=>Null,
    //                     'updated_at'=>date('Y-m-d h:i:s'),
    //                 ];
    //                 $update = Access_code::where(['resort_id'=>$saveArray['resort_id']])->update($updateData);
    //                 /*remove all permission for current resort id and type*/

    //                 /*$removePermission = Permission::where(['resort_id'=>$saveArray['resort_id']])->where(['role_type'=>$saveArray['refreshfor']])->delete();*/

    //                 /*remove all permission for current resort id and type*/
    //                 if($update){
    //                     return response()->json([
    //                         'status'=>'1',
    //                         'message'=>'refreshed successfully '
    //                     ], $this->successStatus);
    //                   }
    //                   else{
    //                     return response()->json([
    //                         'status'=>'0',
    //                         'message'=>'error'
    //                     ], $this->badrequest);
    //                 }
    //             }elseif ($saveArray['refreshfor'] == 'PAT') {
    //                 $PATrandomname = 'PAT';
    //                 $PATgetRstring = ResortAPIController::EDIgenerateRandomString();
    //                 $patrollerCode = $PATrandomname.$PATgetRstring;
    //                 $updateData = [
    //                     'patroller'=>$patrollerCode,
    //                     'ambassador'=>Null,
    //                     'updated_at'=>date('Y-m-d h:i:s'),
    //                 ];
    //                 $update = Access_code::where(['resort_id'=>$saveArray['resort_id']])->update($updateData);
    //                 /*remove all permission for current resort id and type*/

    //                 /*$removePermission = Permission::where(['resort_id'=>$saveArray['resort_id']])->where(['role_type'=>$saveArray['refreshfor']])->delete();*/

    //                 /*remove all permission for current resort id and type*/
    //                 if($update){
    //                     return response()->json([
    //                         'status'=>'1',
    //                         'message'=>'refreshed successfully '
    //                     ], $this->successStatus);
    //                   }
    //                   else{
    //                     return response()->json([
    //                         'status'=>'0',
    //                         'message'=>'error'
    //                     ], $this->badrequest);
    //                 }
    //             }elseif ($saveArray['refreshfor'] == 'LIF') {
    //                 $LIFrandomname = 'LIF';
    //                 $LIFgetRstring = ResortAPIController::LIFgenerateRandomString();
    //                 $liftyCode = $LIFrandomname.$LIFgetRstring;
    //                 $updateData = [
    //                     'lifty'=>$liftyCode,
    //                     'ambassador'=>Null,
    //                     'updated_at'=>date('Y-m-d h:i:s'),
    //                 ];
    //                 $update = Access_code::where(['resort_id'=>$saveArray['resort_id']])->update($updateData);
    //                 /*remove all permission for current resort id and type*/

    //                 /*$removePermission = Permission::where(['resort_id'=>$saveArray['resort_id']])->where(['role_type'=>$saveArray['refreshfor']])->delete();*/

    //                 /*remove all permission for current resort id and type*/
    //                 if($update){
    //                     return response()->json([
    //                         'status'=>'1',
    //                         'message'=>'refreshed successfully '
    //                     ], $this->successStatus);
    //                   }
    //                   else{
    //                     return response()->json([
    //                         'status'=>'0',
    //                         'message'=>'error'
    //                     ], $this->badrequest);
    //                 }
    //             }
    //             }else{
    //                 return response()->json([
    //                         'status'=>'1',
    //                         'message'=>'Only Admin can regenerate codes again'
    //                     ], $this->successStatus);
    //             }
    //         }
    //     }
    //     else{
    //         return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
    //     }
    // }

    public function regenerateCodes(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
            $validator = Validator::make($request->all(), [
                'resort_id' => 'required',
                'refreshfor' => 'required',
            ]);
            if ($validator->fails()) {
                return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
            }

            else{
                $checkAdmin = Permission::where(['resort_id'=>$saveArray['resort_id']])->where(['user_id'=>$userExist['id']])->where(['role_type'=>'ADM'])->first();
                if ($checkAdmin) {
                    if ($saveArray['refreshfor'] == 'ADM') {
                    $ADMrandomname = 'ADM';
                    $ADMgetRstring = ResortAPIController::ADMgenerateRandomString();
                    $adminCode = $ADMrandomname.$ADMgetRstring;
                    $updateData = [
                        'resort_administrator'=> $adminCode,
                        'ambassador'=>Null,
                        'updated_at'=>date('Y-m-d h:i:s'),
                    ];
                    $update = Access_code::where(['resort_id'=>$saveArray['resort_id']])->update($updateData);
                    /*remove all permission for current resort id and type*/

                    /*$removePermission = Permission::where(['resort_id'=>$saveArray['resort_id']])->where(['role_type'=>$saveArray['refreshfor']])->delete();*/

                    /*remove all permission for current resort id and type*/
                    if($update){
                        return response()->json([
                            'status'=>'1',
                            'message'=>'refreshed successfully '
                        ], $this->successStatus);
                      }
                      else{
                        return response()->json([
                            'status'=>'0',
                            'message'=>'error'
                        ], $this->badrequest);
                    }
                }elseif ($saveArray['refreshfor'] == 'EDI') {
                    $EDIrandomname = 'EDI';
                    $EDIgetRstring = ResortAPIController::EDIgenerateRandomString();
                    $editorCode = $EDIrandomname.$EDIgetRstring;
                    $updateData = [
                        'resort_editor'=>$editorCode,
                        'ambassador'=>Null,
                        'updated_at'=>date('Y-m-d h:i:s'),
                    ];
                    $update = Access_code::where(['resort_id'=>$saveArray['resort_id']])->update($updateData);
                    /*remove all permission for current resort id and type*/

                    /*$removePermission = Permission::where(['resort_id'=>$saveArray['resort_id']])->where(['role_type'=>$saveArray['refreshfor']])->delete();*/

                    /*remove all permission for current resort id and type*/
                    if($update){
                        return response()->json([
                            'status'=>'1',
                            'message'=>'refreshed successfully '
                        ], $this->successStatus);
                      }
                      else{
                        return response()->json([
                            'status'=>'0',
                            'message'=>'error'
                        ], $this->badrequest);
                    }
                }elseif ($saveArray['refreshfor'] == 'PAT') {
                    $PATrandomname = 'PAT';
                    $PATgetRstring = ResortAPIController::EDIgenerateRandomString();
                    $patrollerCode = $PATrandomname.$PATgetRstring;
                    $updateData = [
                        'patroller'=>$patrollerCode,
                        'ambassador'=>Null,
                        'updated_at'=>date('Y-m-d h:i:s'),
                    ];
                    $update = Access_code::where(['resort_id'=>$saveArray['resort_id']])->update($updateData);
                    /*remove all permission for current resort id and type*/

                    /*$removePermission = Permission::where(['resort_id'=>$saveArray['resort_id']])->where(['role_type'=>$saveArray['refreshfor']])->delete();*/

                    /*remove all permission for current resort id and type*/
                    if($update){
                        return response()->json([
                            'status'=>'1',
                            'message'=>'refreshed successfully '
                        ], $this->successStatus);
                      }
                      else{
                        return response()->json([
                            'status'=>'0',
                            'message'=>'error'
                        ], $this->badrequest);
                    }
                }elseif ($saveArray['refreshfor'] == 'LIF') {
                    $LIFrandomname = 'LIF';
                    $LIFgetRstring = ResortAPIController::LIFgenerateRandomString();
                    $liftyCode = $LIFrandomname.$LIFgetRstring;
                    $updateData = [
                        'lifty'=>$liftyCode,
                        'ambassador'=>Null,
                        'updated_at'=>date('Y-m-d h:i:s'),
                    ];
                    $update = Access_code::where(['resort_id'=>$saveArray['resort_id']])->update($updateData);
                    /*remove all permission for current resort id and type*/

                    /*$removePermission = Permission::where(['resort_id'=>$saveArray['resort_id']])->where(['role_type'=>$saveArray['refreshfor']])->delete();*/

                    /*remove all permission for current resort id and type*/
                    if($update){
                        return response()->json([
                            'status'=>'1',
                            'message'=>'refreshed successfully '
                        ], $this->successStatus);
                      }
                      else{
                        return response()->json([
                            'status'=>'0',
                            'message'=>'error'
                        ], $this->badrequest);
                    }
                }
                }else{
                    return response()->json([
                            'status'=>'1',
                            'message'=>'Only Admin can regenerate codes again'
                        ], $this->successStatus);
                }
            }
        }
        else{
            return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }

    public  function ADMgenerateRandomString($length = 7) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public  function EDIgenerateRandomString($length = 7) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public  function PATgenerateRandomString($length = 7) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public  function LIFgenerateRandomString($length = 7) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    /*edit resort by admin*/

    public function adminSettings(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
            $validator = Validator::make($request->all(), [
                'resort_id' => 'required',
            ]);
            if ($validator->fails()) {
                return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
            }
            else{
                $checkPermission = Permission::where(['user_id'=>$userExist['id']])->where(['resort_id'=>$saveArray['resort_id']])->where(['role_type'=>'ADM'])->first();
                if ($checkPermission) {
                    $resortData = Resort::where(['id'=>$saveArray['resort_id']])->first();
                    if ($request->hasFile('image')) {
                        $file = time().$request->image->getClientOriginalName();
                        $request->image->move(public_path('/resort_images') . '/', $file);
                    }
                    else{
                        $file = $resortData['image'];
                    }
                    $runGrades = array($saveArray['run_grades']);
                    $run_grades = implode(",", $runGrades);
                    $updateData = [
                        'bcolor'=>$saveArray['bcolor']?$saveArray['bcolor']:$resortData['bcolor'],
                        'fcolor'=>$saveArray['fcolor']?$saveArray['fcolor']:$resortData['fcolor'],
                        'image'=>$file,
                        'website'=>$saveArray['website']?$saveArray['website']:$resortData['website'],
                        'phone_no'=>$saveArray['phone_no']?$saveArray['phone_no']:$resortData['phone_no'],
                        'lift_names'=>$saveArray['lift_names']?$saveArray['lift_names']:$resortData['lift_names'],
                        'lift_numbers'=>$saveArray['lift_numbers']?$saveArray['lift_numbers']:$resortData['lift_numbers'],
                        'run_names'=>$saveArray['run_names']?$saveArray['run_names']:$resortData['run_names'],
                        'run_numbers'=>$saveArray['run_numbers']?$saveArray['run_numbers']:$resortData['run_numbers'],
                        'run_grades'=>$run_grades?$run_grades:$resortData['run_grades'],
                    ];
                    $updateddata = Resort::where(['id'=>$saveArray['resort_id']])->update($updateData);
                }
                else{
                    return response()->json([
                        'status'=>'0',
                        'message'=>'You dont have valid permissions to do this please contact to admin'
                    ], $this->successStatus);
                }
                if($updateddata){
                        return response()->json([
                            'status'=>'1',
                            'message'=>'Resort Updated Successfully'
                        ], $this->successStatus);
                      }
                      else{
                    return response()->json([
                        'status'=>'0',
                        'message'=>'error'
                    ], $this->badrequest);
                }
            }
        }
        else{
            return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }

    public function getadminSettings(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
            $validator = Validator::make($request->all(), [
                'resort_id' => 'required',
            ]);
            if ($validator->fails()) {
                return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
            }
            else{
                $resortDetails = Resort::where(['id'=>$saveArray['resort_id']])->first();
                if ($resortDetails) {
                    $image = url('/resort_images',$resortDetails['image']);
                    $resort_details = array(
                        'id'=>(String)$resortDetails['id'],
                        'resort_name'=>$resortDetails['resort_name'],
                        'image'=>$image,
                        'website'=>$resortDetails['website']?$resortDetails['website']:'',
                        'phone_no'=>$resortDetails['phone_no']?$resortDetails['phone_no']:'',
                        'run_grades'=>$resortDetails['run_grades']?$resortDetails['run_grades']:'',
                        'bcolor'=>$resortDetails['bcolor']?$resortDetails['bcolor']:'',
                        'fcolor'=>$resortDetails['fcolor']?$resortDetails['fcolor']:'',
                        'lift_names'=>$resortDetails['lift_names'],
                        'lift_numbers'=>$resortDetails['lift_numbers'],
                        'run_names'=>$resortDetails['run_names'],
                        'run_numbers'=>$resortDetails['run_numbers'],
                        'created_at'=>$resortDetails['created_at'],
                    );
                    return response()->json([
                        'status'=>'1',
                        'message'=>'Data fetched successfully',
                        'resort_details'=>$resort_details
                    ], $this->successStatus);
                }else{
                    return response()->json([
                        'status'=>'0',
                        'message'=>'No Data'
                    ], $this->successStatus);
                }
            }
        }
        else{
            return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }

/*-----------------------like api for Resort ,Run ,Lift-----------------------*/  

    public function thumbsup(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
            $validator = Validator::make($request->all(), [
                'id' => 'required',             /*RunID , LiftID & ResortID*/
                'event_type' => 'required',     /*Run , Lift & Resort*/
                'like_for' => 'required',       /*values for all*/
            ]);
            if ($validator->fails()) {
                return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
            }
            else{
                $dynamicField = $saveArray['like_for'];
                if ($saveArray['event_type'] == 'resort') {
                    $getvalue = Resort_detail::where(['resort_id'=>$saveArray['id']])
                    ->select($dynamicField)
                    ->first();
                    $updatedValue = $getvalue->$dynamicField;
                    $checkCount = Like::where(['resort_id'=>$saveArray['id']])
                    ->where(['event_type'=>$saveArray['event_type']])
                    ->where(['like_for'=>$saveArray['like_for']])
                    ->where(['like_for_value'=>$updatedValue])
                    ->count();
                    if ($checkCount >= '3') {
                        $updateData = [
                            'deleted_at'=>date('Y-m-d h:i:s'),
                        ];
                        $update = Like::where(['resort_id'=>$saveArray['id']])->update($updateData);
                        return response()->json([
                            'status'=>'1',
                            'message'=>'data is already updated please check again',
                            ], $this->successStatus);
                    }else{
                    $insertData = [
                        'user_id'=>$userExist['id'],
                        'resort_id'=> $saveArray['id'],
                        'event_type'=>$saveArray['event_type'],
                        'like_for'=>$saveArray['like_for'],
                        'like_for_value'=>$updatedValue,
                        'is_like'=>'1',
                        'created_at'=>date('Y-m-d h:i:s'),
                      ];
                      $insertedID = Like::insertGetId($insertData);
                      $likecount = Like::where('resort_id',$saveArray['id'])
                      ->where('event_type',$saveArray['event_type'])
                      ->where('like_for',$saveArray['like_for'])
                      ->where(['like_for_value'=>$updatedValue])
                      ->count();
                      if ($likecount == '3') {
                          $confirmed_status = 'true';
                      }
                      else{
                        $confirmed_status = 'false';
                      }
                      if ($insertedID) {
                        return response()->json([
                            'status'=>'1',
                            'message'=>'Liked',
                            'count'=>$likecount,
                            'confirmed_status'=>$confirmed_status,
                            ], $this->successStatus);
                      }else{
                        return response()->json([
                            'status'=>'1',
                            'message'=>'Error',
                        ], $this->successStatus);
                    }
                }
                }elseif ($saveArray['event_type'] == 'run') {
                    $getvalue = Run::where(['id'=>$saveArray['id']])
                    ->select($dynamicField)
                    ->first();
                    $updatedValue = $getvalue->$dynamicField;
                    $checkCount = Like::where(['run_id'=>$saveArray['id']])
                    ->where(['event_type'=>$saveArray['event_type']])
                    ->where(['like_for'=>$saveArray['like_for']])
                    ->where(['like_for_value'=>$updatedValue])
                    ->count();
                    if ($checkCount >= '3') {
                        $updateData = [
                            'deleted_at'=>date('Y-m-d h:i:s'),
                        ];
                        $update = Like::where(['run_id'=>$saveArray['id']])->update($updateData);
                        return response()->json([
                            'status'=>'1',
                            'message'=>'data is already updated please check again',
                            ], $this->successStatus);
                    }else{
                    $insertData = [
                        'user_id'=>$userExist['id'],
                        'run_id'=> $saveArray['id'],
                        'event_type'=>$saveArray['event_type'],
                        'like_for'=>$saveArray['like_for'],
                        'like_for_value'=>$updatedValue,
                        'is_like'=>'1',
                        'created_at'=>date('Y-m-d h:i:s'),
                      ];
                      $insertedID = Like::insertGetId($insertData);
                      $likecount = Like::where('run_id',$saveArray['id'])
                      ->where('event_type',$saveArray['event_type'])
                      ->where('like_for',$saveArray['like_for'])
                      ->where(['like_for_value'=>$updatedValue])
                      ->count();
                      if ($likecount == '3') {
                          $confirmed_status = 'true';
                      }
                      else{
                        $confirmed_status = 'false';
                      }
                      if ($insertedID) {
                        return response()->json([
                            'status'=>'1',
                            'message'=>'Liked',
                            'count'=>$likecount,
                            'confirmed_status'=>$confirmed_status,
                            ], $this->successStatus);
                      }else{
                        return response()->json([
                            'status'=>'1',
                            'message'=>'Error',
                        ], $this->successStatus);
                    }
                }
                }elseif ($saveArray['event_type'] == 'lift') {
                    /*get update value*/
                    $getvalue = Lift::where(['id'=>$saveArray['id']])
                    ->select($dynamicField)
                    ->first();
                    $updatedValue = $getvalue->$dynamicField;
                    $checkCount = Like::where(['lift_id'=>$saveArray['id']])
                    ->where(['event_type'=>$saveArray['event_type']])
                    ->where(['like_for'=>$saveArray['like_for']])
                    ->where(['like_for_value'=>$updatedValue])
                    ->count();
                    if ($checkCount >= '3') {
                        $updateData = [
                            'deleted_at'=>date('Y-m-d h:i:s'),
                        ];
                        $update = Like::where(['lift_id'=>$saveArray['id']])->update($updateData);
                        return response()->json([
                            'status'=>'1',
                            'message'=>'data is already updated please check again',
                            ], $this->successStatus);
                    }else{
                    $insertData = [
                        'user_id'=>$userExist['id'],
                        'lift_id'=> $saveArray['id'],
                        'event_type'=>$saveArray['event_type'],
                        'like_for'=>$saveArray['like_for'],
                        'like_for_value'=>$updatedValue,
                        'is_like'=>'1',
                        'created_at'=>date('Y-m-d h:i:s'),
                      ];
                      $insertedID = Like::insertGetId($insertData);
                      $likecount = Like::where('lift_id',$saveArray['id'])
                      ->where('event_type',$saveArray['event_type'])
                      ->where('like_for',$saveArray['like_for'])
                      ->where(['like_for_value'=>$updatedValue])
                      ->count();
                      if ($likecount == '3') {
                          $confirmed_status = 'true';
                      }
                      else{
                        $confirmed_status = 'false';
                      }
                      if ($insertedID) {
                        return response()->json([
                            'status'=>'1',
                            'message'=>'Liked',
                            'count'=>$likecount,
                            'confirmed_status'=>$confirmed_status,
                            ], $this->successStatus);
                      }else{
                        return response()->json([
                            'status'=>'1',
                            'message'=>'Error',
                        ], $this->successStatus);
                    }
                }
                }
            }
        }
        else{
            return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }

    public function getthumbscount(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
            $validator = Validator::make($request->all(), [
                'id' => 'required',
                'event_type' => 'required',    /*Run , Lift & Resort*/
                'like_for' => 'required',
            ]);
            if ($validator->fails()) {
                return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
            }
            else{
                $dynamicField = $saveArray['like_for'];
                if ($saveArray['event_type'] == 'resort') {
                    $getvalue = Resort_detail::where(['resort_id'=>$saveArray['id']])
                    ->select($dynamicField)
                    ->first();
                    if ($getvalue) {
                        $updatedValue = $getvalue->$dynamicField;
                        $getvalues = Resort_history::where(['resort_id'=>$saveArray['id']])
                        ->where($dynamicField,'!=','Null')
                        ->select('user_id',$dynamicField)
                        ->orderBy('created_at','DESC')
                        ->limit(1)->get();
                        if($getvalues->isEmpty()){
                            return response()->json([
                            'status'=>'1',
                            'message'=>'No data yet',
                        ], $this->successStatus);
                        }else{
                            foreach ($getvalues as $value) {
                            $userID = $value['user_id'];
                        }
                        //print_r(json_encode($userID));die();
                        $updatedby_userID = $userID;
                        $username = User::where(['id'=>$updatedby_userID])->select('name')->first();
                        $likecount = Like::where('resort_id',$saveArray['id'])
                        ->where('event_type',$saveArray['event_type'])
                        ->where('like_for',$saveArray['like_for'])
                        ->where(['like_for_value'=>$updatedValue])
                        ->count();
                        //print_r($likecount);die();
                        if ($likecount == '3') {
                                $confirmed_status = 'true';

                                /*Reward for likers who thumbs up for correct value*/
                                $getLikers = Like::where('resort_id',$saveArray['id'])
                                ->where('event_type',$saveArray['event_type'])
                                ->where('like_for',$saveArray['like_for'])
                                ->where(['like_for_value'=>$updatedValue])
                                ->select('user_id')
                                ->get()
                                ->toArray();
                                $concateReason = 'thumbs up for correct value ';
                                $getReason = $concateReason.$saveArray['like_for'];
                                foreach ($getLikers as  $value) {
                                $getRewards = Rewards_point::where('user_id',$value['user_id'])->select('total')->first();
                                $rewardedPoint = $getRewards['total'] + 1;
                                $updateData = [
                                    'total'=> $rewardedPoint,
                                    'updated_at'=>date('Y-m-d h:i:s'),
                                ];
                                $update = Rewards_point::where(['user_id'=>$value['user_id']])->update($updateData);
                                $insertData = [
                                    'user_id' => $value['user_id'],
                                    'resort_id' => $saveArray['id'],
                                    'reward_for' => $saveArray['like_for'],
                                    'snowflake_rewards' => '1',
                                    'reward_status' => '1',
                                    'reason' => $getReason,
                                    'created_at' => date('Y-m-d H:i:s')
                                ];
                                $insert = Snowflake::insert($insertData);
                                }
                                /*Reward for likers who thumbs up for correct value*/
                                
                                /*update resorts edit data 10/5*/
                                $getUpdated_reward = Snowflake::where('resort_id',$saveArray['id'])
                                ->where('user_id',$updatedby_userID)
                                ->where('reward_for',$saveArray['like_for'])
                                ->where('reward_status','0')
                                ->select('snowflake_rewards')
                                ->first();
                                $rPoint = $getUpdated_reward['snowflake_rewards'];
                                $getOldPoints = Rewards_point::where(['user_id'=>$updatedby_userID])->select('total')->first();
                                $getoPoints = $getOldPoints['total'] + $rPoint;
                                $updaterPoint = [
                                    'total'=> $getoPoints,
                                    'updated_at'=>date('Y-m-d h:i:s'),
                                ];
                                $update = Rewards_point::where(['user_id'=>$updatedby_userID])->update($updaterPoint);

                                $updateRewards = [
                                    'reward_status'=>'1',
                                ];
                                $update = Snowflake::where('resort_id',$saveArray['id'])
                                ->where('user_id',$updatedby_userID)
                                ->where('reward_for',$saveArray['like_for'])
                                ->where('reward_status','0')
                                ->update($updateRewards);
                                /*update resorts edit data 10/5*/
                            }
                            else{
                                $confirmed_status = 'false';
                                $updateRewards = [
                                    'reward_status'=>'0',
                                ];
                                $update = Snowflake::where('resort_id',$saveArray['id'])
                                ->where('user_id',$updatedby_userID)
                                ->where('reward_for',$saveArray['like_for'])
                                ->update($updateRewards);
                            }
                            $checkLike = Like::where(['resort_id'=>$saveArray['id']])
                            ->where(['event_type'=>$saveArray['event_type']])
                            ->where(['like_for'=>$saveArray['like_for']])
                            ->where(['like_for_value'=>$updatedValue])
                            ->where(['user_id'=>$userExist['id']])
                            ->first();
                            if ($checkLike) {
                                $likedByme = 'yes';
                            }
                            else{
                                $likedByme = 'no';
                            }
                            return response()->json([
                                'status'=>'1',
                                'message'=>'Liked',
                                'count'=>$likecount?$likecount:'',
                                'confirmed_status'=>$confirmed_status,
                                'updatedby_userID'=>$updatedby_userID,
                                'username'=>$username['name'],
                                'likedByme'=>$likedByme,
                            ], $this->successStatus);
                            }
                        }else{
                            return response()->json([
                                'status'=>'0',
                                'message'=>'No resort found',
                            ], $this->successStatus);
                        }
                }
                elseif ($saveArray['event_type'] == 'run') {
                    $getvalue = Run::where(['id'=>$saveArray['id']])
                    ->select($dynamicField)
                    ->first();
                    if ($getvalue) {
                        $getvalues = Run_history::where(['run_id'=>$saveArray['id']])
                        ->where($dynamicField,'!=','Null')
                        ->select('user_id',$dynamicField)
                        ->orderBy('created_at','DESC')
                        ->limit(1)->get();
                        if($getvalues->isEmpty()){
                            return response()->json([
                                'status'=>'1',
                                'message'=>'No data yet',
                            ], $this->successStatus);
                        }
                        else{
                            foreach ($getvalues as $value) {
                                $userID = $value['user_id'];
                            }
                            $updatedby_userID = $userID;
                            $username = User::where(['id'=>$updatedby_userID])->select('name')->first();
                            $updatedValue = $getvalue->$dynamicField;
                            $likecount = Like::where('run_id',$saveArray['id'])
                            ->where('event_type',$saveArray['event_type'])
                            ->where('like_for',$saveArray['like_for'])
                            ->where(['like_for_value'=>$updatedValue])
                            ->count();
                            if ($likecount == '3') {
                                $confirmed_status = 'true';

                                /*Reward for likers who thumbs up for correct value*/
                                $getLikers = Like::where('run_id',$saveArray['id'])
                                ->where('event_type',$saveArray['event_type'])
                                ->where('like_for',$saveArray['like_for'])
                                ->where(['like_for_value'=>$updatedValue])
                                ->select('user_id')
                                ->get()
                                ->toArray();
                                $concateReason = 'thumbs up for correct value ';
                                $getReason = $concateReason.$saveArray['like_for'];
                                foreach ($getLikers as  $value) {
                                $getRewards = Rewards_point::where('user_id',$value['user_id'])->select('total')->first();
                                $rewardedPoint = $getRewards['total'] + 1;
                                $updateData = [
                                    'total'=> $rewardedPoint,
                                    'updated_at'=>date('Y-m-d h:i:s'),
                                ];
                                $update = Rewards_point::where(['user_id'=>$value['user_id']])->update($updateData);
                                $insertData = [
                                    'user_id' => $value['user_id'],
                                    'run_id' => $saveArray['id'],
                                    'reward_for' => $saveArray['like_for'],
                                    'snowflake_rewards' => '1',
                                    'reward_status' => '1',
                                    'reason' => $getReason,
                                    'created_at' => date('Y-m-d H:i:s')
                                ];
                                $insert = Snowflake::insert($insertData);
                                }
                                /*Reward for likers who thumbs up for correct value*/

                                /*update resorts edit data 10/5*/
                                $getUpdated_reward = Snowflake::where('run_id',$saveArray['id'])
                                ->where('user_id',$updatedby_userID)
                                ->where('reward_for',$saveArray['like_for'])
                                ->where('reward_status','0')
                                ->select('snowflake_rewards')
                                ->first();
                                $rPoint = $getUpdated_reward['snowflake_rewards'];
                                $getOldPoints = Rewards_point::where(['user_id'=>$updatedby_userID])->select('total')->first();
                                $getoPoints = $getOldPoints['total'] + $rPoint;
                                $updaterPoint = [
                                    'total'=> $getoPoints,
                                    'updated_at'=>date('Y-m-d h:i:s'),
                                ];
                                $update = Rewards_point::where(['user_id'=>$updatedby_userID])->update($updaterPoint);

                                $updateRewards = [
                                    'reward_status'=>'1',
                                ];
                                $update = Snowflake::where('run_id',$saveArray['id'])
                                ->where('user_id',$updatedby_userID)
                                ->where('reward_for',$saveArray['like_for'])
                                ->where('reward_status','0')
                                ->update($updateRewards);

                                /*update resorts edit data 10/5*/
                            }
                            else{
                                $confirmed_status = 'false';
                                $updateRewards = [
                                    'reward_status'=>'0',
                                ];
                                $update = Snowflake::where('run_id',$saveArray['id'])
                                ->where('user_id',$updatedby_userID)
                                ->where('reward_for',$saveArray['like_for'])
                                ->update($updateRewards);
                            }
                            $checkLike = Like::where(['run_id'=>$saveArray['id']])
                            ->where(['event_type'=>$saveArray['event_type']])
                            ->where(['like_for'=>$saveArray['like_for']])
                            ->where(['like_for_value'=>$updatedValue])
                            ->where(['user_id'=>$userExist['id']])
                            ->first();
                            if ($checkLike) {
                                $likedByme = 'yes';
                            }
                            else{
                                $likedByme = 'no';
                            }
                            return response()->json([
                                'status'=>'1',
                                'message'=>'Liked',
                                'count'=>$likecount?$likecount:'',
                                'confirmed_status'=>$confirmed_status,
                                'updatedby_userID'=>$updatedby_userID,
                                'username'=>$username['name'],
                                'likedByme'=>$likedByme,
                            ], $this->successStatus);
                        }
                        }else{
                            return response()->json([
                                'status'=>'0',
                                'message'=>'No run found',
                            ], $this->successStatus);
                        }
                    }
                    elseif ($saveArray['event_type'] == 'lift') {
                    $getvalue = Lift::where(['id'=>$saveArray['id']])
                    ->select($dynamicField)
                    ->first();
                    if ($getvalue) {
                        $getvalues = lift_history::where(['lift_id'=>$saveArray['id']])
                        ->where($dynamicField,'!=','Null')
                        ->select('user_id',$dynamicField)
                        ->orderBy('created_at','DESC')
                        ->limit(1)->get();
                        if($getvalues->isEmpty()){
                            return response()->json([
                                'status'=>'1',
                                'message'=>'No data yet',
                            ], $this->successStatus);
                            }
                            else{
                                foreach ($getvalues as $value) {
                                    $userID = $value['user_id'];
                                }
                                $updatedby_userID = $userID;
                                $username = User::where(['id'=>$updatedby_userID])->select('name')->first();
                                $updatedValue = $getvalue->$dynamicField;
                                $likecount = Like::where('lift_id',$saveArray['id'])
                                ->where('event_type',$saveArray['event_type'])
                                ->where('like_for',$saveArray['like_for'])
                                ->where(['like_for_value'=>$updatedValue])
                                ->count();
                                if ($likecount == '3') {
                                    $confirmed_status = 'true';

                                    /*Reward for likers who thumbs up for correct value*/
                                    $getLikers = Like::where('lift_id',$saveArray['id'])
                                    ->where('event_type',$saveArray['event_type'])
                                    ->where('like_for',$saveArray['like_for'])
                                    ->where(['like_for_value'=>$updatedValue])
                                    ->select('user_id')
                                    ->get()
                                    ->toArray();
                                    $concateReason = 'thumbs up for correct value ';
                                    $getReason = $concateReason.$saveArray['like_for'];
                                    foreach ($getLikers as  $value) {
                                        $getRewards = Rewards_point::where('user_id',$value['user_id'])->select('total')->first();
                                        $rewardedPoint = $getRewards['total'] + 1;
                                        $updateData = [
                                            'total'=> $rewardedPoint,
                                            'updated_at'=>date('Y-m-d h:i:s'),
                                        ];
                                        $update = Rewards_point::where(['user_id'=>$value['user_id']])->update($updateData);
                                        $insertData = [
                                            'user_id' => $value['user_id'],
                                            'lift_id' => $saveArray['id'],
                                            'reward_for' => $saveArray['like_for'],
                                            'snowflake_rewards' => '1',
                                            'reward_status' => '1',
                                            'reason' => $getReason,
                                            'created_at' => date('Y-m-d H:i:s')
                                        ];
                                        $insert = Snowflake::insert($insertData);
                                    }
                                    /*Reward for likers who thumbs up for correct value*/

                                    /*update resorts edit data 10/5*/
                                    $getUpdated_reward = Snowflake::where('lift_id',$saveArray['id'])
                                    ->where('user_id',$updatedby_userID)
                                    ->where('reward_for',$saveArray['like_for'])
                                    ->where('reward_status','0')
                                    ->select('snowflake_rewards')
                                    ->first();
                                    $rPoint = $getUpdated_reward['snowflake_rewards'];
                                    $getOldPoints = Rewards_point::where(['user_id'=>$updatedby_userID])->select('total')->first();
                                    $getoPoints = $getOldPoints['total'] + $rPoint;
                                    $updaterPoint = [
                                        'total'=> $getoPoints,
                                        'updated_at'=>date('Y-m-d h:i:s'),
                                    ];
                                    $update = Rewards_point::where(['user_id'=>$updatedby_userID])->update($updaterPoint);
                                    $updateRewards = [
                                        'reward_status'=>'1',
                                    ];
                                    $update = Snowflake::where('lift_id',$saveArray['id'])
                                    ->where('user_id',$updatedby_userID)
                                    ->where('reward_for',$saveArray['like_for'])
                                    ->where('reward_status','0')
                                    ->update($updateRewards);

                                    /*update resorts edit data 10/5*/
                                }
                                else{
                                    $confirmed_status = 'false';
                                    $updateRewards = [
                                        'reward_status'=>'0',
                                    ];
                                    $update = Snowflake::where('lift_id',$saveArray['id'])
                                    ->where('user_id',$updatedby_userID)
                                    ->where('reward_for',$saveArray['like_for'])
                                    ->update($updateRewards);
                                }
                                $checkLike = Like::where(['lift_id'=>$saveArray['id']])
                                ->where(['event_type'=>$saveArray['event_type']])
                                ->where(['like_for'=>$saveArray['like_for']])
                                ->where(['like_for_value'=>$updatedValue])
                                ->where(['user_id'=>$userExist['id']])
                                ->first();
                                if ($checkLike) {
                                    $likedByme = 'yes';
                                }
                                else{
                                    $likedByme = 'no';
                                }
                                return response()->json([
                                    'status'=>'1',
                                    'message'=>'Liked',
                                    'count'=>$likecount?$likecount:'',
                                    'confirmed_status'=>$confirmed_status,
                                    'updatedby_userID'=>$updatedby_userID,
                                    'username'=>$username['name'],
                                    'likedByme'=>$likedByme,
                                ], $this->successStatus);
                            }
                        }
                        else{
                            return response()->json([
                                'status'=>'0',
                                'message'=>'No lift found',
                            ], $this->successStatus);
                        }
                    }
                }
            }
            else{
                return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
            }
        }

/*-----------------------like api for Resort Terrain-----------------------*/  

    public function terrainThumbsup(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist)
        {
            $validator = Validator::make($request->all(), [
                'resort_id' => 'required',
                'event_type' => 'required',
                'like_for' => 'required',
            ]);
            if ($validator->fails()) {
                return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
            }
            else {
                $getupdatedvalues = Resort_detail::where(['resort_id'=>$saveArray['resort_id']])
                ->select('terrain_difficulty_beginner','terrain_difficulty_intermediate','terrain_difficulty_advance','terrain_difficulty_expert')
                ->first()->toArray();
                $existingValue = $getupdatedvalues;
                $allvalues = implode(', ', $existingValue);
                
                $checkCount = Like::where(['resort_id'=>$saveArray['resort_id']])
                ->where(['event_type'=>$saveArray['event_type']])
                ->where('like_for','terrain_difficulty_beginner')
                ->where(['like_for_value'=>$allvalues])
                ->count();
                if ($checkCount >= '3') {
                    $deleteData = [
                        'deleted_at'=>date('Y-m-d h:i:s'),
                    ];
                    $delete = Like::where(['resort_id'=>$saveArray['resort_id']])->update($deleteData);
                    return response()->json([
                        'status'=>'1',
                        'message'=>'data is already updated please check again',
                    ], $this->successStatus);
                }else {
                    $insertData = [
                        'user_id'=>$userExist['id'],
                        'resort_id'=> $saveArray['resort_id'],
                        'event_type'=>$saveArray['event_type'],
                        'like_for'=>$saveArray['like_for'],
                        'like_for_value'=>$allvalues,
                        'is_like'=>'1',
                        'created_at'=>date('Y-m-d h:i:s'),
                    ];
                    $insertedID = Like::insertGetId($insertData);
                    $likecount = Like::where('resort_id',$saveArray['resort_id'])
                    ->where('event_type',$saveArray['event_type'])
                    ->where('like_for','terrain_difficulty_beginner')
                    ->where(['like_for_value'=>$allvalues])
                    ->count();
                    if ($insertedID) {
                        return response()->json([
                            'status'=>'1',
                            'message'=>'Liked',
                            'count'=>$likecount,
                        ], $this->successStatus);
                    }else {
                        return response()->json([
                            'status'=>'1',
                            'message'=>'Error',
                        ], $this->successStatus);
                    }
                }
            }
        }
        else{
            return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }

    public function getterrainThumbsup(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
            $validator = Validator::make($request->all(), [
                'resort_id' => 'required',
                'event_type' => 'required',
                'like_for' => 'required',
            ]);
            if ($validator->fails()) {
                return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
            }
            else {
                $getupdatedvalues = Resort_detail::where(['resort_id'=>$saveArray['resort_id']])
                ->select('terrain_difficulty_beginner','terrain_difficulty_intermediate','terrain_difficulty_advance','terrain_difficulty_expert')
                ->first(); /*get updated values*/
                if ($getupdatedvalues) {
                    $existingValue = $getupdatedvalues->toArray();
                    $allvalues = implode(', ', $existingValue);
                    $likecount = Like::where('resort_id',$saveArray['resort_id'])
                    ->where('event_type',$saveArray['event_type'])
                    ->where('like_for','terrain_difficulty_beginner')
                    ->where(['like_for_value'=>$allvalues])
                    ->count();
                    if ($likecount == '3') {
                        $confirmed_status = 'true';
                    }
                    else {
                        $confirmed_status = 'false';
                    }
                    $updatedByme = Resort_history::where('resort_id',$saveArray['resort_id'])
                    ->where('terrain_difficulty_beginner','!=','Null')
                    ->orwhere('terrain_difficulty_intermediate','!=','Null')
                    ->orwhere('terrain_difficulty_advance','!=','Null')
                    ->orwhere('terrain_difficulty_expert','!=','Null')
                    ->select('user_id')
                    ->orderBy('created_at','DESC')
                    ->limit(1)->get();
                    // print_r($updatedByme);die();
                    foreach ($updatedByme as $value) {
                        $userID = $value['user_id'];
                    }
                    $updatedby_userID = $userID;
                    $checkLike = Like::where(['resort_id'=>$saveArray['resort_id']])
                    ->where(['event_type'=>$saveArray['event_type']])
                    ->where(['like_for'=>$saveArray['like_for']])
                    ->where(['like_for_value'=>$allvalues])
                    ->where(['user_id'=>$userExist['id']])
                    ->first();
                        if ($checkLike) {
                            $likedByme = 'yes';
                        }
                        else{
                            $likedByme = 'no';
                        }
                    return response()->json([
                        'status'=>'1',
                        'message'=>'Liked',
                        'count'=>$likecount,
                        'updatedby_userID'=>$updatedby_userID,
                        'confirmed_status'=>$confirmed_status,
                        'likedByme'=>$likedByme,
                    ], $this->successStatus);
                }else{
                    return response()->json([
                        'status'=>'1',
                        'message'=>'there is no data',
                    ], $this->successStatus);
                }
            }
        }
        else{
            return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }

    public function checkAdmin(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
            $validator = Validator::make($request->all(), [
                'resort_id' => 'required',
            ]);
            if ($validator->fails()) {
                return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->successStatus);
            }
            else{
                $checkIsFav = Fav_resort::where('resort_id',$saveArray['resort_id'])
                ->where('user_id',$userExist['id'])
                ->where('favourite','=','1')
                ->first();
                if ($checkIsFav['favourite'] == 1) {
                    $isFav = 'Yes';
                }else{
                    $isFav = 'No';
                }
                $resortData = Resort::where(['id'=>$saveArray['resort_id']])->first();
                $resortDetails = array(
                    'is_favourite' => $isFav,
                    'id'=>(String)$resortData['id']?$resortData['id']:'',
                    'lift_names'=>$resortData['lift_names'],
                    'lift_numbers'=>$resortData['lift_numbers'],
                    'run_names'=>$resortData['run_names'],
                    'run_numbers'=>$resortData['run_numbers'],
                    'website'=>$resortData['website']?$resortData['website']:'',
                    'run_grades'=>$resortData['run_grades']?$resortData['run_grades']:'',
                    'phone_no'=>$resortData['phone_no'],
                    'toplat' => $resortData['top_latitude']?$resortData['top_latitude']:'',
                    'toplong' => $resortData['top_longitude']?$resortData['top_longitude']:'',
                    'lat' => $resortData['latitude'],
                    'long' => $resortData['longitude'],
                    'created_at'=>$resortData['created_at'],
                    'bcolor'=>$resortData['bcolor'],
                    'fcolor'=>$resortData['fcolor'],
                );
                $checkIsAdmin = Permission::where(['resort_id'=>$saveArray['resort_id']])->where(['user_id'=>$userExist['id']])->where(['role_type'=>'ADM'])->first();
                if ($checkIsAdmin) {
                    $isAdmin = 'true';
                }else{
                    $isAdmin = 'false';
                }
                $checkIsEditor = Permission::where(['resort_id'=>$saveArray['resort_id']])->where(['user_id'=>$userExist['id']])->where(['role_type'=>'EDI'])->first();
                if ($checkIsEditor) {
                    $isEditor = 'true';
                }else{
                    $isEditor = 'false';
                }
                $checkIsPatroller = Permission::where(['resort_id'=>$saveArray['resort_id']])->where(['user_id'=>$userExist['id']])->where(['role_type'=>'PAT'])->first();
                if ($checkIsPatroller) {
                    $isPatroller = 'true';
                }else{
                    $isPatroller = 'false';
                }
                $checkIsLifty = Permission::where(['resort_id'=>$saveArray['resort_id']])->where(['user_id'=>$userExist['id']])->where(['role_type'=>'LIF'])->first();
                if ($checkIsLifty) {
                    $isLifty = 'true';
                }else{
                    $isLifty = 'false';
                }
                return response()->json([
                    'status'=>'1',
                    'message'=>'Sucess',
                    'isAdmin'=>$isAdmin,
                    'isEditor'=>$isEditor,
                    'isPatroller'=>$isPatroller,
                    'isLifty'=>$isLifty,
                    'resort_details'=>$resortDetails
                ], $this->successStatus);
            }
        }
        else{
            return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }

    // public function addFavResort(Request $request)
    // {
    //     $saveArray = $request->all();
    //     $token = $request->header('token');
    //     $userExist = User::where(['remember_token'=>$token])->first();
    //     if($userExist){
    //         $validator = Validator::make($request->all(), [
    //             'resort_id' => 'required',
    //         ]);
    //         if ($validator->fails()) {
    //             return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->successStatus);
    //         }
    //         else{}
    //     }
    //     else{
    //         return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
    //     }
    // }

    // public function getFavResorts(Request $request)
    // {

    // }
    
    public function index(Request $request)
    {
        $resorts = $this->resortRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($resorts->toArray(), 'Resorts retrieved successfully');
    }

    /**
     * Store a newly created Resort in storage.
     * POST /resorts
     *
     * @param CreateResortAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateResortAPIRequest $request)
    {
        $input = $request->all();

        $resort = $this->resortRepository->create($input);

        return $this->sendResponse($resort->toArray(), 'Resort saved successfully');
    }

    /**
     * Display the specified Resort.
     * GET|HEAD /resorts/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Resort $resort */
        $resort = $this->resortRepository->find($id);

        if (empty($resort)) {
            return $this->sendError('Resort not found');
        }

        return $this->sendResponse($resort->toArray(), 'Resort retrieved successfully');
    }

    /**
     * Update the specified Resort in storage.
     * PUT/PATCH /resorts/{id}
     *
     * @param int $id
     * @param UpdateResortAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateResortAPIRequest $request)
    {
        $input = $request->all();

        /** @var Resort $resort */
        $resort = $this->resortRepository->find($id);

        if (empty($resort)) {
            return $this->sendError('Resort not found');
        }

        $resort = $this->resortRepository->update($input, $id);

        return $this->sendResponse($resort->toArray(), 'Resort updated successfully');
    }

    /**
     * Remove the specified Resort from storage.
     * DELETE /resorts/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Resort $resort */
        $resort = $this->resortRepository->find($id);

        if (empty($resort)) {
            return $this->sendError('Resort not found');
        }

        $resort->delete();

        return $this->sendResponse($id, 'Resort deleted successfully');
    }
}
