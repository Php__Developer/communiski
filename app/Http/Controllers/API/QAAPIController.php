<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateQAAPIRequest;
use App\Http\Requests\API\UpdateQAAPIRequest;
use App\Models\QA;
use App\Models\Question_history;
use App\Models\Rewards_point;
use App\Models\Snowflake;
use App\Models\Spend_snowflake;
use App\Models\Permission;
use App\Models\User;
use App\Models\Resort;
use App\Repositories\QARepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Carbon\Carbon;
use DB;
use Validator;
use Response;

/**
 * Class QAController
 * @package App\Http\Controllers\API
 */

class QAAPIController extends AppBaseController
{
    /** @var  QARepository */
    private $qARepository;
    public $unauthorised = 401;
    public $successStatus = 200;
    public $badrequest = 400;

    public function __construct(QARepository $qARepo)
    {
        $this->qARepository = $qARepo;
    }

    /**
     * Display a listing of the QA.
     * GET|HEAD /qAS
     *
     * @param Request $request
     * @return Response
     */

    public function askQuestion(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
            $validator = Validator::make($request->all(), [
            'resort_id' => 'required',
            'question' => 'required',
            'topic' => 'required',
            'type' => 'required'
        ]);
            if ($validator->fails()) {
                return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
            }
            else{
                $checkFlakes = Rewards_point::where(['user_id'=>$userExist['id']])->select('total')->first();
                if ($checkFlakes['total'] == 0) {
                    return response()->json([
                        'status'=>'0',
                        'message'=>'You have not enough snowflakes rewards to ask question',
                    ], $this->successStatus);
                }
                else{
                    /*available snowflakes after ask question*/
                    $questionCharges = $checkFlakes['total'] - '50';
                    $updatePoints = [
                        'total'=>$questionCharges,
                        'updated_at'=>date('Y-m-d h:i:s')
                      ];
                      $update = Rewards_point::where(['user_id'=>$userExist['id']])->update($updatePoints);
                    /*available snowflakes after ask question*/

                    /*spend snowflakes about asking questions*/
                    $spendFlakes = [
                        'user_id'=>$userExist['id'],
                        'spend_snowflake'=>'50',
                        'reason'=>'Asked a Question',
                        'created_at'=>date('Y-m-d h:i:s')
                      ];
                      $update = Spend_snowflake::insertGetId($spendFlakes);
                    /*spend snowflakes about asking questions*/

                    $topic = array($saveArray['topic']);
                    $multitopic = implode(",", $topic);
                    $askQuestion = [
                        'user_id'=>$userExist['id'],
                        'resort_id'=>$saveArray['resort_id'],
                        'question'=>$saveArray['question'],
                        'topic'=>$multitopic,
                        'question_type'=>$saveArray['type'],
                        'archive_status'=>'1',
                        'created_at'=>date('Y-m-d h:i:s'),
                    ];
                    $quesId = QA::insertGetId($askQuestion);
                    if($quesId){
                        return response()->json([
                            'status'=>'1',
                            'message'=>'Question Asked Successfully',
                            'questionId'=>(String)$quesId,
                        ], $this->successStatus);
                    }
                    else{
                        return response()->json([
                            'message'=>'Error',
                            'status'=>'0'
                        ], $this->badrequest);
                    }
                }
            }
        }
        else{
            return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }


    public function answerQuestion(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
            $validator = Validator::make($request->all(), [
                'resort_id' => 'required',
                'question_id' => 'required',
                'answer' => 'required',
            ]);
            if ($validator->fails()) {
                return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
            }
            else{
                $question =  QA::where('id',$saveArray['question_id'])->first();
                $answerQue_history = [
                    'resort_id'=>$saveArray['resort_id'],
                    'question_id'=>$saveArray['question_id'],
                    'answer_by_user_id'=>$userExist['id'],
                    'answer'=>$saveArray['answer'],
                    'created_at'=>date('Y-m-d h:i:s'),
                ];
                $ansid = Question_history::insertGetId($answerQue_history);
                $quesType =  $question['question_type'];
                if ($quesType == 'specific') {
                    $updatePoints = [
                        'archive_status'=>'2',
                        'updated_at'=>date('Y-m-d h:i:s')
                    ];
                    $update = QA::where(['id'=>$saveArray['question_id']])->where(['resort_id'=>$saveArray['resort_id']])->update($updatePoints);
                }else{
                    $updatePoints = [
                        'archive_status'=>'1',
                        'updated_at'=>date('Y-m-d h:i:s')
                    ];
                    $update = QA::where(['id'=>$saveArray['question_id']])->where(['resort_id'=>$saveArray['resort_id']])->update($updatePoints);
                }
                /*send notification to user who asked question*/
                $askedBy_user_id = $question->user_id;
                //$device_id = $deviceId->device_id;
                $deviceId = User::where('id',$askedBy_user_id)->select('device_id','device_type')->first();
                $message =['message' =>'Someone has answered your question'];
                $device_id[] =$deviceId->device_id;

                if($deviceId->device_type == 'A'){
                    $r = QAAPIController::android_send_notification($device_id,$message);
                }
                if($deviceId->device_type == 'I'){
                    $i = QAAPIController::iphone_send_notifications($device_id,$message);
                }
                /*send notification to user who asked question*/
                if($ansid){
                    return response()->json([
                        'status'=>'1',
                        'message'=>'Answer Successfully',
                    ], $this->successStatus);
                }
                else{
                    return response()->json([
                        'message'=>'Error',
                        'status'=>'0'
                    ], $this->badrequest);
                }
            }
        }
        else{
            return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }

    /*generic questions api/ and browse question screen*/

    public function browseQuestions(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
            $validator = Validator::make($request->all(), [
                'resort_id' => 'required',
            ]);
                if ($validator->fails()) {
                    return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
                }
                else{
                $questions = QA::where(['resort_id'=>$saveArray['resort_id']])
                ->where('question_type','!=','specific')
                ->where('answer','!=','Null')
                ->where('archive_status','=','1')
                ->orderBy('id','desc')->get();

                $questionArray = [];

                $archiQuestions = QA::where(['resort_id'=>$saveArray['resort_id']])
                ->where('question_type','!=','specific')
                ->where('answer','!=','Null')
                ->where('archive_status','=','2')
                ->orderBy('id','desc')->get();

                $archiQuestionArray = [];

                foreach($archiQuestions as $archques){
                        $getallarchanswer = Question_history::where(['question_id'=>$archques['id']])
                        ->where('answer','=',$archques['answer'])
                        ->get();
                        $archanswerArray = [];
                        foreach($getallarchanswer as $archans){
                            $username_arch = User::where(['id'=>$archans['answer_by_user_id']])->first();
                            $archanswer['question_id'] = $archans['question_id'];
                            $archanswer['answer_id'] = $archans['id'];
                            $archanswer['answer'] = $archans['answer'];
                            $archanswer['answer_by_user_id'] = $archans['answer_by_user_id'];
                            $archanswer['answer_by_user_name'] = $username_arch['name'];
                            $archanswer['created_at']=date('Y-m-d h:i:s');
                            $archanswerArray[] = $archanswer;
                        }
                        $user_arch = User::where(['id'=>$archques['user_id']])->first();
                        $archlisting['resort_id'] = $archques['resort_id'];
                        $archlisting['question_id'] = (String)$archques['id'];
                        $archlisting['asked_by_user_id'] = $archques['user_id'];
                        $archlisting['asked_by_user_name'] = $user_arch['name'];
                        $archlisting['question'] = $archques['question'];
                        $archlisting['selected_answer'] = $archques['answer']?$archques['answer']:'';
                        $archlisting['question_type'] = $archques['question_type'];
                        $archlisting['archive_status'] = $archques['archive_status'];
                        $archlisting['topic'] = $archques['topic'];
                        $archlisting['selectedAnswer_detail'] = $archanswerArray;
                        $archiQuestionArray[] = $archlisting;
                    }

                foreach($questions as $ques){
                    $getallanswer = Question_history::where(['question_id'=>$ques['id']])
                    ->where('answer','=',$ques['answer'])
                    ->get();
                    $answerArray = [];
                    foreach($getallanswer as $ans){
                        $username = User::where(['id'=>$ans['answer_by_user_id']])->first();
                        $answer['question_id'] = $ans['question_id'];
                        $answer['answer_id'] = $ans['id'];
                        $answer['answer'] = $ans['answer'];
                        $answer['answer_by_user_id'] = $ans['answer_by_user_id'];
                        $answer['answer_by_user_name'] = $username['name'];
                        $answer['created_at']=date('Y-m-d h:i:s');
                        $answerArray[] = $answer;
                    }
                    $user = User::where(['id'=>$ques['user_id']])->first();
                    $listing['resort_id'] = $ques['resort_id'];
                    $listing['question_id'] = (String)$ques['id'];
                    $listing['asked_by_user_id'] = $ques['user_id'];
                    $listing['asked_by_user_name'] = $user['name'];
                    $listing['question'] = $ques['question'];
                    $listing['selected_answer'] = $ques['answer']?$ques['answer']:'';
                    $listing['question_type'] = $ques['question_type'];
                    $listing['archive_status'] = $ques['archive_status'];
                    $listing['topic'] = $ques['topic'];
                    $listing['selectedAnswer_detail'] = $answerArray;
                    $questionArray[] = $listing;
                }
                return response()->json([
                    'status'=>'1',
                    'message'=>'Questions fetched successfully',
                    'generic_questions'=>$questionArray,
                    'archive_questions'=>$archiQuestionArray
                ], $this->successStatus);
            }
        }
        else{
            return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }


    public function get_archiveQues(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
            $validator = Validator::make($request->all(), [
                'resort_id' => 'required',
            ]);
            if ($validator->fails()) {
                return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
            }
            else{
                $questions = QA::where(['resort_id'=>$saveArray['resort_id']])
                ->where('question_type','!=','specific')
                ->where('answer','!=','Null')
                ->where('archive_status','=','2')
                ->orderBy('id','desc')->get();
                // print_r($questions);die();
                $questionArray = [];
                if(count($questions)){
                    foreach($questions as $ques){
                        $getallanswer = Question_history::where(['question_id'=>$ques['id']])
                        ->where('answer','=',$ques['answer'])
                        ->get();
                        $answerArray = [];
                        foreach($getallanswer as $ans){
                            $username = User::where(['id'=>$ans['answer_by_user_id']])->first();
                            $answer['question_id'] = $ans['question_id'];
                            $answer['answer_id'] = $ans['id'];
                            $answer['answer'] = $ans['answer'];
                            $answer['answer_by_user_id'] = $ans['answer_by_user_id'];
                            $answer['answer_by_user_name'] = $username['name'];
                            $answer['created_at']=date('Y-m-d h:i:s');
                            $answerArray[] = $answer;
                        }
                        $user = User::where(['id'=>$ques['user_id']])->first();
                        $listing['resort_id'] = $ques['resort_id'];
                        $listing['question_id'] = (String)$ques['id'];
                        $listing['asked_by_user_id'] = $ques['user_id'];
                        $listing['asked_by_user_name'] = $user['name'];
                        $listing['question'] = $ques['question'];
                        $listing['selected_answer'] = $ques['answer']?$ques['answer']:'';
                        $listing['question_type'] = $ques['question_type'];
                        $listing['archive_status'] = $ques['archive_status'];
                        $listing['topic'] = $ques['topic'];
                        $listing['selectedAnswer_detail'] = $answerArray;
                        $questionArray[] = $listing;
                    }
                    return response()->json([
                        'status'=>'1',
                        'message'=>'Questions fetched successfully',
                        'archive_questions'=>$questionArray
                      ], $this->successStatus);
                }
                else{
                    return response()->json([
                        'message'=>'No data Found',
                        'status'=>'0'
                    ], $this->badrequest);
                }
            }
        }
        else{
            return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }


    /*questions that are to be answer yet and answer question screen/module*/

    public function unansweredQuestions(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
            $validator = Validator::make($request->all(), [
                'resort_id' => 'required',
            ]);
                if ($validator->fails()) {
                    return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
                }
                else{
                $questions = QA::where(['resort_id'=>$saveArray['resort_id']])
                ->where('answer','=',Null)
                ->orderBy('id','desc')->get();
                $questionArray = [];
                if(count($questions)){
                    foreach($questions as $ques){
                        $getallanswer = Question_history::where(['question_id'=>$ques['id']])
                        ->get();
                        $answerArray = [];
                        foreach($getallanswer as $ans){
                            $username = User::where(['id'=>$ans['answer_by_user_id']])->first();
                            $answer['question_id'] = $ans['question_id'];
                            $answer['answer_id'] = $ans['id'];
                            $answer['answer'] = $ans['answer'];
                            $answer['answer_by_user_id'] = $ans['answer_by_user_id'];
                            $answer['answer_by_user_name'] = $username['name'];
                            $answer['created_at'] = $ques['updated_at']->format('Y-m-d');
                            $answerArray[] = $answer;
                        }
                        $user = User::where(['id'=>$ques['user_id']])->first();
                        $listing['resort_id'] = $ques['resort_id'];
                        $listing['question_id'] = (String)$ques['id'];
                        $listing['asked_by_user_id'] = $ques['user_id'];
                        $listing['asked_by_user_name'] = $user['name'];
                        $listing['question'] = $ques['question'];
                        $listing['selected_answer'] = $ques['answer']?$ques['answer']:'';
                        $listing['question_type'] = $ques['question_type'];
                        $listing['archive_status'] = $ques['archive_status'];
                        $listing['topic'] = $ques['topic'];
                        $listing['created_at'] = $ques['created_at']->format('Y-m-d');
                        $listing['allAnswers_detail'] = $answerArray;
                        $questionArray[] = $listing;
                    }
                    return response()->json([
                        'status'=>'1',
                        'message'=>'Questions fetched successfully',
                        'generic_questions'=>$questionArray
                      ], $this->successStatus);
                }
                else{
                    return response()->json([
                        'message'=>'No data Found',
                        'status'=>'0'
                    ], $this->badrequest);
                }
            }
        }
        else{
            return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }

    /*current user asked questions and answered questions api*/

    public function getuserQA(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
            $validator = Validator::make($request->all(), [
                'resort_id' => 'required',
            ]);
            if ($validator->fails()) {
                return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
            }
            else{
                $questionArray = [];
                $questions = QA::where(['resort_id'=>$saveArray['resort_id']])
                ->where('user_id',$userExist['id'])
                ->where('answer','=',Null)
                ->where('refund_status','=','2')
                ->orderBy('id','desc')
                ->get();
                //if(count($questions)){
                    foreach($questions as $ques){
                        $getallanswer = Question_history::where(['question_id'=>$ques['id']])->get();
                        $answerArray = [];
                        foreach($getallanswer as $ans){
                            $username = User::where(['id'=>$ans['answer_by_user_id']])->first();
                            $answer['question_id'] = $ans['question_id'];
                            $answer['answer_id'] = $ans['id'];
                            $answer['answer'] = $ans['answer'];
                            $answer['answer_by_user_id'] = $ans['answer_by_user_id'];
                            $answer['answer_by_user_name'] = $username['name'];
                            $answer['created_at']=date('Y-m-d h:i:s');
                            $answerArray[] = $answer;
                        }
                        $user = User::where(['id'=>$ques['user_id']])->first();
                        $resortDetails = Resort::where(['id'=>$ques['resort_id']])->first();
                        $listing['resort_id'] = $ques['resort_id'];
                        $listing['resort_location'] = $resortDetails['resort_name'];
                        $listing['question_id'] = (String)$ques['id'];
                        $listing['question'] = $ques['question'];
                        $listing['selected_answer'] = $ques['answer']?$ques['answer']:'';
                        $listing['askedby_user_id'] = $ques['user_id'];
                        $listing['askedby_user_name'] = $user['name'];
                        $listing['question_type'] = $ques['question_type'];
                        $listing['topic'] = $ques['topic'];
                        $listing['created_at'] =  $ques['created_at']->format('d-m-Y');
                        $listing['answers'] = $answerArray;
                        $questionArray[] = $listing;
                    }
                    $answeredQArray = [];
                    $answered_questions = QA::where(['resort_id'=>$saveArray['resort_id']])
                    ->where('user_id',$userExist['id'])
                    ->where('answer','!=',Null)
                    ->orWhere('refund_status','=','1')
                    ->orderBy('id','desc')
                    ->get();
                    foreach($answered_questions as $ques1){
                        $getallanswer1 = Question_history::where(['question_id'=>$ques1['id']])->get();
                        $answerArray1 = [];
                        foreach($getallanswer1 as $ans1){
                            $username1 = User::where(['id'=>$ans1['answer_by_user_id']])->first();
                            $answer1['question_id'] = $ans1['question_id'];
                            $answer1['answer_id'] = $ans1['id'];
                            $answer1['answer'] = $ans1['answer'];
                            $answer1['answer_by_user_id'] = $ans1['answer_by_user_id'];
                            $answer1['answer_by_user_name'] = $username1['name'];
                            $answer1['created_at'] = $ans1['created_at']->format('d-m-Y');
                            $answerArray1[] = $answer1;
                        }
                        $user1 = User::where(['id'=>$ques1['user_id']])->first();
                        $resortDetails1 = Resort::where(['id'=>$ques1['resort_id']])->first();
                        $listing1['resort_id'] = $ques1['resort_id'];
                        $listing1['resort_location'] = $resortDetails1['resort_name'];
                        $listing1['question_id'] = (String)$ques1['id'];
                        $listing1['question'] = $ques1['question'];
                        $listing1['selected_answer'] = $ques1['answer']?$ques1['answer']:'';
                        $listing1['askedby_user_id'] = $ques1['user_id'];
                        $listing1['askedby_user_name'] = $user1['name'];
                        $listing1['question_type'] = $ques1['question_type'];
                        $listing1['topic'] = $ques1['topic'];
                        $listing1['created_at'] =  $ques1['created_at']->format('d-m-Y');
                        $listing1['answers'] = $answerArray1;
                        $answeredQArray[] = $listing1;
                    }
                    return response()->json([
                        'status'=>'1',
                        'message'=>'Questions fetched successfully',
                        'questionArray'=>$questionArray,
                        'answeredArray'=>$answeredQArray
                      ], $this->successStatus);
                //}
                /*else{
                    return response()->json([
                        'message'=>'No data found',
                        'status'=>'0'
                    ], $this->successStatus);
                }*/
            }
        }
        else{
            return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }

            /*-----------------Admin API's here along with Permission-----------------*/

    public function chooseAnswer(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
            $validator = Validator::make($request->all(), [
                'resort_id' => 'required',
                'question_id' => 'required',
                'answer_id' => 'required',
            ]);
            if ($validator->fails()) {
                return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
            }
            else{
                /*get only 7days old records*/
                $date = \Carbon\Carbon::today()->subDays(7);
                $checkQA = QA::where(['resort_id'=>$saveArray['resort_id']])
                ->where(['id'=>$saveArray['question_id']])
                ->where('choosed_by_user','=','2')
                ->where('created_at', '>=', $date)
                ->first();
                if ($checkQA) {
                    $getQue_ans = Question_history::where(['resort_id'=>$saveArray['resort_id']])
                    ->where(['question_id'=>$saveArray['question_id']])
                    ->where(['id'=>$saveArray['answer_id']])
                    ->select('answer','answer_by_user_id')
                    ->first();

                    /*update rewards after answer choosen*/
                    $getAmount = Rewards_point::where('user_id',$getQue_ans['answer_by_user_id'])->select('total')->first();
                    $rewareded_amount = $getAmount['total'] + 50;
                    $update_rewards = [
                        'total'=>$rewareded_amount,
                        'updated_at'=>date('Y-m-d h:i:s')
                    ];
                    $update = Rewards_point::where('user_id',$getQue_ans['answer_by_user_id'])->update($update_rewards);
                    /*update rewards after answer choosen*/

                    /*choose answer and update answer in question_answers table*/
                    $choose_answer = [
                        'answer'=>$getQue_ans['answer'],
                        'choosed_by_user'=>'1',
                        'updated_at'=>date('Y-m-d h:i:s')
                    ];
                    $update = QA::where(['id'=>$saveArray['question_id']])->update($choose_answer);
                    /*choose answer and update answer in question_answers table*/

                    /*Earn Snowflakes rewards for answering a question*/
                    $earn_rewards = [
                        'user_id'=>$getQue_ans['answer_by_user_id'],
                        'reason'=>'Answer a question',
                        'snowflake_rewards'=>'50',
                        'reward_status'=>'1',
                        'created_at'=>date('Y-m-d h:i:s')
                    ];
                    $update = Snowflake::insert($earn_rewards);
                    /*Earn Snowflakes rewards for answering a question*/

                    /*notification send to user who answered correctly*/
                    $getUserId = $getQue_ans['answer_by_user_id'];
                    $deviceId = User::where('id',$getUserId)->select('device_id','device_type')->first();
                    $message =['message' =>'Your answer was chosen and you’ve been awarded 50 snowflakes'];
                    $device_id[] =$deviceId->device_id;

                    if($deviceId->device_type == 'A'){
                        $r = QAAPIController::android_send_notification($device_id,$message);
                    }
                    if($deviceId->device_type == 'I'){
                        $i = QAAPIController::iphone_send_notifications($device_id,$message);
                    }
                    if ($update) {
                        return response()->json([
                            'status'=>'1',
                            'message'=>'choose successfully',
                        ], $this->successStatus);
                    }else{
                        return response()->json([
                            'status'=>'1',
                            'message'=>'Error',
                        ], $this->badrequest);
                    }
                    }else{
                        return response()->json([
                            'status'=>'1',
                            'message'=>'you can choose your answer only one time',
                        ], $this->successStatus);
                    }
                }
            }
            else{
                return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
            }
        }

    public function markDefault(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
            $validator = Validator::make($request->all(), [
                'resort_id' => 'required',
                'question_id' => 'required',
                'answer_id' => 'required',
            ]);
            if ($validator->fails()) {
                return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
            }
            else{
                $getQue_ans = Question_history::where(['resort_id'=>$saveArray['resort_id']])
                ->where(['question_id'=>$saveArray['question_id']])
                ->where(['id'=>$saveArray['answer_id']])
                ->select('answer','answer_by_user_id')
                ->first();
                $default_answer = [
                    'answer'=>$getQue_ans['answer'],
                    'mark_default'=>'1',
                    'updated_at'=>date('Y-m-d h:i:s')
                ];
                $update = QA::where(['id'=>$saveArray['question_id']])->update($default_answer);

                if ($update) {
                    return response()->json([
                        'status'=>'1',
                        'message'=>'mark default successfully',
                    ], $this->successStatus);
                }else{
                    return response()->json([
                        'status'=>'1',
                        'message'=>'Error',
                    ], $this->badrequest);
                }
            }
        }
        else{
            return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }

    public function newAnswer(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
            $validator = Validator::make($request->all(), [
                'resort_id' => 'required',
                'question_id' => 'required',
                'answer' => 'required',
            ]);
            if ($validator->fails()) {
                return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
            }
            else{
                $getQuestion = QA::where('resort_id',$saveArray['resort_id'])
                ->where('id',$saveArray['question_id'])
                ->select('id')
                ->first();
                $updateNewanswer = [
                    'answer'=>$saveArray['answer'],
                    'mark_default'=>'1',
                    'updated_at'=>date('Y-m-d h:i:s')
                ];
                $update = QA::where(['id'=>$getQuestion['id']])->update($updateNewanswer);
                $insertanswer = [
                    'resort_id'=>$saveArray['resort_id'],
                    'question_id'=>$saveArray['question_id'],
                    'answer_by_user_id'=>$userExist['id'],
                    'answer'=>$saveArray['answer'],
                    'created_at'=>date('Y-m-d h:i:s')
                ];
                $insert = Question_history::insertGetId($insertanswer);
                if ($insert) {
                    return response()->json([
                        'status'=>'1',
                        'message'=>'New answer successfully',
                    ], $this->successStatus);
                }else{
                    return response()->json([
                        'status'=>'1',
                        'message'=>'Error',
                    ], $this->badrequest);
                }
            }
        }
        else{
            return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }

    public function refundRewards(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
            $validator = Validator::make($request->all(), [
                'resort_id' => 'required',
            ]);
                if ($validator->fails()) {
                    return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
                }
                else{
                    $fromDate = date("Y-m-d",strtotime("-7 days"));
                    $questions = QA::where(['user_id'=>$userExist['id']])
                    ->where(['resort_id'=>$saveArray['resort_id']])
                    ->where('answer','=',Null)
                    ->where('refund_status','=','2')
                    ->where('created_at','<=',$fromDate)
                    ->select('id')
                    ->get();
                    $qCount = count($questions);
                    if ($qCount > '0') {
                    foreach ($questions as $value) {
                        $updateStatus = [
                            'refund_status'=>'1',
                            'archive_status'=>'2',
                            'updated_at'=>date('Y-m-d h:i:s')
                        ];
                        $update = QA::where(['id'=>$value['id']])->update($updateStatus);
                    }
                    $rPoints = '50'*$qCount;
                    $getoldReward = Rewards_point::where(['user_id'=>$userExist['id']])->first();
                    $refundPoints = $rPoints + $getoldReward['total'];
                    $updateData = [
                        'total'=>$refundPoints,
                        'updated_at'=>date('Y-m-d h:i:s')
                    ];
                    $update = Rewards_point::where(['user_id'=>$userExist['id']])->update($updateData);
                    $getUserId = $userExist['id'];
                    $deviceId = User::where('id',$getUserId)->select('device_id','device_type')->first();
                    $message =['message' =>'Sorry we didn’t get any answers to your question and your Snowflakes have been refunded'];
                    $device_id[] =$deviceId->device_id;

                    if($deviceId->device_type == 'A'){
                        $r = QAAPIController::android_send_notification($device_id,$message);
                    }
                    if($deviceId->device_type == 'I'){
                        $i = QAAPIController::iphone_send_notifications($device_id,$message);
                    }
                    if($update){
                        return response()->json([
                          'status'=>'1',
                          'message'=>'Rewards Refunded Successfully',
                        ], $this->successStatus);
                      }
                      else{
                        return response()->json([
                          'message'=>'errors',
                          'status'=>'0'
                        ], $this->badrequest);
                      }
                    }
                    else {
                        return response()->json([
                          'message'=>'There is no data',
                          'status'=>'0'
                        ], $this->successStatus);
                    }
                }
        }
        else{
            return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }

    public function editTopics(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
            $validator = Validator::make($request->all(), [
                'resort_id' => 'required',
                'question_id' => 'required',
            ]);
            if ($validator->fails()) {
                return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
            }
            else{
                $getTopics = QA::where('resort_id',$saveArray['resort_id'])->where('id',$saveArray['question_id'])->first();
                $topic = array($saveArray['topic']);
                $multitopic = implode(",", $topic);
                $updateTopic = [
                    'topic'=>$saveArray['topic']?$saveArray['topic']:$getTopics['topic'],
                    'updated_at'=>date('Y-m-d h:i:s')
                ];
                $update = QA::where('resort_id',$saveArray['resort_id'])->where('id',$saveArray['question_id'])->update($updateTopic);
                if($update) {
                    return response()->json([
                        'status'=>'1',
                        'message'=>'Topics Updated Successfully'
                    ], $this->successStatus);
                }
                else{
                    return response()->json([
                        'message'=>'does not Update',
                        'status'=>'0'
                    ], $this->badrequest);
                }
            }
        }
        else{
            return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }

    public function archiveUnarchive(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
            $validator = Validator::make($request->all(), [
                'resort_id' => 'required',
                'question_id' => 'required',
            ]);
            if ($validator->fails()) {
                return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
            }
            else{
                $getQuestion = QA::where('id',$saveArray['question_id'])->first();
                if ($getQuestion) {
                    if ($getQuestion['archive_status'] == '1') {
                        $archive_question = [
                            'archive_status'=>'2',
                            'updated_at'=>date('Y-m-d h:i:s')
                        ];
                        $update = QA::where(['id'=>$saveArray['question_id']])->update($archive_question);
                        return response()->json([
                            'status'=>'1',
                            'message'=>'Archive successfully',
                        ], $this->successStatus);
                    }
                    else{
                        $unarchive_question = [
                            'archive_status'=>'1',
                            'updated_at'=>date('Y-m-d h:i:s')
                        ];
                        $update = QA::where(['id'=>$saveArray['question_id']])->update($unarchive_question);
                        return response()->json([
                            'status'=>'1',
                            'message'=>'Unarchive successfully',
                        ], $this->successStatus);
                    }
                }
                else{
                    return response()->json([
                        'status'=>'1',
                        'message'=>'Question not found',
                    ], $this->successStatus);
                }
            }
        }
        else{
            return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }

    public function demo1(Request $request)
    {
        $device_id[] ="dSRo4sbuQfE:APA91bE0mG3gT6jGcgomBkeoIuQqvASSajv3zPSYACztJWy2TtANSrZ_8puY3R_PbJ9o6OU3ly7NAFHYUapJVuAbNAJj0hA2YSItZ4I-pI44xmcn1fKL_OE0XASkEzQXDoLX6DmK4WoO";
        $message=array('message'=>"hello");
        $i = QAAPIController::android_send_notification($device_id,$message);
    }

    public function android_send_notification($registatoin_ids,$message) {
        //print_r($message);die();
        //$url = 'https://android.googleapis.com/fcm/send';
        $url = 'https://fcm.googleapis.com/fcm/send';
        $fields = array(
            'registration_ids' => $registatoin_ids,
            'data' => $message,
        );
         if(!defined('GOOGLE_API_KEY')){
            $GOOGLE_API_KEY = 'AAAAYBmHz5g:APA91bHGee6_av7cgW_nA9eNQF_J0SEWlqcS_arCLAdnMStD69eFyjdDx5iDdCV5obWP_9sYD_D3eMHn06hHSKNSD0vSVkTP4-JTl7kOyjPSd8ubu4xgtpy_1nh_9mtBvWvyi6fMd-j4';
        }
        $headers = array(
            'Authorization: key='.$GOOGLE_API_KEY,
            'Content-Type: application/json'
        );
        //pr($headers);die;
        // Open connection
        $ch = curl_init();

        // Set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);

        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

        $result = curl_exec($ch);
        if ($result === FALSE){
            die('Curl failed: ' . curl_error($ch));
        }

        curl_close($ch);
        return $result;
        //print_r($result);die;
    }

    /*-----------------Admin API's here along with Permission-----------------*/

    public function searchQuestions(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
            $rules = array(
                'resort_id'=>'required',
                'by_values' => 'required_without_all:by_text',
                'by_text' => 'required_without_all:by_values',
            );
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
            }
            else{
                if ($saveArray['by_values']) {

                    $terms = explode(',',$saveArray['by_values']);

                    $query = QA::where('resort_id','=',$saveArray['resort_id'])
                    ->where('archive_status','1')
                    ->where('answer','!=','Null')
                    ->where(function($query) use($terms) {
                        foreach($terms as $term) {
                            $query->orWhere('topic', 'like', "%$term%");
                        };
                    })
                    ->inRandomOrder()
                    //->take(3)
                    ->get();
                    $searchArray = [];
                    if(count($query)){
                    foreach($query as $ques){
                        $getallanswer = Question_history::where(['question_id'=>$ques['id']])->get();
                        $answerArray = [];
                        foreach($getallanswer as $ans){
                            $username = User::where(['id'=>$ans['answer_by_user_id']])->first();
                            $answer['question_id'] = $ans['question_id'];
                            $answer['answer_id'] = $ans['id'];
                            $answer['answer'] = $ans['answer'];
                            $answer['answer_by_user_id'] = $ans['answer_by_user_id'];
                            $answer['answer_by_user_name'] = $username['name'];
                            $answer['created_at']=date('Y-m-d h:i:s');
                            $answerArray[] = $answer;
                        }
                        $user = User::where(['id'=>$ques['user_id']])->first();
                        $resortDetails = Resort::where(['id'=>$ques['resort_id']])->first();
                        $listing['resort_id'] = $ques['resort_id'];
                        $listing['question_id'] = (String)$ques['id'];
                        $listing['asked_by_user_id'] = $ques['user_id'];
                        $listing['asked_by_user_name'] = $user['name'];
                        $listing['question'] = $ques['question'];
                        $listing['selected_answer'] = $ques['answer']?$ques['answer']:'';
                        $listing['question_type'] = $ques['question_type'];
                        $listing['archive_status'] = $ques['archive_status'];
                        $listing['topic'] = $ques['topic'];
                        $listing['alternativeAnswer_detail'] = $answerArray;
                        $searchArray[] = $listing;
                    }
                    return response()->json([
                        'status'=>'1',
                        'message'=>'data fetched successfully',
                        'searchData'=>$searchArray
                    ], $this->successStatus);
                }
                else{
                    return response()->json([
                        'message'=>'No Data',
                        'status'=>'0'
                    ], $this->successStatus);
                }
                }
                elseif ($saveArray['by_text']) {
                    $query = QA::where('resort_id','=',$saveArray['resort_id'])
                    ->where('topic',$saveArray['by_text'])
                    ->where('archive_status','1')
                    ->where('answer','!=','Null')
                    ->get();
                    $searchArray = [];
                    if(count($query)){
                    foreach($query as $ques){
                        $getallanswer = Question_history::where(['question_id'=>$ques['id']])->get();
                        $answerArray = [];
                        foreach($getallanswer as $ans){
                            $username = User::where(['id'=>$ans['answer_by_user_id']])->first();
                            $answer['question_id'] = $ans['question_id'];
                            $answer['answer_id'] = $ans['id'];
                            $answer['answer'] = $ans['answer'];
                            $answer['answer_by_user_id'] = $ans['answer_by_user_id'];
                            $answer['answer_by_user_name'] = $username['name'];
                            $answer['created_at']=date('Y-m-d h:i:s');
                            $answerArray[] = $answer;
                        }
                        $user = User::where(['id'=>$ques['user_id']])->first();
                        $resortDetails = Resort::where(['id'=>$ques['resort_id']])->first();
                        $listing['resort_id'] = $ques['resort_id'];
                        $listing['question_id'] = (String)$ques['id'];
                        $listing['asked_by_user_id'] = $ques['user_id'];
                        $listing['asked_by_user_name'] = $user['name'];
                        $listing['question'] = $ques['question'];
                        $listing['selected_answer'] = $ques['answer']?$ques['answer']:'';
                        $listing['question_type'] = $ques['question_type'];
                        $listing['archive_status'] = $ques['archive_status'];
                        $listing['topic'] = $ques['topic'];
                        $listing['alternativeAnswer_detail'] = $answerArray;
                        $searchArray[] = $listing;
                    }
                    return response()->json([
                        'status'=>'1',
                        'message'=>'data fetched successfully',
                        'searchData'=>$searchArray
                    ], $this->successStatus);
                }
                else{
                    return response()->json([
                        'message'=>'No Data',
                        'status'=>'0'
                    ], $this->successStatus);
                }
                }
            }
        }
        else{
            return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }

    public function index(Request $request)
    {
        $qAS = $this->qARepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($qAS->toArray(), 'Q A S retrieved successfully');
    }

    /**
     * Store a newly created QA in storage.
     * POST /qAS
     *
     * @param CreateQAAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateQAAPIRequest $request)
    {
        $input = $request->all();

        $qA = $this->qARepository->create($input);

        return $this->sendResponse($qA->toArray(), 'Q A saved successfully');
    }

    /**
     * Display the specified QA.
     * GET|HEAD /qAS/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var QA $qA */
        $qA = $this->qARepository->find($id);

        if (empty($qA)) {
            return $this->sendError('Q A not found');
        }

        return $this->sendResponse($qA->toArray(), 'Q A retrieved successfully');
    }

    /**
     * Update the specified QA in storage.
     * PUT/PATCH /qAS/{id}
     *
     * @param int $id
     * @param UpdateQAAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateQAAPIRequest $request)
    {
        $input = $request->all();

        /** @var QA $qA */
        $qA = $this->qARepository->find($id);

        if (empty($qA)) {
            return $this->sendError('Q A not found');
        }

        $qA = $this->qARepository->update($input, $id);

        return $this->sendResponse($qA->toArray(), 'QA updated successfully');
    }

    /**
     * Remove the specified QA from storage.
     * DELETE /qAS/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var QA $qA */
        $qA = $this->qARepository->find($id);

        if (empty($qA)) {
            return $this->sendError('Q A not found');
        }

        $qA->delete();

        return $this->sendResponse($id, 'Q A deleted successfully');
    }
}
