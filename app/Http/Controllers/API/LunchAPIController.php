<?php

namespace App\Http\Controllers\API;
use App\Traits\CommonFunctionTrait;

use App\Http\Requests\API\CreateLunchAPIRequest;
use App\Http\Requests\API\UpdateLunchAPIRequest;
use App\Models\Lunch;
use App\Models\Lunch_setting;
use App\Models\Lunch_item;
use App\Models\Lunch_order;
use App\Models\Lunch_order_detail;
use App\Models\Resort;
use App\Models\Permission;
use App\Models\Access_code;
use App\Models\State;
use App\Models\Country;
use App\Models\Fav_resort;
use App\Models\Snowflake;
use App\Models\Rewards_point;
use App\Models\User;
use App\Models\Device;
use App\Models\Business;
use App\Repositories\LunchRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\Mail;
use App\Mail\WelcomeMail;
use DB;
use Validator;
use Response;

/**
 * Class LunchController
 * @package App\Http\Controllers\API
 */

    /*
      * Lunch API Controller
    */ 

class LunchAPIController extends AppBaseController
{
    /** @var  LunchRepository */
    private $lunchRepository;
    public $unauthorised = 401;
    public $successStatus = 200;
    public $badrequest = 400;
    use CommonFunctionTrait;

    public function __construct(LunchRepository $lunchRepo)
    {
        $this->lunchRepository = $lunchRepo;
    }

    /**
     * Display a listing of the Lunch.
     * GET|HEAD /lunches
     *
     * @param Request $request
     * @return Response
     */

/*Admin apis*/

    /*
      * Lunch Request API to Admin
    */ 

    public function lunchRequest(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
            $validator = Validator::make($request->all(), [
                'business_id' => 'required',
                'resort_id' => 'required',
              ]);
              if ($validator->fails()) {
                return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
              }
              else{
                $checkRequest = Lunch::where('business_service_id',$saveArray['business_id'])->first();
                if ($checkRequest) {
                    return response()->json([
                            'status'=>'1',
                            'message'=>'you have already requested for lunches'
                        ], $this->badrequest);
                }else{
                    $requestLunch = [
                        'resort_id'=>$saveArray['resort_id'],
                        'business_service_id'=>$saveArray['business_id'],
                        'request_status'=>'2',
                        'created_at'=>date('Y-m-d h:i:s'),
                    ];
                    $lunchID = Lunch::insertGetId($requestLunch);
                    $businessName = Business::where('id',$saveArray['business_id'])->first();
                    $name = $businessName['business_name'];
                    $content = 'We want to start Lunch service in our business please approve our request';
                    \Mail::send('lunchrequest', ['name' => $name, 'content' => $content], function ($message) { 
                        $message->to('resorts@communiski.com')->subject('Lunch approval request');
                    });
                    /*insert in lunch setting*/
                    $resortID = $businessName['resort_id'];
                    $business = Business::where('id',$saveArray['business_id'])->first();
                    $phone = $business['phone_number'];
                    $lunchSetting = [
                        'business_service_id'=>$saveArray['business_id'],
                        'lunch_id'=>$lunchID,
                        'phone'=>$phone?$phone:'',
                        'created_at'=>date('Y-m-d h:i:s'),
                    ];
                    $settings = Lunch_setting::insertGetId($lunchSetting);
                    /*insert in lunch setting*/
                    if($lunchID){
                        return response()->json([
                            'status'=>'1',
                            'message'=>'Request for lunches submit successfully',
                        ], $this->successStatus);
                    }
                    else{
                        return response()->json([
                            'status'=>'0',
                            'message'=>'error'
                        ], $this->badrequest);
                    }
                }
              }
        }
        else{
            return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }

    /*
      * Update Lunch Provider Details API by Group Admin
    */ 

    public function updateLunchSettings(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
            $validator = Validator::make($request->all(), [
                'business_id' => 'required'
              ]);
            if ($validator->fails()) {
                return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
            }
            else{
                $check = $this->businessPermission($request ,$saveArray['business_id'],$userExist['id']);
                if($check == 1){
                    $getSettings = Lunch_setting::where('business_service_id',$saveArray['business_id'])->first();

                    if($request->hasFile('business_license')){
                        $license = time().$request->business_license->getClientOriginalName();
                        $request->business_license->move(public_path('/lunch_images') . '/', $license);
                    }else{
                        $license = $getSettings['business_license'];
                    }
                    if($request->hasFile('business_insurance')){
                        $insurance = time().$request->business_insurance->getClientOriginalName();
                        $request->business_insurance->move(public_path('/lunch_images') . '/', $insurance);
                    }else{
                        $insurance = $getSettings['business_insurance'];
                    }
                    if($request->hasFile('food_hygiene_certificate')){
                        $food = time().$request->food_hygiene_certificate->getClientOriginalName();
                        $request->food_hygiene_certificate->move(public_path('/lunch_images') . '/', $food);
                    }else{
                        $food = $getSettings['food_hygiene_certificate'];
                    }

                    $days = array($saveArray['days_available']);
                    $days_available = implode(",", $days);

                    $updateSettings = [
                        'days_available'=>$days_available?$days_available:$getSettings['days_available'],
                        'order_cut_off_time'=>$saveArray['order_cut_off_time']?$saveArray['order_cut_off_time']:$getSettings['order_cut_off_time'],
                        'pick_up_location'=>$saveArray['pick_up_location']?$saveArray['pick_up_location']:$getSettings['pick_up_location'],
                        'pick_up_start_time'=>$saveArray['pick_up_start_time']?$saveArray['pick_up_start_time']:$getSettings['pick_up_start_time'],
                        'pick_up_end_time'=>$saveArray['pick_up_end_time']?$saveArray['pick_up_end_time']:$getSettings['pick_up_end_time'],
                        'phone'=>$saveArray['phone']?$saveArray['phone']:$getSettings['phone'],
                        'instructions'=>$saveArray['instructions']?$saveArray['instructions']:$getSettings['instructions'],
                        'tax'=>$saveArray['tax']?$saveArray['tax']:$getSettings['tax'],
                        'business_license'=>$license,
                        'business_insurance'=>$insurance,
                        'food_hygiene_certificate'=>$food,
                        'updated_at'=>date('Y-m-d h:i:s')
                    ];
                    $update = Lunch_setting::where('business_service_id',$saveArray['business_id'])
                    ->update($updateSettings);
                    if($update){
                        return response()->json([
                            'status'=>'1',
                            'message'=>'Settings Updated Successfully'
                        ], $this->successStatus);
                    }
                    else{
                        return response()->json([
                            'message'=>'you have not started lunches service yet',
                            'status'=>'0'
                        ], $this->badrequest);
                    }
                }
                else{
                    return response()->json(['message'=>'you have not Admin Permission to do this','status'=>'0'], $this->badrequest);
                }
            }
        }
        else{
            return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }

    /*
      * Get Lunch Provider Details API
    */   

    public function getLunchSettings(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
            $validator = Validator::make($request->all(), [
                'business_id' => 'required'
              ]);
              if ($validator->fails()) {
                return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
              }
              else{
                $check = $this->businessPermission($request ,$saveArray['business_id'],$userExist['id']);
                if($check == 1){
                    $getSettings = Lunch_setting::where('business_service_id',$saveArray['business_id'])->first();
                    if ($getSettings) {
                        $license = url('/lunch_images',$getSettings['business_license']);
                        $insurance = url('/lunch_images',$getSettings['business_insurance']);
                        $food = url('/lunch_images',$getSettings['food_hygiene_certificate']);
                        $settingDetails = array(
                            'id'=>(String)$getSettings['id'],
                            'lunch_id'=>(String)$getSettings['lunch_id'],
                            'resort_id'=>(String)$getSettings['resort_id'],
                            'business_id'=>(String)$getSettings['business_service_id'],
                            'days_available'=>$getSettings['days_available']?$getSettings['days_available']:'',
                            'order_cut_off_time'=>$getSettings['order_cut_off_time']?$getSettings['order_cut_off_time']:'',
                            'pick_up_location'=>$getSettings['pick_up_location']?$getSettings['pick_up_location']:'',
                            'pick_up_start_time'=>$getSettings['pick_up_start_time']?$getSettings['pick_up_start_time']:'',
                            'pick_up_end_time'=>$getSettings['pick_up_end_time']?$getSettings['pick_up_end_time']:'',
                            'phone'=>$getSettings['phone']?$getSettings['phone']:'',
                            'instructions'=>$getSettings['instructions']?$getSettings['instructions']:'',
                            'tax'=>$getSettings['tax']?$getSettings['tax']:'',
                            'business_license'=>$license?$license:'',
                            'business_insurance'=>$insurance?$insurance:'',
                            'food_hygiene_certificate'=>$food?$food:'',
                            'created_at'=>$userExist['created_at'],
                          );
                        return response()->json([
                            'status'=>'1',
                            'message'=>'Lunch Settings fetched successfully',
                            'settingDetails'=>$settingDetails
                            ], $this->successStatus);
                    }else{
                        return response()->json(['message'=>'you have not started lunches service yet','status'=>'0'], $this->badrequest);
                    }
                }
                else{
                    $getSettings = Lunch_setting::where('business_service_id',$saveArray['business_id'])->first();
                    $settingDetails = array(
                        'lunch_id'=>(String)$getSettings['lunch_id'],
                        'business_id'=>(String)$getSettings['business_service_id'],
                        'days_available'=>$getSettings['days_available']?$getSettings['days_available']:'',
                        'order_cut_off_time'=>$getSettings['order_cut_off_time']?$getSettings['order_cut_off_time']:'',
                        'pick_up_location'=>$getSettings['pick_up_location']?$getSettings['pick_up_location']:'',
                        'pick_up_start_time'=>$getSettings['pick_up_start_time']?$getSettings['pick_up_start_time']:'',
                        'pick_up_end_time'=>$getSettings['pick_up_end_time']?$getSettings['pick_up_end_time']:'',
                        'phone'=>$getSettings['phone']?$getSettings['phone']:'',
                        'instructions'=>$getSettings['instructions']?$getSettings['instructions']:'',
                      );
                    return response()->json(['status'=>'1','message'=>'Lunch Settings fetched successfully','settingDetails'=>$settingDetails], $this->successStatus);
                    // return response()->json(['message'=>'you have not Admin Permission to do this','status'=>'0'], $this->badrequest);
                }
            }
        }
        else{
            return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }

    /*
      * Add Lunch Items (menus) API by Admin to a Lunch Provider
    */ 

    public function addLunchItems(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
            $validator = Validator::make($request->all(), [
                'lunch_id' => 'required',
                'lunch_option_type' => 'required',
                'item_name' => 'required',
            ]);
            if ($validator->fails()) {
                return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
            }
            else{
                $getBusiness = Lunch_setting::where('lunch_id',$saveArray['lunch_id'])->first();
                $check = $this->businessPermission($request ,$getBusiness['business_service_id'],$userExist['id']);
                if ($check == 1) {
                    $checkLunch = Lunch::where('id',$saveArray['lunch_id'])->first();
                    if ($checkLunch) {
                        $dietary = array($saveArray['dietary_prefernces']);
                        $multiDietary = implode(",", $dietary);

                        $allergy = array($saveArray['allergy_info']);
                        $multiAllergy = implode(",", $allergy);

                        if($request->hasFile('item_picture')){
                            $file = time().$request->item_picture->getClientOriginalName();
                            $request->item_picture->move(public_path('/lunch_images/lunch_items') . '/', $file);
                        }
                        else{
                            $file = 'lunch_item.jpg';
                        }

                        $addItem = [
                            'lunch_option_type'=>$saveArray['lunch_option_type'],
                            'lunch_id'=>$saveArray['lunch_id'],
                            'item_status'=>'1',
                            'name'=>$saveArray['item_name'],
                            'item_description'=>$saveArray['item_description']?$saveArray['item_description']:'',
                            'dietary_prefernces'=>$multiDietary?$multiDietary:'',
                            'allergy_info'=>$multiAllergy?$multiAllergy:'',
                            'item_picture'=>$file,
                            'created_at'=>date('Y-m-d h:i:s'),
                        ];
                        $itemId = Lunch_item::insertGetId($addItem);
                        $getItem = Lunch_item::where('id',$itemId)->first();
                        $itemimage = url('/lunch_images/lunch_items',$getItem['item_picture']);
                        $lunchItem = array(
                            'item_id'=>$getItem['id'],
                            'lunch_option_type'=>$getItem['lunch_option_type'],
                            'item_status'=>$getItem['item_status'],
                            'name'=>$getItem['name'],
                            'item_picture'=>$itemimage,
                            'item_description'=>$getItem['item_description']?$getItem['item_description']:'',
                            'dietary_prefernces'=>$getItem['dietary_prefernces']?$getItem['dietary_prefernces']:'',
                            'allergy_info'=>$getItem['allergy_info']?$getItem['allergy_info']:'',
                            'created_at'=>$getItem['created_at']->format('Y-m-d'),
                        );
                        if($itemId){
                            return response()->json(['status'=>'1','message'=>'Item Added Successfully','lunchItem'=>$lunchItem,], $this->successStatus);
                        }
                        else{
                            return response()->json(['message'=>'Error','status'=>'0'], $this->badrequest);
                        }
                    }else{
                        return response()->json(['message'=>'Lunch not found','status'=>'0'], $this->badrequest);
                    }
                }
                else{
                    return response()->json(['message'=>'you have not Admin Permission to do this','status'=>'0'], $this->badrequest);
                }
            }
        }
        else{
            return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }

    /*
      * Get Lunch Provider Items API in Admin section
    */ 

    public function getLunchItems(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
            $validator = Validator::make($request->all(), [
                'lunch_id' => 'required',
            ]);
            if ($validator->fails()) {
                return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
            }
            else{
                $getBItems = Lunch_item::where('lunch_id',$saveArray['lunch_id'])
                ->where('lunch_option_type','Bread')
                ->get();
                $getBService = json_decode(json_encode($getBItems), true);

                $getFItems = Lunch_item::where('lunch_id',$saveArray['lunch_id'])
                ->where('lunch_option_type','Filling')
                ->get();
                $getFService = json_decode(json_encode($getFItems), true);

                $getOItems = Lunch_item::where('lunch_id',$saveArray['lunch_id'])
                ->where('lunch_option_type','Other')
                ->get();
                $getOService = json_decode(json_encode($getOItems), true);

                $getSItems = Lunch_item::where('lunch_id',$saveArray['lunch_id'])
                ->where('lunch_option_type','Snack')
                ->get();
                $getSService = json_decode(json_encode($getSItems), true);

                $getDItems = Lunch_item::where('lunch_id',$saveArray['lunch_id'])
                ->where('lunch_option_type','Drink')
                ->get();

                $getDService = json_decode(json_encode($getDItems), true);

                $breadArray = [];
                $fillingArray = [];
                $otherArray = [];
                $snackArray = [];
                $drinkArray = [];

                foreach ($getBService as $bkey) {
                    $bimage = url('/lunch_images/lunch_items',$bkey['item_picture']);
                    $blisting['item_id'] = (String)$bkey['id'];
                    $blisting['lunch_option_type'] = $bkey['lunch_option_type'];
                    $blisting['item_status'] = $bkey['item_status'];
                    $blisting['name'] = $bkey['name'];
                    $blisting['item_picture'] = $bimage;
                    $blisting['item_description'] = $bkey['item_description']?$bkey['item_description']:'';
                    $blisting['dietary_prefernces'] = $bkey['dietary_prefernces']?$bkey['dietary_prefernces']:'';
                    $blisting['allergy_info'] = $bkey['allergy_info']?$bkey['allergy_info']:'';
                    $blisting['created_at'] = $bkey['created_at'];

                    $breadArray[] = $blisting;
                }

                foreach ($getFService as $fkey) {
                    $fimage = url('/lunch_images/lunch_items',$fkey['item_picture']);
                    $flisting['item_id'] = (String)$fkey['id'];
                    $flisting['lunch_option_type'] = $fkey['lunch_option_type'];
                    $flisting['item_status'] = $fkey['item_status'];
                    $flisting['name'] = $fkey['name'];
                    $flisting['item_picture'] = $fimage;
                    $flisting['item_description'] = $fkey['item_description']?$fkey['item_description']:'';
                    $flisting['dietary_prefernces'] = $fkey['dietary_prefernces']?$fkey['dietary_prefernces']:'';
                    $flisting['allergy_info'] = $fkey['allergy_info']?$fkey['allergy_info']:'';
                    $flisting['created_at'] = $fkey['created_at'];
                    $fillingArray[] = $flisting;
                }

                foreach ($getOService as $okey) {
                    $oimage = url('/lunch_images/lunch_items',$okey['item_picture']);
                    $olisting['item_id'] = (String)$okey['id'];
                    $olisting['lunch_option_type'] = $okey['lunch_option_type'];
                    $olisting['item_status'] = $okey['item_status'];
                    $olisting['name'] = $okey['name'];
                    $olisting['item_picture'] = $oimage;
                    $olisting['item_description'] = $okey['item_description']?$okey['item_description']:'';
                    $olisting['dietary_prefernces'] = $okey['dietary_prefernces']?$okey['dietary_prefernces']:'';
                    $olisting['allergy_info'] = $okey['allergy_info']?$okey['allergy_info']:'';
                    $olisting['created_at'] = $okey['created_at'];
                    $otherArray[] = $olisting;
                }

                foreach ($getSService as $skey) {
                    $simage = url('/lunch_images/lunch_items',$skey['item_picture']);
                    $slisting['item_id'] = (String)$skey['id'];
                    $slisting['lunch_option_type'] = $skey['lunch_option_type'];
                    $slisting['item_status'] = $skey['item_status'];
                    $slisting['name'] = $skey['name'];
                    $slisting['item_picture'] = $simage;
                    $slisting['item_description'] = $skey['item_description']?$skey['item_description']:'';
                    $slisting['dietary_prefernces'] = $skey['dietary_prefernces']?$skey['dietary_prefernces']:'';
                    $slisting['allergy_info'] = $skey['allergy_info']?$skey['allergy_info']:'';
                    $slisting['created_at'] = $skey['created_at'];
                    $snackArray[] = $slisting;
                }

                foreach ($getDService as $dkey) {
                    $dimage = url('/lunch_images/lunch_items',$dkey['item_picture']);
                    $dlisting['item_id'] = (String)$dkey['id'];
                    $dlisting['lunch_option_type'] = $dkey['lunch_option_type'];
                    $dlisting['item_status'] = $dkey['item_status'];
                    $dlisting['name'] = $dkey['name'];
                    $dlisting['item_picture'] = $dimage;
                    $dlisting['item_description'] = $dkey['item_description']?$dkey['item_description']:'';
                    $dlisting['dietary_prefernces'] = $dkey['dietary_prefernces']?$dkey['dietary_prefernces']:'';
                    $dlisting['allergy_info'] = $dkey['allergy_info']?$dkey['allergy_info']:'';
                    $dlisting['created_at'] = $dkey['created_at'];
                    $drinkArray[] = $dlisting;
                }
                return response()->json([
                    'status'=>'1',
                    'message'=>'data fetched successfully',
                    'breadListing'=>$breadArray,
                    'fillingListing'=>$fillingArray,
                    'otherListing'=>$otherArray,
                    'snackListing'=>$snackArray,
                    'drinkListing'=>$drinkArray
                ], $this->successStatus);
            }
        }
        else{
            return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }

    /*
      * Mark available and unavailable for Lunch Items in Admin section
    */   

    public function updateStatus(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
            $validator = Validator::make($request->all(), [
                'item_id' => 'required',
            ]);
            if ($validator->fails()) {
                return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
            }
            else{
                $checkItem = Lunch_item::where('id',$saveArray['item_id'])->first();
                if ($checkItem) {
                    $getBusiness = Lunch_setting::where('lunch_id',$checkItem['lunch_id'])->first();
                    $check = $this->businessPermission($request ,$getBusiness['business_service_id'],$userExist['id']);
                    if ($check == 1) {
                        if ($checkItem['item_status'] == '1' || $checkItem['item_status'] == '3') {
                            $markunavail = [
                                'item_status'=>'2',
                                'updated_at'=>date('Y-m-d h:i:s')
                            ];
                            $update = Lunch_item::where(['id'=>$saveArray['item_id']])->update($markunavail);
                            return response()->json(['status'=>'1','message'=>'Item Unavailable',], $this->successStatus);
                        }
                        elseif ($checkItem['item_status'] == '2' || $checkItem['item_status'] == '3') {
                            $markavail = [
                                'item_status'=>'1',
                                'updated_at'=>date('Y-m-d h:i:s')
                            ];
                            $update = Lunch_item::where(['id'=>$saveArray['item_id']])->update($markavail);
                            return response()->json(['status'=>'1','message'=>'Item available successfully',], $this->successStatus);
                        }
                    }else{
                        return response()->json(['message'=>'you have not Admin Permission to do this','status'=>'0'], $this->badrequest);
                    }
                }
                else{
                    return response()->json(['status'=>'1','message'=>'Item not found',], $this->successStatus);
                }
            }
        }
        else{
            return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }

    public function deleteLunchItem(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
            $validator = Validator::make($request->all(), [  
                'item_id' => 'required',
              ]);
              if ($validator->fails()) {
                return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
            }
            else{
                $getItem = Lunch_item::where(['id'=>$saveArray['item_id']])->first();
                if ($getItem) {
                    $getBusiness = Lunch_setting::where('lunch_id',$getItem['lunch_id'])->first();
                    $check = $this->businessPermission($request ,$getBusiness['business_service_id'],$userExist['id']);
                    if ($check == 1) {
                        $deleteItem = [
                            'item_status'=>'3',
                            'updated_at'=>date('Y-m-d h:i:s')
                        ];
                        $update = Lunch_item::where(['id'=>$saveArray['item_id']])->update($deleteItem);
                        if ($deleteItem) {
                            return response()->json([
                                'status'=>'1',
                                'message'=>'Item deleted successfully'
                            ], $this->successStatus);
                        }
                    }else{
                        return response()->json(['message'=>'you have not Admin Permission to do this','status'=>'0'], $this->badrequest);
                    }
                }else{
                    return response()->json([
                        'status'=>'1',
                        'message'=>'Item not found'
                    ], $this->successStatus);
                }
            }
        }
        else{
            return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }

    /*admin apis*/

/*User APIs*/

    /*
      * API for Get Lunch Items for selecting on Lunch Order
    */ 

    public function availablelunchItems(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
            $validator = Validator::make($request->all(), [
                'lunch_id' => 'required',
            ]);
            if ($validator->fails()) {
                return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
            }
            else{
                $getBItems = Lunch_item::where('lunch_id',$saveArray['lunch_id'])
                // ->where('lunch_id',$saveArray['lunch_id'])
                ->where('lunch_option_type','Bread')
                ->where('item_status','1')
                ->get();
                $getBService = json_decode(json_encode($getBItems), true);
                // print_r($getBService);die();
                $getFItems = Lunch_item::where('lunch_id',$saveArray['lunch_id'])
                ->where('lunch_option_type','Filling')
                ->where('item_status','1')
                ->get();
                $getFService = json_decode(json_encode($getFItems), true);

                $getOItems = Lunch_item::where('lunch_id',$saveArray['lunch_id'])
                ->where('lunch_option_type','Other')
                ->where('item_status','1')
                ->get();
                $getOService = json_decode(json_encode($getOItems), true);

                $getSItems = Lunch_item::where('lunch_id',$saveArray['lunch_id'])
                ->where('lunch_option_type','Snack')
                ->where('item_status','1')
                ->get();
                $getSService = json_decode(json_encode($getSItems), true);

                $getDItems = Lunch_item::where('lunch_id',$saveArray['lunch_id'])
                ->where('lunch_option_type','Drink')
                ->where('item_status','1')
                ->get();

                $getDService = json_decode(json_encode($getDItems), true);

                $breadArray = [];
                $fillingArray = [];
                $otherArray = [];
                $snackArray = [];
                $drinkArray = [];

                foreach ($getBService as $bkey) {
                    $bimage = url('/lunch_images/lunch_items',$bkey['item_picture']);
                    $blisting['item_id'] = (String)$bkey['id'];
                    $blisting['lunch_option_type'] = $bkey['lunch_option_type'];
                    $blisting['item_status'] = $bkey['item_status'];
                    $blisting['name'] = $bkey['name'];
                    $blisting['item_picture'] = $bimage;
                    $blisting['item_description'] = $bkey['item_description']?$bkey['item_description']:'';
                    $blisting['dietary_prefernces'] = $bkey['dietary_prefernces']?$bkey['dietary_prefernces']:'';
                    $blisting['allergy_info'] = $bkey['allergy_info']?$bkey['allergy_info']:'';
                    $blisting['created_at'] = $bkey['created_at'];

                    $breadArray[] = $blisting;
                }

                foreach ($getFService as $fkey) {
                    $fimage = url('/lunch_images/lunch_items',$fkey['item_picture']);
                    $flisting['item_id'] = (String)$fkey['id'];
                    $flisting['lunch_option_type'] = $fkey['lunch_option_type'];
                    $flisting['item_status'] = $fkey['item_status'];
                    $flisting['name'] = $fkey['name'];
                    $flisting['item_picture'] = $fimage;
                    $flisting['item_description'] = $fkey['item_description']?$fkey['item_description']:'';
                    $flisting['dietary_prefernces'] = $fkey['dietary_prefernces']?$fkey['dietary_prefernces']:'';
                    $flisting['allergy_info'] = $fkey['allergy_info']?$fkey['allergy_info']:'';
                    $flisting['created_at'] = $fkey['created_at'];
                    $fillingArray[] = $flisting;
                }

                foreach ($getOService as $okey) {
                    $oimage = url('/lunch_images/lunch_items',$okey['item_picture']);
                    $olisting['item_id'] = (String)$okey['id'];
                    $olisting['lunch_option_type'] = $okey['lunch_option_type'];
                    $olisting['item_status'] = $okey['item_status'];
                    $olisting['name'] = $okey['name'];
                    $olisting['item_picture'] = $oimage;
                    $olisting['item_description'] = $okey['item_description']?$okey['item_description']:'';
                    $olisting['dietary_prefernces'] = $okey['dietary_prefernces']?$okey['dietary_prefernces']:'';
                    $olisting['allergy_info'] = $okey['allergy_info']?$okey['allergy_info']:'';
                    $olisting['created_at'] = $okey['created_at'];
                    $otherArray[] = $olisting;
                }

                foreach ($getSService as $skey) {
                    $simage = url('/lunch_images/lunch_items',$skey['item_picture']);
                    $slisting['item_id'] = (String)$skey['id'];
                    $slisting['lunch_option_type'] = $skey['lunch_option_type'];
                    $slisting['item_status'] = $skey['item_status'];
                    $slisting['name'] = $skey['name'];
                    $slisting['item_picture'] = $simage;
                    $slisting['item_description'] = $skey['item_description']?$skey['item_description']:'';
                    $slisting['dietary_prefernces'] = $skey['dietary_prefernces']?$skey['dietary_prefernces']:'';
                    $slisting['allergy_info'] = $skey['allergy_info']?$skey['allergy_info']:'';
                    $slisting['created_at'] = $skey['created_at'];
                    $snackArray[] = $slisting;
                }

                foreach ($getDService as $dkey) {
                    $dimage = url('/lunch_images/lunch_items',$dkey['item_picture']);
                    $dlisting['item_id'] = (String)$dkey['id'];
                    $dlisting['lunch_option_type'] = $dkey['lunch_option_type'];
                    $dlisting['item_status'] = $dkey['item_status'];
                    $dlisting['name'] = $dkey['name'];
                    $dlisting['item_picture'] = $dimage;
                    $dlisting['item_description'] = $dkey['item_description']?$dkey['item_description']:'';
                    $dlisting['dietary_prefernces'] = $dkey['dietary_prefernces']?$dkey['dietary_prefernces']:'';
                    $dlisting['allergy_info'] = $dkey['allergy_info']?$dkey['allergy_info']:'';
                    $dlisting['created_at'] = $dkey['created_at'];
                    $drinkArray[] = $dlisting;
                }
                return response()->json([
                    'status'=>'1',
                    'message'=>'data fetched successfully',
                    'breadListing'=>$breadArray,
                    'fillingListing'=>$fillingArray,
                    'otherListing'=>$otherArray,
                    'snackListing'=>$snackArray,
                    'drinkListing'=>$drinkArray
                ], $this->successStatus);
            }
        }
        else{
            return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }

    /*
      * API for Get Active Providers for Lunches
    */   

    public function countrystateResortLunches(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
            $rules = array(
                'user_id' => 'required_without_all:state,country,resort_id',
                'country' => 'required_without_all:user_id,state,resort_id',
                'state' => 'required_without_all:user_id,country,resort_id',
                'resort_id' => 'required_without_all:user_id,country,state'
            );
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
            }
            else{
                if($saveArray['user_id'] == $userExist['id']){
                    $countries = Country::all();
                    $countryArray = [];
                    if(count($countries)){
                        foreach($countries as $ls){
                            $listing['country_id'] = (String)$ls['id'];
                            $listing['country_name'] = $ls['name'];
                            $listing['created_at'] = date('Y-m-d h:i:s');
                            $countryArray[] = $listing;
                        }
                        return response()->json([
                            'status'=>'1',
                            'message'=>'Countries fetched successfully',
                            'countryListing'=>$countryArray
                        ], $this->successStatus);
                    }
                    else{
                        return response()->json([
                            'message'=>'No Country Found',
                            'status'=>'0'
                        ], $this->successStatus);
                    }
                }
                elseif(in_array($saveArray['country'], ['USA','Canada'], true ) ){
                    $country_id = Country::where(['name'=>$saveArray['country']])->select('id')->first();
                    $resorts = Resort::select('state')->groupBy('state')->get();
                    $state_id = $resorts;
                    $states = State::where(['country_id'=>$country_id['id']])
                    ->whereIn('id', $state_id)
                    ->select('id','name')->get();
                    $stateArray = [];
                    if(count($states)){
                        foreach($states as $state){
                            $listing['state_id'] = (String)$state['id'];
                            $listing['state_name'] = $state['name'];
                            $listing['created_at'] = date('Y-m-d h:i:s');
                            $stateArray[] = $listing;
                        }
                        return response()->json([
                            'status'=>'1',
                            'message'=>'States fetched successfully',
                            'stateListing'=>$stateArray
                        ], $this->successStatus);
                    }
                    else{
                        return response()->json([
                            'message'=>'No State Found',
                            'status'=>'0'
                        ], $this->successStatus);
                    }
                }
                elseif($saveArray['state']){
                    $checkState = State::where(['name'=>$saveArray['state']])->select('id','name')->first();
                    if ($checkState) {
                        $getResort = Resort::where(['state'=>$checkState['id']])->get();
                        $resortArray = [];
                        if(count($getResort)){
                            foreach($getResort as $ls){
                                $checkIsAdmin = Permission::where(['resort_id'=>$ls['id']])->where(['user_id'=>$userExist['id']])->where(['role_type'=>'ADM'])->first();
                                if ($checkIsAdmin) {
                                    $isAdmin = 'true';
                                }else{
                                    $isAdmin = 'false';
                                }
                                $checkIsFav = Fav_resort::where('resort_id',$ls['id'])
                                ->where('user_id',$userExist['id'])
                                ->where('favourite','=','1')
                                ->first();
                                if ($checkIsFav['favourite'] == 1) {
                                    $isFav = 'Yes';
                                }else{
                                    $isFav = 'No';
                                }
                                $image = url('/resort_images',$ls['image']);
                                $countryName = Country::where(['id'=>$ls['country']])->first();
                                $stateName = State::where(['id'=>$ls['state']])->first();
                                $listing['is_favourite'] = $isFav;
                                $listing['isAdmin'] = $isAdmin;
                                $listing['resort_id'] = (String)$ls['id'];
                                $listing['resort_name'] = $ls['resort_name'];
                                $listing['address'] = $ls['address'];
                                $listing['phone_no'] = $ls['phone_no'];
                                $listing['toplat'] = $ls['top_latitude']?$ls['top_latitude']:'';
                                $listing['toplong'] = $ls['top_longitude']?$ls['top_longitude']:'';
                                $listing['lat'] = $ls['latitude'];
                                $listing['long'] = $ls['longitude'];
                                $listing['country_id'] = $ls['country'];
                                $listing['country'] = $countryName['name'];
                                $listing['state_id'] = $ls['state'];
                                $listing['state'] = $stateName['name'];
                                $listing['bcolor'] = $ls['bcolor'];
                                $listing['fcolor'] = $ls['fcolor'];
                                $listing['image'] = $image;
                                $listing['lift_names'] = $ls['lift_names'];
                                $listing['lift_numbers'] = $ls['lift_numbers'];
                                $listing['run_names'] = $ls['run_names'];
                                $listing['run_numbers'] = $ls['run_numbers'];
                                $listing['created_at'] = date('Y-m-d h:i:s');
                                $resortArray[] = $listing;
                            }
                            return response()->json([
                                'status'=>'1',
                                'message'=>'Resorts fetched successfully',
                                'resortListing'=>$resortArray
                            ], $this->successStatus);
                        }
                        else{
                            return response()->json([
                                'message'=>'No resorts',
                                'status'=>'1',
                                'resortListing'=>$resortArray
                            ], $this->successStatus);
                        }
                    }
                    else{
                        return response()->json([
                                'message'=>'No state Found',
                                'status'=>'0'
                            ], $this->badrequest);
                    }
                }
                elseif($saveArray['resort_id']){
                    $getBusiness = Lunch::where('resort_id',$saveArray['resort_id'])
                    ->where('request_status','4')
                    ->get();
                    $getProviders = json_decode(json_encode($getBusiness), true);
                    if ($getProviders) {
                        $businessArray = [];
                        foreach($getProviders as $lunch){
                            // $checkLunch = Lunch::where('business_service_id',$ls['id'])
                            // ->where('request_status','4')
                            // ->get();
                            // foreach ($checkLunch as $lunch) {
                                $businesName = Business::where('id',$lunch['business_service_id'])->first();
                                $lunchDetails = Lunch_setting::where('lunch_id',$lunch['id'])
                                ->first();
                                $listing['lunch_id'] = (String)$lunch['id'];
                                $listing['provider_id'] = (String)$lunch['business_service_id'];
                                $listing['provider_name'] = (String)$businesName['business_address'];
                                $listing['days_available'] = $lunchDetails['days_available'];
                                $listing['created_at'] = $lunch['created_at'];
                                $businessArray[] = $listing;
                            // }
                        }
                        return response()->json([
                            'status'=>'1',
                            'message'=>'Providers fetched successfully',
                            'providerArray'=>$businessArray
                        ], $this->successStatus);
                    }else{
                        return response()->json([
                                'status'=>'1',
                                'message'=>'No Providers at this resort',
                                'providerArray'=>[]
                            ], $this->successStatus);
                    }
                }else{
                    return response()->json([
                        'message'=>'Error',
                        'status'=>'0'
                    ], $this->badrequest);
                }
            }
        }
        else{
            return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }

    /*
      * API for Order a Lunch
    */    

    public function orderLunch(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
            $validator = Validator::make($request->all(), [
                'provider_id' => 'required',
                'lunch_date' => 'required',
                'user_ids_name' => 'required',
                'phone_no' => 'required',
              ]);
              if ($validator->fails()) {
                return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
            }
            else{
                $getRstring = $this->generateRandomString();
                $order = $getRstring;
                // $user_id = $userExist['id'];
                // $date = date('d-m-h:i');
                // $order = $order_id.''.$user_id.'&'.$date;

                $multiId = array($saveArray['user_ids_name']);
                $multiIds = implode(",", $multiId);


                $multi_users = explode(',',$multiIds);
                $ordersArray = array();
                foreach ($multi_users as $key ) {
                    $multi_user_ordering = explode('$',$key);
                    $ordersArray[] = $multi_user_ordering[0];
                }
                $usersIDs = implode(',', $ordersArray);

                $idsCount = explode(',',$saveArray['user_ids_name']);
                $count = count($idsCount);

                $country_name['name'] = 'USA';

                if ($country_name['name'] == 'USA') {
                    $lunchRate = '10';
                }elseif ($country_name['name'] == 'Canada') {
                    $lunchRate = '12.50';
                }

                $getTax = Lunch_setting::where('business_service_id',$saveArray['provider_id'])->first();
                $tax = $getTax['tax'];
                $sub = $lunchRate + $tax;

                $total = $sub * $count;

                $requestLunch = [
                    'orderID'=>$order,
                    'business_service_id'=>$saveArray['provider_id'],
                    'user_id'=>$userExist['id'],
                    'order_date'=>$saveArray['lunch_date'],
                    'phone_no'=>$saveArray['phone_no'],
                    'child_ids'=>$usersIDs,
                    'order_count'=>$count,
                    'subtotal'=>$sub,
                    'tax'=>$tax,
                    'total_amount'=>$total,
                    'payment_status'=>'Paid',
                    'order_status'=>'Incomplete',
                    'created_at'=> date('Y-m-d h:i:s'),
                ];

                $lunchID = Lunch_order::insertGetId($requestLunch);

                if ($lunchID) {
                    $users = $saveArray['user_ids_name'];

                    $multi_users = explode(',',$users);
                    $ordersArray = array();
                    foreach ($multi_users as $key ) {
                        $multi_user_ordering = explode('$',$key);
                        $ordersArray = array(
                            'orderID'=>$order,
                            'child_users'=>$multi_user_ordering[0],
                            'parent_user'=>$multi_user_ordering[1],
                            'created_at'=>date('Y-m-d h:i:s'),
                        );
                        $lunchID = Lunch_order_detail::insert($ordersArray);
                    }
                    $getLunchDetails = Lunch_order_detail::where('orderID',$order)->get();
                    $luncesDetails = json_decode(json_encode($getLunchDetails), true);
                    $orderArray = [];
                    foreach ($luncesDetails as $lun) {
                        $ord['id'] = $lun['id'];
                        $ord['order_id'] = $order;
                        $ord['child_user'] = (String)$lun['child_users'];
                        $ord['parent_user'] = (String)$lun['parent_user'];
                        $ord['created_at'] = $lun['created_at'];
                        $orderArray[] = $ord;
                    }

                    /*send notifications*/

                    $myArray = explode(',', $usersIDs);
                    $del_val= array($userExist['id']);
                    $username = User::where('id',$userExist['id'])->first();
                    $myarr = array_diff( $myArray, $del_val);
                    $message = array(
                        'message'=>" ". $username['name'] ." has ordered lunch for you, but you need to make your choices",
                        'type'=>'lunch',
                        'order_id'=> $order
                    );
                    $this->send_notification($request ,$myarr ,$message);
                    
                    /*send notifications*/
                                     
                    return response()->json([
                        'status'=>'1',
                        'message'=>'Lunch Ordered successfully',
                        'order_id'=>$order,
                        'orderArray'=>$orderArray
                    ], $this->successStatus);
                }else{
                    return response()->json([
                        'status'=>'1',
                        'message'=>'Lunch Order Failed'
                    ], $this->successStatus);
                }
            }
        }
        else{
            return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }

    /*
      * API for Choose lunches by Parent user and child user
    */  

    public function chooseLunch(Request $request){
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
            $validator = Validator::make($request->all(), [
                'order_items' => 'required',
            ]);
            if ($validator->fails()) {
                return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
            }
            else{

                $items = $saveArray['order_items'];

                $multi_items = explode('$',$items);

                $chooseItemsArray = array();

                foreach ($multi_items as $key ) {
                    $multi_user_items = explode(',',$key);
                    $chooselunchid = $multi_user_items[5];
                    $order = $multi_user_items[6];
                    $chooseItemsArray = array(
                        'bread_id'=>$multi_user_items[0],
                        'filling_id'=>$multi_user_items[1],
                        'main_id'=>$multi_user_items[2],
                        'snack_id'=>$multi_user_items[3],
                        'drink_id'=>$multi_user_items[4],
                        'notes'=>$multi_user_items[7],
                        'status'=>'1',
                        'created_at'=>date('Y-m-d h:i:s'),
                    );
                    $lunchItems = Lunch_order_detail::where('id',$chooselunchid)
                    ->update($chooseItemsArray);

                    /*updating incomplete status to pending*/
                    $checkCount = Lunch_order_detail::where('orderID',$order)
                    ->where('status','0')->get();
                    $count = $checkCount->count();
                    if ($count == 0) {
                        $update_status = [
                            'order_status' => 'Pending',
                            'updated_at' => date('Y-m-d h:i:s')
                        ];
                        $update = Lunch_order::where('orderID',$order)->update($update_status);
                    }
                }
                /*updating incomplete status to pending*/

                if($lunchItems){
                    return response()->json([
                        'status'=>'1',
                        'message'=>'Items Choosen Successfully'
                    ], $this->successStatus);
                }
                else{
                    return response()->json([
                        'message'=>'You have an error with wrong credentials',
                        'status'=>'0'
                    ], $this->badrequest);
                }
            }
        }else{
            return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }

    /*
      * API for get Ordered Lunch Detail
    */  

    public function getOrderedLunch(Request $request){
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
            $validator = Validator::make($request->all(), [
                'order_id' => 'required',
            ]);
            if ($validator->fails()) {
                return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
            }
            else{
                $checkOrder = Lunch_order_detail::where('orderID',$saveArray['order_id'])->get();
                if ($checkOrder) {
                    $luncesDetails = json_decode(json_encode($checkOrder), true);
                    $order_data = Lunch_order::where('orderID',$saveArray['order_id'])->first();
                    $business_name = Business::where('id',$order_data['business_service_id'])->first();
                    $resort_name = Resort::where('id',$order_data['resort_id'])->first();
                    $order_details = array(
                        'order_id'=>(String)$order_data['orderID'],
                        'provider_id'=>$order_data['business_service_id'],
                        'provider_name'=>$business_name['business_address'],
                        'user_id'=>$order_data['user_id'],
                        'order_date'=>$order_data['order_date']->format('Y-m-d'),
                        'order_count'=>$order_data['order_count'],
                        'subtotal'=>$order_data['subtotal'],
                        'tax'=>$order_data['tax'],
                        'total_amount'=>$order_data['total_amount'],
                        'order_status'=>$order_data['order_status'],
                    );

                    $providerData = Lunch_setting::where('business_service_id',$order_data['business_service_id'])->first();
                    $provider_detail = array(
                        'provider_id'=>(String)$providerData['business_service_id'],
                        'order_cut_off_time'=>$providerData['order_cut_off_time'],
                        'pick_up_location'=>$providerData['pick_up_location'],
                        'pick_up_start_time'=>$providerData['pick_up_start_time'],
                        'pick_up_end_time'=>$providerData['pick_up_end_time'],
                        'phone'=>$providerData['phone'],
                        'instructions'=>$providerData['instructions'],
                    );
                    $lu_id = Lunch::where('business_service_id',$providerData['business_service_id'])->first();
                    $lunchID = $lu_id['id'];
                    $orderedArray = [];

                    foreach ($luncesDetails as $lunch) {
                        $bread = Lunch_item::where('id',$lunch['bread_id'])->select('name')->first();
                        $filling = Lunch_item::where('id',$lunch['filling_id'])->select('name')->first();
                        $main = Lunch_item::where('id',$lunch['main_id'])->select('name')->first();
                        $snack = Lunch_item::where('id',$lunch['snack_id'])->select('name')->first();
                        $drink = Lunch_item::where('id',$lunch['drink_id'])->select('name')->first();
                        $userDetail = User::where('id',$lunch['child_users'])->first();
                        if ($userDetail) {
                            $image = url('/images',$userDetail['image']);
                        }else{
                            $dumimage = 'default.jpg';
                            $image = url('/images',$dumimage);
                        }
                        if ($userDetail['name'] == '') {
                            $name = $lunch['child_users'];
                        }
                        $ord['order_id'] = $saveArray['order_id'];
                        $ord['id'] = $lunch['id'];
                        $ord['child_user'] = $lunch['child_users'];
                        $ord['child_user_name'] = $userDetail['name']?$userDetail['name']:$name;
                        $ord['child_user_image'] = $image;
                        $ord['parent_user'] = (String)$lunch['parent_user'];
                        // $ord['bread'] = (String)$lunch['bread_id'];
                        $ord['bread_item'] = $bread;
                        // $ord['filling'] = $lunch['filling_id'];
                        $ord['filling_item'] = $filling;
                        // $ord['main'] = $lunch['main_id'];
                        $ord['main_item'] = $main;
                        // $ord['snack'] = $lunch['snack_id'];
                        $ord['snack_item'] = $snack;
                        // $ord['drink'] = $lunch['drink_id'];
                        $ord['drink_item'] = $drink;
                        $ord['notes'] = $lunch['notes'];
                        $ord['status'] = $lunch['status'];
                        // $ord['provider_detail'] = $providerData;
                        $ord['created_at'] = $lunch['created_at'];
                        $orderedArray[] = $ord;
                    }
                    return response()->json([
                        'status'=>'1',
                        'message'=>'Ordered Lunch fetched successfully',
                        'lunch_id'=>$lunchID,
                        'providerArray'=>$provider_detail,
                        'orderArray'=>$order_details,
                        'orderedArray'=>$orderedArray,
                    ], $this->successStatus);
                }else{
                    return response()->json(['message'=>'error','status'=>'0'], $this->badrequest);
                }
            }
        }else{
            return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }

    /*
      * API for get Upcomming Orders
    */  

    public function upcomming_old_Orders(Request $request){
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
            $validator = Validator::make($request->all(), [
                // 'order_id' => 'required',
            ]);
            if ($validator->fails()) {
                return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
            }
            else{
                if (!empty($saveArray['from']) && !empty($saveArray['to'])) {
                    $from = $saveArray['from'];
                    $to = $saveArray['to'];
                    $today = date('Y-m-d');
                    $upcommingOrders = Lunch_order::where('user_id',$userExist['id'])
                    ->where('order_date','>=',$today)
                    ->orderBy('order_date','DESC')
                    ->get();
                    $pastOrders = Lunch_order::where('user_id',$userExist['id'])
                    ->whereBetween('order_date', [$from, $to])
                    ->orderBy('order_date','DESC')
                    ->get();
                }elseif (empty($saveArray['from']) && empty($saveArray['to'])) {
                    $today = date('Y-m-d');
                    $upcommingOrders = Lunch_order::where('user_id',$userExist['id'])
                    ->where('order_date','>=',$today)
                    ->orderBy('order_date','DESC')
                    ->get();
                    $pastOrders = Lunch_order::where('user_id',$userExist['id'])
                    ->where('order_date','<',$today)
                    ->orderBy('order_date','DESC')
                    ->get();
                }
                $upArray = [];
                $pastArray = [];
                foreach($upcommingOrders as $up){
                    $resort_detail = Lunch::where('business_service_id',$up['business_service_id'])->select('resort_id')->first();
                    $rname = Resort::where('id',$resort_detail['resort_id'])->first();
                    $user = User::where('id',$up['user_id'])->first();
                    $upcom['id'] = (String)$up['id'];
                    $upcom['orderID'] = $up['orderID'];
                    $upcom['resort_id'] = $resort_detail['resort_id'];
                    $upcom['resort_name'] = $rname['resort_name'];
                    $upcom['user_id'] = $up['user_id'];
                    $upcom['username'] = $user['name'];
                    $upcom['order_date'] = $up['order_date']->format('Y-m-d');
                    // $upcom['child_ids'] = $up['child_ids'];
                    // $upcom['phone_no'] = $up['phone_no'];
                    $upcom['total_lunch'] = $up['order_count'];
                    // $upcom['subtotal'] = $up['subtotal'];
                    // $upcom['tax'] = $up['tax'];
                    // $upcom['total_amount'] = $up['total_amount'];
                    $upcom['order_status'] = $up['order_status'];
                    // $upcom['payment_status'] = $up['payment_status'];
                    $upcom['created_at'] = $up['created_at']->format('Y-m-d');
                    $upArray[] = $upcom;
                }
                foreach($pastOrders as $pas){
                    $resort_detail = Lunch::where('business_service_id',$pas['business_service_id'])->select('resort_id')->first();
                    $rname = Resort::where('id',$resort_detail['resort_id'])->first();
                    $user = User::where('id',$pas['user_id'])->first();
                    $past['id'] = (String)$pas['id'];
                    $past['orderID'] = $pas['orderID'];
                    $past['resort_id'] = $resort_detail['resort_id'];
                    $past['resort_name'] = $rname['resort_name'];
                    $past['business_service_id'] = $pas['business_service_id'];
                    $past['user_id'] = $pas['user_id'];
                    $past['username'] = $user['name'];
                    $past['order_date'] = $pas['order_date']->format('Y-m-d');
                    // $past['child_ids'] = $pas['child_ids'];
                    // $past['phone_no'] = $pas['phone_no'];
                    $past['total_lunch'] = $pas['order_count'];
                    // $past['subtotal'] = $pas['subtotal'];
                    // $past['tax'] = $pas['tax'];
                    // $past['total_amount'] = $pas['total_amount'];
                    $past['order_status'] = $pas['order_status'];
                    // $past['payment_status'] = $pas['payment_status'];
                    $past['created_at'] = $pas['created_at']->format('Y-m-d');
                    $pastArray[] = $past;
                }
                return response()->json([
                    'status'=>'1',
                    'message'=>'Orders fetched successfully',
                    'upcomming_orders'=>$upArray,
                    'past_orders'=>$pastArray
                ], $this->successStatus);
            }
        }else{
            return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }

    /*Admin APIs*/

    /*
      * API for get comming orders
    */  

    public function commingOrders(Request $request){
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
            $validator = Validator::make($request->all(), [
                'business_id' => 'required',
            ]);
            if ($validator->fails()) {
                return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
            }
            else{
                if ($saveArray['from'] && $saveArray['to']) {
                    $from = $saveArray['from'];
                    $to = $saveArray['to'];
                    $today = date('Y-m-d');
                    $commingOrders = Lunch_order::where('business_service_id',$saveArray['business_id'])
                    ->whereBetween('order_date', [$from, $to])
                    ->orderBy('order_date','DESC')
                    ->get();
                }else{
                    $today = date('Y-m-d');
                    $commingOrders = Lunch_order::where('business_service_id',$saveArray['business_id'])
                    ->where('order_date','>=',$today)
                    ->orderBy('order_date','DESC')
                    ->get();
                }
                $commingArray = [];
                foreach($commingOrders as $up){
                    $user = User::where('id',$up['user_id'])->first();
                    $totalLunches = Lunch_order_detail::where('orderID',$up['orderID'])->get();
                    $lunchesArr = [];
                    foreach ($totalLunches as $lunch) {
                        $bread = Lunch_item::where('id',$lunch['bread_id'])->first();
                        $filling = Lunch_item::where('id',$lunch['filling_id'])->first();
                        $main = Lunch_item::where('id',$lunch['main_id'])->first();
                        $snack = Lunch_item::where('id',$lunch['snack_id'])->first();
                        $drink = Lunch_item::where('id',$lunch['drink_id'])->first();
                        $userDetail = User::where('id',$lunch['child_users'])->first();
                        if ($userDetail) {
                            $image = url('/images',$userDetail['image']);
                        }else{
                            $dumimage = 'default.jpg';
                            $image = url('/images',$dumimage);
                        }
                        if ($userDetail['name'] == '') {
                            $name = $lunch['child_users'];
                        }
                        $ord['order_id'] = $up['orderID'];
                        $ord['child_user'] = $lunch['child_users'];
                        $ord['child_user_name'] = $userDetail['name']?$userDetail['name']:$name;
                        $ord['child_user_image'] = $image;
                        $ord['parent_user'] = (String)$lunch['parent_user'];
                        $ord['bread'] = (String)$lunch['bread_id'];
                        $ord['bread_item'] = $bread;
                        $ord['filling'] = $lunch['filling_id'];
                        $ord['filling_item'] = $filling;
                        $ord['main'] = $lunch['main_id'];
                        $ord['main_item'] = $main;
                        $ord['snack'] = $lunch['snack_id'];
                        $ord['snack_item'] = $snack;
                        $ord['drink'] = $lunch['drink_id'];
                        $ord['drink_item'] = $drink;
                        $ord['created_at'] = $lunch['created_at']->format('Y-m-d');
                        $lunchesArr[] = $ord;
                    }
                    $upcom['id'] = (String)$up['id'];
                    $upcom['resort_id'] = $up['resort_id'];
                    $upcom['business_service_id'] = $up['business_service_id'];
                    $upcom['orderID'] = $up['orderID'];
                    $upcom['date'] = $up['order_date']->format('Y-m-d');
                    $upcom['owner'] = $up['user_id'];
                    $upcom['owner_name'] = $user['name'];
                    $upcom['lunches'] = $up['order_count'];
                    $upcom['order_status'] = $up['order_status'];
                    $upcom['phone_no'] = $up['phone_no'];
                    $upcom['subtotal'] = $up['subtotal'];
                    $upcom['tax'] = $up['tax'];
                    $upcom['total_amount'] = $up['total_amount'];
                    $upcom['payment_status'] = $up['payment_status'];
                    $upcom['created_at'] = $up['created_at']->format('Y-m-d');
                    $upcom['user_lunches'] = $lunchesArr;
                    $commingArray[] = $upcom;
                }
                return response()->json([
                    'status'=>'1',
                    'message'=>'Orders fetched successfully',
                    'comming_orders'=>$commingArray
                ], $this->successStatus);
            }
        }else{
            return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }

    /*
      * API for accept reject order
    */ 

    public function accept_reject_orders(Request $request){
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
            $validator = Validator::make($request->all(), [  
                'order_id' => 'required',
                'business_id' => 'required',
                'mark_order' => 'required' //A = Accepted , R = Rejected
              ]);
              if ($validator->fails()) {
                return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
            }
            else{
                $check = $this->businessPermission($request ,$saveArray['business_id'],$userExist['id']);
                if ($check == 1) {
                    $checkOrder = Lunch_order::where('orderID',$saveArray['order_id'])
                    ->where('business_service_id',$saveArray['business_id'])
                    ->first();
                    if ($checkOrder['order_status'] == 'Incomplete') {
                        return response()->json(['message'=>'You Order is not complete please choose items for all','status'=>'0'], $this->successStatus);
                    }else{
                        if ($saveArray['mark_order'] == 'A') {
                            $updateStatus = [
                                'order_status'=>'Accepted',
                                'updated_at'=>date('Y-m-d h:i:s')
                            ];
                            $update = Lunch_order::where('orderID',$saveArray['order_id'])
                            ->where('business_service_id',$saveArray['business_id'])
                            ->update($updateStatus);
                            /*send notifications for parent user*/
                            $resortID = $checkOrder['resort_id'];
                            $rname = Resort::where('id',$resortID)->first();
                            $array =  array($checkOrder['user_id']);
                            $message = array(
                                'message'=>" Your lunch order at ". $rname['resort_name'] ." on ". $checkOrder['order_date'] ." has been accepted and paid",
                                'type'=>'parent',
                                'status'=>'accepted',
                                'order_id'=>$checkOrder['orderID'],
                            );
                            $this->send_notification($request ,$array ,$message);
                            /*send notifications to parent user*/

                            /*send notifications to child users*/
                            $multiId = $checkOrder['child_ids'];
                            $myArray = explode(',', $multiId);
                            $username = User::where('id',$checkOrder['user_id'])->first();
                            $message = array(
                                'message'=>" ". $username['name'] ." has brought you lunch at ". $rname['resort_name'] ." on ". $checkOrder['order_date'] ." ",
                                'type'=>'child',
                                'status'=>'accepted',
                                'order_id'=>$checkOrder['orderID'],
                            );
                            $this->send_notification($request ,$myArray ,$message);
                            /*send notifications to child users*/
                            
                            if($update){
                                return response()->json(['status'=>'1','message'=>'Order Accepted Successfully'], $this->successStatus);
                            }
                            else{
                                return response()->json(['message'=>'Order not found','status'=>'0'], $this->successStatus);
                            }
                        }elseif ($saveArray['mark_order'] == 'R') {
                            $updateStatus = [
                                'order_status'=>'Rejected',
                                'reason'=>$saveArray['reason'],
                                'updated_at'=>date('Y-m-d h:i:s')
                            ];
                            $update = Lunch_order::where('orderID',$saveArray['order_id'])
                            ->where('business_service_id',$saveArray['business_id'])
                            ->update($updateStatus);
                            /*send notifications to parent user and child users*/
                            $parent = array($checkOrder['user_id']);
                            $childIDs = $checkOrder['child_ids'];
                            $child = explode(',', $childIDs);
                            $both = array_merge($child, $parent);
                            $resortID = $checkOrder['resort_id'];
                            $rname = Resort::where('id',$resortID)->first();
                            $reason = $checkOrder['reason'];
                            $message = array(
                                'message'=>" Your lunch order at ". $rname['resort_name'] ." on ". $checkOrder['order_date'] ." was rejected because ". $reason ." ",
                                'type'=>'rejected',
                                'status'=>'rejected',
                                'order_id'=>$checkOrder['orderID'],
                            );
                            $this->send_notification($request ,$both ,$message);
                            /*send notifications*/
                            if($update){
                                return response()->json(['status'=>'1','message'=>'Order Rejected'], $this->successStatus);
                            }
                            else{
                                return response()->json(['message'=>'Order not found','status'=>'0'], $this->successStatus);
                            }
                        }
                    }
                }else{
                    return response()->json(['message'=>'you have not Admin Permission to do this','status'=>'0'], $this->badrequest);
                }
            }
        }else{
            return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }

    /*
      * API for cancel order
    */  

    public function cancelOrder(Request $request){
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
            $validator = Validator::make($request->all(), [  
                'order_id' => 'required',
              ]);
              if ($validator->fails()) {
                return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
            }
            else{
                $checkOrder = Lunch_order::where('orderID',$saveArray['order_id'])->first();
                if ($checkOrder) {
                    $updateStatus = [
                        'order_status'=>'Cancelled',
                        'reason'=>'your order is incopmlete',
                        'updated_at'=>date('Y-m-d h:i:s')
                    ];
                    $update = Lunch_order::where('orderID',$saveArray['order_id'])
                    ->update($updateStatus);
                    /*send notifications to parent user and child users*/
                    $parent = array($checkOrder['user_id']);
                    $childIDs = $checkOrder['child_ids'];
                    $child = explode(',', $childIDs);
                    $both = array_merge($child, $parent);
                    $resortID = $checkOrder['resort_id'];
                    $rname = Resort::where('id',$resortID)->first();
                    $reason = $checkOrder['reason'];
                    $message = array(
                        'message'=>" Your lunch order at ". $rname['resort_name'] ." on ". $checkOrder['order_date'] ." was cancelled because ". $reason ." ",'type'=>'rejected','status'=>'rejected','order_id'=>$checkOrder['orderID'],
                    );
                    $this->send_notification($request ,$both ,$message);
                    /*send notifications*/
                    $deleteOrder = Lunch_order::where('orderID',$saveArray['order_id'])->delete();
                    $suborders = Lunch_order_detail::where('orderID',$saveArray['order_id'])->delete();
                    if ($deleteOrder) {
                        return response()->json(['status'=>'1','message'=>'Order Cancelled Successfully' ], $this->successStatus);
                    }
                }else{
                    return response()->json(['status'=>'1','message'=>'Order not found' ], $this->successStatus);
                }
            }
        }else{
            return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }

    public function index(Request $request)
    {
        $lunches = $this->lunchRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($lunches->toArray(), 'Lunches retrieved successfully');
    }

    /**
     * Store a newly created Lunch in storage.
     * POST /lunches
     *
     * @param CreateLunchAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateLunchAPIRequest $request)
    {
        $input = $request->all();

        $lunch = $this->lunchRepository->create($input);

        return $this->sendResponse($lunch->toArray(), 'Lunch saved successfully');
    }

    /**
     * Display the specified Lunch.
     * GET|HEAD /lunches/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Lunch $lunch */
        $lunch = $this->lunchRepository->find($id);

        if (empty($lunch)) {
            return $this->sendError('Lunch not found');
        }

        return $this->sendResponse($lunch->toArray(), 'Lunch retrieved successfully');
    }

    /**
     * Update the specified Lunch in storage.
     * PUT/PATCH /lunches/{id}
     *
     * @param int $id
     * @param UpdateLunchAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateLunchAPIRequest $request)
    {
        $input = $request->all();

        /** @var Lunch $lunch */
        $lunch = $this->lunchRepository->find($id);

        if (empty($lunch)) {
            return $this->sendError('Lunch not found');
        }

        $lunch = $this->lunchRepository->update($input, $id);

        return $this->sendResponse($lunch->toArray(), 'Lunch updated successfully');
    }

    /**
     * Remove the specified Lunch from storage.
     * DELETE /lunches/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Lunch $lunch */
        $lunch = $this->lunchRepository->find($id);

        if (empty($lunch)) {
            return $this->sendError('Lunch not found');
        }

        $lunch->delete();

        return $this->sendSuccess('Lunch deleted successfully');
    }
}
