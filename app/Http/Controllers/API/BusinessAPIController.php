<?php

namespace App\Http\Controllers\API;
use App\Traits\CommonFunctionTrait;

use App\Http\Requests\API\CreateBusinessAPIRequest;
use App\Http\Requests\API\UpdateBusinessAPIRequest;
use App\Models\Business;
use App\Models\Business_address;
use App\Models\Event;
use App\Models\Event_attendee;
use App\Models\Friend;
use App\Models\Country;
use App\Models\Business_history;
use App\Models\Business_hour;
use App\Models\Business_hour_history;
use App\Models\Business_service_activity;
use App\Models\User;
use App\Models\Resort;
use App\Models\Snowflake;
use App\Models\Rewards_point;
use App\Models\Permission;
use App\Models\Access_code;
use App\Models\Nearbytown;
use App\Models\Lunch;
use App\Models\Job;
use App\Repositories\BusinessRepository;
use Illuminate\Http\Request;
use DB;
use Validator;
use App\Http\Controllers\AppBaseController;
use Response;


    /*
      * Business Services API Controller
    */ 

/**
 * Class BusinessController
 * @package App\Http\Controllers\API
 */

class BusinessAPIController extends AppBaseController
{
    /** @var  BusinessRepository */
    private $businessRepository;
    public $unauthorised = 401;
    public $successStatus = 200;
    public $badrequest = 400;
    use CommonFunctionTrait;

    public function __construct(BusinessRepository $businessRepo)
    {
        $this->businessRepository = $businessRepo;
    }

    /**
     * Display a listing of the Business.
     * GET|HEAD /businesses
     *
     * @param Request $request
     * @return Response
     */

    /*
      * Checking Business if already exist in a location API Controller
    */ 

    public function checkBusinessLocation(Request $request)
    {
        $saveArray = $request->all();
        $validator = Validator::make($request->all(), [
            'business_location' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
        }
        else{
            $getLocation = Business::where('business_address',$saveArray['business_location'])
            ->first();
            if ($getLocation) {
                return response()->json([
                    'status'=>'1',
                    'message'=>'This venue already exists in the CommuniSki database - would you like to visit it”',
                    'business_id'=>(String)$getLocation['id']
                ], $this->successStatus);
            }else{
                return response()->json([
                    'status'=>'1',
                    'message'=>'Great you can add business on this location',
                ], $this->successStatus);
            }
        }
    }

    /*
      * Adding Business by User
    */ 

    public function addBusiness(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
            $validator = Validator::make($request->all(), [
                'business_type' => 'required',
                'own_work' => 'required'
            ]);
            if ($validator->fails()) {
                return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
            }
            else{
                $features = array($saveArray['features']);
                $multiFeatures = implode(",", $features);

                $cuisines = array($saveArray['cuisines']);
                $multicuisines = implode(",", $cuisines);

                $activities = array($saveArray['activities']);
                $multiActivities = implode(",", $activities);

                $services = array($saveArray['services']);
                $multiServices = implode(",", $services);

                if($request->hasFile('service_image')){
                    $file = time().$request->service_image->getClientOriginalName();
                    $request->service_image->move(public_path('/service_images') . '/', $file);
                }
                else{
                    $file = '';
                }

                if ($saveArray['business_type'] == 'Restaurant/Bar') {
                    $category = 'food_drinks';
                }

                elseif ($saveArray['business_type'] == 'Hotel') {
                    $category = 'hotels';
                }

                elseif ($saveArray['business_type'] != 'Restaurant/Bar' && $saveArray['business_type'] != 'Hotel') {
                    $category = 'shops';
                }

                $business_address = $saveArray['business_address'];

                $added_by = $userExist['id'];
                $category = $category;
                $business_type = $saveArray['business_type'];
                $latitude = $saveArray['latitude'];
                $longitude = $saveArray['longitude'];
                $features = $multiFeatures;
                $cuisines = $multicuisines;
                $activities = $multiActivities;
                $services = $multiServices;
                $price = $saveArray['price'];
                $star_rating = $saveArray['star_rating'];
                $description = $saveArray['description'];
                $website = $saveArray['website'];
                $email = $saveArray['email'];
                $phone_number = $saveArray['phone_number'];
                $self_working = $saveArray['own_work'];

                $BUIrandomname = 'BUI';
                $BUIgetRstring = $this->generateRandomString();
                $businessCode = $BUIrandomname.$BUIgetRstring;
                $addBusiness = [
                    'added_by'=>$added_by,
                    'business_code'=>$businessCode,
                    'category'=>$category,
                    'business_type'=>$business_type?$business_type:'',
                    'business_address'=>$business_address?$business_address:'',
                    'latitude'=>$latitude,
                    'longitude'=>$longitude,
                    'features'=>$features?$features:'',
                    'cuisines'=>$cuisines?$cuisines:'',
                    'activities'=>$activities?$activities:'',
                    'services'=>$services?$services:'',
                    'price'=>$price?$price:'',
                    'star_rating'=>$star_rating?$star_rating:'',
                    'service_image'=>$file,
                    'description'=>$description?$description:'',
                    'website'=>$website?$website:'',
                    'email'=>$email?$email:'',
                    'phone_number'=>$phone_number?$phone_number:'',
                    'own_work'=>$self_working,
                    'created_at'=>date('Y-m-d h:i:s'),
                ];
                $businessId = Business::insertGetId($addBusiness);

                /*saving business address*/
                $breakingAdd = explode(',',$business_address);
                $address = [
                    'business_id'=>$businessId,
                    'street_address1'=>$breakingAdd[0],
                    'street_address2'=>$breakingAdd[1],
                    'town_city'=>$saveArray['town'],
                    'state'=>$saveArray['state'],
                    'country'=>$saveArray['country'],
                    'created_at'=>date('Y-m-d h:i:s'),
                ];
                $businessAddress = Business_address::insert($address);
                /*saving business address*/

                /*adding permission if business owned by a user*/
                if ($self_working == 1) {
                    $addPermission = [
                        'user_id'=>$userExist['id'],
                        'entity_type'=>'Business',
                        'entity_id'=>$businessId,
                        'code'=>$businessCode,
                        'permission_type'=>'ADM',
                        'created_at'=>date('Y-m-d h:i:s'),
                    ];
                    $per_id = Permission::insert($addPermission);
                }
                /*adding permission if business owned by a user*/
                if($businessId){
                    return response()->json(['status'=>'1','message'=>'Business Added Successfully',], $this->successStatus);
                }
                else{
                    return response()->json(['message'=>'Error','status'=>'0'], $this->badrequest);
                }
            }
        }else{
            return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }

    /*
    * Get Business Permission
  */

  public function getBusinessPermission(Request $request)
  {
    $saveArray = $request->all();
    $token = $request->header('token');
    $userExist = User::where(['remember_token'=>$token])->first();
    if($userExist){
      $validator = Validator::make($request->all(), [
        'business_id' => 'required',
      ]);
      if ($validator->fails()) {
        return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
      }
      else{
        $getPermission = Permission::where('business_service_id',$saveArray['business_id'])->get();
        $permissionArray = [];
            if(count($getPermission)){
              foreach($getPermission as $permission){
                $userDetails = User::where(['id'=>$permission['user_id']])->first();
                $resortDetails = Resort::where(['id'=>$permission['resort_id']])->first();
                $businessDetails = Business::where(['id'=>$permission['business_service_id']])->first();
                $listing['user_id'] = (String)$permission['user_id'];
                $listing['user_name'] = (String)$userDetails['name'];
                $listing['resort_id'] = (String)$permission['resort_id'];
                $listing['resort_location'] = (String)$resortDetails['resort_name'];
                $listing['business_service_id'] = (String)$businessDetails['id'];
                $listing['business_service_name'] = (String)$businessDetails['business_name'];
                $listing['role_type'] = $permission['role_type'];
                $listing['created_at'] = $permission['created_at'];
                $permissionArray[] = $listing;
            }
            return response()->json([
              'status'=>'1',
              'message'=>'data fetched successfully',
              'listing'=>$permissionArray
            ], $this->successStatus);
          }
          else{
            return response()->json([
              'status'=>'0',
              'message'=>'no data',
            ], $this->successStatus);
          }
      }
        }
        else{
          return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
  }

    /*
      * Edit Business information by anyone
    */ 

    public function editBusiness(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
          $validator = Validator::make($request->all(), [  
            'business_id' => 'required'
          ]);
          if ($validator->fails()) { 
            return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);            
          }
          else{
            $getBusiness = Business::where('id',$saveArray['business_id'])->first();
            if ($getBusiness) {

                $features = array($saveArray['features']);
                $multiFeatures = implode(",", $features);

                $cuisines = array($saveArray['cuisines']);
                $multicuisines = implode(",", $cuisines);

                $activities = array($saveArray['activities']);
                $multiActivities = implode(",", $activities);

                $services = array($saveArray['services']);
                $multiServices = implode(",", $services);

                $updateService = [
                    'description'=>$saveArray['description']?$saveArray['description']:$getBusiness['description'],
                    'features'=>$multiFeatures?$multiFeatures:$getBusiness['features'],
                    'cuisines'=>$multicuisines?$multicuisines:$getBusiness['cuisines'],
                    'activities'=>$multiActivities?$multiActivities:$getBusiness['activities'],
                    'services'=>$multiServices?$multiServices:$getBusiness['services'],
                    'price'=>$saveArray['price']?$saveArray['price']:$getBusiness['price'],
                    'star_rating'=>$saveArray['star_rating']?$saveArray['star_rating']:$getBusiness['star_rating'],
                    'phone_number'=>$saveArray['phone_number']?$saveArray['phone_number']:$getBusiness['phone_number'],
                    'email'=>$saveArray['email']?$saveArray['email']:$getBusiness['email'],
                    'website'=>$saveArray['website']?$saveArray['website']:$getBusiness['website']
                ];
                $update = Business::where('id',$saveArray['business_id'])
                ->update($updateService);

                $service_history = [
                    'note_for'=>$saveArray['note_for']?$saveArray['note_for']:'',
                    'added_note'=>$saveArray['added_note']?$saveArray['added_note']:'',
                    'post_anonymously'=>$saveArray['post_anonymously'],
                    'user_id'=>$userExist['id'],
                    'business_service_id'=>$saveArray['business_id'],
                    'description'=>$saveArray['description']?$saveArray['description']:'',
                    'features'=>$multiFeatures?$multiFeatures:'',
                    'cuisines'=>$multicuisines?$multicuisines:'',
                    'activities'=>$multiActivities?$multiActivities:'',
                    'services'=>$multiServices?$multiServices:'',
                    'price'=>$saveArray['price']?$saveArray['price']:'',
                    'star_rating'=>$saveArray['star_rating']?$saveArray['star_rating']:'',
                    'phone_number'=>$saveArray['phone_number']?$saveArray['phone_number']:'',
                    'email'=>$saveArray['email']?$saveArray['email']:'',
                    'website'=>$saveArray['website']?$saveArray['website']:'',
                    'created_at'=>date('Y-m-d h:i:s')
                ];
                $insertedID = Business_history::insertGetId($service_history);
                if($update){
                    return response()->json([
                        'status'=>'1',
                        'message'=>'Service Updated Successfully',
                    ], $this->successStatus);
                }
                else{
                    return response()->json([
                        'message'=>'Service does not Update',
                        'status'=>'0'
                    ], $this->badrequest);
                }
            }else{
                return response()->json([
                        'message'=>'Service not found',
                        'status'=>'0'
                    ], $this->successStatus);
            }
          }
        }
        else{
          return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }

    /*
      * To get Business Notes
    */    

    public function businessNotes(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
            $validator = Validator::make($request->all(), [
                'business_id' => 'required',
                'note_for' => 'required',
            ]);
            if ($validator->fails()) {
                return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
            }
            else{
                $notes = Business_history::where(['business_service_id'=>$saveArray['business_id']])
                ->where(['note_for'=>$saveArray['note_for']])
                ->orderBy('id','DESC')
                ->select('id','user_id','business_service_id','note_for','added_note')->get();
                $noteArray = [];
                if(count($notes)){
                    foreach($notes as $ls){
                        $username = User::where(['id'=>$ls['user_id']])->first();
                        $note['id'] = $ls['id'];
                        $note['business_service_id'] = $ls['business_service_id'];
                        $note['user_id'] = $ls['user_id'];
                        $note['user_name'] = $username['name'];
                        $note['note_for'] = $ls['note_for'];
                        $note['added_note'] = $ls['added_note'];
                        $note['created_at'] = date('Y-m-d h:i:s');
                        $noteArray[] = $note;
                    }
                    return response()->json([
                        'status'=>'1',
                        'message'=>'Service Notes fetched successfully',
                        'listing'=>$noteArray
                    ], $this->successStatus);
                }
                else{
                    return response()->json(['message'=>'No Notes','status'=>'0'], $this->successStatus);
                }
            }
        }
        else{
            return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }

    /*
      * API for get Business service history provided by users
    */ 

    public function serviceHistory(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
            $validator = Validator::make($request->all(), [
                'business_id' => 'required',
                'history_of' => 'required',
            ]);
            if ($validator->fails()) {
                return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
            }
            else{
                $dynamicField = $saveArray['history_of'];
                $serviceHist = Business_history::where(['business_service_id'=>$saveArray['business_id']])
                ->where($dynamicField, '!=', '')
                ->select('id','user_id','business_service_id','post_anonymously',$dynamicField)
                ->orderBy('id','DESC')
                ->get();
                $histArray = [];
                if(count($serviceHist)){
                    foreach($serviceHist as $serv){
                        $username = User::where(['id'=>$serv['user_id']])->first();
                        $histry['post_anonymously'] = $serv['post_anonymously'];
                        $histry['id'] = $serv['id'];
                        $histry['business_service_id'] = $serv['business_service_id'];
                        $histry['user_id'] = $serv['user_id'];
                        $histry['user_name'] = $username['name'];
                        $histry['history_key'] = $dynamicField;
                        $histry['history_value'] = $serv->$dynamicField;
                        $histry['created_at'] = date('Y-m-d h:i:s');
                        $histArray[] = $histry;
                    }
                    return response()->json(['status'=>'1','message'=>'Service History fetched successfully','historylisting'=>$histArray], $this->successStatus);
                }
                else{
                    return response()->json(['message'=>'No History','status'=>'0'], $this->successStatus);
                }
            }
        }
        else{
            return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }

    /*
      * API for get business listing
    */ 

    public function getListing(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
          $validator = Validator::make($request->all(), [
            'business_type' => 'required'
          ]);
          if ($validator->fails()) { 
            return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);            
          }
          else{
            $business = Business::where('category',$saveArray['business_type'])
            ->orderBy('id','DESC')
            ->get();
            $record = [];
            if(count($business)){
              foreach($business as $ls){
                if ($ls['service_image'] == '') {
                    $service_image = '';
                }else{
                    $service_image = url('/service_images',$ls['service_image']);
                }
                $businessAdd = Business_address::where('business_id',$ls['id'])->first();
                $listing['state'] = $businessAdd['state']?$businessAdd['state']:'';
                $listing['country'] = $businessAdd['country']?$businessAdd['country']:'';
                $listing['bcolor']= $ls['bcolor'];
                $listing['fcolor']= $ls['fcolor'];
                $listing['service_image'] = $service_image;
                $listing['id'] = (String)$ls['id'];
                $listing['business_type'] = $ls['business_type'];
                $listing['business_name'] = $ls['business_address'];
                $listing['latitude'] = $ls['latitude'];
                $listing['longitude'] = $ls['longitude'];
                $listing['features'] = $ls['features']?$ls['features']:'';
                $listing['cuisines'] = $ls['cuisines']?$ls['cuisines']:'';
                $listing['activities'] = $ls['activities']?$ls['activities']:'';
                $listing['services'] = $ls['services']?$ls['services']:'';
                $listing['price'] = $ls['price']?$ls['price']:'';
                $listing['star_rating'] = $ls['star_rating']?$ls['star_rating']:'';
                $listing['description'] = $ls['description'];
                $listing['website'] = $ls['website'];
                $listing['email'] = $ls['email'];
                $listing['phone_number'] = (String)$ls['phone_number'];
                $listing['self_working'] = (String)$ls['own_declaration'];
                $record[] = $listing;
              }
              return response()->json([
                'status'=>'1',
                'message'=>'Business fetched successfully',
                'businessListing'=>$record
              ], $this->successStatus);
            }
            else{
              return response()->json([
                'message'=>'No data Found',
                'status'=>'0'
              ], $this->successStatus);
            }
          }
        }
        else{
          return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }

    /*
      * To get detail of a business service
    */ 

    public function getService(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
            $validator = Validator::make($request->all(), [  
                'service_id' => 'required'
              ]);
              if ($validator->fails()) { 
                return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);            
              }
              else{
                $service = Business::where('id',$saveArray['service_id'])->first();
                if ($service) {
                    if ($service['service_image'] == '') {
                        $service_image = '';
                    }else{
                        $service_image = url('/service_images',$service['service_image']);
                    }
                    $image = url('/service_images',$service['service_image']);
                    $owner_name = User::where('id',$service['added_by'])->first();
                    $businessAdd = Business_address::where('business_id',$service['id'])->first();
                    $serviceDetail = array(
                        'state' => $businessAdd['state'],
                        'country' => $businessAdd['country'],
                        'bcolor'=>$service['bcolor'],
                        'fcolor'=>$service['fcolor'],
                        'service_image'=>$service_image,
                        'id'=>(String)$service['id'],
                        'owner_id'=>$service['added_by'],
                        'owner_name'=>$owner_name['name'],
                        'business_type'=>$service['business_type'],
                        'business_name'=>$service['business_address'],
                        'latitude'=>$service['latitude']?$service['latitude']:'',
                        'longitude'=>$service['longitude']?$service['longitude']:'',
                        'cuisines'=>$service['cuisines']?$service['cuisines']:'',
                        'features'=>$service['features']?$service['features']:'',
                        'activities'=>$service['activities']?$service['activities']:'',
                        'services'=>$service['services']?$service['services']:'',
                        'price'=>$service['price']?$service['price']:'',
                        'star_rating'=>$service['star_rating']?$service['star_rating']:'',
                        'description'=>$service['description']?$service['description']:'',
                        'website'=>$service['website']?$service['website']:'',
                        'email'=>$service['email']?$service['email']:'',
                        'phone_number'=>$service['phone_number']?$service['phone_number']:'',
                        'self_working'=>$service['own_work'],
                        'created_at'=>$service['created_at']->format('Y-m-d H:i:s'),
                    );
                    return response()->json(['status'=>'1','message'=>'Service fetched successfully','serviceDetail'=>$serviceDetail], $this->successStatus);
                }else{
                    return response()->json(['status'=>'1','message'=>'No Service Found'], $this->successStatus);
                }
            }
        }
        else{
            return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }

    /*
      * Get Services and Activities for a Business Service
    */ 

    public function getServices_Activities(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
          $validator = Validator::make($request->all(), [  
            'business_type' => 'required',
            'type' => 'required'
          ]);
          if ($validator->fails()) {
            return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
        }
        else{
            if ($saveArray['type'] == 'service') {
                $service = Business_service_activity::where('business_type',$saveArray['business_type'])
                ->where('service','!=','Null')
                ->select('id','business_id','business_type','service')
                ->get();
                $getService = json_decode(json_encode($service), true);
                $serviceArray = [];
                if(count($getService)){
                    foreach($getService as $ls){
                        $serv['id'] = $ls['id'];
                        $serv['business_id'] = $ls['business_id'];
                        $serv['business_type'] = $ls['business_type'];
                        $serv['service'] = $ls['service'];
                        $serviceArray[] = $serv;
                    }
                    return response()->json([
                        'status'=>'1',
                        'message'=>'Services fetched successfully',
                        'serviceListing'=>$serviceArray
                    ], $this->successStatus);
                }
                else{
                    return response()->json([
                        'message'=>'No Services',
                        'status'=>'1'
                    ], $this->successStatus);
                }
        }elseif ($saveArray['type'] == 'activity') {
            $activity = Business_service_activity::where('business_type',$saveArray['business_type'])
                ->where('activity','!=','Null')
                ->select('id','business_id','business_type','activity')
                ->get();
                $getActivity = json_decode(json_encode($activity), true);
                $serviceArray = [];
                if(count($getActivity)){
                    foreach($getActivity as $ls){
                        $serv['id'] = $ls['id'];
                        $serv['business_id'] = $ls['business_id'];
                        $serv['business_type'] = $ls['business_type'];
                        $serv['activity'] = $ls['activity'];
                        $activityArray[] = $serv;
                    }
                    return response()->json([
                        'status'=>'1',
                        'message'=>'Activities fetched successfully',
                        'activityListing'=>$activityArray
                    ], $this->successStatus);
                }
                else{
                    return response()->json([
                        'message'=>'No Activities',
                        'status'=>'1'
                    ], $this->successStatus);
                }
            }
        }
        }
        else{
            return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }

    /*
      * update business hours of a Business service
    */ 

    public function updatebusinessHours(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
            $validator = Validator::make($request->all(), [  
                'business_id' => 'required',
              ]);
              if ($validator->fails()) {
                return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
            }
            else{
                if (!empty($saveArray['notes'])) {
                    $addHour = [
                        'user_id'=>$userExist['id'],
                        'business_service_id'=>$saveArray['business_id'],
                        'notes'=>$saveArray['notes'],
                        'created_at'=>date('Y-m-d h:i:s'),
                    ];
                    $notes = Business_hour::insertGetId($addHour);
                    if ($notes) {
                        return response()->json([
                            'status'=>'1',
                            'message'=>'notes added successfully',
                          ], $this->successStatus);
                    }
                }else{
                    $days = $saveArray['day'];
                    $multi_days = explode(',',$days);
                    $hourArray = array();
                    foreach ($multi_days as $key) {
                        $multiday = explode('$',$key);
                        foreach ($multiday as $key) {
                            if ($key == 'Monday') {
                                $weekday = '2';
                            }
                            elseif ($key == 'Tuesday') {
                                $weekday = '3';
                            }elseif ($key == 'Wednesday') {
                                $weekday = '4';
                            }elseif ($key == 'Thursday') {
                                $weekday = '5';
                            }elseif ($key == 'Friday') {
                                $weekday = '6';
                            }elseif ($key == 'Saturday') {
                                $weekday = '7';
                            }elseif ($key == 'Sunday') {
                                $weekday = '1';
                            }
                            $addHour = [
                                'user_id'=>$userExist['id'],
                                'business_service_id'=>$saveArray['business_id'],
                                'day'=>$weekday,
                                'open_time'=>$saveArray['open_time'],
                                'close_time'=>$saveArray['close_time'],
                                'notes'=>$saveArray['notes'],
                                'created_at'=>date('Y-m-d h:i:s'),
                            ];
                            $hourId = Business_hour::insertGetId($addHour);
                        }
                    }
                    if ($hourId) {
                        return response()->json([
                            'status'=>'1',
                            'message'=>'hours saved successfully',
                          ], $this->successStatus);
                    }else{
                        return response()->json([
                            'status'=>'0',
                            'message'=>'error',
                          ], $this->successStatus);
                    }
                }
            }
        }
        else{
            return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }

    /*
      * get Working hours of a business sevice
    */ 

    public function getBusinessHours(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
            $validator = Validator::make($request->all(), [  
                'business_id' => 'required',
              ]);
              if ($validator->fails()) {
                return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
            }
            else{
                $getHours = Business_hour::where('business_service_id',$saveArray['business_id'])
                ->where('day','!=','NULL')
                ->where('open_time','!=','NULL')
                ->where('close_time','!=','NULL')
                ->orderBy('day', 'asc')
                ->get();
                $getHour = json_decode(json_encode($getHours), true);
                $hourArray = [];
                if(count($getHour)){
                    foreach($getHour as $ls){
                        if ($ls['day'] == '1') {
                            $day = 'Sunday';
                        }if ($ls['day'] == '2') {
                            $day = 'Monday';
                        }if ($ls['day'] == '3') {
                            $day = 'Tuesday';
                        }if ($ls['day'] == '4') {
                            $day = 'Wednesday';
                        }if ($ls['day'] == '5') {
                            $day = 'Thursday';
                        }if ($ls['day'] == '6') {
                            $day = 'Friday';
                        }if ($ls['day'] == '7') {
                            $day = 'Saturday';
                        }
                        $businessName = Business::where('id',$ls['business_service_id'])->first();
                        $userName = User::where('id',$ls['user_id'])->first();
                        $hour['id'] = $ls['id'];
                        $hour['business_id'] = $ls['business_service_id'];
                        $hour['business_name'] = $businessName['business_name'];
                        $hour['user_id'] = $ls['user_id'];
                        $hour['user_name'] = $userName['name'];
                        $hour['day'] = $ls['day'];
                        $hour['day_name'] = $day;
                        $hour['open_time'] = $ls['open_time'];
                        $hour['close_time'] = $ls['close_time'];
                        $hour['created_at'] = $ls['created_at'];
                        $hourArray[] = $hour;
                    }
                    return response()->json([
                        'status'=>'1',
                        'message'=>'hours fetched successfully',
                        'hourArray'=>$hourArray
                    ], $this->successStatus);
                }
                else{
                    return response()->json([
                        'message'=>'No data',
                        'status'=>'1',
                        'hourArray'=>[]
                    ], $this->successStatus);
                }
            }
        }
        else{
            return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }

    /*
      * To delete Business hours
    */ 

    public function deleteBusinessHour(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
            $validator = Validator::make($request->all(), [  
                'business_hour_id' => 'required',
              ]);
              if ($validator->fails()) {
                return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
            }
            else{
                $getHourData = Business_hour::where('id',$saveArray['business_hour_id'])->first();
                if ($getHourData) {
                    $deleteHour = Business_hour::where(['id'=>$saveArray['business_hour_id']])->delete();
                    if ($deleteHour) {
                        return response()->json([
                        'status'=>'1',
                        'message'=>'hours deleted successfully'
                    ], $this->successStatus);
                    }
                }else{
                    return response()->json([
                        'status'=>'1',
                        'message'=>'hours deleted already'
                    ], $this->successStatus);
                }
            }
        }
        else{
            return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }

    public function searchHotels(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
            $validator = Validator::make($request->all(), [  
                // 'area' => 'required',
                // 'radius' => 'required',
                // 'cuisines' => 'required',
                // 'star_rating' => 'required',
                // 'features' => 'required',
              ]);
              if ($validator->fails()) {
                return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
            }
            else{
                if ( ( empty($saveArray['area']) && empty($saveArray['radius']) ) || !empty($saveArray['features']) || !empty($saveArray['star_rating']) ) 
                {
                    // print_r("nrml");die();
                    $query = Business::query();
                    if ( $saveArray['features'] )  {
                        $features = explode(',',$saveArray['features']);
                        $query = $query->where(function($query) use($features) {
                            foreach($features as $feature) {
                                $query->orWhere('features', 'like', "%$feature%");
                            };
                        })->inRandomOrder();
                    }
                    if ( $saveArray['star_rating'] )  {
                        $query = $query->where('star_rating','like','%'.$saveArray['star_rating'].'%');
                    }

                    $business = $query->where('category','hotels')->get();
                    $businessData = json_decode(json_encode($business), true);
                    $record = [];
                    if(count($businessData)){
                        foreach($businessData as $ls){
                            if ($ls['service_image'] == '') {
                                $service_image = '';
                            }else{
                                $service_image = url('/service_images',$ls['service_image']);
                            }
                            
                            $listing['service_image'] = $service_image;
                            $listing['id'] = (String)$ls['id'];
                            $listing['business_type'] = $ls['business_type'];
                            $listing['business_name'] = $ls['business_name'];
                            $listing['features'] = $ls['features']?$ls['features']:'';
                            $listing['cuisines'] = $ls['cuisines']?$ls['cuisines']:'';
                            $listing['activities'] = $ls['activities']?$ls['activities']:'';
                            $listing['services'] = $ls['services']?$ls['services']:'';
                            // $listing['price'] = $ls['price']?$ls['price']:'';
                            $listing['star_rating'] = $ls['star_rating']?$ls['star_rating']:'';
                            $listing['description'] = $ls['description'];
                            $listing['website'] = $ls['website'];
                            $listing['email'] = $ls['email'];
                            $listing['phone_number'] = (String)$ls['phone_number'];
                            $listing['self_working'] = (String)$ls['own_declaration'];
                            $record[] = $listing;
                          }
                          return response()->json([
                            'status'=>'1',
                            'message'=>'Business fetched successfully',
                            'businessListing'=>$record
                          ], $this->successStatus);
                        }
                        else{
                          return response()->json([
                            'message'=>'No data Found',
                            'status'=>'0'
                          ], $this->successStatus);
                        }
                    }else{
                    $areas = explode('|',$saveArray['area']);
                    $mainArray = [];
                    $i = 0;
                    foreach($areas as $are) {
                        $query = Business::query();
                        $latlng = explode(',',$are);
                        $lat = $latlng[0];
                        $lng = $latlng[1];
                        $radius = $saveArray['radius']?$saveArray['radius']:'5';
                        $query = $query->selectRaw(" * ,( 3959 * acos( cos( radians(?) ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians(?)) + sin( radians(?) ) * sin( radians( latitude ) ))) AS distance", [$lat, $lng, $lat])
                            ->having("distance", "<", $radius)
                            ->orderBy("distance",'asc')
                            ->offset(0)
                            ->limit(20);

                            if ( $saveArray['features'] )  {
                                $features = explode(',',$saveArray['features']);
                                $query = $query->where(function($query) use($features) {
                                    foreach($features as $feature) {
                                        $query->orWhere('features', 'like', "%$feature%");
                                    };
                                })->inRandomOrder();
                            }
                            if ( $saveArray['star_rating'] )  {
                                $query = $query->where('star_rating','like','%'.$saveArray['star_rating'].'%');
                            }

                            $business = $query->where('category','hotels')->get();
                            $businessData = json_decode(json_encode($business), true);
                            if(count($businessData)){
                                foreach ($businessData as $ls) {
                                    if ($ls['service_image'] == '') {
                                        $service_image = '';
                                    }else{
                                        $service_image = url('/service_images',$ls['service_image']);
                                    }

                                    $result[$i]['id'] = (string)$ls['id'];
                                    $result[$i]['service_image'] = $service_image;
                                    $result[$i]['id'] = (String)$ls['id'];
                                    $result[$i]['business_type'] = $ls['business_type'];
                                    $result[$i]['business_name'] = $ls['business_name'];
                                    $result[$i]['features'] = $ls['features']?$ls['features']:'';
                                    $result[$i]['cuisines'] = $ls['cuisines']?$ls['cuisines']:'';
                                    $result[$i]['activities'] = $ls['activities']?$ls['activities']:'';
                                    $result[$i]['services'] = $ls['services']?$ls['services']:'';
                                    // $listing['price'] = $ls['price']?$ls['price']:'';
                                    $result[$i]['star_rating'] = $ls['star_rating']?$ls['star_rating']:'';
                                    $result[$i]['description'] = $ls['description'];
                                    $result[$i]['website'] = $ls['website'];
                                    $result[$i]['email'] = $ls['email'];
                                    $result[$i]['phone_number'] = (String)$ls['phone_number'];
                                    $result[$i]['self_working'] = (String)$ls['own_declaration'];
                                    $result[$i]['distance'] = (string)$ls['distance'];
                                    $i++;
                                }
                            }
                            else{
                              return response()->json([
                                'message'=>'No data Found',
                                'status'=>'0'
                              ], $this->successStatus);
                            }
                        }
                        return response()->json([
                            'status'=>'1',
                            'message'=>'Business fetched successfully',
                            'eventListing'=>$result
                          ], $this->successStatus);
                    }
                }
            }
            else{
                return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
            }
        }

    public function searchRestaurants(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
            $validator = Validator::make($request->all(), [  
                // 'area' => 'required',
                // 'radius' => 'required',
                // 'cuisines' => 'required',
                // 'star_rating' => 'required',
                // 'features' => 'required',
              ]);
              if ($validator->fails()) {
                return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
            }
            else{
                if ( ( empty($saveArray['area']) && empty($saveArray['radius']) ) || !empty($saveArray['features']) || !empty($saveArray['cuisines']) ) 
                {
                    // print_r("nrml");die();
                    $query = Business::query();
                    if ( $saveArray['features'] )  {
                        $features = explode(',',$saveArray['features']);
                        $query = $query->where(function($query) use($features) {
                            foreach($features as $feature) {
                                $query->orWhere('features', 'like', "%$feature%");
                            };
                        })->inRandomOrder();
                    }
                    if ( $saveArray['cuisines'] )  {
                        $cuisines = explode(',',$saveArray['cuisines']);
                        $query = $query->where(function($query) use($cuisines) {
                            foreach($cuisines as $cuisine) {
                                $query->orWhere('cuisines', 'like', "%$cuisine%");
                            };
                        })
                        ->inRandomOrder();
                    }

                    if ( $saveArray['price'] )  {
                        $query = $query->where('price','like','%'.$saveArray['price'].'%');
                    }

                    $business = $query->where('category','food_drinks')->get();
                    $businessData = json_decode(json_encode($business), true);
                    $record = [];
                    if(count($businessData)){
                        foreach($businessData as $ls){
                            if ($ls['service_image'] == '') {
                                $service_image = '';
                            }else{
                                $service_image = url('/service_images',$ls['service_image']);
                            }
                            $listing['service_image'] = $service_image;
                            $listing['id'] = (String)$ls['id'];
                            $listing['business_type'] = $ls['business_type'];
                            $listing['business_name'] = $ls['business_name'];
                            $listing['features'] = $ls['features']?$ls['features']:'';
                            $listing['cuisines'] = $ls['cuisines']?$ls['cuisines']:'';
                            $listing['activities'] = $ls['activities']?$ls['activities']:'';
                            $listing['services'] = $ls['services']?$ls['services']:'';
                            $listing['price'] = $ls['price']?$ls['price']:'';
                            $listing['star_rating'] = $ls['star_rating']?$ls['star_rating']:'';
                            $listing['description'] = $ls['description'];
                            $listing['website'] = $ls['website'];
                            $listing['email'] = $ls['email'];
                            $listing['phone_number'] = (String)$ls['phone_number'];
                            $listing['self_working'] = (String)$ls['own_declaration'];
                            $record[] = $listing;
                          }
                          return response()->json([
                            'status'=>'1',
                            'message'=>'Business fetched successfully',
                            'businessListing'=>$record
                          ], $this->successStatus);
                        }
                        else{
                          return response()->json([
                            'message'=>'No data Found',
                            'status'=>'0'
                          ], $this->successStatus);
                        }
                    }else{
                    $areas = explode('|',$saveArray['area']);
                    $mainArray = [];
                    $i = 0;
                    foreach($areas as $are) {
                        $query = Business::query();
                        $latlng = explode(',',$are);
                        $lat = $latlng[0];
                        $lng = $latlng[1];
                        $radius = $saveArray['radius']?$saveArray['radius']:'5';
                        $query = $query->selectRaw(" * ,( 3959 * acos( cos( radians(?) ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians(?)) + sin( radians(?) ) * sin( radians( latitude ) ))) AS distance", [$lat, $lng, $lat])
                            ->having("distance", "<", $radius)
                            // ->where('category','food_drinks')
                            ->orderBy("distance",'asc')
                            ->offset(0)
                            ->limit(20);

                            if ( $saveArray['features'] )  {
                                $features = explode(',',$saveArray['features']);
                                $query = $query->where(function($query) use($features) {
                                    foreach($features as $feature) {
                                        $query->orWhere('features', 'like', "%$feature%");
                                    };
                                })->inRandomOrder();
                            }
                            if ( $saveArray['cuisines'] )  {
                                $cuisines = explode(',',$saveArray['cuisines']);
                                $query = $query->where(function($query) use($cuisines) {
                                    foreach($cuisines as $cuisine) {
                                        $query->orWhere('cuisines', 'like', "%$cuisine%");
                                    };
                                })
                                ->inRandomOrder();
                            }

                            if ( $saveArray['price'] )  {
                                $query = $query->where('price','like','%'.$saveArray['price'].'%');
                            }

                            $business = $query->where('category','food_drinks')->get();
                            $businessData = json_decode(json_encode($business), true);
                            if(count($businessData)){
                            foreach ($businessData as $ls) {
                                // $result[$i] = $ls;
                                if ($ls['service_image'] == '') {
                                    $service_image = '';
                                }else{
                                    $service_image = url('/service_images',$ls['service_image']);
                                }
                                $result[$i]['id'] = (string)$ls['id'];
                                $result[$i]['service_image'] = $service_image;
                                $result[$i]['id'] = (String)$ls['id'];
                                $result[$i]['business_type'] = $ls['business_type'];
                                $result[$i]['business_name'] = $ls['business_name'];
                                $result[$i]['features'] = $ls['features']?$ls['features']:'';
                                $result[$i]['cuisines'] = $ls['cuisines']?$ls['cuisines']:'';
                                $result[$i]['activities'] = $ls['activities']?$ls['activities']:'';
                                $result[$i]['services'] = $ls['services']?$ls['services']:'';
                                $result[$i]['price'] = $ls['price']?$ls['price']:'';
                                $result[$i]['star_rating'] = $ls['star_rating']?$ls['star_rating']:'';
                                $result[$i]['description'] = $ls['description'];
                                $result[$i]['website'] = $ls['website'];
                                $result[$i]['email'] = $ls['email'];
                                $result[$i]['phone_number'] = (String)$ls['phone_number'];
                                $result[$i]['self_working'] = (String)$ls['own_declaration'];
                                $result[$i]['distance'] = (string)$ls['distance'];
                                $i++;
                            }
                            }
                            else{
                                return response()->json([
                                    'message'=>'No data Found',
                                    'status'=>'0'
                                  ], $this->successStatus);
                                }
                        }
                        return response()->json([
                            'status'=>'1',
                            'message'=>'Business fetched successfully',
                            'eventListing'=>$result
                          ], $this->successStatus);
                    }
                }
            }
            else{
                return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
            }
        }

    public function searchShops(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
            $validator = Validator::make($request->all(), [  
                // 'area' => 'required',
                // 'radius' => 'required',
                // 'cuisines' => 'required',
                // 'star_rating' => 'required',
                // 'features' => 'required',
              ]);
              if ($validator->fails()) {
                return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
            }
            else{
                if ( ( empty($saveArray['area']) && empty($saveArray['radius']) ) || !empty($saveArray['features']) || !empty($saveArray['cuisines']) ) 
                {
                    // print_r("nrml");die();
                    $query = Business::query();
                    if ( $saveArray['business_type'] )  {
                        $business_type = explode(',',$saveArray['business_type']);
                        $query = $query->where(function($query) use($business_type) {
                            foreach($business_type as $business) {
                                $query->orWhere('business_type', 'like', "%$business%");
                            };
                        })
                        ->inRandomOrder();
                    }

                    if ( $saveArray['services'] )  {
                        $services = explode(',',$saveArray['services']);
                        $query = $query->where(function($query) use($services) {
                            foreach($services as $service) {
                                $query->orWhere('services', 'like', "%$service%");
                            };
                        })
                        ->inRandomOrder();
                    }

                    $business = $query->where('category','shops')->get();
                    $businessData = json_decode(json_encode($business), true);
                    $record = [];
                    if(count($businessData)){
                        foreach($businessData as $ls){
                            if ($ls['service_image'] == '') {
                                $service_image = '';
                            }else{
                                $service_image = url('/service_images',$ls['service_image']);
                            }
                            
                            $listing['service_image'] = $service_image;
                            $listing['id'] = (String)$ls['id'];
                            $listing['business_type'] = $ls['business_type'];
                            $listing['business_name'] = $ls['business_name'];
                            $listing['features'] = $ls['features']?$ls['features']:'';
                            $listing['cuisines'] = $ls['cuisines']?$ls['cuisines']:'';
                            $listing['activities'] = $ls['activities']?$ls['activities']:'';
                            $listing['services'] = $ls['services']?$ls['services']:'';
                            // $listing['price'] = $ls['price']?$ls['price']:'';
                            $listing['star_rating'] = $ls['star_rating']?$ls['star_rating']:'';
                            $listing['description'] = $ls['description'];
                            $listing['website'] = $ls['website'];
                            $listing['email'] = $ls['email'];
                            $listing['phone_number'] = (String)$ls['phone_number'];
                            $listing['self_working'] = (String)$ls['own_declaration'];
                            $record[] = $listing;
                          }
                          return response()->json([
                            'status'=>'1',
                            'message'=>'Business fetched successfully',
                            'businessListing'=>$record
                          ], $this->successStatus);
                        }
                        else{
                          return response()->json([
                            'message'=>'No data Found',
                            'status'=>'0'
                          ], $this->successStatus);
                        }
                    }else{
                    $areas = explode('|',$saveArray['area']);
                    $mainArray = [];
                    $i = 0;
                    foreach($areas as $are) {
                        $query = Business::query();
                        $latlng = explode(',',$are);
                        $lat = $latlng[0];
                        $lng = $latlng[1];
                        $radius = $saveArray['radius']?$saveArray['radius']:'5';
                        $query = $query->selectRaw(" * ,( 3959 * acos( cos( radians(?) ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians(?)) + sin( radians(?) ) * sin( radians( latitude ) ))) AS distance", [$lat, $lng, $lat])
                            ->having("distance", "<", $radius)
                            // ->where('category','food_drinks')
                            ->orderBy("distance",'asc')
                            ->offset(0)
                            ->limit(20);

                            if ( $saveArray['business_type'] )  {
                                $business_type = explode(',',$saveArray['business_type']);
                                $query = $query->where(function($query) use($business_type) {
                                    foreach($business_type as $business) {
                                        $query->orWhere('business_type', 'like', "%$business%");
                                    };
                                })
                                ->inRandomOrder();
                            }

                            if ( $saveArray['services'] )  {
                                $services = explode(',',$saveArray['services']);
                                $query = $query->where(function($query) use($services) {
                                    foreach($services as $service) {
                                        $query->orWhere('services', 'like', "%$service%");
                                    };
                                })
                                ->inRandomOrder();
                            }

                            $business = $query->where('category','shops')->get();
                            $businessData = json_decode(json_encode($business), true);
                            if(count($businessData)){
                            foreach ($businessData as $ls) {
                                // $result[$i] = $ls;
                                if ($ls['service_image'] == '') {
                                    $service_image = '';
                                }else{
                                    $service_image = url('/service_images',$ls['service_image']);
                                }
                                $result[$i]['id'] = (string)$ls['id'];
                                $result[$i]['service_image'] = $service_image;
                                $result[$i]['id'] = (String)$ls['id'];
                                $result[$i]['business_type'] = $ls['business_type'];
                                $result[$i]['business_name'] = $ls['business_name'];
                                $result[$i]['features'] = $ls['features']?$ls['features']:'';
                                $result[$i]['cuisines'] = $ls['cuisines']?$ls['cuisines']:'';
                                $result[$i]['activities'] = $ls['activities']?$ls['activities']:'';
                                $result[$i]['services'] = $ls['services']?$ls['services']:'';
                                // $listing['price'] = $ls['price']?$ls['price']:'';
                                $result[$i]['star_rating'] = $ls['star_rating']?$ls['star_rating']:'';
                                $result[$i]['description'] = $ls['description'];
                                $result[$i]['website'] = $ls['website'];
                                $result[$i]['email'] = $ls['email'];
                                $result[$i]['phone_number'] = (String)$ls['phone_number'];
                                $result[$i]['self_working'] = (String)$ls['own_declaration'];
                                $result[$i]['distance'] = (string)$ls['distance'];
                                $i++;
                            }
                        }
                        else{
                            return response()->json([
                                'message'=>'No data Found',
                                'status'=>'0'
                              ], $this->successStatus);
                            }
                        }
                        return response()->json([
                            'status'=>'1',
                            'message'=>'Business fetched successfully',
                            'eventListing'=>$result
                          ], $this->successStatus);
                    }
                }
            }
            else{
                return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
            }
        }


    public function searchActivities(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
            $validator = Validator::make($request->all(), [  
                // 'area' => 'required',
                // 'radius' => 'required',
                // 'cuisines' => 'required',
                // 'star_rating' => 'required',
                // 'features' => 'required',
              ]);
              if ($validator->fails()) {
                return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
            }
            else{
                if ( ( empty($saveArray['area']) && empty($saveArray['radius']) ) || !empty($saveArray['activities']) ) 
                {
                    // print_r("nrml");die();
                    $query = Business::query();

                    if ( $saveArray['activities'] )  {
                        $activities = explode(',',$saveArray['activities']);
                        $query = $query->where(function($query) use($activities) {
                            foreach($activities as $activity) {
                                $query->orWhere('activities', 'like', "%$activity%");
                            };
                        })
                        ->inRandomOrder();
                    }

                    $business = $query->get();
                    $businessData = json_decode(json_encode($business), true);
                    $record = [];
                    if(count($businessData)){
                        foreach($businessData as $ls){
                            if ($ls['service_image'] == '') {
                                $service_image = '';
                            }else{
                                $service_image = url('/service_images',$ls['service_image']);
                            }
                            $listing['service_image'] = $service_image;
                            $listing['id'] = (String)$ls['id'];
                            $listing['business_type'] = $ls['business_type'];
                            $listing['business_name'] = $ls['business_name'];
                            $listing['features'] = $ls['features']?$ls['features']:'';
                            $listing['cuisines'] = $ls['cuisines']?$ls['cuisines']:'';
                            $listing['activities'] = $ls['activities']?$ls['activities']:'';
                            $listing['services'] = $ls['services']?$ls['services']:'';
                            // $listing['price'] = $ls['price']?$ls['price']:'';
                            $listing['star_rating'] = $ls['star_rating']?$ls['star_rating']:'';
                            $listing['description'] = $ls['description'];
                            $listing['website'] = $ls['website'];
                            $listing['email'] = $ls['email'];
                            $listing['phone_number'] = (String)$ls['phone_number'];
                            $listing['self_working'] = (String)$ls['own_declaration'];
                            $record[] = $listing;
                          }
                          return response()->json([
                            'status'=>'1',
                            'message'=>'Business fetched successfully',
                            'businessListing'=>$record
                          ], $this->successStatus);
                        }
                        else{
                          return response()->json([
                            'message'=>'No data Found',
                            'status'=>'0'
                          ], $this->successStatus);
                        }
                    }else{
                    $areas = explode('|',$saveArray['area']);
                    $mainArray = [];
                    $i = 0;
                    foreach($areas as $are) {
                        $query = Business::query();
                        $latlng = explode(',',$are);
                        $lat = $latlng[0];
                        $lng = $latlng[1];
                        $radius = $saveArray['radius']?$saveArray['radius']:'5';
                        $query = $query->selectRaw(" * ,( 3959 * acos( cos( radians(?) ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians(?)) + sin( radians(?) ) * sin( radians( latitude ) ))) AS distance", [$lat, $lng, $lat])
                            ->having("distance", "<", $radius)
                            // ->where('category','food_drinks')
                            ->orderBy("distance",'asc')
                            ->offset(0)
                            ->limit(20);

                            if ( $saveArray['activities'] )  {
                                $activities = explode(',',$saveArray['activities']);
                                $query = $query->where(function($query) use($activities) {
                                    foreach($activities as $activity) {
                                        $query->orWhere('activities', 'like', "%$activity%");
                                    };
                                })
                                ->inRandomOrder();
                            }

                            $business = $query->get();
                            $businessData = json_decode(json_encode($business), true);
                            if(count($businessData)){
                            foreach ($businessData as $ls) {
                                // $result[$i] = $ls;
                                if ($ls['service_image'] == '') {
                                    $service_image = '';
                                }else{
                                    $service_image = url('/service_images',$ls['service_image']);
                                }
                                $result[$i]['id'] = (string)$ls['id'];
                                $result[$i]['service_image'] = $service_image;
                                $result[$i]['id'] = (String)$ls['id'];
                                $result[$i]['business_type'] = $ls['business_type'];
                                $result[$i]['business_name'] = $ls['business_name'];
                                $result[$i]['features'] = $ls['features']?$ls['features']:'';
                                $result[$i]['cuisines'] = $ls['cuisines']?$ls['cuisines']:'';
                                $result[$i]['activities'] = $ls['activities']?$ls['activities']:'';
                                $result[$i]['services'] = $ls['services']?$ls['services']:'';
                                // $listing['price'] = $ls['price']?$ls['price']:'';
                                $result[$i]['star_rating'] = $ls['star_rating']?$ls['star_rating']:'';
                                $result[$i]['description'] = $ls['description'];
                                $result[$i]['website'] = $ls['website'];
                                $result[$i]['email'] = $ls['email'];
                                $result[$i]['phone_number'] = (String)$ls['phone_number'];
                                $result[$i]['self_working'] = (String)$ls['own_declaration'];
                                $result[$i]['distance'] = (string)$ls['distance'];
                                $i++;
                            }
                        }
                        else{
                            return response()->json([
                                'message'=>'No data Found',
                                'status'=>'0'
                              ], $this->successStatus);
                            }
                        }
                        return response()->json([
                            'status'=>'1',
                            'message'=>'Business fetched successfully',
                            'eventListing'=>$result
                          ], $this->successStatus);
                    }
                }
            }
            else{
                return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
            }
    }

    /*
      * This API is not using from backend front-end handle this process code is here for test purpose only 
    */ 

    public function getLocationsInRadius(Request $request)
    {
        $saveArray = $request->all();
        $validator = Validator::make($request->all(), [
            'lat' => 'required',
            'lng' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
        }
        else{
            $radius = $saveArray['radius'];
            $lat = $saveArray['lat'];
            $lng = $saveArray['lng'];

            $radius = $radius * 0.62137; //km to miles
            
            // $url = 'http://gd.geobytes.com/GetNearbyCities?radius='.$radius.'&Latitude='.$lat.'&Longitude='.$lng.'&limit=999';

            // $url = 'http://overpass.kumi.systems/api/interpreter?data?node[place=city][bbox=-1.150818,50.761653,-0.987396,50.851908]';

            // $url = 'http://overpass.kumi.systems/api/interpreter?data=[out:json];(node["place"~"town|city|village"][population](if: t["population"] > 2000)(around:55000,'.$lat.'',''.$lng.'););out;';

            $overpass = 'http://overpass.kumi.systems/api/interpreter?data=[out:json];(node["place"~"town|city|village"][population](if: t["population"] > 2000)(around:55000,'.$lat.','.$lng.'););out geom;';

            $html = file_get_contents($overpass);
            $result = json_decode($html, true); // "true" to get PHP array instead of an object

            // elements key contains the array of all required elements
            $data = $result['elements'];

            // $url = 'http://gd.geobytes.com/GetNearbyCities?radius='.$radius.'&Latitude=51.11519&Longitude=-115.765&limit=999';
            /*
            $lat = $saveArray['lat'];
                $long = $saveArray['long'];
                $type = $saveArray['type'];
                $url ='https://api.aerisapi.com/forecasts/{'. $lat .' , '. $long .'}?format=json&filter=day&limit=8&client_id=T2T2vjYw61ScZ1JyMSASB&client_secret=YVxmh5048hOCFWry1mY2prDfWgt8IQIdl1ZgcWXJ';
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, false);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                //curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
                $result = curl_exec ($ch);
                curl_close ($ch);
            */
            // $response_json = file_get_contents($url);
            // $response = json_decode($response_json, true);
            // print_r($response);die();
            // $overpass = 'https://overpass.kumi.systems/api/interpreter?data=[out:json];area(3600046663)->.searchArea;(node["place"="town|city|village|"](area.searchArea););out;';
            // collecting results in JSON format
            // $html = file_get_contents($overpass);
            // $jsonout = json_decode($html);

            $nearbydata = [];

            foreach ($response as $key) {
                if (!empty($key)) {
                    // $near['1'] = $key[0];
                    $near['city_town'] = $key[1];
                    $near['state_code'] = $key[2];
                    $near['state'] = $key[12];
                    $near['zone'] = $key[4];
                    // $near['1'] = $key[5];
                    $near['country_code'] = $key[6];
                    $near['country'] = $key[3];
                    // $near['3'] = $key[7];
                    $near['latitude'] = $key[8];
                    // $near['5'] = $key[9];
                    $near['longitude'] = $key[10];
                    // $near['7'] = $key[11];
                    $nearbydata[] = $near;
                }else{
                    return response()->json(['status'=>'0','message'=>'Invalid Location',], $this->successStatus);
                }
            }
            return response()->json(['status'=>'1','message'=>'nearby towns fetched successfully','listing'=>$nearbydata], $this->successStatus);
        }
    }

    /*
      * Updating nearby town fetching from overpass api in frontend and updating in db
    */

    public function updatenearbytowns(Request $request)
    {
        $saveArray = $request->all();
        $validator = Validator::make($request->all(), [
            'resort_id' => 'required',
            'nearby_towns' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
        }
        else{
            $checkData = Nearbytown::where('resort_id',$saveArray['resort_id'])->first();
            if ($checkData) {
                $updateData = [
                    'nearby_towns'=> $saveArray['nearby_towns']?$saveArray['nearby_towns']:'',
                    'updated_at'=>date('Y-m-d h:i:s'),
                    ];
                    $update = Nearbytown::where(['resort_id'=>$saveArray['resort_id']])->update($updateData);
                    return response()->json([
                        'status'=>'1',
                        'message'=>'nearby towns updated successfully'
                    ], $this->successStatus);
            }else{
                $insertData = [
                    'resort_id'=> $saveArray['resort_id'],
                    'nearby_towns'=>$saveArray['nearby_towns'],
                    'created_at'=>date('Y-m-d h:i:s'),
                ];
                $insertedID = Nearbytown::insertGetId($insertData);
                return response()->json([
                        'status'=>'1',
                        'message'=>'nearby towns saved successfully'
                    ], $this->successStatus);
            }
        }
    }

    /*
      * get updated nearby towns from db resort wise
    */    

    public function getNearbyTowns(Request $request)
    {
        $saveArray = $request->all();
        $validator = Validator::make($request->all(), [
            'resort_id' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
        }
        else{
            $getData = Nearbytown::where('resort_id',$saveArray['resort_id'])->first();
            $resortname = Resort::where('id',$saveArray['resort_id'])->first();
            if ($getData) {
                $first =  json_decode($getData['nearby_towns']);
                return response()->json([
                    'status' => '1',
                    'message' => 'data fetched successfully',
                    'nearByArea' => $resortname['resort_name'],
                    'lat' => $resortname['latitude'],
                    'lon' => $resortname['longitude'],
                    'nearbyListing'=>$first?$first:'',
                ], $this->successStatus);
            }else{
                return response()->json([
                    'status'=>'1',
                    'message'=>'no nearby towns',
                    'nearByArea'=>$resortname['resort_name'],
                    'lat' => $resortname['latitude'],
                    'lon' => $resortname['longitude'],
                    'nearbyListing'=>'',
                ], $this->successStatus);
            }
        }
    }

    /*
      * Edit Business Admin setting by Admin
    */   

    public function editBusinessAdminSetting(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
            $validator = Validator::make($request->all(), [ 
                'business_id' => 'required'
            ]);
              if ($validator->fails()) { 
                return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);            
              }
              else{
                $getBusiness = Business::where('id',$saveArray['business_id'])->first();
                if ($getBusiness) {
                    $check = $this->businessPermission($request ,$saveArray['business_id'],$userExist['id']);
                    if ($check == 1) {
                        if($request->hasFile('service_image')){
                            $file = time().$request->service_image->getClientOriginalName();
                            $request->service_image->move(public_path('/service_images') . '/', $file);
                        }
                        else{
                            $file = $getBusiness['service_image'];
                        }
                        $updateData = [
                            'bcolor'=>$saveArray['bcolor']?$saveArray['bcolor']:$getBusiness['bcolor'],
                            'fcolor'=>$saveArray['fcolor']?$saveArray['fcolor']:$getBusiness['fcolor'],
                            'service_image'=>$file,
                            'updated_at'=>date('Y-m-d h:i:s'),
                        ];
                        $update = Business::where('id',$saveArray['business_id'])->update($updateData);
                        if ($update) {
                            return response()->json(['status'=>'1','message'=>'data updated'], $this->successStatus);
                        }else{
                            return response()->json(['status'=>'0','message'=>'error',], $this->successStatus);
                        }
                    }else{
                        return response()->json(['message'=>'you have not Admin Permission to do this','status'=>'0'], $this->badrequest);
                    }
                }else{
                    return response()->json(['status'=>'1','message'=>'no business found',], $this->successStatus);
                }
            }
        }
        else{
            return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }

    /*
      * get Business Admin setting by Business Admin
    */      

    public function getbusinessAdminSetting(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
            $validator = Validator::make($request->all(), [ 
                'business_id' => 'required'
            ]);
              if ($validator->fails()) { 
                return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);            
              }
              else{
                $getBusiness = Business::where('id',$saveArray['business_id'])->first();
                if ($getBusiness) {
                    $check = $this->businessPermission($request ,$saveArray['business_id'],$userExist['id']);
                    if ($check == 1) {
                        $service_image = url('/service_images',$getBusiness['service_image']);
                        $setting_details = array(
                            'id'=>(String)$getBusiness['id'],
                            'bcolor'=>$getBusiness['bcolor'],
                            'fcolor'=>$getBusiness['fcolor'],
                            'service_image'=>$service_image,
                            'updated_at'=>$userExist['created_at'],
                        );
                        $businessStatus = Lunch::where('business_service_id',$saveArray['business_id'])->first();
                        $jobStatus = Job::where('business_service_id',$saveArray['business_id'])->first();
                        $serviceStatus = array(
                            'lunch_id'=>$businessStatus['id']?$businessStatus['id']:'',
                            'request_status'=>$businessStatus['request_status']?$businessStatus['request_status']:'',
                            'job_id'=>$jobStatus['id']?$jobStatus['id']:'',
                            'job_status'=>$jobStatus['job_request']?$jobStatus['job_request']:'',
                        );
                        return response()->json(['status'=>'1','message'=>'settings fetched successfully','admin_setting'=>$setting_details,'serviceStatus'=>$serviceStatus], $this->successStatus);
                    }else{
                        return response()->json(['message'=>'you have not Admin Permission to do this','status'=>'0'], $this->badrequest);
                    }
                }else{
                    return response()->json([
                        'status'=>'1',
                        'message'=>'No Business found',
                    ], $this->successStatus);
                }
            }
          }
          else{
            return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }

    /*
      * Regenerate Business Codes by Business Admin
    */   

    public function regenerateBusinessCode(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
            $validator = Validator::make($request->all(), [ 
                'business_id' => 'required'
            ]);
              if ($validator->fails()) { 
                return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);            
              }
              else{
                if ($getBusiness) {
                    $getBusiness = Business::where('id',$saveArray['business_id'])->first();
                    $check = $this->businessPermission($request ,$saveArray['business_id'],$userExist['id']);
                    if ($check == 1) {
                        $BUIrandomname = 'BUI';
                        $BUIgetRstring = $this->generateRandomString();
                        $businessCode = $BUIrandomname.$BUIgetRstring;
                        $updateData = [
                            'business_code'=>$businessCode,
                            'updated_at'=>date('Y-m-d h:i:s'),
                        ];
                        $update = Business::where('id',$saveArray['business_id'])
                        ->update($updateData);
                        return response()->json([
                            'status'=>'1',
                            'message'=>'codes regenerated',
                        ], $this->successStatus);
                    }else{
                        return response()->json(['message'=>'you have not Admin Permission to do this','status'=>'0'], $this->badrequest);
                    }
                }else{
                    return response()->json(['status'=>'1','message'=>'No Business found',], $this->successStatus);
                }
            }
        }
        else{
            return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }

    /*
      * reverse geocoding API
    */

    public function reverseOpenCage(Request $request){
        $saveArray = $request->all();
        $validator = Validator::make($request->all(), [
            'lat' => 'required',
            'lng' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
        }
        else{
            $lat = $saveArray['lat'];
            $lng = $saveArray['lng'];
            $geocoder = new \OpenCage\Geocoder\Geocoder('051b24843af041939d917f58aca6ca8a');
            $result = $geocoder->geocode($lat.','.$lng); # latitude,longitude (y,x)
            $array = (array) $result;
            $location = array(
                'confidence'=>(String)$array['results'][0]['confidence'],
                'location'=>$array['results'][0]['formatted'],
                '_category'=>$array['results'][0]['components']['_category'],
                // 'city'=>$array['results'][0]['components']['city'],
                'state'=>$array['results'][0]['components']['state'],
                'country'=>$array['results'][0]['components']['country'],
                // 'postcode'=>$array['results'][0]['components']['postcode'],
                'components'=>$array['results'][0]['components'],
                'geometry'=>$array['results'][0]['geometry']
              );

            return response()->json([
                'status'=>'1',
                'message'=>'location fetched successfully',
                'location'=>$location
            ]);
        }
    }

    /*
      * forward geocoding API
    */

    public function openCageData(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
            $validator = Validator::make($request->all(), [ 
                'resort_id' => 'required',
                'search' => 'required'
            ]);
              if ($validator->fails()) { 
                return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);            
              }
              else{
                $getResort = Resort::where('id',$saveArray['resort_id'])->first();
                if ($getResort) {
                    $search = $saveArray['search'];
                    $country = $getResort['country'];
                    $countryName = Country::where('id',$country)->select('name')->first();
                    $cname = $countryName;
                    
                    $lat_degrees = $getResort['latitude'];
                    $lon_degrees = $getResort['longitude'];
                    $distance_in_miles = '37.2823';
                    $radius = '6378.1'; // of earth in miles

                    // bearings - FIX   
                    $due_north = deg2rad(0);
                    $due_south = deg2rad(180);
                    $due_east = deg2rad(90);
                    $due_west = deg2rad(270);

                    // convert latitude and longitude into radians 
                    $lat_r = deg2rad($lat_degrees);
                    $lon_r = deg2rad($lon_degrees);

                    // find the northmost, southmost, eastmost and westmost corners $distance_in_miles away
                    $northmost  = asin(sin($lat_r) * cos($distance_in_miles/$radius) + cos($lat_r) * sin ($distance_in_miles/$radius) * cos($due_north));
                    $southmost  = asin(sin($lat_r) * cos($distance_in_miles/$radius) + cos($lat_r) * sin ($distance_in_miles/$radius) * cos($due_south));

                    $eastmost = $lon_r + atan2(sin($due_east)*sin($distance_in_miles/$radius)*cos($lat_r),cos($distance_in_miles/$radius)-sin($lat_r)*sin($lat_r));
                    $westmost = $lon_r + atan2(sin($due_west)*sin($distance_in_miles/$radius)*cos($lat_r),cos($distance_in_miles/$radius)-sin($lat_r)*sin($lat_r));


                    $northmost = rad2deg($northmost);
                    $southmost = rad2deg($southmost);
                    $eastmost = rad2deg($eastmost);
                    $westmost = rad2deg($westmost);

                    // sort the lat and long so that we can use them for a between query        
                    if ($northmost > $southmost) { 
                        $lat1 = $southmost;
                        $lat2 = $northmost;

                    } else {
                        $lat1 = $northmost;
                        $lat2 = $southmost;
                    }


                    if ($eastmost > $westmost) { 
                        $lon1 = $westmost;
                        $lon2 = $eastmost;

                    } else {
                        $lon1 = $eastmost;
                        $lon2 = $westmost;
                    }

                    // return array($lat1,$lat2,$lon1,$lon2);

                    // $searchq = 'spaghetti%2CCanada';
                    $new = str_replace(' ', '%20', $search);

                    $searchq =  $new.','.$cname['name'];

                    $squery = str_replace(',', '%2C', $searchq);

                    $url = 'https://api.opencagedata.com/geocode/v1/json?key=051b24843af041939d917f58aca6ca8a&q='.$squery.'&pretty=1&min_confidence=7&bounds='.$lon1.','.$lat1.','.$lon2.','.$lat2.'';

                    // $url = 'https://api.opencagedata.com/geocode/v1/json?key=051b24843af041939d917f58aca6ca8a&q='.$squery.'&pretty=1&min_confidence=7&bounds=-116.62279663247443,50.57638870279786,-114.90554936752561,51.65437329720212';
                    // print_r($url1);die();

                    $response_json = file_get_contents($url);
                    $response = json_decode($response_json, TRUE);
                    // $data = json_decode($json, TRUE);
                    $array = (array) $response;
                    $getData = $array['results'];
                    // print_r($getData);die();

                    /*with filters*/ 
                    $stateArray = [];
                    if(count($getData)){

                        // foreach ($getData as $key) {
                        //     $listing['location'] = $key['formatted'];
                        //     $listing['confidence'] = (String)$key['confidence'];
                        //     $listing['_category'] = $key['components']['_category'];
                        //     // $listing['_type'] = $key['components']['_type'];
                        //     // $listing['city'] = $key['components']['city'];
                        //     // $listing['country'] = $key['components']['country'];
                        //     // $listing['postcode'] = $key['components']['postcode']?$key['components']['postcode']:'';
                        //     // $listing['state'] = $key['components']['state'];
                        //     $listing['components'] = $key['components'];
                        //     $listing['geometry'] = $key['geometry'];
                        //     $stateArray[] = $listing;
                        // }

                        foreach ($getData as $key) {
                            if ($key['components']['_category'] == 'commerce' || $key['components']['_category'] == 'building' || $key['components']['_category'] == 'travel/tourism') {
                                $listing['location'] = $key['formatted'];
                                $listing['confidence'] = (String)$key['confidence'];
                                $listing['_category'] = $key['components']['_category'];
                                $listing['components'] = $key['components'];
                                $listing['geometry'] = $key['geometry'];
                                $stateArray[] = $listing;
                            }else{
                                return response()->json([
                                    'message'=>'No Address Found',
                                    'status'=>'0',
                                    'opencageData'=>'No suggestions found please enter manually'
                                ], $this->successStatus);
                            }
                        }
                        return response()->json([
                            'status'=>'1',
                            'message'=>'data fetched successfully',
                            'opencageData'=>$stateArray
                        ], $this->successStatus);
                    }else{
                        return response()->json([
                            'message'=>'No Address Found',
                            'status'=>'0',
                            'opencageData'=>'No suggestions found please enter manually'
                        ], $this->successStatus);
                    }
                }else{

                }
              }
          }
          else{
            return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }


    public function index(Request $request)
    {
        $businesses = $this->businessRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($businesses->toArray(), 'Businesses retrieved successfully');
    }

    /**
     * Store a newly created Business in storage.
     * POST /businesses
     *
     * @param CreateBusinessAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateBusinessAPIRequest $request)
    {
        $input = $request->all();

        $business = $this->businessRepository->create($input);

        return $this->sendResponse($business->toArray(), 'Business saved successfully');
    }

    /**
     * Display the specified Business.
     * GET|HEAD /businesses/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Business $business */
        $business = $this->businessRepository->find($id);

        if (empty($business)) {
            return $this->sendError('Business not found');
        }

        return $this->sendResponse($business->toArray(), 'Business retrieved successfully');
    }

    /**
     * Update the specified Business in storage.
     * PUT/PATCH /businesses/{id}
     *
     * @param int $id
     * @param UpdateBusinessAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateBusinessAPIRequest $request)
    {
        $input = $request->all();

        /** @var Business $business */
        $business = $this->businessRepository->find($id);

        if (empty($business)) {
            return $this->sendError('Business not found');
        }

        $business = $this->businessRepository->update($input, $id);

        return $this->sendResponse($business->toArray(), 'Business updated successfully');
    }

    /**
     * Remove the specified Business from storage.
     * DELETE /businesses/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Business $business */
        $business = $this->businessRepository->find($id);

        if (empty($business)) {
            return $this->sendError('Business not found');
        }

        $business->delete();

        return $this->sendSuccess('Business deleted successfully');
    }
}
