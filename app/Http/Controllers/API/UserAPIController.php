<?php

namespace App\Http\Controllers\API;
use App\Traits\CommonFunctionTrait;

use App\Http\Requests\API\CreateUserAPIRequest;
use App\Http\Requests\API\UpdateUserAPIRequest;
use App\Http\Controllers\AppBaseController;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;
use Hash;
use Crypt;
use Response;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Support\Facades\Password;
use App\Mail\WelcomeMail;
use Illuminate\Support\Facades\Mail;

/**
 * Class UserController
 * @package App\Http\Controllers\API
 */

/*
  * User Controller is for APIs related with Users
*/

class UserAPIController extends AppBaseController
{
    /** @var  UserRepository */
    private $userRepository;
    public $unauthorised = 401;
    public $successStatus = 200;
    public $badrequest = 400;
    use SendsPasswordResetEmails;
    use CommonFunctionTrait;

    public function __construct(UserRepository $userRepo)
    {
        $this->userRepository = $userRepo;
    }

    /**
     * Display a listing of the User.
     * GET|HEAD /users
     *
     * @param Request $request
     * @return Response
    */

    /*
      * Login API
    */

    public function login(Request $request)
    {
      $validator = Validator::make($request->all(), [
            'email' => 'required',
            'password' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->successStatus);
        }
        else{
          $existingUser = \App\User::where(['email' => $request->email])->first();
          if($existingUser){
            if(Hash::check($request->password , $existingUser['password'])){ // add Hash::check() back in here
              //$stripe_id = $existingUser->stripe_id?$existingUser->stripe_id:'';
             $existingUser->load(['flags' => function($query){
                $query->where('flag', 'first_login'); 
              }])->get();
              $resorts = \App\Resort::select('id','name','state','country','patrol_no')->get();
            
              //create newUser's token
              if($existingUser->remember_token == null){
                  $token = (string)mt_rand(100,1000000);
                  $token_en = Crypt::encrypt($token);
                  \App\User::where('id', '=', $existingUser->id)->update(['remember_token' => $token]);
              } else {
                $token_en = Crypt::encrypt($existingUser->remember_token);
              }
              echo $existingUser->remember_token;
              //create newUser's token

              //check device
              $checkDevice = \App\Device::where('device_id', $request->device_id)->where('device_type', $request->device_type)->where('user_id', $existingUser->id)->first();
              if($checkDevice){
                $checkDevice->update(['updated_at' => date('Y-m-d h:i:s')]);
              } else{
                $device = new \App\Device;
                $device->user_id = $existingUser->id;
                $device->device_id = $request->device_id;
                $device->device_type = $request->device_type;
                $device->save();
              }
              //check device
              
              if($existingUser->flags[0]->flag_status == 0){ //first time logged in
                $this->changeFlagStatus($existingUser->id, 'first_login');
                return response()->json(['status' => '1', 'message' => 'Logged in successfully', 'already_logged_in' => "no", 'user_id'=>$existingUser->id, 'permissions' => $existingUser->accessCodes, 'name'=>$existingUser->name,'email'=>$existingUser->email,'country'=>$existingUser->country, 'image'=>$existingUser->image, 'resorts' => $resorts, 'token'=>$token_en], $this->successStatus);
              } else if ($existingUser->flags[0]->flag_status == 1){ // not first time logged in
                return response()->json(['status' => '1','message'=>'Logged in successfully', 'already_logged_in' => "yes", 'user_id'=>$existingUser->id, 'permissions' => $existingUser->accessCodes, 'name'=>$existingUser->name,'email'=>$existingUser->email,'units'=>$existingUser->units, 'fav_resorts' => $existingUser->fav_resorts, 'image'=>$existingUser->image, 'resorts' => $resorts,'token'=>$token_en], $this->successStatus);
              } else {
                return response()->json(['error' => 'debug error message - error in login flag check']);
              }
            } else {
              return response()->json(['message'=>'Email or Password is incorrect','status'=>'0'], $this->successStatus);
            }
          } else {
            return response()->json(['message'=>'User does not exist','status'=>'0'], $this->successStatus);
          };
          
        }
      }


    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'bail|required|email|unique:users,email', 
            'password' => 'required',
            'country' => 'required',
            'phone_no' => 'required',
            'DOB' => 'required',
            'referral_code' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->successStatus);
        }
        else{
          //check referral code first, before creating user in DB
          $checkReferralCode = \App\User::where('referral_code', '=', $request->referral_code)->select('id')->first();
          //check this following if statement - if referral code is null send error message
          if(isset($checkReferralCode->id)){
            $newUser =  new \App\User;
            $newUser->name = $request->name; 
            $newUser->email = $request->email; 
            $newUser->country = $request->country; 
            if ($request->country == 'USA'){
              $newUser->units = 'imperial'; 
            } else{
              $newUser->units = 'metric';
            }
            $newUser->phone_no = $request->phone_no;
            $newUser->DOB = $request->DOB;
            $newUser->password = Hash::make($request->password);

            /* create newUser's referral_code*/
            $rcode = $this->generateRandomString();
            $newUser->referral_code = $rcode;
            /*referral_code*/

            //rewards to referrer user 
            $this->rewardSnowflakes(100, 'referred_friend', $checkReferralCode->id);
            \App\User::where('id', '=', $checkReferralCode->id)->update(['snowflakes' => 100]);
            $newUser->referrer_user_id = $checkReferralCode->id;
            //rewards to referrer user

            if(isset($checkReferralCode->id)){
              $newUser->save();

              //rewards to newUser
              $this->rewardSnowflakes(100, 'signed_up', $newUser->id); 
              $this->updateSnowflakeSum(100, $checkReferralCode->id);
              //rewards to newUser

              //create first_login flag
                $flag = new \App\Flag;
                $flag->user_id = $newUser->id; 
                $flag->flag = 'first_login';
                $flag->save();
              //create first_login flag

              //create device 
              $device = new \App\Device;
              $device->user_id = $newUser->id;
              $device->device_id = $request->device_id;
              $device->device_type = $request->device_type;
              $device->save();
              //create device
              
              return response()->json([
                  'status'=>'1',
                  'message'=>'User Registered successfully',
                  'id'=>(String)$checkReferralCode,
              ], $this->successStatus);
              } else {
                  return response()->json([
                      'status'=>'0',
                      'message'=>'User Registration failed'
                  ], $this->badrequest);
              }


          } else {
            return response()->json(['status'=>'0', 'message'=>'Referral Code Is Invalid'], $this->successStatus);
            } 
          }

            // /*Tracking data*/
            // $insertTrackData = [
            //     'user_id'=>$user_id,
            //     'current_track'=> '2', //false
            //     'future_track'=>'2', //false
            //     'resort_id'=>'1',
            //     'created_at'=>date('Y-m-d h:i:s'),
            // ];
            // $trackStatus = Tracking::insert($insertTrackData);
            // /*Tracking data*/
    }
  /*
    * Get Profile API 
  */  
  
  public function getOwnProfile(Request $request)
  {
    try{
      $token_de = Crypt::decrypt($request->header('token'));
      echo $token_de;
    } catch (DecryptException $e){
      return response()->json(['status' => '0', 'message' => $e]);
    }
    $existingUser = \App\User::where(['remember_token'=>$token_de])->first();
    if($existingUser){
      $existingUser->load('activities');
      return response()->json(['status'=>'1','message'=>'User fetched successfully', 'name' => $existingUser->name, 'email' => $existingUser->email, 'phone' => $existingUser->phone_no, 'bio' => $existingUser->bio, 'activities' => $existingUser->activities]);
    }
    else{
      return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
    }
  }

  /*
    * Edit Profile API 
  */    

  public function editProfile(Request $request)
  {
      //remember to validate that new email is unique (if there is a new email)
      //receiving data - save the key value pairs in variable 
      //use User::where($existingUser->id, =, id)->update([$variable])
      if($request->hasFile('image')){
        $image = $request->file('image')->store('storage/app/public/images/user_images');
      } else if($request->has('email')){
        $validator = Validator::make($request->email, [
          'email' => 'bail|required|email|unique:users,email', 
        ]);
        if($validator->fails()){
          return response()->json(['message' => 'This email address is already in use', 'status' => '0']);
        } else {
          User::where($existingUser->id, '=', 'id')->update(['email' => $request->email]);
        }
      }



      $skier = array($saveArray['skier_fav_tarrian']);
      $skier_fav_tarrian = implode(",", $skier);
      $snowboarder = array($saveArray['snowboarder_fav_tarrian']);
      $snowboarder_fav_tarrian = implode(",", $snowboarder);

      if ($saveArray['share_location'] == '2') {
        $shareloc = 'false';
      }elseif ($saveArray['share_location'] == '1') {
        $shareloc = 'true';
      }elseif ($saveArray['share_location'] == '') {
        $shareloc = $userExist['share_location'];
      }

      /*tracking data*/
      $updateTrack = [
        'current_track'=>$saveArray['share_location'],
        'future_track'=>$saveArray['share_location'],
      ];
      $update_track = Tracking::where(['user_id'=>$userExist['id']])->update($updateTrack);
      /*tracking data*/

      $updateProfile = [
        'name'=>$saveArray['name']?$saveArray['name']:$userExist['name'],
        'email'=>$saveArray['email']?$saveArray['email']:$userExist['email'],
        'country'=>$saveArray['country']?$saveArray['country']:$userExist['country'],
        'unit'=>$saveArray['unit']?$saveArray['unit']:$userExist['unit'],
        'phone_no'=>$saveArray['phone_no']?$saveArray['phone_no']:$userExist['phone_no'],
        'description'=>$saveArray['description']?$saveArray['description']:$userExist['description'],
        'DOB'=>date("d-m-Y", strtotime($saveArray['DOB']))?date("d-m-Y", strtotime($saveArray['DOB'])):date("d-m-Y", strtotime($saveArray['DOB'])),
        'image'=>$file,
        'share_location'=>$shareloc?$shareloc:$userExist['share_location'],
        'skier'=>$saveArray['skier']?$saveArray['skier']:$userExist['skier'],
        'skier_sky_level'=>$saveArray['skier_sky_level']?$saveArray['skier_sky_level']:$userExist['skier_sky_level'],

        'skier_fav_tarrian'=>$skier_fav_tarrian?$skier_fav_tarrian:$userExist['skier_fav_tarrian'],

        'snowboarder'=>$saveArray['snowboarder']?$saveArray['snowboarder']:$userExist['snowboarder'],
        'snowboarder_sky_level'=>$saveArray['snowboarder_sky_level']?$saveArray['snowboarder_sky_level']:$userExist['snowboarder_sky_level'],

        'snowboarder_fav_tarrian'=>$snowboarder_fav_tarrian?$snowboarder_fav_tarrian:$userExist['snowboarder_fav_tarrian'],

        'updated_at'=>date('Y-m-d h:i:s')
      ];
      $update = User::where(['remember_token'=>$token])->update($updateProfile);
      if($update){
        $userDetails = User::where(['remember_token'=>$token])->first();
        $user_details = array(
          'user_id'=>(String)$userDetails['id'],
        );
        return response()->json([
          'status'=>'1',
          'message'=>'User Updated Successfully',
          'user_details'=>$user_details
        ], $this->successStatus);
      }
      else{
        return response()->json([
          'message'=>'User does not Update',
          'status'=>'0'
        ], $this->badrequest);
      }
  }

  /*
    * Forgot Password API 
  */   

  public function forgotPassword(Request $request)
  {
    if (User::where('email', '=', $request->email)->exists()) {
      $response = $this->broker()->sendResetLink(
        $request->only('email')
      );
      if($response == 'passwords.sent'){
        return response()->json(['status' =>'1' ,'message'=>'success', 'data'=> 'We have e-mailed your password reset link!'], $this->successStatus);
      } else{
        return response()->json(['status' =>'0' ,'message'=>'error', 'data'=> 'Error - we were unable to send you a reset link'], $this->successStatus);
      }
    } else{
      return response()->json(['status' =>'error' ,'message'=>'Unauthorised', 'data'=> 'We couldn\'t find your email' ], 401);
    }
  }

  /*
    * Change Password API 
  */   

  public function changePassword(Request $request)
  {
      $validator = Validator::make($request->all(), [
        'old_password' => 'required',
        'new_password' => 'required',
      ]);
      if ($validator->fails()) {
        return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
      }
      elseif ($request->old_password == $request->new_password) {
        return response()->json(['message'=>'New password is same as old password','status'=>'0'], $this->successStatus);
      }
      else{
          if(Hash::check($request->old_password, $request->existingUser->password)){
            $password_en =  Hash::make($request->new_password);
            $update = \App\User::where(['id'=>$request->existingUser->id])->update(['password' => $password_en]);
            echo $update;
            if($update){
              return response()->json([
                    'status'=>'1',
                    'message'=>'Password changed Successfully'
                  ], $this->successStatus);
            }
            else{
              return response()->json([
                    'status'=>'0',
                    'message'=>'Error on changing password'
                  ], $this->successStatus);
            }
          }
          else{
            return response()->json([
                    'status'=>'0',
                    'message'=>'Old password is incorrect'
                  ], $this->successStatus);
          }
      }
  }

  /*
    * Add Permission on user permissions page
  */   

  public function addPermission(Request $request)
  {
      $validator = Validator::make($request->all(), [
        'access_code' => 'required',
        'user_id' => 'required',
      ]);
      if ($validator->fails()) {
        return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
      } else {
        $existingCode = \App\Access_code::where('code', '=', $request->access_code)->first();
        if($existingCode){
          //check if the current user is admin! if an admin can add permissions for a user
          foreach ($request->existingUser->accessCodes as $accessCode){
            if($accessCode == $request->access_code){
              return response()->json(['message'=>'You already have this permission','status'=>'0'], $this->successStatus);
            }
          }
          if($request->existingUser->id == $request->user_id){
            $request->existingUser->accessCodes()->attach($existingCode->id);
          }
          return response()->json(['status'=>'1','message'=>'Permission successfully added',], $this->successStatus);
        } else {
          return response()->json(['message'=>'Invalid Code','status'=>'0'], $this->successStatus);
        }
      }
  }

  /*
    * Remove permission on user permissions page
  */ 

  public function removePermission(Request $request)
  {
    //check if the current user is ADM or the User himself 
      $validator = Validator::make($request->all(), [ 
        'user_id' => 'required', // of the user whose permission is being deleted
        'access_code' => 'required',
      ]);
      if ($validator->fails()) { 
        return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);            
      } else {
        //check if code exists
        $existingCode = \App\Access_code::where('code', '=', $request->access_code)->first();
        if($existingCode){
          //check if current user is admin
          foreach ($request->existingUser->accessCodes as $accessCode){
            if($accessCode->permission_type == 'ADM'){
              $removed = $request->existingUser->accessCodes()->detach($existingCode->id);
            }
          }
          //check if current user is also the owner of the permission
          if($request->existingUser->id == $request->user_id){
            $removed = $request->existingUser->accessCodes()->detach($existingCode->id);
          }
          if($removed){
            return response()->json([
              'status'=>'1',
              'message'=>'Permission Cancelled Successfully',
              ], $this->successStatus);
          } else {
            return response()->json([
              'message'=>'You have cancelled permission already',
              'status'=>'0'
              ], $this->successStatus);
          }
        } else {
          return response()->json([
            'message'=>'You do not have this permission',
            'status'=>'0'
            ], $this->successStatus);
        }
      }
  }

  /*
    * To search any User
  */ 

  public function searchNewBuddies(Request $request)
  {
      $validator = Validator::make($request->all(), [  
        'search' => 'required'
      ]);
      if ($validator->fails()) { 
        return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);            
      }
      else{
        $request->existingUser->load(['buddies' => function($query){
          $query->where('status', 'accepted'); 
        }])->get(); 
        $usersSearch = \App\User::where('id', '!=', $request->existingUser->id)->where('name', 'LIKE', '%' . $request->search . '%')->select('id', 'name', 'image')->without('accessCodes')->get();
        if(count($usersSearch)){
        //check the current user's buddies ids (buddy_user_id) against the queried array of users' ids
          $newBuddies = []; 
          if(count($request->existingUser->buddies) >= 1){
            foreach($request->existingUser->buddies as $oldBuddy){
              foreach($usersSearch as $newBuddy){
                if ($oldBuddy->buddy_user_id != $newBuddy->id){
                  array_push($newBuddies, ['name' => $newBuddy->name, 'id' => $newBuddy->id, 'image' => $newBuddy->image]);
                }
              }
            }
          } else {
            foreach($usersSearch as $newBuddy){
              array_push($newBuddies, ['name' => $newBuddy->name, 'id' => $newBuddy->id, 'image' => $newBuddy->image]);
            }
          }
        } else{
          return response()->json(['status' => '1', 'message' => 'Your search did not find any users']);
        }
        return response()->json(['status' => '1', 'message' => 'success', 'users' => $newBuddies]);
      }
  }

  /*
    * Send friend request to anyone
  */ 

  public function newBuddyRequest(Request $request)
  {
      $validator = Validator::make($request->all(), [
        'user_id' => 'required',
        'name' => 'required'
      ]);
    if ($validator->fails()) {
      return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
    } else {
      //check if the request is already sent - if it exists in buddies with status requested
      $matchThese = ['user_id' => $request->existingUser->id, 'buddy_user_id' => $request->user_id, 'status' => 'requested'];
      $orMatchThese = ['user_id' => $request->user_id, 'buddy_user_id' => $request->existingUser->id, 'status' => 'requested'];
      $requested = \App\Buddy::where($matchThese)->first();
      $received = \App\Buddy::where($orMatchThese)->first();
      if($requested){
        return response()->json(['status'=>'0','message'=>'This buddy request is already pending'], $this->successStatus);
      } else if ($received){
        return response()->json(['status'=>'0','message'=>'You already have a pending buddy request from this user'], $this->successStatus);
      } else {
        $buddy= new \App\Buddy;
        $buddy->user_id = $request->existingUser->id; 
        $buddy->buddy_user_id = $request->user_id; 
        $buddy->status = 'requested'; 
        $buddy->save();

        /*send notification to the requested buddy*/
        $message = array(
          'message'=>" You’ve got a friend request from ". $request->name,
          'type'=>'send_request'
        );
        $this->send_notification($request->user_id, $message);
        /*send notification*/
        return response()->json(['status'=>'1','message'=>'Friend Request sent successfully', $this->successStatus]);
        }
        return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
      }
    }

  /*
    * Accept friend request
  */ 

  public function acceptBuddyRequest(Request $request)
  {
    // existingUser is the receiver of the request
    $validator = Validator::make($request->all(), [
      'user_id' => 'required', //user_id = the id of the user who sent the request (and is also user_id in the current pending request in the Buddy table)
    ]);
    if ($validator->fails()) {
      return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
    } else {
      try {
        \App\Buddy::where(['user_id'=>$request->user_id])->where(['buddy_user_id'=>$request->existingUser->id])->update(['status'=> 'accepted']);
        $buddy = new \App\Buddy;
        $buddy->user_id = $request->existingUser->id;
        $buddy->buddy_user_id = $request->user_id;
        $buddy->status = "accepted";
        $buddy->save();

        /*send notification*/
        $message = array(
          'message'=>" ". $request->existingUser->name ." has accepted your buddy request",
          'type'=>'accept_request',
        );
        $this->send_notification($request->user_id, $message);
        /*send notification*/

        return response()->json([
          'status'=>'1',
          'message'=>'Friend Request Accepted',
        ], $this->successStatus);

      } catch (Exception $e){
        return response()->json([
          'status'=>'0',
          'message'=>'error'
          ], $this->badrequest);
      }
    }
  }

  /*
    * Reject friend request
  */ 

  public function rejectBuddyRequest(Request $request)
  {
    $validator = Validator::make($request->all(), [
      'user_id' => 'required',
    ]);
    if ($validator->fails()) {
      return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
    }
    else{
      try{
        \App\Buddy::where(['user_id'=>$request->user_id])->where(['buddy_user_id'=>$request->existingUser->id])->update(['status' => 'rejected']);
        return response()->json([
          'status'=>'1',
          'message'=>'Friend Request Rejected',
        ], $this->successStatus);
      } catch (Exception $e){
        return response()->json([
          'status'=>'0',
          'message'=>'error'
          ], $this->badrequest);
      }
    }
  }

  /*
    * Cancel sent buddy request
  */ 

  public function cancelBuddyRequest(Request $request)
  {
    $validator = Validator::make($request->all(), [
      'user_id' => 'required',
    ]);
    if ($validator->fails()) {
      return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
    } else {
      try {
      \App\Buddy::where(['user_id'=> $request->existingUser->id, 'buddy_user_id' => $request->user_id])->delete();
      return response()->json([
        'status'=>'1',
        'message'=>'Buddy Request Canceled',
        ], $this->successStatus);
      } catch (Exception $e){
        return response()->json([
          'status'=>'0',
          'message'=>'error'
          ], $this->badrequest);
      }
    }
  }

  /*
    * Remove a buddy from the users list
  */ 

  public function removeBuddy(Request $request)
  {
    $validator = Validator::make($request->all(), [
      'user_id' => 'required',
    ]);
    if ($validator->fails()) {
      return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
    } else {
      try {
        \App\Buddy::whereIn('user_id', [$request->user_id, $request->existingUser->id])->whereIn('buddy_user_id', [$request->existingUser->id, $request->user_id])->delete();
        return response()->json([
          'status'=>'1',
          'message'=>'friend removed successfully',
        ], $this->successStatus);
      } catch (Exception $e){
        return response()->json([
          'status'=>'0',
          'message'=>'error'
          ], $this->badrequest);
      }
    }
  }

  /*
    * Get all accepted buddies
  */ 

  public function getBuddies(Request $request)
  {
     $buddyRelationships = \App\Buddy::where('user_id', $request->existingUser->id)->where('status', 'accepted')->get();
     //echo $buddyRelationships;
     if($buddyRelationships){
      $buddies = [];
      foreach($buddyRelationships as $buddy){
         array_push($buddies, $buddy->buddy_user_id);
      }
      $buddyInfo = \App\User::whereIn('id', $buddies)
      ->select('id', 'name', 'image')
      ->without('accessCodes')
      ->with(['groups' => function ($query) {
                $query->where('status', 'accepted')
                ->where('date', '>=', now('UTC'))->take(1);
             }])->get();
       
       $outgoing = \App\Buddy::where('user_id', $request->existingUser->id)->where('status', 'requested')->get();
       $outgoingBuddiesArr = [];
       foreach($outgoing as $outgoingBuddy){
         array_push($outgoingBuddiesArr, $outgoingBuddy->buddy_user_id);
       }
       $outgoingBuddies = \App\User::whereIn('id', $outgoingBuddiesArr)->select('id', 'name', 'image')->without('accessCodes')->get();
       
       
       $incoming = \App\Buddy::where('buddy_user_id', $request->existingUser->id)->where('status', 'requested')->get();
       $incomingBuddiesArr = [];
       foreach($incoming as $incomingBuddy){
         array_push($incomingBuddiesArr, $incomingBuddy->user_id);
       }
       $incomingBuddies = \App\User::whereIn('id', $incomingBuddiesArr)->select('id', 'name', 'image')->without('accessCodes')->get();
       return response()->json([
                 'status'=>'1',
                 'message'=>'Buddies fetched successfully',
                 'outgoingRequests' => $outgoingBuddies,
                 'incomingRequests' => $incomingBuddies,
                 'buddies' => $buddyInfo,
               ], $this->successStatus);
     } else {
      return response()->json([
                'status'=>'0',
                'message'=>'No Buddies Found',
              ], $this->successStatus);
     }
  }

  /*
    * To view anyone's profile
  */

public function getUserProfile(Request $request)
{
    $validator = Validator::make($request->all(), [
      'user_id' => 'required'
    ]);
    if ($validator->fails()) {
      return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
    }
    else{
      $getUser = \App\User::where('id', $request->user_id)->select('id', 'name', 'image', 'description')->without('accessCodes')->first();
      $checkFriendstatus = \App\Buddy::where(['user_id'=> $request->existingUser->id])->where(['buddy_user_id' => $request->user_id])
      ->orWhere(['user_id' => $request->user_id])->where(['buddy_user_id' => $request->existingUser->id])->first();
      if($checkFriendstatus->status == "accepted"){
        //they are buddies
        $getUser->load('activities')->load(['groups' => function ($query) {
          $query->where('status', 'accepted')
          ->where('date', '>=', now('UTC'))->take(3);
       }])->load('groups.userAdmin', 'groups.userAdmin.activities')->get();
  //missing the number of buddies in each group
       echo $getUser;
      }
      else{
        if($checkFriendstatus->status == 'requested'){
          //current user has sent a request to this user
          return response()->json([
            'status'=>'1',
            'message'=>'User fetched successfully',
            'buddy_status' => 'Pending buddyrequest',
            'user_details' => $userDetails,
          ], $this->successStatus);
        } else {
          return response()->json([
            'status'=>'1',
            'message'=>'User fetched successfully',
            'buddy_status' => 'Not buddies',
            'user_details' => $userDetails,
          ], $this->successStatus);
        }
      }
    }
  }

  /*
    * Contact Us query and logs
  */

  public function contactUs (Request $request) {
    $validator = \Validator::make($request->all(), [
      'name' => 'required|max:255',
      'email' => 'required|email|max:255',
      'subject' => 'required',
      'bodymessage' => 'required'
    ]);
    if ($validator->fails()) {
      return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
    }
    $name = $request->name;
    $email = $request->email;
    $title = $request->subject;
    $content = $request->bodymessage;

    \Mail::send('contactUsMail', ['name' => $name, 'email' => $email, 'title' => $title, 'content' => $content], function ($message) {
      $message->to('contact@communiski.com')->subject('Contact Us Email');
          });
    /*saving log in db*/
    $logData = [
      'name'=>$name,
      'email'=>$email,
      'subject'=>$title,
      'content'=>$content,
      'created_at'=>date('Y-m-d h:i:s'),
    ];
    $logs = Log::insert($logData);
    /*saving log in db*/
      return response()->json([
        'status'=>'1',
        'message'=>'Thanks for contacting us!'
      ], $this->successStatus);
  }

  /*
    * To Delete account permanently
  */

  public function deleteProfile(Request $request)
  {
    try{
      \App\User::where(['id'=> $request->existingUser->id])->delete();
      return response()->json([
        'status'=>'1',
        'message'=>'Account Deleted successfully '
      ], $this->successStatus);
    } catch(Exception $e){
      return response()->json([
        'status'=>'0',
        'message'=>'error'
        ], $this->badrequest);
    }
  }
}
