<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateSnowflakeAPIRequest;
use App\Http\Requests\API\UpdateSnowflakeAPIRequest;
use App\Models\Snowflake;
use App\Models\Rewards_point;
use App\Models\Spend_snowflake;
use App\Models\Group;
use App\Models\User;
use App\Models\Friend;
use App\Models\Group_member;
use App\Repositories\SnowflakeRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Validator;
use Response;

/**
 * Class SnowflakeController
 * @package App\Http\Controllers\API
 */

class SnowflakeAPIController extends AppBaseController
{
    /** @var  SnowflakeRepository */
    private $snowflakeRepository;
    public $unauthorised = 401;
    public $successStatus = 200;
    public $badrequest = 400;

    public function __construct(SnowflakeRepository $snowflakeRepo)
    {
        $this->snowflakeRepository = $snowflakeRepo;
    }



    public function snowflakes_history(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
            $validator = Validator::make($request->all(), [  
                'user_id' => 'required',
                'type' => 'required_without_all:user_id'    /*type is earn and spent*/
            ]);
            if ($validator->fails()) {
                return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);            
            }
            else{
                $totalSF = Snowflake::where(['user_id'=>$saveArray['user_id']])->where(['reward_status'=>'1'])->sum('snowflake_rewards');
                $availableSF = Rewards_point::where(['user_id'=>$saveArray['user_id']])->first();
                $avail_snowflakes = $availableSF['total'];
                $pendingSF = Snowflake::where(['user_id'=>$saveArray['user_id']])->where(['reward_status'=>'0'])->sum('snowflake_rewards');
                $historyArray = [];
                $spendhistoryArray = [];
                if($saveArray['user_id'] && $saveArray['type']){
                    $userid = $saveArray['user_id'];
                    $type = $saveArray['type'];
                    if($type == 'earn'){
                        $history = Snowflake::where(['user_id'=>$saveArray['user_id']])
                        ->where(['reward_status'=>'1'])
                        ->orderBy('id','DESC')
                        ->get();
                        foreach($history as $his){
                            if($his['reward_status'] == 1){
                                $reward = 'Confirmed';
                            }else{
                                $reward = 'Pending';
                            }
                            $userDetails = User::where(['id'=>$his['user_id']])->first();
                            $listing['user_id'] = $his['user_id'];
                            $listing['user_name'] = $userDetails['name'];
                            $listing['snowflake_rewards'] = $his['snowflake_rewards'];
                            $listing['reason'] = $his['reason'];
                            $listing['reward_status'] = $reward;
                            $listing['created_at'] = $his['created_at']->format('Y-m-d');
                            $historyArray[] = $listing;
                        }
                        return response()->json([
                            'status'=>'1',
                            'message'=>'Earn Snowflakes history fetched successfully',
                            'Available'=>$avail_snowflakes,
                            'Pending'=>$pendingSF,
                            'Lifetime'=>$totalSF,
                            'historyArray'=>$historyArray
                        ], $this->successStatus);
                    }elseif($type == 'spent'){
                        $history = Spend_snowflake::where(['user_id'=>$saveArray['user_id']])
                        ->orderBy('id','DESC')
                        ->get();
                        foreach($history as $his){
                            $userDetails = User::where(['id'=>$his['user_id']])->first();
                            $listing['user_id'] = $his['user_id'];
                            $listing['user_name'] = $userDetails['name'];
                            $listing['spend_snowflake'] = $his['spend_snowflake'];
                            $listing['reason'] = $his['reason'];
                            $listing['created_at'] = $his['created_at']->format('Y-m-d');
                            $historyArray[] = $listing;
                        }
                        return response()->json([
                            'status'=>'1',
                            'message'=>'Spent Snowflakes history fetched successfully',
                            'Available'=>$avail_snowflakes,
                            'Pending'=>$pendingSF,
                            'Lifetime'=>$totalSF,
                            'historyArray'=>$historyArray
                        ], $this->successStatus);
                    }
                }
                elseif($saveArray['user_id']){
                    $history = Snowflake::where(['user_id'=>$saveArray['user_id']])->orderBy('id','DESC')->get();
                    foreach($history as $his){
                        if($his['reward_status'] == 1){
                            $reward = 'Confirmed';
                        }else{
                            $reward = 'Pending';
                        }
                        $userDetails = User::where(['id'=>$his['user_id']])->first();
                        $listing['user_id'] = $his['user_id'];
                        $listing['user_name'] = $userDetails['name'];
                        $listing['snowflake_rewards'] = $his['snowflake_rewards'];
                        $listing['reason'] = $his['reason'];
                        $listing['reward_for'] = $his['reward_for']?$his['reward_for']:'';
                        $listing['reward_status'] = $reward;
                        $listing['created_at'] = $his['created_at']->format('Y-m-d');
                        $historyArray[] = $listing;
                    }
                    $spendhistory = Spend_snowflake::where(['user_id'=>$saveArray['user_id']])->get();
                        foreach($spendhistory as $his){
                            $userDetails = User::where(['id'=>$his['user_id']])->first();
                            $listing1['user_id'] = $his['user_id'];
                            $listing1['user_name'] = $userDetails['name'];
                            $listing1['spend_snowflake'] = $his['spend_snowflake'];
                            $listing1['reason'] = $his['reason'];
                            $listing1['created_at'] = $his['created_at']->format('Y-m-d');
                            $spendhistoryArray[] = $listing1;
                        }
                    return response()->json([
                        'status'=>'1',
                        'message'=>'Snowflakes history fetched successfully',
                        'Available'=>$avail_snowflakes,
                        'Pending'=>$pendingSF,
                        'Lifetime'=>$totalSF,
                        'historyArray'=>array_merge($historyArray,$spendhistoryArray)
                    ], $this->successStatus);
                }
            }
        }
        else{
            return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }


    /**
     * Display a listing of the Snowflake.
     * GET|HEAD /snowflakes
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $snowflakes = $this->snowflakeRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($snowflakes->toArray(), 'Snowflakes retrieved successfully');
    }

    /**
     * Store a newly created Snowflake in storage.
     * POST /snowflakes
     *
     * @param CreateSnowflakeAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateSnowflakeAPIRequest $request)
    {
        $input = $request->all();

        $snowflake = $this->snowflakeRepository->create($input);

        return $this->sendResponse($snowflake->toArray(), 'Snowflake saved successfully');
    }

    /**
     * Display the specified Snowflake.
     * GET|HEAD /snowflakes/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Snowflake $snowflake */
        $snowflake = $this->snowflakeRepository->find($id);

        if (empty($snowflake)) {
            return $this->sendError('Snowflake not found');
        }

        return $this->sendResponse($snowflake->toArray(), 'Snowflake retrieved successfully');
    }

    /**
     * Update the specified Snowflake in storage.
     * PUT/PATCH /snowflakes/{id}
     *
     * @param int $id
     * @param UpdateSnowflakeAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSnowflakeAPIRequest $request)
    {
        $input = $request->all();

        /** @var Snowflake $snowflake */
        $snowflake = $this->snowflakeRepository->find($id);

        if (empty($snowflake)) {
            return $this->sendError('Snowflake not found');
        }

        $snowflake = $this->snowflakeRepository->update($input, $id);

        return $this->sendResponse($snowflake->toArray(), 'Snowflake updated successfully');
    }

    /**
     * Remove the specified Snowflake from storage.
     * DELETE /snowflakes/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Snowflake $snowflake */
        $snowflake = $this->snowflakeRepository->find($id);

        if (empty($snowflake)) {
            return $this->sendError('Snowflake not found');
        }

        $snowflake->delete();

        return $this->sendResponse($id, 'Snowflake deleted successfully');
    }
}
