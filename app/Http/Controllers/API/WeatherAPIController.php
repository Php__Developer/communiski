<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateWeatherAPIRequest;
use App\Http\Requests\API\UpdateWeatherAPIRequest;
use App\Models\User;
use App\Models\Weather;
use App\Models\Weather_history;
use App\Models\Weather_report_day;
use App\Models\Weather_report_hour;
use App\Models\Friend;
use App\Models\Resort;
use App\Models\Snowflake;
use App\Models\Rewards_point;
use App\Models\Permission;
use App\Models\Access_code;
use App\Repositories\WeatherRepository;
use Illuminate\Http\Request;
use Validator;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class WeatherController
 * @package App\Http\Controllers\API
 */

class WeatherAPIController extends AppBaseController
{
    /** @var  WeatherRepository */
    private $weatherRepository;
    public $unauthorised = 401;
    public $successStatus = 200;
    public $badrequest = 400;

    public function __construct(WeatherRepository $weatherRepo)
    {
        $this->weatherRepository = $weatherRepo;
    }

    public function eighthourData(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
            $validator = Validator::make($request->all(), [
                'lat' => 'required',
                'long' => 'required',
                'type' => 'required',
              ]);
              if ($validator->fails()) {
                return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
              }
              else{
                $lat = $saveArray['lat'];
                $long = $saveArray['long'];
                $type = $saveArray['type'];
                $url ='https://api.aerisapi.com/forecasts/{'. $lat .' , '. $long .'}?format=json&filter=day&limit=8&client_id=T2T2vjYw61ScZ1JyMSASB&client_secret=YVxmh5048hOCFWry1mY2prDfWgt8IQIdl1ZgcWXJ';
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, false);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                //curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
                $result = curl_exec ($ch);
                curl_close ($ch);
                // print_r($result);die();
                $checkReport = Weather_report_day::where('user_id',$userExist['id'])->first();
                if ($checkReport) {
                    $updateReport = [
                        'latitude'=> $lat,
                        'longitude'=>$long,
                        'report'=>$result,
                        'type'=>$saveArray['type'],
                        'updated_at' => date('Y-m-d H:i:s')
                    ];
                    $update = Weather_report_day::where(['user_id'=>$userExist['id']])->update($updateReport);
                    if ($update) {
                    return response()->json([
                        'status'=>'1',
                        'message'=>'Report Updated Successfully'
                      ], $this->successStatus);
                }
                else{
                  return response()->json([
                        'status'=>'0',
                        'message'=>'Error on updating report'
                      ], $this->successStatus);
                }
                }else{
                    $insertReport = [
                        'user_id'=>$userExist['id'],
                        'latitude'=> $lat,
                        'longitude'=>$long,
                        'report'=>$result,
                        'type'=>$saveArray['type'],
                        'created_at'=>date('Y-m-d h:i:s'),
                    ];
                    $insert = Weather_report_day::insertGetId($insertReport);
                }
                if ($insert) {
                    return response()->json([
                        'status'=>'1',
                        'message'=>'Report saved Successfully'
                      ], $this->successStatus);
                }
                else{
                  return response()->json([
                        'status'=>'0',
                        'message'=>'Error on saving report'
                      ], $this->successStatus);
                }
            }
        }
        else{
            return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }

    public function getEightHour(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
            $validator = Validator::make($request->all(), [
                'type' => 'required',
              ]);
              if ($validator->fails()) {
                return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
              }
              else{
                $getData = Weather_report_day::where('user_id',$userExist['id'])
                ->where('type',$saveArray['type'])
                ->select('report')->first();
                print_r($getData['report']);
            }
        }else{
            return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }

    public function threehourData(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
            $validator = Validator::make($request->all(), [
                'lat' => 'required',
                'long' => 'required',
                'type' => 'required',
              ]);
              if ($validator->fails()) {
                return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
              }
              else{
                $lat = $saveArray['lat'];
                $long = $saveArray['long'];
                $url ='https://api.aerisapi.com/forecasts/{'. $lat .' , '. $long .'}?&format=json&filter=3hr&limit=8&client_id=T2T2vjYw61ScZ1JyMSASB&client_secret=YVxmh5048hOCFWry1mY2prDfWgt8IQIdl1ZgcWXJ';
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, false);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                //curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
                $result = curl_exec ($ch);
                curl_close ($ch);
                $checkReport = Weather_report_hour::where('user_id',$userExist['id'])->first();
                if ($checkReport) {
                    $updateReport = [
                        'latitude'=> $lat,
                        'longitude'=>$long,
                        'report'=>$result,
                        'type'=>$saveArray['type'],
                        'updated_at' => date('Y-m-d H:i:s')
                    ];
                    $update = Weather_report_hour::where(['user_id'=>$userExist['id']])->update($updateReport);
                    if ($update) {
                    return response()->json([
                        'status'=>'1',
                        'message'=>'Report Updated Successfully'
                      ], $this->successStatus);
                }
                else{
                  return response()->json([
                        'status'=>'0',
                        'message'=>'Error on updating report'
                      ], $this->successStatus);
                }
                }else{
                    $insertReport = [
                        'user_id'=>$userExist['id'],
                        'latitude'=> $lat,
                        'longitude'=>$long,
                        'report'=>$result,
                        'type'=>$saveArray['type'],
                        'created_at'=>date('Y-m-d h:i:s'),
                    ];
                    $insert = Weather_report_hour::insertGetId($insertReport);
                }
                if ($insert) {
                    return response()->json([
                        'status'=>'1',
                        'message'=>'Report saved Successfully'
                      ], $this->successStatus);
                }
                else{
                  return response()->json([
                        'status'=>'0',
                        'message'=>'Error on saving report'
                      ], $this->successStatus);
                }
            }
        }
        else{
            return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }

    public function getthreeHour(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
            $validator = Validator::make($request->all(), [
                'type' => 'required',
              ]);
              if ($validator->fails()) {
                return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
              }
              else{
                $getData = Weather_report_hour::where('user_id',$userExist['id'])
                ->where('type',$saveArray['type'])
                ->select('report')->first();
            print_r($getData['report']);
        }
        }else{
            return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }

    /**
     * Display a listing of the Weather.
     * GET|HEAD /weathers
     *
     * @param Request $request
     * @return Response
     */

    public function weatherReport(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
            $validator = Validator::make($request->all(), [
                'resort_id' => 'required',
                'report_date' => 'required',
                // 'new_snow_top' => 'required',
                // 'new_snow_bottom' => 'required',
                // 'base_depth_top' => 'required',
                // 'base_depth_bottom' => 'required',
                // 'on_piste' => 'required',
                // 'off_piste' => 'required',
                'official_report' => 'required',
            ]);
            if ($validator->fails()) {
                return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->successStatus);
            }
            else{
                $checkReport = Weather::where('resort_id',$saveArray['resort_id'])
                ->where('report_date',$saveArray['report_date'])
                ->first();
                $getDate = $saveArray['report_date'];
                $expldate = explode('-', $getDate);
                $month   = $expldate[1];
                $year  = $expldate[0];
                if ($checkReport) {
                    $updateReport = [
                        'official_report'=>$saveArray['official_report']?$saveArray['official_report']:$checkReport['official_report'],
                        'report_date'=>$saveArray['report_date']?$saveArray['report_date']:$checkReport['report_date'],
                        'new_snow_top'=>$saveArray['new_snow_top']?$saveArray['new_snow_top']:$checkReport['new_snow_top'],
                        'new_snow_bottom'=>$saveArray['new_snow_bottom']?$saveArray['new_snow_bottom']:$checkReport['new_snow_bottom'],
                        'base_depth_top'=>$saveArray['base_depth_top']?$saveArray['base_depth_top']:$checkReport['base_depth_top'],
                        'base_depth_bottom'=>$saveArray['base_depth_bottom']?$saveArray['base_depth_bottom']:$checkReport['base_depth_bottom'],
                        'unit'=>$saveArray['unit']?$saveArray['unit']:$checkReport['unit'],
                        'on_piste'=>$saveArray['on_piste']?$saveArray['on_piste']:$checkReport['on_piste'],
                        'off_piste'=>$saveArray['off_piste']?$saveArray['off_piste']:$checkReport['off_piste'],
                        'updated_at'=>date('Y-m-d h:i:s')
                    ];
                    /*giving rewards*/
                    $givingReward = $this->giveRewards($saveArray,$userExist);
                    /*giving rewards*/
                    $update = Weather::where('resort_id',$saveArray['resort_id'])
                    ->where('report_date',$saveArray['report_date'])
                    ->update($updateReport);

                    /*insert report history*/
                    $insertData = [
                        'official_report'=>$saveArray['official_report']?$saveArray['official_report']:'',
                        'user_id'=>$userExist['id'],
                        'resort_id'=>$saveArray['resort_id'],
                        'report_date'=>$saveArray['report_date'],
                        'new_snow_top'=>$saveArray['new_snow_top'],
                        'new_snow_bottom'=>$saveArray['new_snow_bottom'],
                        'base_depth_top'=>$saveArray['base_depth_top'],
                        'base_depth_bottom'=>$saveArray['base_depth_bottom'],
                        'unit'=>$saveArray['unit'],
                        'on_piste'=>$saveArray['on_piste'],
                        'off_piste'=>$saveArray['off_piste'],
                        'notes'=>$saveArray['notes'],
                        'month'=>$month,
                        'year'=>$year,
                        'created_at'=>date('Y-m-d h:i:s'),
                    ];
                    $reportID = Weather_history::insertGetId($insertData);
                    /*insert report history*/

                    if ($reportID) {
                        return response()->json([
                            'status'=>'1',
                            'message'=>'Weather updated succesfully',
                        ], $this->successStatus);
                    }else{
                        return response()->json([
                            'status'=>'0',
                            'message'=>'Weather not saved',
                        ], $this->badrequest);
                    }
                }else{
                    $insertData = [
                        'official_report'=>$saveArray['official_report']?$saveArray['official_report']:'',
                        'resort_id'=>$saveArray['resort_id'],
                        'report_date'=>$saveArray['report_date'],
                        'new_snow_top'=>$saveArray['new_snow_top'],
                        'new_snow_bottom'=>$saveArray['new_snow_bottom'],
                        'base_depth_top'=>$saveArray['base_depth_top'],
                        'base_depth_bottom'=>$saveArray['base_depth_bottom'],
                        'unit'=>$saveArray['unit'],
                        'on_piste'=>$saveArray['on_piste'],
                        'off_piste'=>$saveArray['off_piste'],
                        'created_at'=>date('Y-m-d h:i:s'),
                    ];
                    /*giving rewards*/
                    $givingReward = $this->giveRewards($saveArray,$userExist);
                    /*giving rewards*/
                    $reportID = Weather::insertGetId($insertData);

                    /*insert report history*/
                    $insertData = [
                        'user_id'=>$userExist['id'],
                        'resort_id'=>$saveArray['resort_id'],
                        'report_date'=>$saveArray['report_date'],
                        'new_snow_top'=>$saveArray['new_snow_top'],
                        'new_snow_bottom'=>$saveArray['new_snow_bottom'],
                        'base_depth_top'=>$saveArray['base_depth_top'],
                        'base_depth_bottom'=>$saveArray['base_depth_bottom'],
                        'unit'=>$saveArray['unit'],
                        'on_piste'=>$saveArray['on_piste'],
                        'off_piste'=>$saveArray['off_piste'],
                        'notes'=>$saveArray['notes'],
                        'month'=>$month,
                        'year'=>$year,
                        'created_at'=>date('Y-m-d h:i:s'),
                    ];
                    $reportID = Weather_history::insertGetId($insertData);
                    /*insert report history*/

                    if ($reportID) {
                        return response()->json([
                            'status'=>'1',
                            'message'=>'Weather saved succesfully',
                        ], $this->successStatus);
                    }else{
                        return response()->json([
                            'status'=>'0',
                            'message'=>'Weather not saved',
                        ], $this->badrequest);
                    }
                }
            }
        }
        else{
            return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }

    public function giveRewards ($request,$userExist)
    {
        $saveArray = $request;
        $getUpdatedValue = (array_filter($saveArray));
        $reportDate = $saveArray['report_date'];
        $filteredcols = array_except($getUpdatedValue, ['resort_id','official_report','report_date','unit','notes']);
        // echo "newly request";
        // print_r($filteredcols);
        $count = count($filteredcols);
        $checkRecord = Weather_history::where('user_id',$userExist['id'])
        ->where('resort_id',$saveArray['resort_id'])
        ->where('report_date',$saveArray['report_date'])
        ->orderBy('created_at', 'desc')
        ->take(1)
        ->first();
        if ($checkRecord) {
            $checkoldupdatedCols = Weather_history::where('resort_id',$saveArray['resort_id'])
            ->where('user_id',$userExist['id'])
            ->where('report_date',$saveArray['report_date'])
            ->select('new_snow_top','new_snow_bottom','base_depth_top','base_depth_bottom','on_piste','off_piste')
            ->orderBy('created_at', 'DESC')
            ->first()->toArray();
            // echo "old record";
            // print_r($checkoldupdatedCols);
            $emptyvar = array_intersect($checkoldupdatedCols, ['']);
            // echo "Empty roecords";
            // print_r($emptyvar);
            $intersectKeys = array_intersect_key($filteredcols,$emptyvar);
            // echo "intersect Keys";
            // print_r($intersectKeys);
            $getsnowcount = count($intersectKeys);
            // echo "count";
            // print_r($getsnowcount);die();
            // echo "null_records by user";
            // $getNullrecord = in_array('Null', $checkoldupdatedCols);
            // print_r($getNullrecord);die();
            /*$emptyKeys = array_diff($checkoldupdatedCols, $filteredcols);
            echo "emptyKeys";
            print_r(json_encode($emptyKeys));
            $intersectKeys = array_intersect_key($filteredcols,$emptyKeys);
            echo "intersect Keys";
            print_r(json_encode($intersectKeys));
            $getsnowcount = count($intersectKeys);
            echo "count";
            print_r($getsnowcount);die();*/
            if ($getsnowcount > 0) {
                $snow_snowflakes = $getsnowcount * 5;
                $resortName = Resort::where('id',$saveArray['resort_id'])->select('resort_name')->first();
                $concateReason = 'Submit Report for Snow History at '.$resortName->resort_name.' on date : '.$reportDate;
                $insertFlakes = [
                    'user_id' => $userExist['id'],
                    'resort_id' => $saveArray['resort_id'],
                    'reward_for' => 'Snow history',
                    'snowflake_rewards' => $snow_snowflakes,
                    'reward_status' => '1',
                    'reason' => $concateReason,
                    'created_at' => date('Y-m-d H:i:s')
                ];
                $insert = Snowflake::insert($insertFlakes);
                $getRewards = Rewards_point::where('user_id',$userExist['id'])->select('total')->first();
                $rewardedPoint = $getRewards['total'] + $snow_snowflakes;
                $updaterewardData = [
                    'total'=> $rewardedPoint,
                    'updated_at'=>date('Y-m-d h:i:s'),
                ];
                $update = Rewards_point::where(['user_id'=>$userExist['id']])->update($updaterewardData);
            }
        }else{
            $snowcount = $count * 5;
             $resortName = Resort::where('id',$saveArray['resort_id'])->select('resort_name')->first();
                $concateReason = 'Submit Report for Snow History at '.$resortName->resort_name.' on date : '.$reportDate;
            $insertFlakes = [
                'user_id' => $userExist['id'],
                'resort_id' => $saveArray['resort_id'],
                'reward_for' => 'Snow history',
                'snowflake_rewards' => $snowcount,
                'reward_status' => '1',
                'reason' => $concateReason,
                'created_at' => date('Y-m-d H:i:s')
            ];
            $insert = Snowflake::insert($insertFlakes);
            $getRewards = Rewards_point::where('user_id',$userExist['id'])->select('total')->first();
                $rewardedPoint = $getRewards['total'] + $snowcount;
                $updaterewardData = [
                    'total'=> $rewardedPoint,
                    'updated_at'=>date('Y-m-d h:i:s'),
                ];
                $update = Rewards_point::where(['user_id'=>$userExist['id']])->update($updaterewardData);
        }
    }

    public function getcurrentReport(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
            $validator = Validator::make($request->all(), [
                'resort_id' => 'required',
            ]);
            if ($validator->fails()) {
                return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->successStatus);
            }
            else{
                if ($saveArray['date'] != '') {
                    $getreport = Weather::where('resort_id',$saveArray['resort_id'])
                    ->where('report_date',$saveArray['date'])
                    ->first();
                    if ($getreport) {
                        $dateWiseReport = array(
                        'id'=>(String)$getreport['id'],
                        'official_report'=>$getreport['official_report'],
                        'report_date'=>$getreport['report_date']->format('Y-m-d'),
                        'new_snow_top'=>(String)$getreport['new_snow_top'],
                        'new_snow_bottom'=>(String)$getreport['new_snow_bottom'],
                        'base_depth_top'=>(String)$getreport['base_depth_top'],
                        'base_depth_bottom'=>(String)$getreport['base_depth_bottom'],
                        'unit'=>$getreport['unit'],
                        'on_piste'=>$getreport['on_piste']?$getreport['on_piste']:'',
                        'off_piste'=>$getreport['off_piste']?$getreport['off_piste']:'',
                        'created_at'=>$getreport['created_at']->format('Y-m-d H:i:s'),
                    );
                    return response()->json([
                        'status'=>'1',
                        'message'=>'Report fetched successfully',
                        'report'=>$dateWiseReport,
                      ], $this->successStatus);
                    }else{
                        return response()->json([
                        'status'=>'1',
                        'report'=>'',
                      ], $this->successStatus);
                    }
                }else{
                    $today = date('Y-m-d');
                    $lastseven = date('Y-m-d', strtotime('-7 days'));
                    $resortData = Weather::where('resort_id',$saveArray['resort_id'])
                    ->whereBetween('report_date', [$lastseven, $today])->first();
                   if ($resortData) {
                    //Last Snow with date and unit
                    //24 hour update
                    //48 hour update
                    //72 hour update
                    //on piste / off piste only current or last : with date month
                    //Base depth only current or last

                    /*last record date wise*/
                    $currentRecord = Weather::where('resort_id',$saveArray['resort_id'])
                    ->orderBy('report_date', 'desc')
                    ->take(1)
                    ->first();
                    if ($currentRecord) {
                        $lastRecord = $currentRecord;
                    }else{
                    $last_record = Weather::where('resort_id',$saveArray['resort_id'])
                    ->orderBy('report_date', 'desc')
                    ->skip(1)
                    ->take(1)->first();
                    $lastRecord = $last_record;
                    }
                    $lastRecord = array(
                        'id'=>(String)$lastRecord['id'],
                        'report_date'=>$lastRecord['report_date']->format('Y-m-d'),
                        'new_snow_top'=>(String)$lastRecord['new_snow_top'],
                        'new_snow_bottom'=>(String)$lastRecord['new_snow_bottom'],
                        'unit'=>$lastRecord['unit'],
                        'created_at'=>$lastRecord['created_at']->format('Y-m-d H:i:s'),
                    );
                    /*last record date wise*/

                    /*24 hour update*/
                    $currentdate = new \DateTime();
                    $formatted_currentDay = $currentdate->format('Y-m-d');
                    
                    #lastday
                    $currentdate1 = new \DateTime();
                    $lastday = $currentdate1->modify('-24 hours');
                    $formatted_lastDay = $lastday->format('Y-m-d');
                    $gettwentyfour1 = Weather::where('resort_id',$saveArray['resort_id'])
                    ->where('report_date',$formatted_currentDay)
                    ->first();
                    $gettwentyfour2 = Weather::where('resort_id',$saveArray['resort_id'])
                    ->where('report_date',$formatted_lastDay)
                    ->first();

                    $currentReport = json_decode(json_encode($gettwentyfour1), true);
                    $lastReport = json_decode(json_encode($gettwentyfour2), true);

                    if ($currentReport) {
                        $new_snow_top_current_top = $currentReport['new_snow_top'];
                        $new_snow_top_current_bottom = $currentReport['new_snow_bottom'];
                        $current_unit = $currentReport['unit'];
                    }else{
                        $new_snow_top_current_top = 0;
                        $new_snow_top_current_bottom = 0;
                        $current_unit = '';
                    }
                    if ($lastReport) {
                        $new_snow_top_last_top = $lastReport['new_snow_top'];
                        $new_snow_top_last_bottom = $lastReport['new_snow_bottom'];
                        $last_unit = $lastReport['unit'];
                    }else{
                        $new_snow_top_last_top = 0;
                        $new_snow_top_last_bottom = 0;
                        $last_unit ='';
                    }

                    $newSnowTop = $new_snow_top_current_top + $new_snow_top_last_top;
                    $newSnowbottom = $new_snow_top_current_bottom + $new_snow_top_last_bottom;

                    if ($current_unit != '') {
                        $twenty_unit = $current_unit;
                    }
                    elseif($last_unit != '') {
                        $twenty_unit = $last_unit;
                    }elseif($last_unit == '' && $current_unit == ''){
                        $twenty_unit = '';
                    }
                    $twentyfourArray = array(
                        'new_snow_top'=>$newSnowTop,
                        'new_snow_bottom'=>$newSnowbottom,
                        'unit'=>$twenty_unit?$twenty_unit:'',
                    );
                    /*24 hour update*/

                    /*48 hour update*/
                    $currentdate1 = new \DateTime();
                    $lastfourtyeight = $currentdate1->modify('-48 hours');
                    $formatted_fourtyeight = $lastfourtyeight->format('Y-m-d');
                    $fourtyEight = Weather::where('resort_id',$saveArray['resort_id'])
                    ->where('report_date',$formatted_fourtyeight)
                    ->first();
                    $newSnowTop_fourty = $fourtyEight['new_snow_top'] + $newSnowTop;
                    $newSnowBottom_fouty = $fourtyEight['new_snow_bottom'] + $newSnowbottom;

                    $funit = $fourtyEight['unit']?$fourtyEight['unit']:$twenty_unit;

                    $fourtyeightArray = array(
                        'new_snow_top'=>$newSnowTop_fourty,
                        'new_snow_bottom'=>$newSnowBottom_fouty,
                        'unit'=>$funit?$funit:$twenty_unit,
                    );
                    /*48 hour update*/

                    /*72 hour update*/
                    $currentdate2 = new \DateTime();
                    $lastseventyEight = $currentdate2->modify('-72 hours');
                    $formatted_seventyEight = $lastseventyEight->format('Y-m-d');
                    $seventyEight = Weather::where('resort_id',$saveArray['resort_id'])
                    ->where('report_date',$formatted_seventyEight)
                    ->first();
                    $newSnowTop_seventy = $seventyEight['new_snow_top'] + $newSnowTop_fourty;
                    $newSnowBottom_seventy = $seventyEight['new_snow_bottom'] + $newSnowBottom_fouty;
                    $sunit = $seventyEight['unit']?$seventyEight['unit']:$funit;
                    $seventytwoArray = array(
                        'new_snow_top'=>$newSnowTop_seventy,
                        'new_snow_bottom'=>$newSnowBottom_seventy,
                        'unit'=>$sunit,
                    );
                    /*72 hour update*/

                    /*base depth top and bottom resort wise from all records*/
                    $today = date('Y-m-d');
                    $lastseven = date('Y-m-d', strtotime('-6 days'));
                    $top_base = Weather::where('resort_id',$saveArray['resort_id'])
                    ->whereBetween('report_date', [$lastseven, $today])
                    ->where('base_depth_top','!=','Null')
                    ->orwhere('base_depth_bottom','!=','Null')
                    ->whereBetween('report_date', [$lastseven, $today])
                    ->where('resort_id',$saveArray['resort_id'])
                    ->orderBy('report_date', 'desc')
                    ->select('official_report','resort_id','report_date','base_depth_top','base_depth_bottom','unit')
                    ->first();
                    // print_r($top_base);die();
                    $top_depth_base = $top_base->base_depth_top;
                    $botton_depth_base = $top_base->base_depth_bottom;
                    $depthArray = array(
                        'top_depth_base'=>$top_depth_base?$top_depth_base:'',
                        'botton_depth_base'=>$botton_depth_base?$botton_depth_base:'',
                        'unit'=>$top_base['unit']?$top_base['unit']:'',
                    );
                    /*base depth top and bottom resort wise from all records*/

                    /*on piste and off piste only current*/
                    $currentonPiste = Weather_history::where('resort_id',$saveArray['resort_id'])
                    ->whereBetween('report_date', [$lastseven, $today])
                    ->where('on_piste','!=','Null')
                    ->orderBy('created_at', 'desc')
                    ->select('report_date','on_piste','created_at')
                    ->first();
                    $onReport = json_decode(json_encode($currentonPiste), true);
                    // print_r($onReport);
                    $currentoffPiste = Weather_history::where('resort_id',$saveArray['resort_id'])
                    ->whereBetween('report_date', [$lastseven, $today])
                    ->where('off_piste','!=','Null')
                    ->orderBy('created_at', 'desc')
                    ->select('report_date','off_piste','created_at')
                    ->first();
                    $offReport = json_decode(json_encode($currentoffPiste), true);
                    // print_r($offReport);die();
                    $on_piste = $onReport['on_piste'];
                    $on_piste_date = $onReport['report_date'];
                    $off_piste = $offReport['off_piste'];
                    $off_piste_date = $offReport['report_date'];
                    $pisteArray = array(
                        'on_piste'=>$on_piste?$on_piste:'',
                        'on_piste_date'=>$on_piste_date?$on_piste_date:'',
                        'off_piste'=>$off_piste?$off_piste:'',
                        'off_piste_date'=>$off_piste_date?$off_piste_date:'',
                        // 'date'=>$piste_date->format('Y-m-d'),
                    );
                    /*on piste and off piste only current*/

                    /*last snow*/
                    $lastSnow = Weather::where('resort_id',$saveArray['resort_id'])
                    ->where('new_snow_top','!=','Null')
                    ->orwhere('new_snow_bottom','!=','Null')
                    ->where('resort_id',$saveArray['resort_id'])
                    ->orderBy('report_date', 'desc')
                    ->first();
                    if ($lastSnow != '') {
                        $lastSnowArray = array(
                            'id'=>(String)$lastSnow['id'],
                            'report_date'=>$lastSnow['report_date']->format('Y-m-d'),
                            'new_snow_top'=>(String)$lastSnow['new_snow_top'],
                            'new_snow_bottom'=>(String)$lastSnow['new_snow_bottom'],
                            'unit'=>$lastSnow['unit'],
                            'created_at'=>$lastSnow['created_at']->format('Y-m-d H:i:s'),
                        );
                    }
                    else{
                        $lastSnowArray = array(
                            'id'=> '',
                            'report_date'=>'',
                            'new_snow_top'=>'',
                            'new_snow_bottom'=>'',
                            'unit'=>'',
                            'created_at'=>'',
                        );
                    }
                    /*last snow*/
                    return response()->json([
                        'status'=>'1',
                        'message'=>'Reports fetched successfully',
                        //'lastRecord'=>$lastRecord,
                        'twentyfourArray'=>$twentyfourArray,
                        'fourtyeightArray'=>$fourtyeightArray,
                        'seventytwoArray'=>$seventytwoArray,
                        'depthArray'=>$depthArray,
                        'pisteArray'=>$pisteArray,
                        'lastSnowArray'=>$lastSnowArray,
                      ], $this->successStatus);
                   }else{
                    return response()->json([
                        'status'=>'1',
                        'message'=>'No Data',
                        'twentyfourArray'=>'',
                        'fourtyeightArray'=>'',
                        'seventytwoArray'=>'',
                        'depthArray'=>'',
                        'pisteArray'=>'',
                        'lastSnowArray'=>'',
                    ], $this->successStatus);
                   }
                }
            }
        }else{
            return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }

    public function getSnowHistory(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
            $validator = Validator::make($request->all(), [
                'resort_id' => 'required',
                'month' => 'required_with_all:resort_id,year',
                'year' => 'required_with_all:resort_id,month',
            ]);
            if ($validator->fails()) {
                return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->successStatus);
            }
            else{
                if ($saveArray['resort_id'] && !$saveArray['month'] && !$saveArray['year'])
                {
                    $cMonthHistory = [];
                    $currentMonth = date('m');
                    $getcurrentReport1 = Weather::whereRaw('MONTH(report_date) = ?',[$currentMonth])
                    ->where('resort_id',$saveArray['resort_id'])
                    ->orderBy('report_date','ASC')
                    ->get();
                    $getcurrentReport = json_decode(json_encode($getcurrentReport1), true);
                    if(count($getcurrentReport)){
                        foreach ($getcurrentReport as $current) {
                            $reportHistory = [];
                            $report_history1 = Weather_history::where('report_date',$current['report_date'])->where('resort_id',$saveArray['resort_id'])
                            ->orderBy('created_at','DESC')
                            ->get();
                            $report_history = json_decode(json_encode($report_history1), true);
                            foreach ($report_history as $history) {
                                $username = User::where('id',$history['user_id'])->first();
                                $hist['report_date'] = $history['report_date'];
                                $hist['official_report'] = $history['official_report'];
                                $hist['user_id'] = $history['user_id'];
                                $hist['username'] = $username['name'];
                                $hist['resort_id'] = $history['resort_id'];
                                $hist['new_snow_top'] = $history['new_snow_top'];
                                $hist['new_snow_bottom'] = $history['new_snow_bottom'];
                                $hist['base_depth_top'] = $history['base_depth_top'];
                                $hist['base_depth_bottom'] = $history['base_depth_bottom'];
                                $hist['on_piste'] = $history['on_piste']?$history['on_piste']:'';
                                $hist['off_piste'] = $history['off_piste']?$history['off_piste']:'';
                                $hist['notes'] = $history['notes']?$history['notes']:'';
                                $reportHistory[] = $hist;
                            }
                            $month['report_date'] = $current['report_date'];
                            $month['id'] = $current['id'];
                            $month['resort_id'] = $current['resort_id'];
                            $month['official_report'] = $current['official_report'];
                            $month['new_snow_top'] = $current['new_snow_top'];
                            $month['new_snow_bottom'] = $current['new_snow_bottom'];
                            $month['base_depth_top'] = $current['base_depth_top'];
                            $month['base_depth_bottom'] = $current['base_depth_bottom'];
                            $month['unit'] = $current['unit']?$current['unit']:'';
                            $month['on_piste'] = $current['on_piste']?$current['on_piste']:'';
                            $month['off_piste'] = $current['off_piste']?$current['off_piste']:'';
                            // $month['notes'] = $current['notes']?$current['notes']:'';
                            $month['historyArray'] = $reportHistory;
                            $cMonthHistory[] = $month;
                        }
                        return response()->json([
                            'status'=>'1',
                            'message'=>'data fetched successfully',
                            'monthlyArray'=>$cMonthHistory
                        ], $this->successStatus);
                    }else{
                        return response()->json([
                            'status'=>'1',
                            'message'=>'No data yet',
                            'monthlyArray'=> []
                        ], $this->successStatus);
                    }
                }
                elseif ($saveArray['resort_id'] && $saveArray['month'] && $saveArray['year']) 
                {
                    $cMonthHistory = [];
                    $getMonth = $saveArray['month'];
                    $getYear = $saveArray['year'];
                    $getcurrentReport1 = Weather::whereRaw('MONTH(report_date) = ?',[$getMonth])
                    ->whereRaw('YEAR(report_date) = ?',[$getYear])
                    ->where('resort_id',$saveArray['resort_id'])
                    ->orderBy('report_date','ASC')
                    ->get();
                    $getcurrentReport = json_decode(json_encode($getcurrentReport1), true);
                    // print_r(json_encode($getcurrentReport));die();
                    if (count($getcurrentReport)) {
                        foreach ($getcurrentReport as $current) {
                        $reportHistory = [];
                        $report_history1 = Weather_history::where('report_date',$current['report_date'])
                        ->where('resort_id',$saveArray['resort_id'])
                        ->get();
                        $report_history = json_decode(json_encode($report_history1), true);
                        // print_r(json_encode($report_history));die();
                        foreach ($report_history as $history) {
                            $username = User::where('id',$history['user_id'])->first();
                            $hist['report_date'] = $history['report_date'];
                            $hist['official_report'] = (String)$history['official_report'];
                            $hist['user_id'] = $history['user_id'];
                            $hist['username'] = $username['name'];
                            $hist['resort_id'] = $history['resort_id'];
                            $hist['new_snow_top'] = $history['new_snow_top'];
                            $hist['new_snow_bottom'] = $history['new_snow_bottom'];
                            $hist['base_depth_top'] = $history['base_depth_top'];
                            $hist['base_depth_bottom'] = $history['base_depth_bottom'];
                            $hist['on_piste'] = $history['on_piste']?$history['on_piste']:'';
                            $hist['off_piste'] = $history['off_piste']?$history['off_piste']:'';
                            $hist['notes'] = $history['notes']?$history['notes']:'';
                            $reportHistory[] = $hist;
                        }
                        $month['report_date'] = $current['report_date'];
                        $month['id'] = $current['id'];
                        $month['resort_id'] = $current['resort_id'];
                        $month['official_report'] = $current['official_report'];
                        $month['new_snow_top'] = $current['new_snow_top'];
                        $month['new_snow_bottom'] = $current['new_snow_bottom'];
                        $month['base_depth_top'] = $current['base_depth_top'];
                        $month['base_depth_bottom'] = $current['base_depth_bottom'];
                        $month['unit'] = $current['unit']?$current['unit']:'';
                        $month['on_piste'] = $current['on_piste']?$current['on_piste']:'';
                        $month['off_piste'] = $current['off_piste']?$current['off_piste']:'';
                        // $month['notes'] = $current['notes']?$current['notes']:'';
                        $month['historyArray'] = $reportHistory;
                        $cMonthHistory[] = $month;
                    }
                    return response()->json([
                        'status'=>'1',
                        'message'=>'data fetched successfully',
                        'monthlyYearlyArray'=>$cMonthHistory
                    ], $this->successStatus);
                    }else{
                        return response()->json([
                        'status'=>'1',
                        'message'=>'No reports yet',
                        'monthlyYearlyArray'=>[]
                    ], $this->successStatus);
                    }
                }
            }
        }else{
            return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }

    public function index(Request $request)
    {
        $weathers = $this->weatherRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($weathers->toArray(), 'Weathers retrieved successfully');
    }

    /**
     * Store a newly created Weather in storage.
     * POST /weathers
     *
     * @param CreateWeatherAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateWeatherAPIRequest $request)
    {
        $input = $request->all();

        $weather = $this->weatherRepository->create($input);

        return $this->sendResponse($weather->toArray(), 'Weather saved successfully');
    }

    /**
     * Display the specified Weather.
     * GET|HEAD /weathers/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Weather $weather */
        $weather = $this->weatherRepository->find($id);

        if (empty($weather)) {
            return $this->sendError('Weather not found');
        }

        return $this->sendResponse($weather->toArray(), 'Weather retrieved successfully');
    }

    /**
     * Update the specified Weather in storage.
     * PUT/PATCH /weathers/{id}
     *
     * @param int $id
     * @param UpdateWeatherAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateWeatherAPIRequest $request)
    {
        $input = $request->all();

        /** @var Weather $weather */
        $weather = $this->weatherRepository->find($id);

        if (empty($weather)) {
            return $this->sendError('Weather not found');
        }

        $weather = $this->weatherRepository->update($input, $id);

        return $this->sendResponse($weather->toArray(), 'Weather updated successfully');
    }

    /**
     * Remove the specified Weather from storage.
     * DELETE /weathers/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Weather $weather */
        $weather = $this->weatherRepository->find($id);

        if (empty($weather)) {
            return $this->sendError('Weather not found');
        }

        $weather->delete();

        return $this->sendSuccess('Weather deleted successfully');
    }
}
