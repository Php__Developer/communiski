<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateGroupAPIRequest;
use App\Http\Requests\API\UpdateGroupAPIRequest;
use App\Models\Group;
use App\Models\User;
use App\Models\Friend;
use App\Models\Group_member;
use App\Repositories\GroupRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Carbon\Carbon;
use Validator;
use Response;

/**
 * Class GroupController
 * @package App\Http\Controllers\API
 */

/*
  * Group API Controller is for APIs related with Groups
*/

class GroupAPIController extends AppBaseController
{
    /** @var  GroupRepository */
    private $groupRepository;
    public $unauthorised = 401;
    public $successStatus = 200;
    public $badrequest = 400;

    public function __construct(GroupRepository $groupRepo)
    {
        $this->groupRepository = $groupRepo;
    }

    /**
     * Display a listing of the Group.
     * GET|HEAD /groups
     *
     * @param Request $request
     * @return Response
     */

    /*
      * Create Group API
    */

    public function createGroup(Request $request)
    {
      $saveArray = $request->all();
      $token = $request->header('token');
      $userExist = User::where(['remember_token'=>$token])->first();
      if($userExist){
        $validator = Validator::make($request->all(), [
          'location_resort_id' => 'required',
          'location' => 'required',
          'date' => 'required', 
          'info' => 'required',
          'privacy' => 'required',
        ]);
        if ($validator->fails()) {
          return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
        }
        else{
          $location = $saveArray['location'];
          $location_resort_id = $saveArray['location_resort_id'];
          $convertdate = $saveArray['date'];
          $info = $saveArray['info'];
          $privacy = $saveArray['privacy'];
          $date = str_replace('/', '-', $convertdate);
          $getdate = date('Y-m-d', strtotime($date));
          $insertData = [
            'group_location'=>$location,
            'location_resort_id'=>$location_resort_id,
            'group_date'=>$getdate,
            'group_info'=>$info,
            'privacy_status'=>$privacy,
            'group_image'=> 'groupdefault.jpg',
            'group_owner_id'=> $userExist->id,
            'total_users'=>1,
            'created_at'=>date('Y-m-d h:i:s'),
          ];
          $group_id = Group::insertGetId($insertData);
          $insertgrpData = [
            'group_id'=>$group_id,
            'message'=>'Admin',
            'group_location'=>$location,
            'location_resort_id'=>$saveArray['location_resort_id'],
            'user_id'=>$userExist['id'],
            'request_status'=>'accepted',
            'created_at'=>$getdate,
          ];
          $group = Group_member::insert($insertgrpData);
          if($group_id){
            return response()->json([
              'status'=>'1','message'=>'Group Created Successfully','group_id'=>(String)$group_id], $this->successStatus);
          }
          else{
            return response()->json(['status'=>'0','message'=>'Group Creation Failed'], $this->badrequest);
          }
        }
      }
      else{
        return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
      }
    }


    /*
      * Join Group API
    */


    public function joinGroup(Request $request)
    {
      $saveArray = $request->all();
      $token = $request->header('token');
      $userExist = User::where(['remember_token'=>$token])->first();
      if($userExist){
        $validator = Validator::make($request->all(), [
          'group_id' => 'required',
          'message' => 'required', 
        ]);
        if ($validator->fails()) {
          return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
        }
        else{
          $sender_id = $userExist['id'];
          $group_id = $saveArray['group_id'];
          $status = 1;
          $requestDetail = Group_member::where(['user_id'=>$sender_id])->where(['group_id'=>$group_id])->where(['request_status'=>'sent'])->first();
          if($requestDetail){
            return response()->json([
              'status'=>'0',
              'message'=>'Request Already Sent'
            ], $this->successStatus);
          }
          else{
            $group_id = $saveArray['group_id'];
            $message = $saveArray['message'];
            $location = Group::where(['id'=>$group_id])->select('group_location','location_resort_id')->first();
            $group_location = $location['group_location'];
            $location_resort_id = $location['location_resort_id'];
            $insertData = [
              'group_id'=>$group_id,
              'group_location'=> $group_location,
              'location_resort_id'=> $location_resort_id,
              'message'=>$message,
              'user_id'=>$userExist['id'],
              'created_at'=>date('Y-m-d h:i:s'),
            ];
            $user_id = Group_member::insertGetId($insertData);
            $requestStatus = Group_member::where(['id'=>$user_id])->first();
            if($user_id){
              return response()->json([
                'status'=>'1',
                'message'=>'Request sent Successfully',
                'group_id'=>$group_id,
                'request_status'=>$requestStatus->request_status,
              ], $this->successStatus);
            }
          }
        }
      }
      else{
        return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
      }
    }

    /*
      * Group Info API
    */

    public function groupInfo(Request $request)
    {
      $saveArray = $request->all();
      $token = $request->header('token');
      $userExist = User::where(['remember_token'=>$token])->first();
      if($userExist){
        $validator = Validator::make($request->all(), [
          'group_id' => 'required',
        ]);
        if ($validator->fails()) {
          return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->successStatus);
        }
        else{
          $checkUser = Group_member::where(['group_id'=>$saveArray['group_id']])->where(['user_id'=>$userExist['id']])->where(['request_status'=>'accepted'])->first();
          if ($checkUser) {
            $groupDetail = Group::where(['id'=>$saveArray['group_id']])->first();
            $grpOwner = User::where(['id'=>$groupDetail['group_owner_id']])->select('name','image')->first();
            $image = url('/images',$grpOwner['image']);
            $outgoingRequests = [];
            $record = [];
            $groupmemberDetail = Group_member::where(['group_id'=> $groupDetail['id']])
            ->where(['request_status'=>'accepted'])
            ->where('user_id','!=',$groupDetail['group_owner_id'])
            ->select('user_id')->orderBy('id','DESC')->get();
            foreach($groupmemberDetail as $ls){
              $userDetails = User::where(['id'=>$ls['user_id']])->first();
              $listing['user_id'] = $ls['user_id'];
              $listing['user_name'] = $userDetails['name'];
              $listing['user_image'] = url('/images',$userDetails['image']);
              $listing['skier'] = $userDetails['skier'];
              $listing['skier_sky_level'] = $userDetails['skier_sky_level'];
              $listing['skier_fav_tarrian'] = $userDetails['skier_fav_tarrian'];
              $listing['snowboarder'] = $userDetails['snowboarder'];
              $listing['snowboarder_sky_level'] = $userDetails['snowboarder_sky_level'];
              $listing['snowboarder_fav_tarrian'] = $userDetails['snowboarder_fav_tarrian'];
              $record[] = $listing;
            }
            if($groupDetail['group_owner_id'] == $userExist['id']){
              $grprqstDetail = Group_member::where(['group_id'=> $groupDetail['id']])->where(['request_status'=>'sent'])->select('user_id','message')->orderBy('id','DESC')->get();
              foreach($grprqstDetail as $ls1){
                $userDetails = User::where(['id'=>$ls1['user_id']])->first();
                $listing1['user_id'] = $userDetails['id'];
                $listing1['user_name'] = $userDetails['name'];
                $listing1['message'] = $ls1['message'];
                $listing1['user_image'] = url('/images',$userDetails['image']);
                $listing1['skier'] = $userDetails['skier'];
                $listing1['skier_sky_level'] = $userDetails['skier_sky_level'];
                $listing1['snowboarder'] = $userDetails['snowboarder'];
                $listing1['snowboarder_sky_level'] = $userDetails['snowboarder_sky_level'];
                $outgoingRequests[] = $listing1;
              }
            }
            else{
              $outgoingRequests = [];
            }
            $memberCount = Group_member::where(['group_id'=> $saveArray['group_id']])
            ->where(['request_status'=>'accepted'])
            ->where('user_id','!=',$groupDetail['group_owner_id'])
            ->select('user_id')->get();
            $checktokenUser = Group_member::where(['group_id'=> $saveArray['group_id']])->where(['user_id'=> $userExist['id']])->where(['request_status'=>'accepted'])->first();
            if($checktokenUser){
              $isAdded = 'Yes';
            }else{
              $isAdded = 'No';
            }
            $group_details = array(
              'is_AddedUser'=> $isAdded,
              'total_members'=> count($memberCount),
              'group_id'=>(String)$groupDetail['id'],
              'group_location'=>$groupDetail['group_location'],
              'group_name'=>$groupDetail['group_name'],
              'group_owner'=>$grpOwner['name'],
              'group_owner_id'=>$groupDetail['group_owner_id'],
              'group_info'=>$groupDetail['group_info'],
              'group_date'=>$groupDetail['group_date'],
              'privacy_status'=>$groupDetail['privacy_status'],
              'image'=>$image?$image:'',
              'created_at'=>$groupDetail['created_at'],
              'outgoingRequests'=>$outgoingRequests,
              'group_members'=>$record,
            );
            return response()->json([
              'status'=>'1',
              'message'=>'Details fetched successfully',
              'group_details'=>$group_details,
            ], $this->successStatus);
          }
          else{
            $checktokenUser = Group_member::where(['group_id'=> $saveArray['group_id']])->where(['user_id'=> $userExist['id']])->where(['request_status'=>'accepted'])->first();
            if($checktokenUser){
              $isAdded = 'Yes';
            }else{
              $isAdded = 'No';
            }
            $groupDetail = Group::where(['id'=>$saveArray['group_id']])->first();
            if($groupDetail){
              $memberCount = Group_member::where(['group_id'=> $saveArray['group_id']])
              ->where(['request_status'=>'accepted'])
              ->where('user_id','!=',$groupDetail['group_owner_id'])
              ->select('user_id')->get();
              $groupDetail = Group::where(['id'=>$saveArray['group_id']])->first();
              $grpOwner = User::where(['id'=>$groupDetail['group_owner_id']])->select('name','image')->first();
              $image = url('/images',$grpOwner['image']);
              $outgoingRequests = [];
              $record = [];
              $groupmemberDetail = Group_member::where(['group_id'=> $groupDetail['id']])
              ->where(['request_status'=>'accepted'])
              ->where('user_id','!=',$groupDetail['group_owner_id'])
              ->select('user_id')->orderBy('id','DESC')->get();
              foreach($groupmemberDetail as $ls){
                $userDetails = User::where(['id'=>$ls['user_id']])->first();
                $listing['user_id'] = $ls['user_id'];
                $listing['user_name'] = $userDetails['name'];
                $listing['user_image'] = url('/images',$userDetails['image']);
                $listing['skier'] = $userDetails['skier'];
                $listing['skier_sky_level'] = $userDetails['skier_sky_level'];
                $listing['skier_fav_tarrian'] = $userDetails['skier_fav_tarrian'];
                $listing['snowboarder'] = $userDetails['snowboarder'];
                $listing['snowboarder_sky_level'] = $userDetails['snowboarder_sky_level'];
                $listing['snowboarder_fav_tarrian'] = $userDetails['snowboarder_fav_tarrian'];
                $record[] = $listing;
              }
              $grpOwner = User::where(['id'=>$groupDetail['group_owner_id']])->select('name')->first();
              $image = url('/images',$groupDetail['group_image']);
              $group_details = array(
                'is_AddedUser'=> $isAdded,
                'total_members'=> count($memberCount),
                'id'=>(String)$groupDetail['id'],
                'group_name'=>$groupDetail['group_name'],
                'group_location'=>$groupDetail['group_location'],
                'group_info'=>$groupDetail['group_info'],
                'group_owner_id'=>$groupDetail['group_owner_id'],
                'group_owner'=>$grpOwner['name'],
                'group_date'=>$groupDetail['group_date'],
                'privacy_status'=>$groupDetail['privacy_status'],
                'image'=>$image?$image:'',
                'created_at'=>$groupDetail['created_at']->format('d-m-Y'),
                'group_members'=>$record,
              );
              return response()->json([
                'status'=>'1',
                'message'=>'Details fetched successfully',
               'group_details'=>$group_details
             ], $this->successStatus);
            }
            else{
              return response()->json([
                'status'=>'0',
                'message'=>'No Group Found'
             ], $this->successStatus);
            }
          }
        }
      }
      else{
        return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
      }
    }

  /*
    * My Group API with searching -by location and date
  */

  public function myGroup(Request $request)
  {
    $saveArray = $request->all();
    $token = $request->header('token');
    $userExist = User::where(['remember_token'=>$token])->first();
    if($userExist){
      $rules = array(
        'user_id' => 'required_without_all:location,created_date',
        'location' => 'required_without_all:user_id,created_date',
        'created_date' => 'required_without_all:user_id,location'
      );
      $validator = Validator::make($request->all(), $rules);
      if ($validator->fails()) {
        return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
      }
      else{
        $today = today()->format('Y-m-d');
        if ($saveArray['location'] && $saveArray['created_date']) {
          $mygroup = Group::where('group_location', 'LIKE', '' . $saveArray['location'] . '%')
          ->where('group_date', 'LIKE', '' . $saveArray['created_date'] . '%')
          ->where(['group_owner_id'=>$userExist['id']])
          ->orderBy('id','DESC')->get();
          $groupsinME = Group_member::where('group_location', 'LIKE', '' . $saveArray['location'] . '%')
          ->where('created_at', 'LIKE', '' . $saveArray['created_date'] . '%')
          ->where(['user_id'=>$userExist['id']])
          ->where(['request_status'=>'accepted'])
          ->where('message','!=','Admin')
          ->select('group_id')
          ->orderBy('id','DESC')
          ->get();
          $groupjoinRequest = Group_member::where('group_location', 'LIKE', '' . $saveArray['location'] . '%')
          ->where('created_at', 'LIKE', '' . $saveArray['created_date'] . '%')
          ->where(['user_id'=>$saveArray['user_id']])
          ->where(['request_status'=>'sent'])
          ->where('created_at','>=', $today)
          ->orderBy('id','DESC')->get();
        }
        elseif ($saveArray['created_date']) {
          $mygroup = Group::where('group_date', 'LIKE', '' . $saveArray['created_date'] . '%')
          ->where(['group_owner_id'=>$userExist['id']])
          ->where('group_date','>=', $today)
          ->orderBy('id','DESC')->get();
          $groupsinME = Group_member::where('created_at', 'LIKE', '' . $saveArray['created_date'] . '%')
          ->where(['user_id'=>$userExist['id']])
          ->where(['request_status'=>'accepted'])
          ->where('message','!=','Admin')
          ->select('group_id')
          ->orderBy('id','DESC')->get();
          $groupjoinRequest = Group_member::where('created_at', 'LIKE', '' . $saveArray['created_date'] . '%')
          ->where(['user_id'=>$saveArray['user_id']])
          ->where(['request_status'=>'sent'])
          ->where('created_at','>=', $today)
          ->orderBy('id','DESC')->get();
        }
        elseif ($saveArray['location']) {
          $mygroup = Group::where('group_location', 'LIKE', '' . $saveArray['location'] . '%')
          ->where(['group_owner_id'=>$userExist['id']])
          ->where('group_date','>=', $today)
          ->orderBy('id','DESC')->get();
          $groupsinME = Group_member::where('group_location', 'LIKE', '' . $saveArray['location'] . '%')
          ->where(['user_id'=>$userExist['id']])
          ->where(['request_status'=>'accepted'])
          ->where('message','!=','Admin')
          ->where('created_at','>=', $today)
          ->select('group_id')
          ->orderBy('id','DESC')->get();
          $groupjoinRequest = Group_member::where('group_location', 'LIKE', '' . $saveArray['location'] . '%')
          ->where(['user_id'=>$saveArray['user_id']])
          ->where(['request_status'=>'sent'])
          ->where('created_at','>=', $today)
          ->orderBy('id','DESC')->get();
        }
        elseif ($saveArray['user_id']) {
          $mygroup = Group::where(['group_owner_id'=>$userExist['id']])
          ->where('group_date','>=', $today)
          ->orderBy('id','DESC')->get();
          $groupsinME = Group_member::where(['user_id'=>$userExist['id']])
          ->where(['request_status'=>'accepted'])
          ->where('message','!=','Admin')
          ->where('created_at','>=', $today)
          ->select('group_id')->orderBy('id','DESC')->get();
          $groupjoinRequest = Group_member::where(['user_id'=>$saveArray['user_id']])
          ->where(['request_status'=>'sent'])
          ->where('created_at','>=', $today)
          ->orderBy('id','DESC')->get();
        }

        $myGroupArray = [];
        $inGroupArray = [];
        $joinRequestArray = [];

        foreach($mygroup as $ls){
          $groupID = $ls->id;
          $groupMembers = Group_member::where(['group_id'=>$groupID])
          ->where('request_status','=', 'accepted')
          ->where('user_id','!=',$ls['group_owner_id'])
          ->select('user_id')
          ->get();
          $userRequests = Group_member::where(['group_id'=>$groupID])->where('request_status','=', 'sent')->select('user_id')->get();
          $request_status = Group_member::where(['user_id'=>$userExist['id']])->where(['group_id'=>$ls['id']])->first();
          $userDetails = User::where(['id'=>$ls['group_owner_id']])->first();
          $listing['group_members'] = (count($groupMembers));
          $listing['user_requests'] = (count($userRequests));
          $listing['group_id'] = $ls['id'];
          $listing['group_owner_id'] = $ls['group_owner_id'];
          $listing['group_owner_name'] = $userDetails['name'];
          $listing['location'] = $ls['group_location'];
          $listing['group_info'] = $ls['group_info'];
          $listing['group_date'] = $ls['group_date'];
          $listing['privacy_status'] = $ls['privacy_status'];
          $listing['created_at'] = $ls['created_at'];
          $listing['image'] = url('/images',$userDetails['image']);
          $listing['request_status'] = $request_status['request_status']?$request_status['request_status']:'';
          $myGroupArray[] = $listing;
         }
         foreach($groupsinME as $ls1){
          $groupID = $ls1->group_id;
          $request_status = Group_member::where(['user_id'=>$userExist['id']])->where(['group_id'=>$ls1['id']])->first();
          $groupDetails1 = Group::where(['id'=>$ls1['group_id']])->first();
          $groupMembers = Group_member::where(['group_id'=>$groupID])
          ->where('request_status','=', 'accepted')
          ->where('user_id','!=',$groupDetails1['group_owner_id'])
          ->select('user_id')->get();
          $userDetails = User::where(['id'=>$groupDetails1['group_owner_id']])->first();
          $listing1['group_members'] = (count($groupMembers));
          $listing1['group_id'] = $groupDetails1['id'];
          $listing1['location'] = $groupDetails1['group_location'];
          $listing1['group_info'] = $groupDetails1['group_info'];
          $listing1['group_owner_id'] = $groupDetails1['group_owner_id'];
          $listing1['group_owner_name'] = $userDetails['name'];
          $listing1['group_date'] = $groupDetails1['group_date'];
          $listing1['privacy_status'] = $groupDetails1['privacy_status'];
          $listing1['group_name'] = $groupDetails1['group_name'];
          $listing1['group_image'] = url('/images',$userDetails['image']);
          $listing1['request_status'] = $request_status['request_status']?$request_status['request_status']:'';
          $inGroupArray[] = $listing1;
        }
        foreach($groupjoinRequest as $ls2){
          $groupID = $ls2->group_id;
          $grpDetail = Group::where(['id'=>$ls2['group_id']])->first();
          $userDetails2 = User::where(['id'=>$ls2['user_id']])->first();
          $gpDetail = User::where(['id'=>$grpDetail['group_owner_id']])->first();
          $groupMembers = Group_member::where(['group_id'=>$groupID])
          ->where('request_status','=', 'accepted')
          ->where('user_id','!=',$grpDetail['group_owner_id'])
          ->select('user_id')
          ->get();
          $listing2['group_members'] = (count($groupMembers));
          $listing2['group_id'] = $grpDetail['id'];
          $listing2['group_owner_id'] = $grpDetail['group_owner_id'];
          $listing2['group_owner_name'] = $gpDetail['name'];
          $listing2['group_location'] = $grpDetail['group_location'];
          $listing2['group_info'] = $grpDetail['group_info'];
          $listing2['privacy_status'] = $grpDetail['privacy_status'];
          $listing2['created_at'] = $ls2['created_at']->format('Y-m-d');
          $listing2['user_id'] = $userDetails2['id'];
          $listing2['user_name'] = $userDetails2['name'];
          $listing2['image'] = url('/images',$userDetails2['image']);
          $listing2['skier'] = $userDetails2['skier'];
          $listing2['skier_sky_level'] = $userDetails2['skier_sky_level'];
          $listing2['skier_fav_tarrian'] = $userDetails2['skier_fav_tarrian'];
          $listing2['snowboarder'] = $userDetails2['snowboarder'];
          $listing2['snowboarder_sky_level'] = $userDetails2['snowboarder_sky_level'];
          $listing2['snowboarder_fav_tarrian'] = $userDetails2['snowboarder_fav_tarrian'];
          $joinRequestArray[] = $listing2;
        }
        return response()->json([
          'status'=>'1',
          'message'=>'Details fetched successfully',
          'myGroupsArray'=>$myGroupArray,
          'inGroupsArray'=>$inGroupArray,
          'joinRequestsArray'=>$joinRequestArray], $this->successStatus);
      }
    }else{
      return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
    }
  }

  /*
    * Open Group API with searching by location and date
  */


  public function openGroup(Request $request)
  {
    $saveArray = $request->all();
    $token = $request->header('token');
    $userExist = User::where(['remember_token'=>$token])->first();
    if($userExist){
      $rules = array(
        'user_id' => 'required_without_all:location,created_date',
        'location' => 'required_without_all:user_id,created_date',
        'created_date' => 'required_without_all:user_id,location'
      );
      $validator = Validator::make($request->all(), $rules);
      if ($validator->fails()) {
        return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
      }
      else{
        #1st
        if ($saveArray['location'] && $saveArray['created_date']) {
            $buddiesGroupArray = [];
            $memberbuddiesgrpArray = [];
            $openGroupArray = [];
            $today = today()->format('Y-m-d');
            $buddies = Friend::where(['sender_id'=>$saveArray['user_id']])
            ->where(['status'=>'accepted'])
            ->orwhere(['reciever_id'=>$saveArray['user_id']])
            ->where(['status'=>'accepted'])
            ->get();
            foreach($buddies as $ls){
              if($saveArray['user_id'] == $ls['reciever_id']){
                $loginUserfriends = $ls['sender_id'];
              }elseif( $saveArray['user_id'] == $ls['sender_id']){
                $loginUserfriends = $ls['reciever_id'];
              }
              $buddiesgroups = Group::where('group_location', 'LIKE', '' . $saveArray['location'] . '%')
              ->where('group_date', 'LIKE', '' . $saveArray['created_date'] . '%')
              ->where('group_owner_id','=', $loginUserfriends)
              ->orderBy('id','DESC')
              ->get();
              $memberbuddiesgroupid = [];
              foreach($buddiesgroups as $ls1){
                $groupID = $ls1->id;
                $groupMembers = Group_member::where(['group_id'=>$groupID])
                ->where('request_status','=', 'accepted')
                ->where('user_id','!=',$ls1['group_owner_id'])
                ->select('user_id')->get();
                $request_status = Group_member::where(['user_id'=>$userExist['id']])->where(['group_id'=>$ls1['id']])->first();
                $userDetails1 = User::where(['id'=>$ls1['group_owner_id']])->first();
                $listing1['group_members'] = (count($groupMembers));
                $listing1['group_id'] = $ls1['id'];
                $listing1['group_location'] = $ls1['group_location'];
                $listing1['group_info'] = $ls1['group_info'];
                $listing1['privacy_status'] = $ls1['privacy_status'];
                $listing1['group_date'] = $ls1['group_date'];
                $listing1['group_owner_id'] = $ls1['group_owner_id'];
                $listing1['group_owner_name'] = $userDetails1['name'];
                $listing1['image'] = url('/images',$userDetails1['image']);
                $listing1['request_status'] = $request_status['request_status']?$request_status['request_status']:'';
                $buddiesGroupArray[] = $listing1;
                $memberbuddiesgroupid['group_id'] = $ls1['id'];
                $memberbuddiesgroupid['group_owner_id'] = $ls1['group_owner_id'];
              }
              $mmbrbudgrp = [];
              $memberbuddiesgrp = Group_member::where('created_at', 'LIKE', '' . $saveArray['created_date'] . '%')
              ->where('group_location', 'LIKE', '' . $saveArray['location'] . '%')
              ->where(['user_id'=>$loginUserfriends])
              ->where('message','!=','Admin')
              ->where('request_status','=','accepted')
              ->select('group_id','user_id')
              ->get();
              foreach($memberbuddiesgrp as $ls3){
                $mmbrbudgrp['group_id'] = $ls3['group_id'];
                $mmbrbudgrp['user_id'] = $ls3['user_id'];
                $friendsjoinedgrp = Group::where('group_date', 'LIKE', '' . $saveArray['created_date'] . '%')
                ->where('group_location', 'LIKE', '' . $saveArray['location'] . '%')
                ->where(['id'=>$mmbrbudgrp['group_id']])
                ->where('group_owner_id','!=', $userExist['id'])
                ->where(['privacy_status'=>'MembersBuddies'])
                ->get();
                foreach($friendsjoinedgrp as $ls2){
                  $groupID = $ls2->id;
                  $groupMembers = Group_member::where(['group_id'=>$groupID])
                  ->where('request_status','=', 'accepted')
                  ->where('user_id','!=',$ls2['group_owner_id'])
                  ->select('user_id')->get();
                  $userDetails1 = User::where(['id'=>$ls2['group_owner_id']])->first();
                  $listing2['group_members'] = (count($groupMembers));
                  $listing2['group_id'] = $ls2['id'];
                  $listing2['group_location'] = $ls2['group_location'];
                  $listing2['group_info'] = $ls2['group_info'];
                  $listing2['privacy_status'] = $ls2['privacy_status'];
                  $listing2['group_info'] = $ls2['group_info'];
                  $listing2['group_owner_id'] = $userDetails1['id'];
                  $listing2['group_owner_name'] = $userDetails1['name'];
                  $listing2['image'] = url('/images',$userDetails1['image']);
                  $listing2['request_status'] = $ls2['request_status']?$ls2['request_status']:'';
                  $memberbuddiesgrpArray[] = $listing2;
                }
              }
            }
            $buddies = Friend::where(['sender_id'=>$saveArray['user_id']])
            ->where(['status'=>'accepted'])
            ->orwhere(['reciever_id'=>$saveArray['user_id']])
            ->where(['status'=>'accepted'])
            ->get();
            $friendIDs = [];
            
            foreach($buddies as $ls){
              if($saveArray['user_id'] == $ls['reciever_id']){
                $friendIDs[] = $ls['sender_id'];
              }elseif( $saveArray['user_id'] == $ls['sender_id']){
                $friendIDs[] = $ls['reciever_id'];
              }
            }
            
            if(count($friendIDs) > 0){
              $groups = Group::where(['privacy_status'=>'Everyone'])
              ->where('group_date', 'LIKE', '' . $saveArray['created_date'] . '%')
              ->where('group_location', 'LIKE', '' . $saveArray['location'] . '%')
              ->where('group_owner_id','!=', $saveArray['user_id'])
              ->whereNotIn('group_owner_id', [$friendIDs])
              ->orderBy('id','DESC')
              ->get();
            }else{
              $groups = Group::where(['privacy_status'=>'Everyone'])
              ->where('group_date', 'LIKE', '' . $saveArray['created_date'] . '%')
              ->where('group_location', 'LIKE', '' . $saveArray['location'] . '%')
              ->where('group_owner_id','!=', $saveArray['user_id'])
              ->orderBy('id','DESC')
              ->get();
            }
            foreach($groups as $ls){
              $groupID = $ls->id;
              $groupMembers = Group_member::where(['group_id'=>$groupID])->where('request_status','=', 'accepted')->select('user_id')->get();
              $userDetails = User::where(['id'=>$ls['group_owner_id']])->first();
              $request_status = Group_member::where(['user_id'=>$saveArray['user_id']])->where(['group_id'=>$ls['id']])->first();
              $listing['group_members'] = (count($groupMembers));
              $listing['group_id'] = $ls['id'];
              $listing['group_location'] = $ls['group_location'];
              $listing['group_info'] = $ls['group_info'];
              $listing['privacy_status'] = $ls['privacy_status'];
              $listing['group_date'] = $ls['group_date'];
              $listing['group_owner_id'] = $ls['group_owner_id'];
              $listing['group_owner_name'] = $userDetails['name'];
              $listing['image'] = url('/images',$userDetails['image']);
              $listing['request_status'] = $request_status['request_status']?$request_status['request_status']:'';
              $openGroupArray[] = $listing;
            }
            return response()->json([
              'status'=>'1',
              'message'=>'Details fetched successfully',
              'buddiesGroupArray'=>array_merge($buddiesGroupArray, $memberbuddiesgrpArray),
              'openGroupArray'=>$openGroupArray
            ], $this->successStatus);
          }
          #2nd
          elseif ($saveArray['created_date']) {
            $buddiesGroupArray = [];
            $memberbuddiesgrpArray = [];
            $openGroupArray = [];
            $buddies = Friend::where(['sender_id'=>$saveArray['user_id']])
            ->where(['status'=>'accepted'])
            ->orwhere(['reciever_id'=>$saveArray['user_id']])
            ->where(['status'=>'accepted'])
            ->get();
            foreach($buddies as $ls){
              if($saveArray['user_id'] == $ls['reciever_id']){
                $loginUserfriends = $ls['sender_id'];
              }elseif( $saveArray['user_id'] == $ls['sender_id']){
                $loginUserfriends = $ls['reciever_id'];
              }
              $buddiesgroups = Group::where('group_date', 'LIKE', '' . $saveArray['created_date'] . '%')
              ->where('group_owner_id','=', $loginUserfriends)
              ->orderBy('id','DESC')
              ->get();
              //print_r(json_encode($buddiesgroups));
              $memberbuddiesgroupid = [];
              foreach($buddiesgroups as $ls1){
                $groupID = $ls1->id;
                $groupMembers = Group_member::where(['group_id'=>$groupID])
                ->where('request_status','=', 'accepted')
                ->where('user_id','!=',$ls1['group_owner_id'])
                ->select('user_id')->get();
                $request_status = Group_member::where(['user_id'=>$userExist['id']])->where(['group_id'=>$ls1['id']])->first();
                $userDetails1 = User::where(['id'=>$ls1['group_owner_id']])->first();
                $listing1['group_members'] = (count($groupMembers));
                $listing1['group_id'] = $ls1['id'];
                $listing1['group_location'] = $ls1['group_location'];
                $listing1['group_info'] = $ls1['group_info'];
                $listing1['privacy_status'] = $ls1['privacy_status'];
                $listing1['group_date'] = $ls1['group_date'];
                $listing1['group_owner_id'] = $ls1['group_owner_id'];
                $listing1['group_owner_name'] = $userDetails1['name'];
                $listing1['image'] = url('/images',$userDetails1['image']);
                $listing1['request_status'] = $request_status['request_status']?$request_status['request_status']:'';
                $buddiesGroupArray[] = $listing1;
                $memberbuddiesgroupid['group_id'] = $ls1['id'];
                $memberbuddiesgroupid['group_owner_id'] = $ls1['group_owner_id'];
              }
              $mmbrbudgrp = [];
              $memberbuddiesgrp = Group_member::where('created_at', 'LIKE', '' . $saveArray['created_date'] . '%')
              ->where(['user_id'=>$loginUserfriends])
              ->where('message','!=','Admin')
              ->where('request_status','=','accepted')
              ->select('group_id','user_id')
              ->get();
              foreach($memberbuddiesgrp as $ls3){
                $mmbrbudgrp['group_id'] = $ls3['group_id'];
                $mmbrbudgrp['user_id'] = $ls3['user_id'];
                $friendsjoinedgrp = Group::where('group_date', 'LIKE', '' . $saveArray['created_date'] . '%')
                ->where(['id'=>$mmbrbudgrp['group_id']])
                ->where('group_owner_id','!=', $userExist['id'])
                ->where(['privacy_status'=>'MembersBuddies'])
                ->get();
                foreach($friendsjoinedgrp as $ls2){
                  $groupID = $ls2->id;
                  $groupMembers = Group_member::where(['group_id'=>$groupID])
                  ->where('request_status','=', 'accepted')
                  ->where('user_id','!=',$ls2['group_owner_id'])
                  ->select('user_id')->get();
                  $userDetails1 = User::where(['id'=>$ls2['group_owner_id']])->first();
                  $listing2['group_members'] = (count($groupMembers));
                  $listing2['group_id'] = $ls2['id'];
                  $listing2['group_location'] = $ls2['group_location'];
                  $listing2['group_info'] = $ls2['group_info'];
                  $listing2['privacy_status'] = $ls2['privacy_status'];
                  $listing2['group_date'] = $ls2['group_date'];
                  $listing2['group_owner_id'] = $ls2['group_owner_id'];
                  $listing2['group_owner_name'] = $userDetails1['name'];
                  $listing2['image'] = url('/images',$userDetails1['image']);
                  $listing2['request_status'] = $ls2['request_status']?$ls2['request_status']:'';
                  $memberbuddiesgrpArray[] = $listing2;
                }
              }
            }
            return response()->json([
              'status'=>'1',
              'message'=>'Details fetched successfully',
              'buddiesGroupArray'=>array_merge($buddiesGroupArray, $memberbuddiesgrpArray),
              'openGroupArray'=>$openGroupArray
            ], $this->successStatus);
          }
          #3rd
          elseif ($saveArray['location']) {
            $buddiesGroupArray = [];
            $memberbuddiesgrpArray = [];
            $openGroupArray = [];
            $today = today()->format('Y-m-d');
            $buddies = Friend::where(['sender_id'=>$saveArray['user_id']])
            ->where(['status'=>'accepted'])
            ->orwhere(['reciever_id'=>$saveArray['user_id']])
            ->where(['status'=>'accepted'])
            ->get();
            foreach($buddies as $ls){
              if($saveArray['user_id'] == $ls['reciever_id']){
                $loginUserfriends = $ls['sender_id'];
              }elseif( $saveArray['user_id'] == $ls['sender_id']){
                $loginUserfriends = $ls['reciever_id'];
              }
              $buddiesgroups = Group::where('group_location', 'LIKE', '' . $saveArray['location'] . '%')
              ->where('group_owner_id','=', $loginUserfriends)
              ->where('group_date','>=', $today)
              ->orderBy('id','DESC')
              ->get();
              //print_r(json_encode($buddiesgroups));die();
              $memberbuddiesgroupid = [];
              foreach($buddiesgroups as $ls1){
                $groupID = $ls1->id;
                $groupMembers = Group_member::where(['group_id'=>$groupID])
                ->where('request_status','=', 'accepted')
                ->where('user_id','!=',$ls1['group_owner_id'])
                ->select('user_id')->get();
                $request_status = Group_member::where(['user_id'=>$userExist['id']])->where(['group_id'=>$ls1['id']])->first();
                $userDetails1 = User::where(['id'=>$ls1['group_owner_id']])->first();
                $listing1['group_members'] = (count($groupMembers));
                $listing1['group_id'] = $ls1['id'];
                $listing1['group_location'] = $ls1['group_location'];
                $listing1['group_info'] = $ls1['group_info'];
                $listing1['privacy_status'] = $ls1['privacy_status'];
                $listing1['group_date'] = $ls1['group_date'];
                $listing1['group_owner_id'] = $ls1['group_owner_id'];
                $listing1['group_owner_name'] = $userDetails1['name'];
                $listing1['image'] = url('/images',$userDetails1['image']);
                $listing1['request_status'] = $request_status['request_status']?$request_status['request_status']:'';
                $buddiesGroupArray[] = $listing1;
                $memberbuddiesgroupid['group_id'] = $ls1['id'];
                $memberbuddiesgroupid['group_owner_id'] = $ls1['group_owner_id'];
              }
              $mmbrbudgrp = [];
              $memberbuddiesgrp = Group_member::where('group_location', 'LIKE', '' . $saveArray['location'] . '%')
              ->where(['user_id'=>$loginUserfriends])
              ->where('message','!=','Admin')
              ->where('request_status','=','accepted')
              ->where('created_at','>=', $today)
              ->select('group_id','user_id')
              ->get();
              foreach($memberbuddiesgrp as $ls3){
                $mmbrbudgrp['group_id'] = $ls3['group_id'];
                $mmbrbudgrp['user_id'] = $ls3['user_id'];
                $friendsjoinedgrp = Group::where('group_location', 'LIKE', '' . $saveArray['location'] . '%')
                ->where(['id'=>$mmbrbudgrp['group_id']])
                ->where('group_owner_id','!=', $userExist['id'])
                ->where(['privacy_status'=>'MembersBuddies'])
                ->get();
                foreach($friendsjoinedgrp as $ls2){
                  $groupID = $ls2->id;
                  $groupMembers = Group_member::where(['group_id'=>$groupID])
                  ->where('request_status','=', 'accepted')
                  ->where('user_id','!=',$ls2['group_owner_id'])
                  ->select('user_id')->get();
                  $userDetails1 = User::where(['id'=>$ls2['group_owner_id']])->first();
                  $listing2['group_members'] = (count($groupMembers));
                  $listing2['group_id'] = $ls2['id'];
                  $listing2['group_location'] = $ls2['group_location'];
                  $listing2['group_info'] = $ls2['group_info'];
                  $listing2['privacy_status'] = $ls2['privacy_status'];
                  $listing2['group_date'] = $ls2['group_date'];
                  $listing2['group_owner_id'] = $ls2['group_owner_id'];
                  $listing2['group_owner_name'] = $userDetails1['name'];
                  $listing2['image'] = url('/images',$userDetails1['image']);
                  $listing2['request_status'] = $ls2['request_status']?$ls2['request_status']:'';
                  $memberbuddiesgrpArray[] = $listing2;
                }
              }
            }
            return response()->json([
              'status'=>'1',
              'message'=>'Details fetched successfully',
              'buddiesGroupArray'=>array_merge($buddiesGroupArray, $memberbuddiesgrpArray),
              'openGroupArray'=>$openGroupArray
            ], $this->successStatus);
          }
          #4rth
          elseif ($saveArray['user_id']) {
            $today = today()->format('Y-m-d');
            $buddiesGroupArray = [];
            $memberbuddiesgrpArray = [];
            $openGroupArray = [];
            $buddies = Friend::where(['sender_id'=>$saveArray['user_id']])
            ->where(['status'=>'accepted'])
            ->orwhere(['reciever_id'=>$saveArray['user_id']])
            ->where(['status'=>'accepted'])
            ->get();
            foreach($buddies as $ls){
              if($saveArray['user_id'] == $ls['reciever_id']){
                $loginUserfriends = $ls['sender_id'];
              }elseif( $saveArray['user_id'] == $ls['sender_id']){
                $loginUserfriends = $ls['reciever_id'];
              }
              $buddiesgroups = Group::where(['group_owner_id'=>$loginUserfriends])
              //->where('privacy_status','!=', 'MembersBuddies')
              ->where('group_date','>=', $today)
              ->orderBy('id','DESC')
              ->get();
              $memberbuddiesgroupid = [];
              foreach($buddiesgroups as $ls1){
                $groupID = $ls1->id;
                $groupMembers = Group_member::where(['group_id'=>$groupID])
                ->where('request_status','=', 'accepted')
                ->where('user_id','!=',$ls1['group_owner_id'])
                ->select('user_id')->get();
                $request_status = Group_member::where(['user_id'=>$userExist['id']])->where(['group_id'=>$ls1['id']])->first();
                $userDetails1 = User::where(['id'=>$ls1['group_owner_id']])->first();
                $listing1['group_members'] = (count($groupMembers));
                $listing1['group_id'] = $ls1['id'];
                $listing1['group_location'] = $ls1['group_location'];
                $listing1['group_info'] = $ls1['group_info'];
                $listing1['group_date'] = $ls1['group_date'];
                $listing1['group_owner_id'] = $ls1['group_owner_id'];
                $listing1['group_owner_name'] = $userDetails1['name'];
                $listing1['image'] = url('/images',$userDetails1['image']);
                $listing1['request_status'] = $request_status['request_status']?$request_status['request_status']:'';
                $buddiesGroupArray[] = $listing1;
                $memberbuddiesgroupid['group_id'] = $ls1['id'];
                $memberbuddiesgroupid['group_owner_id'] = $ls1['group_owner_id'];
              }
              $mmbrbudgrp = [];
              $memberbuddiesgrp = Group_member::where(['user_id'=>$loginUserfriends])
              ->where('message','!=','Admin')
              ->where('request_status','=','accepted')
              ->select('group_id','user_id')
              ->get();
              foreach($memberbuddiesgrp as $ls3){
                $mmbrbudgrp['group_id'] = $ls3['group_id'];
                $mmbrbudgrp['user_id'] = $ls3['user_id'];
                $friendsjoinedgrp = Group::where(['id'=>$mmbrbudgrp['group_id']])
                ->where('group_owner_id','!=', $userExist['id'])
                ->where(['privacy_status'=>'MembersBuddies'])
                ->where('group_date','>=', $today)
                ->get();
                foreach($friendsjoinedgrp as $ls2){
                  $groupID = $ls2->id;
                  $groupMembers = Group_member::where(['group_id'=>$groupID])
                  ->where('request_status','=', 'accepted')
                  ->where('user_id','!=',$ls2['group_owner_id'])
                  ->select('user_id')->get();
                  $request_status = Group_member::where(['user_id'=>$userExist['id']])->where(['group_id'=>$ls2['id']])->first();
                  $userDetails1 = User::where(['id'=>$ls2['group_owner_id']])->first();
                  $listing2['group_members'] = (count($groupMembers));
                  $listing2['group_id'] = $ls2['id'];
                  $listing2['group_location'] = $ls2['group_location'];
                  $listing2['group_info'] = $ls2['group_info'];
                  $listing2['privacy_status'] = $ls2['privacy_status'];
                  $listing2['group_date'] = $ls2['group_date'];
                  $listing2['group_owner_id'] = $ls2['group_owner_id'];
                  $listing2['group_owner_name'] = $userDetails1['name'];
                  $listing2['image'] = url('/images',$userDetails1['image']);
                  $listing2['request_status'] = $request_status['request_status']?$request_status['request_status']:'';
                  $memberbuddiesgrpArray[] = $listing2;
                }
              }
            }
             return response()->json([
                'status'=>'1',
                'message'=>'Details fetched successfully',
                'buddiesGroupArray'=>array_merge($buddiesGroupArray, $memberbuddiesgrpArray),
                'openGroupArray'=>$openGroupArray
              ], $this->successStatus);
            }
          }
        }
        else{
          return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }

    /*
      * Accept Group Request API by Group Admin
    */

    public function acceptgroupRequest(Request $request)
    {
      $saveArray = $request->all();
      $token = $request->header('token');
      $userExist = User::where(['remember_token'=>$token])->first();
      if($userExist){
        $validator = Validator::make($request->all(), [
          'user_id' => 'required',
          'group_id' => 'required',
        ]);
        if ($validator->fails()) {
          return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
        }
        else{
          $user_id = $saveArray['user_id'];
          $group_id = $saveArray['group_id'];
          $groupDate = Group::where('id',$group_id)->select('group_date')->first();
          // print_r($groupDate['group_date']);die();
          $updateData = [
            'request_status'=>2,
            'created_at'=>$groupDate['group_date'],
            'updated_at'=>date('Y-m-d h:i:s'),
          ];
          $update = Group_member::where(['user_id'=>$user_id])->where(['group_id'=>$group_id])->update($updateData);
          if($update){
            return response()->json([
              'status'=>'1',
              'message'=>'Request Accepted',
              'user_id'=> $user_id,
              'group_id'=> $group_id
            ], $this->successStatus);
          }
          else{
            return response()->json([
              'status'=>'0',
              'message'=>'error'
              ], $this->badrequest);
          }
        }
      }
      else{
        return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
      }
    }

    /*
      * Reject Group Request API by Group Admin
    */   

    public function rejectgroupRequest(Request $request)
    {
      $saveArray = $request->all();
      $token = $request->header('token');
      $userExist = User::where(['remember_token'=>$token])->first();
      if($userExist){
        $validator = Validator::make($request->all(), [
          'user_id' => 'required',
          'group_id' => 'required',
        ]);
        if ($validator->fails()) {
          return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
        }
        else{
          $user_id = $saveArray['user_id'];
          $group_id = $saveArray['group_id'];
          $updateData = [
            'request_status'=>3,
            'deleted_at'=>date('Y-m-d h:i:s'),
          ];
          $update = Group_member::where(['user_id'=>$user_id])->where(['group_id'=>$group_id])->update($updateData);
          if($update){
            return response()->json([
              'status'=>'1',
              'message'=>'Request Rejected'
            ], $this->successStatus);
          }
          else{
            return response()->json([
              'status'=>'0',
              'message'=>'error'
              ], $this->badrequest);
          }
        }
      }
      else{
        return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
      }
    }


    /*
      * Cancel Group Request API by Group Member
    */ 

    public function cancelgrpRequest(Request $request)
    {
      $saveArray = $request->all();
      $token = $request->header('token');
      $userExist = User::where(['remember_token'=>$token])->first();
      if($userExist){
        $validator = Validator::make($request->all(), [
          'user_id' => 'required',
          'group_id' => 'required',
        ]);
        if ($validator->fails()) {
          return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
        }
        else{
          $user_id = $saveArray['user_id'];
          $group_id = $saveArray['group_id'];
          $updateData = [
            'request_status'=>4,
            'deleted_at'=>date('Y-m-d h:i:s'),
          ];
          $update = Group_member::where(['user_id'=>$user_id])->where(['group_id'=>$group_id])->where(['request_status'=>'sent'])->update($updateData);
          if($update){
            return response()->json(['status'=>'1','message'=>'Request Canceled'], $this->successStatus);
          }
          else{
            return response()->json(['status'=>'0','message'=>'error'], $this->badrequest);
          }
        }
      }
      else{
        return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
      }
    }

    public function index(Request $request)
    {
        $groups = $this->groupRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($groups->toArray(), 'Groups retrieved successfully');
    }

    /**
     * Store a newly created Group in storage.
     * POST /groups
     *
     * @param CreateGroupAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateGroupAPIRequest $request)
    {
        $input = $request->all();

        $group = $this->groupRepository->create($input);

        return $this->sendResponse($group->toArray(), 'Group saved successfully');
    }

    /**
     * Display the specified Group.
     * GET|HEAD /groups/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Group $group */
        $group = $this->groupRepository->find($id);

        if (empty($group)) {
            return $this->sendError('Group not found');
        }

        return $this->sendResponse($group->toArray(), 'Group retrieved successfully');
    }

    /**
     * Update the specified Group in storage.
     * PUT/PATCH /groups/{id}
     *
     * @param int $id
     * @param UpdateGroupAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateGroupAPIRequest $request)
    {
        $input = $request->all();

        /** @var Group $group */
        $group = $this->groupRepository->find($id);

        if (empty($group)) {
            return $this->sendError('Group not found');
        }

        $group = $this->groupRepository->update($input, $id);

        return $this->sendResponse($group->toArray(), 'Group updated successfully');
    }

    /**
     * Remove the specified Group from storage.
     * DELETE /groups/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Group $group */
        $group = $this->groupRepository->find($id);

        if (empty($group)) {
            return $this->sendError('Group not found');
        }

        $group->delete();

        return $this->sendResponse($id, 'Group deleted successfully');
    }
}
