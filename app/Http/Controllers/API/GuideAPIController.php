<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateGuideAPIRequest;
use App\Http\Requests\API\UpdateGuideAPIRequest;
use App\Models\Guide;
use App\Models\User;
use App\Models\Run;
use App\Models\Resort;
use App\Models\Recommendations_like;
use App\Models\Snowflake;
use App\Models\Rewards_point;
use App\Models\Recommendations_rating;
use App\Models\Recommendation_detail;
use App\Models\Recommendation_history;
use App\Models\Recommended_items;
use App\Models\Recommendation_notes;
use App\Models\Permission;
use App\Repositories\GuideRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Validator;
use Hash;
use Crypt;
use Response;


/**
 * Class GuideController
 * @package App\Http\Controllers\API
 */

class GuideAPIController extends AppBaseController
{
    /** @var  GuideRepository */
    private $guideRepository;
    public $unauthorised = 401;
    public $successStatus = 200;
    public $badrequest = 400;

    public function __construct(GuideRepository $guideRepo)
    {
        $this->guideRepository = $guideRepo;
    }

    /**
     * Display a listing of the Guide.
     * GET|HEAD /guides
     *
     * @param Request $request
     * @return Response
     */
    public function getGuides(Request $request)
    {
        $guides = $this->guideRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($guides->toArray(), 'Guides retrieved successfully');
    }

    /*get all guides with description on basis of resort id*/

    public function get_guides_desc(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
            $validator = Validator::make($request->all(), [
                'resort_id' => 'required'
            ]);
            if ($validator->fails()) {
                return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
            }
            else{
                $getGuides = Guide::orderBy('id','ASC')->get();
                $guidesArray = [];
                    if(count($getGuides)){
                        foreach($getGuides as $ls){
                            $getDesc = Recommendation_detail::where('resort_id',$saveArray['resort_id'])
                            ->where('recommendation_id',$ls['id'])
                            ->select('id','resort_id','recommendation_id','description')
                            ->get()->toArray();
                            $descArray = [];
                            foreach ($getDesc as $value) {
                                $desc['id'] = (string)$value['id'];
                                $desc['resort_id'] = $value['resort_id'];
                                $desc['recommendation_id'] = $value['recommendation_id'];
                                $desc['description'] = $value['description'];
                                $descArray[] = $desc;
                            }
                            /*ratings*/
                            $getCount = Recommendations_rating::where('resort_id',$saveArray['resort_id'])
                            ->where('recommendation_id',$ls['id'])
                            ->where('like_dislike',1)
                            ->get()
                            ->count();
                            $ratebyme = Recommendations_rating::where('user_id',$userExist['id'])
                            ->where('resort_id',$saveArray['resort_id'])
                            ->where('recommendation_id',$ls['id'])
                            ->first();
                            if ($ratebyme['like_dislike'] == '1') {
                                $like = 'like';
                            }elseif($ratebyme['like_dislike'] == '2'){
                                $like = 'dislike';
                            }else{
                                $like = '';
                            }
                            if ($getCount > '3') {
                            /*total vote count*/
                            $totalVote = Recommendations_rating::where('resort_id',$saveArray['resort_id'])
                            ->where('recommendation_id',$ls['id'])
                            ->get()
                            ->count();
                            /*like count*/
                            $getLikeCount = Recommendations_rating::where('resort_id',$saveArray['resort_id'])
                            ->where('recommendation_id',$ls['id'])
                            ->where('like_dislike','1')
                            ->get()->count();
                            /*dislike count*/
                            $getDislikeCount = Recommendations_rating::where('resort_id',$saveArray['resort_id'])
                            ->where('recommendation_id',$ls['id'])
                            ->where('like_dislike','2')
                            ->get()->count();
                            $percentage = ($getLikeCount*100)/$totalVote;
                            if ($percentage > 1 && $percentage <= 20) {
                                $rating = "1";
                            }elseif (($percentage > 20 && $percentage <= 40)) {
                                $rating = "2";
                            }elseif (($percentage > 40 && $percentage <= 60)) {
                                $rating = "3";
                            }elseif (($percentage > 60 && $percentage <= 80)) {
                                $rating = "4";
                            }elseif (($percentage > 80 && $percentage <= 100)) {
                                $rating = "5";
                            }
                            $guidestars = $rating;
                            $rateby_me = $like;
                            }else{
                                $guidestars = '0';
                                $rateby_me = $like;
                            }
                            /*ratings*/
                            $info['recommendation_id'] = (string)$ls['id'];
                            $info['guide_title'] = $ls['guide_title'];
                            $info['created_at'] = date('Y-m-d h:i:s');
                            $info['guide_description'] = $descArray;
                            $info['ratings'] = $guidestars;
                            $info['rated_by_me'] = $rateby_me;
                            $guidesArray[] = $info;
                        }
                        return response()->json([
                            'status'=>'1',
                            'message'=>'guides fetched successfully',
                            'guide_decs_info'=>$guidesArray
                        ], $this->successStatus);
                    }
                    else{
                        return response()->json([
                            'message'=>'No data',
                            'status'=>'1'
                        ], $this->successStatus);
                    }
            }
        }
        else{
          return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }

    /*mark notes archive unarchive*/
    public function mark_archive_unarchive(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
            $validator = Validator::make($request->all(), [
                'resort_id' => 'required',
                'recommendation_id' => 'required',
                'note_id' => 'required',
            ]);
            if ($validator->fails()) {
                return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
            }
            else{
                $checkAdmin = Permission::where('resort_id',$saveArray['resort_id'])
                ->where('user_id',$userExist['id'])
                ->where('role_type','ADM')
                ->first();
                if ($checkAdmin) {
                    $getNote = Recommendation_history::where('resort_id',$saveArray['resort_id'])
                    ->where('recommendation_id',$saveArray['recommendation_id'])
                    ->where('id',$saveArray['note_id'])
                    ->first();
                    if ($getNote) {
                        if ($getNote['archive_unarchive'] == '1') {
                            $updateData = [
                                'archive_unarchive' => '2',
                                'updated_at' => date('Y-m-d H:i:s')
                            ];
                            $update = Recommendation_history::where('id',$saveArray['note_id'])
                            ->update($updateData);
                            if ($update) {
                                return response()->json([
                                    'status'=>'1',
                                    'message'=>'note unarchieved'
                                  ], $this->successStatus);
                            }
                        }elseif ($getNote['archive_unarchive'] == '2') {
                            $updateData = [
                                'archive_unarchive' => '1',
                                'updated_at' => date('Y-m-d H:i:s')
                            ];
                            $update = Recommendation_history::where('id',$saveArray['note_id'])
                            ->update($updateData);
                            if ($update) {
                                 return response()->json([
                                    'status'=>'1',
                                    'message'=>'note archieved'
                                  ], $this->successStatus);
                            }
                        }
                    }else{
                        return response()->json([
                            'status'=>'1',
                            'message'=>'No notes yet'
                        ], $this->successStatus);
                    }
                }else{
                    return response()->json([
                        'status'=>'1',
                        'message'=>'You are not admin'
                    ], $this->successStatus);
                }
            }
        }
        else{
          return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }
    /*mark notes archive unarchive*/

    /*add like/dislike , description , add note in this api*/

    public function add_like_desc(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
            $validator = Validator::make($request->all(), [
                'resort_id' => 'required',
                'recommendation_id' => 'required',
                'like' => 'required_without_all:description,added_note,post_anonymously',
                'description' => 'required_without_all:like,added_note',
                'added_note' => 'required_without_all:like,description,post_anonymously',
                'post_anonymously' => 'required_without_all:like,added_note',
            ]);
            if ($validator->fails()) {
                return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
            }
            else{
                $checkResort = Resort::where('id',$saveArray['resort_id'])->first();
                if ($checkResort) {
                    if (!empty($saveArray['like'])) {
                        $checkLike = Recommendations_rating::where('user_id',$userExist['id'])
                            ->where('resort_id',$saveArray['resort_id'])
                            ->where('recommendation_id',$saveArray['recommendation_id'])
                            ->first();
                            if ($checkLike) {
                                $updateData = [
                                    'user_id' => $userExist['id'],
                                    'resort_id' => $saveArray['resort_id'],
                                    'recommendation_id' => $saveArray['recommendation_id'],
                                    'like_dislike' => $saveArray['like'],
                                    'created_at' => date('Y-m-d H:i:s')
                                ];
                                $update = Recommendations_rating::where('user_id',$userExist['id'])
                                ->where('resort_id',$saveArray['resort_id'])
                                ->where('recommendation_id',$saveArray['recommendation_id'])
                                ->update($updateData);
                                $getLikeStatus = Recommendations_rating::where('user_id',$userExist['id'])
                                ->where('resort_id',$saveArray['resort_id'])
                                ->where('recommendation_id',$saveArray['recommendation_id'])
                                ->select('like_dislike')
                                ->first();
                                if ($getLikeStatus['like_dislike'] == '1') {
                                    $liked = 'Like';
                                }elseif ($getLikeStatus['like_dislike'] == '2') {
                                    $liked = 'Dislike';
                                }
                                if($update){
                                    return response()->json([
                                        'status'=>'1',
                                        'message'=>'Done',
                                        'like_status'=>$liked
                                        ], $this->successStatus);
                                }else{
                                    return response()->json([
                                        'status'=>'1',
                                        'message'=>'error'
                                    ], $this->badrequest);
                                }
                            }else{
                            $insertData = [
                                'user_id' => $userExist['id'],
                                'resort_id' => $saveArray['resort_id'],
                                'recommendation_id' => $saveArray['recommendation_id'],
                                'like_dislike' => $saveArray['like'],
                                'created_at' => date('Y-m-d H:i:s')
                            ];
                            $insert = Recommendations_rating::insertGetId($insertData);
                            $getLikeStatus = Recommendations_rating::where('id',$insert)
                                ->select('like_dislike')
                                ->first();
                                if ($getLikeStatus['like_dislike'] == '1') {
                                    $liked = 'Like';
                                }elseif ($getLikeStatus['like_dislike'] == '2') {
                                    $liked = 'Dislike';
                                }
                            $insertLikeReward = [
                                'user_id' => $userExist['id'],
                                'resort_id' => $saveArray['resort_id'],
                                'recommendation_id' => $saveArray['recommendation_id'],
                                'reward_for' => 'Guide Recommendation Suggesstion',
                                'snowflake_rewards' => '1',
                                'reason' => 'Vote for guide recommendation',
                                'reward_status' => '1',
                                'created_at' => date('Y-m-d H:i:s')
                            ];
                            $insertReward = Snowflake::insertGetId($insertLikeReward);
                            $get_Rpoints = Rewards_point::where('user_id',$userExist['id'])
                            ->select('total')->first();
                            $rewardedPoint = $get_Rpoints['total'] + 1;
                            $updateRData = [
                                'user_id'=> $userExist['id'],
                                'total'=> $rewardedPoint,
                                'updated_at'=>date('Y-m-d h:i:s'),
                            ];
                            $update = Rewards_point::where(['user_id'=>$userExist['id']])->update($updateRData);
                            if($insert){
                                return response()->json([
                                    'status'=>'1',
                                    'message'=>'Done',
                                    'like_status'=>$liked
                                  ], $this->successStatus);
                            }else{
                                return response()->json([
                                    'status'=>'1',
                                    'message'=>'error'
                                  ], $this->badrequest);
                                }
                            }
                    }
                    elseif (!empty($saveArray['description']) || !empty($saveArray['added_note']))
                    {
                        if ($saveArray['description']) {
                            $checkDesc = Recommendation_detail::where('resort_id',$saveArray['resort_id'])
                            ->where('recommendation_id',$saveArray['recommendation_id'])
                            ->first();
                            if ($checkDesc) {
                                $updateDesc = [
                                    'user_id' => $userExist['id'],
                                    'resort_id' => $saveArray['resort_id'],
                                    'recommendation_id' => $saveArray['recommendation_id'],
                                    'description' => $saveArray['description'],
                                    'updated_at'=>date('Y-m-d h:i:s'),
                                  ];
                                  $update = Recommendation_detail::where(['resort_id'=>$saveArray['resort_id']])->where(['recommendation_id'=>$saveArray['recommendation_id']])->update($updateDesc);
                                  $insertHistory = [
                                    'added_note' => $saveArray['added_note'],
                                    'post_anonymously' => $saveArray['post_anonymously'],
                                    'user_id' => $userExist['id'],
                                    'resort_id' => $saveArray['resort_id'],
                                    'recommendation_id' => $saveArray['recommendation_id'],
                                    'description' => $saveArray['description'],
                                    'archive_unarchive' => 'NULL',
                                    'created_at' => date('Y-m-d H:i:s')
                                ];
                                $insertValue_id = Recommendation_history::insertGetId($insertHistory);

                                /*update rewards history with sf point*/
                                $insertLikeReward = [
                                    'user_id' => $userExist['id'],
                                    'resort_id' => $saveArray['resort_id'],
                                    'recommendation_id' => $saveArray['recommendation_id'],
                                    'reward_for' => 'Guide Recommendation Description',
                                    'snowflake_rewards' => '5',
                                    'value_id' =>$insertValue_id,
                                    'reason' => 'Update Guide Description',
                                    'reward_status' => '0',
                                    'created_at' => date('Y-m-d H:i:s')
                                ];
                                $insertReward = Snowflake::insertGetId($insertLikeReward);
                                /*update rewards history with sf point*/
                                if ($insertValue_id) {
                                    return response()->json([
                                        'status'=>'1',
                                        'message'=>'Details Updated Successfully'
                                      ], $this->successStatus);
                                    }else{
                                        return response()->json([
                                            'status'=>'1',
                                            'message'=>'error'
                                          ], $this->badrequest);
                                        }
                                    }
                                    else{
                                        $insertDetails = [
                                            'user_id' => $userExist['id'],
                                            'resort_id' => $saveArray['resort_id'],
                                            'recommendation_id' => $saveArray['recommendation_id'],
                                            'description' => $saveArray['description'],
                                            'created_at' => date('Y-m-d H:i:s')
                                        ];
                                        $insert = Recommendation_detail::insert($insertDetails);
                                        $insertHistory = [
                                            'post_anonymously' => $saveArray['post_anonymously'],
                                            'user_id' => $userExist['id'],
                                            'resort_id' => $saveArray['resort_id'],
                                            'recommendation_id' => $saveArray['recommendation_id'],
                                            'description' => $saveArray['description'],
                                            'created_at' => date('Y-m-d H:i:s')
                                        ];
                                        $insertValue_id1 = Recommendation_history::insertGetId($insertHistory);
                                        /*update rewards history with sf point*/
                                        $insertLikeReward = [
                                            'user_id' => $userExist['id'],
                                            'resort_id' => $saveArray['resort_id'],
                                            'recommendation_id' => $saveArray['recommendation_id'],
                                            'reward_for' => 'Guide Recommendation Description',
                                            'snowflake_rewards' => '10',
                                            'value_id' =>$insertValue_id1,
                                            'reason' => 'Added Guide Description',
                                            'reward_status' => '0',
                                            'created_at' => date('Y-m-d H:i:s')
                                        ];
                                        $insertReward = Snowflake::insertGetId($insertLikeReward);
                                        /*update rewards history with sf point*/
                                        if ($insertValue_id1) {
                                            return response()->json([
                                                'status'=>'1',
                                                'message'=>'Details Added Successfully'
                                              ], $this->successStatus);
                                        }else{
                                            return response()->json([
                                                'status'=>'1',
                                                'message'=>'error'
                                              ], $this->badrequest);
                                            }
                                        }
                                    }elseif ($saveArray['added_note']) {
                                    $insertHistory = [
                                        'user_id' => $userExist['id'],
                                        'resort_id' => $saveArray['resort_id'],
                                        'recommendation_id' => $saveArray['recommendation_id'],
                                        'added_note' => $saveArray['added_note'],
                                        'description' => 'NULL',
                                        // 'archive_unarchive' => '2',
                                        'post_anonymously' => 'NULL',
                                        'created_at' => date('Y-m-d H:i:s')
                                    ];
                                    $insert = Recommendation_history::insert($insertHistory);
                                    /*update rewards history with sf point*/
                                    $insertNoteReward = [
                                        'user_id' => $userExist['id'],
                                        'resort_id' => $saveArray['resort_id'],
                                        'recommendation_id' => $saveArray['recommendation_id'],
                                        'reward_for' => 'Note',
                                        'snowflake_rewards' => '2',
                                        'reason' => 'Added a note',
                                        'reward_status' => '1',
                                        'created_at' => date('Y-m-d H:i:s')
                                    ];
                                    $insertReward = Snowflake::insertGetId($insertNoteReward);
                                    $get_Rpoints = Rewards_point::where('user_id',$userExist['id'])->select('total')->first();
                                    $rewardedPoint = $get_Rpoints['total'] + 2;
                                    $updateRData = [
                                        'total'=> $rewardedPoint,
                                        'updated_at'=>date('Y-m-d h:i:s'),
                                    ];
                                    $update = Rewards_point::where(['user_id'=>$userExist['id']])->update($updateRData);
                                    /*update rewards history with sf point*/
                                    if ($insert) {
                                        return response()->json([
                                            'status'=>'1',
                                            'message'=>'Note added Successfully'
                                        ], $this->successStatus);
                                    }else{
                                        return response()->json([
                                            'status'=>'1',
                                            'message'=>'error'
                                        ], $this->badrequest);
                                    }
                                        }
                                    }
                                }else{
                                    return response()->json([
                                            'status'=>'1',
                                            'message'=>'No resort found'
                                        ], $this->successStatus);
                                }
                            }
                        }
                        else{
                            return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
                        }
                    }

    /*get stars*/

    public function guidesRating(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
            $validator = Validator::make($request->all(), [
                'resort_id' => 'required',
                'recommendation_id' => 'required',
            ]);
            if ($validator->fails()) {
                return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
            }
            else{
                $getCount = Recommendations_rating::where('resort_id',$saveArray['resort_id'])
                ->where('recommendation_id',$saveArray['recommendation_id'])
                ->where('like_dislike',1)
                ->get()
                ->count();
                $ratebyme = Recommendations_rating::where('user_id',$userExist['id'])
                ->where('resort_id',$saveArray['resort_id'])
                ->where('recommendation_id',$saveArray['recommendation_id'])
                //->where('like_dislike','1')
                ->first();
                if ($ratebyme['like_dislike'] == '1') {
                    $like = 'like';
                }elseif($ratebyme['like_dislike'] == '2'){
                    $like = 'dislike';
                }else{
                    $like = 'you have not rated this recommendation yet';
                }
                if ($getCount > '3') {
                    /*total vote count*/
                    $totalVote = Recommendations_rating::where('resort_id',$saveArray['resort_id'])
                    ->where('recommendation_id',$saveArray['recommendation_id'])
                    ->get()
                    ->count();
                    /*like count*/
                    $getLikeCount = Recommendations_rating::where('resort_id',$saveArray['resort_id'])
                    ->where('recommendation_id',$saveArray['recommendation_id'])
                    ->where('like_dislike','1')
                    ->get()->count();
                    /*dislike count*/
                    $getDislikeCount = Recommendations_rating::where('resort_id',$saveArray['resort_id'])
                    ->where('recommendation_id',$saveArray['recommendation_id'])
                    ->where('like_dislike','2')
                    ->get()->count();
                    $percentage = ($getLikeCount*100)/$totalVote;
                    if ($percentage > 1 && $percentage <= 20) {
                        $rating = "1";
                    }elseif (($percentage > 20 && $percentage <= 40)) {
                        $rating = "2";
                    }elseif (($percentage > 40 && $percentage <= 60)) {
                        $rating = "3";
                    }elseif (($percentage > 60 && $percentage <= 80)) {
                        $rating = "4";
                    }elseif (($percentage > 80 && $percentage <= 100)) {
                        $rating = "5";
                    }
                    return response()->json([
                        'status'=>'1',
                        'message'=>'Recommendation Rating',
                        'stars' => $rating,
                        'rate_by_me' => $like
                    ], $this->successStatus);
                }else{
                    return response()->json([
                        'status'=>'1',
                        'message'=>'Recommendation Rating',
                        'stars' => "0",
                        'rate_by_me' => $like
                    ], $this->successStatus);
                }
            }
        }
        else{
          return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }

    /*get Description*/

    public function guideDescription(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
            $validator = Validator::make($request->all(), [
                'resort_id' => 'required',
                'recommendation_id' => 'required',
            ]);
            if ($validator->fails()) {
                return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
            }
            else{
                $checkResort = Resort::where('id',$saveArray['resort_id'])->first();
                if ($checkResort) {
                    $getDesc = Recommendation_detail::where('resort_id',$saveArray['resort_id'])
                    ->where('recommendation_id',$saveArray['recommendation_id'])
                    ->select('id','resort_id','recommendation_id','description')
                    ->first();
                    if ($getDesc) {
                        $selectRecmmendation = Guide::where('id',$getDesc['recommendation_id'])
                    ->select('guide_title')
                    ->first();
                    $guidedetails = array(
                        'id'=>$getDesc['id'],
                        'resort_id'=>$getDesc['resort_id'],
                        'recommendation_id'=>$getDesc['recommendation_id'],
                        'recommendation'=>$selectRecmmendation['guide_title'],
                        'description'=>$getDesc['description'],
                      );
                    return response()->json([
                    'status'=>'1',
                    'message'=>'Details fetched successfully',
                    'guide_details'=>$guidedetails
                  ], $this->successStatus);
                    }
                    else{
                        return response()->json([
                            'status'=>'1',
                            'message'=>'no data',
                          ], $this->successStatus);
                            }
                }else{
                    return response()->json([
                    'status'=>'1',
                    'message'=>'No resort found'
                  ], $this->successStatus);
                }
            }
        }
        else{
          return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }

    /*get description history*/

    public function guideHistory(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
            $validator = Validator::make($request->all(), [
                'resort_id' => 'required',
                'recommendation_id' => 'required',
            ]);
            if ($validator->fails()) {
                return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
            }
            else{
                $checkResort = Resort::where('id',$saveArray['resort_id'])->first();
                if ($checkResort) {
                    $gethistory = Recommendation_history::where('resort_id',$saveArray['resort_id'])
                    ->where('recommendation_id',$saveArray['recommendation_id'])
                    ->select('id','user_id','post_anonymously','description','archive_unarchive')
                    ->orderBy('id','DESC')
                    ->get();
                    $detailArray = [];
                    if(count($gethistory)){
                        foreach($gethistory as $ls){
                            $username = User::where(['id'=>$ls['user_id']])->first();
                            $info['id'] = $ls['id'];
                            $info['post_anonymously'] = $ls['post_anonymously'];
                            $info['user_id'] = $ls['user_id'];
                            $info['user_name'] = $username['name'];
                            $info['description'] = $ls['description'];
                            $info['archive_unarchive'] = $ls['archive_unarchive'];
                            $info['created_at'] = date('Y-m-d h:i:s');
                            $detailArray[] = $info;
                        }
                        return response()->json([
                            'status'=>'1',
                            'message'=>'data fetched successfully',
                            'guide_desc_history'=>$detailArray
                        ], $this->successStatus);
                    }
                    else{
                        return response()->json([
                            'message'=>'No data',
                            'status'=>'0'
                        ], $this->successStatus);
                    }
                    }else{
                    return response()->json([
                    'status'=>'1',
                    'message'=>'No resort found'
                  ], $this->successStatus);
                }
            }
        }
        else{
          return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }

    /*description/guide note history*/

    public function noteHistory(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
            $validator = Validator::make($request->all(), [
                'resort_id' => 'required',
                'recommendation_id' => 'required',
            ]);
            if ($validator->fails()) {
                return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
            }
            else{
                $checkResort = Resort::where('id',$saveArray['resort_id'])->first();
                if ($checkResort) {
                    $gethistory = Recommendation_history::where('resort_id',$saveArray['resort_id'])
                    ->where('recommendation_id',$saveArray['recommendation_id'])
                    ->where('added_note','!=','NULL')
                    ->where('archive_unarchive','=','2')
                    ->select('id','user_id','added_note','archive_unarchive')
                    ->orderBy('id','DESC')
                    ->get();
                    $detailArray = [];
                    //if(count($gethistory)){
                        foreach($gethistory as $ls){
                            $username = User::where(['id'=>$ls['user_id']])->first();
                            $info['id'] = $ls['id'];
                            $info['user_id'] = $ls['user_id'];
                            $info['user_name'] = $username['name'];
                            $info['added_note'] = $ls['added_note'];
                            $info['archive_unarchive'] = $ls['archive_unarchive'];
                            $info['created_at'] = date('Y-m-d h:i:s');
                            $detailArray[] = $info;
                        }
                        $gethistory1 = Recommendation_history::where('resort_id',$saveArray['resort_id'])
                        ->where('recommendation_id',$saveArray['recommendation_id'])
                        ->where('added_note','!=','NULL')
                        ->where('archive_unarchive','=','1')
                        ->select('id','user_id','added_note','archive_unarchive')
                        ->orderBy('id','DESC')
                        ->get();
                        $detailArray1 = [];
                        foreach($gethistory1 as $ls1){
                            $username1 = User::where(['id'=>$ls1['user_id']])->first();
                            $info1['id'] = $ls1['id'];
                            $info1['user_id'] = $ls1['user_id'];
                            $info1['user_name'] = $username1['name'];
                            $info1['added_note'] = $ls1['added_note'];
                            $info1['archive_unarchive'] = $ls1['archive_unarchive'];
                            $info1['created_at'] = date('Y-m-d h:i:s');
                            $detailArray1[] = $info1;
                        }
                        return response()->json([
                            'status'=>'1',
                            'message'=>'Notes fetched successfully',
                            'unarchive_history'=>$detailArray,
                            'archive_history'=>$detailArray1,
                        ], $this->successStatus);
                    // }
                    // else{
                    //     return response()->json([
                    //         'message'=>'No data',
                    //         'status'=>'0'
                    //     ], $this->successStatus);
                    //}
                }else{
                    return response()->json([
                        'status'=>'1',
                        'message'=>'No resort found'
                    ], $this->successStatus);
                }
            }
        }
        else{
          return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }

    /*like description for confirmation (3Likes) and reward points for users*/

    public function thumbs_up_rec_desc(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
            $validator = Validator::make($request->all(), [
                'resort_id' => 'required',
                'recommendation_id' => 'required',
                'recommendations_for' => 'required', /* Run , Lift , Activity ,Others */
                'value_id' => 'required',
                'like_for' => 'required', /* Description ,Others */
            ]);
            if ($validator->fails()) {
                return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
            }
            else{
                $dynamicField = $saveArray['like_for'];
                $getvalue = Recommendation_detail::where(['resort_id'=>$saveArray['resort_id']])
                ->where(['recommendation_id'=>$saveArray['recommendation_id']])
                ->select($dynamicField)
                ->first();
                $updatedValue = $getvalue->$dynamicField;
                $checkCount = Recommendations_like::where(['recommendation_id'=>$saveArray['recommendation_id']])
                ->where(['recommendations_for'=>$saveArray['recommendations_for']])
                ->where(['like_for'=>$saveArray['like_for']])
                ->where(['like_for_value'=>$updatedValue])
                ->count();
                if ($checkCount >= '3') {
                    $updateData = [
                        'deleted_at'=>date('Y-m-d h:i:s'),
                    ];
                    $update = Recommendations_like::where('resort_id',$saveArray['resort_id'])
                    ->where('recommendations_for',$saveArray['recommendations_for'])
                    ->update($updateData);
                    return response()->json([
                        'status'=>'1',
                        'message'=>'data is already updated please check again',
                    ], $this->successStatus);
                }else{
                    $insertData = [
                        'user_id'=>$userExist['id'],
                        'resort_id'=> $saveArray['resort_id'],
                        'recommendation_id'=> $saveArray['recommendation_id'],
                        'recommendations_for'=>$saveArray['recommendations_for'],
                        'value_id'=>$saveArray['value_id'],
                        'like_for'=>$saveArray['like_for'],
                        'like_for_value'=>$updatedValue,
                        'is_like'=>'1',
                        'created_at'=>date('Y-m-d h:i:s'),
                    ];
                    $insertedID = Recommendations_like::insertGetId($insertData);

                    /*insert vote reward in pending state*/
                    $insertVoteReward = [
                        'user_id'=>$userExist['id'],
                        'resort_id'=> $saveArray['resort_id'],
                        'recommendation_id'=> $saveArray['recommendation_id'],
                        'value_id'=> $saveArray['value_id'],
                        'reward_for'=>$saveArray['recommendations_for'],
                        'snowflake_rewards'=>'1',
                        'reason'=>'Vote for Correct Value',
                        'reward_status'=>'0',
                        'created_at'=>date('Y-m-d h:i:s'),
                    ];
                    $insertVR = Snowflake::insert($insertVoteReward);
                    /*insert vote reward in pending state*/

                    $likecount = Recommendations_like::where('resort_id',$saveArray['resort_id'])
                    ->where('recommendation_id',$saveArray['recommendation_id'])
                    ->where('recommendations_for',$saveArray['recommendations_for'])
                    ->where('like_for',$saveArray['like_for'])
                    ->where('value_id',$saveArray['value_id'])
                    ->where(['like_for_value'=>$updatedValue])
                    ->count();
                    if ($likecount == '3') {
                        $confirmed_status = 'true';
                    }
                    else{
                        $confirmed_status = 'false';
                    }
                    if ($insertedID) {
                        return response()->json([
                            'status'=>'1',
                            'message'=>'Liked',
                            'count'=>$likecount,
                            'confirmed_status'=>$confirmed_status,
                        ], $this->successStatus);
                    }else{
                        return response()->json([
                            'status'=>'1',
                            'message'=>'Error',
                        ], $this->successStatus);
                    }
                }
            }
        }
        else{
          return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }

    public function get_desc_count(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
            $validator = Validator::make($request->all(), [
                'resort_id' => 'required',
                'recommendation_id' => 'required',
                'recommendations_for' => 'required',    /*Run(Description) , Hotel , Restaurant and Others*/
                'value_id' => 'required',
                'like_for' => 'required',
            ]);
            if ($validator->fails()) {
                return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
            }
            else{
                $updatedByUserID = Recommendation_detail::where('resort_id',$saveArray['resort_id'])
                ->where('recommendation_id',$saveArray['recommendation_id'])
                ->where('id',$saveArray['value_id'])
                ->select('user_id')
                ->first();
                /*add a check if value does not exists its giving error*/
                $userName = User::where('id',$updatedByUserID['user_id'])->select('name')->first();
                $likecount = Recommendations_like::where('resort_id',$saveArray['resort_id'])
                ->where('recommendation_id',$saveArray['recommendation_id'])
                ->where('recommendations_for',$saveArray['recommendations_for'])
                ->where('like_for',$saveArray['like_for'])
                ->where('value_id',$saveArray['value_id'])
                ->count();
                if ($likecount == '3') {
                    $confirmed_status = 'true';
                }
                else{
                    $confirmed_status = 'false';
                }
                $checkLike = Recommendations_like::where(['resort_id'=>$saveArray['resort_id']])
                ->where('recommendation_id',$saveArray['recommendation_id'])
                ->where('recommendations_for',$saveArray['recommendations_for'])
                ->where('like_for',$saveArray['like_for'])
                ->where('value_id',$saveArray['value_id'])
                ->where(['user_id'=>$userExist['id']])
                ->first();
                if ($checkLike) {
                    $likedByme = 'yes';
                }
                else{
                    $likedByme = 'no';
                }
                $checkCount = Recommendations_like::where('value_id',$saveArray['value_id'])
                ->get()
                ->count();
                if ($checkCount == '3') {
                    $getUser = Recommendation_history::where('id',$saveArray['value_id'])
                    ->select('user_id','description')->first();
                    $updatedByUser = $getUser['user_id']; /*user id that updated current value*/
                    $updatedvalue = $getUser['description']; /*updated value*/
                    $getVotedUsers = Recommendations_like::where('value_id',$saveArray['value_id'])->select('user_id')->get()->toArray(); /*voted users*/

                    /*update rewards for users*/
                    foreach ($getVotedUsers as  $value) {
                        $updateData = [
                            'reward_status'=> '1',
                            'updated_at'=>date('Y-m-d h:i:s'),
                        ];
                        $update = Snowflake::where('user_id',$value['user_id'])
                        ->where('value_id',$saveArray['value_id'])
                        ->where('reward_status','0')
                        ->update($updateData);
                    }
                    if ($update) {
                        /*change reward_status of added/updated data*/
                        $update_reward_Data = [
                            'reward_status'=> '1',
                            'updated_at'=>date('Y-m-d h:i:s'),
                        ];
                        $update = Snowflake::where('user_id',$updatedByUserID->user_id)
                        ->where('value_id',$saveArray['value_id'])
                        ->where('reward_status','0')
                        ->update($update_reward_Data);
                        /*change reward_status of added/updated data*/

                        /*added/updated user rewards*/
                        $getRewards1 = Rewards_point::where('user_id',$updatedByUserID->user_id)->select('total')->first();
                        $rewardedPoint1 = $getRewards1['total'] + 10;
                        $updateData1 = [
                            'total'=> $rewardedPoint1,
                            'updated_at'=>date('Y-m-d h:i:s'),
                        ];
                        $update1 = Rewards_point::where('user_id',$updatedByUserID->user_id)->update($updateData1);
                        /*added/updated user rewards*/

                        /*voted users rewards*/
                        foreach ($getVotedUsers as  $value) {
                            $getRewards2 = Rewards_point::where('user_id',$value['user_id'])->select('total')->first();
                            $rewardedPoint2 = $getRewards2['total'] + 1;
                            $updateData2 = [
                                'total'=> $rewardedPoint2,
                                'updated_at'=>date('Y-m-d h:i:s'),
                            ];
                            $update2 = Rewards_point::where('user_id',$value['user_id'])->update($updateData2);
                        }
                        /*voted users rewards*/

                        return response()->json([
                            'status'=>'1',
                            'message'=>'Rewards Awarded',
                            'likecount' => $likecount?$likecount:'0',
                            'confirmed_status' => $confirmed_status,
                            'likedByme' => $likedByme,
                            'updatedby_userID' => $updatedByUserID->user_id,
                            'username' => $userName->name
                        ], $this->successStatus);
                    }else{
                        return response()->json([
                            'status'=>'1',
                            'message'=>'Data already updated',
                            'likecount' => $likecount?$likecount:'0',
                            'confirmed_status' => $confirmed_status,
                            'likedByme' => $likedByme,
                            'updatedby_userID' => $updatedByUserID->user_id,
                            'username' => $userName->name
                        ], $this->successStatus);
                    }
                    /*update rewards for users*/
                }else{
                    return response()->json([
                            'status'=>'1',
                            'message'=>'data fetched',
                            'likecount' => $likecount?$likecount:'0',
                            'confirmed_status' => $confirmed_status,
                            'likedByme' => $likedByme,
                            'updatedby_userID' => $updatedByUserID->user_id,
                            'username' => $userName->name
                        ], $this->successStatus);
                }
            }
        }
        else{
            return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }


    /*recommend a run*/

    public function makeRecommendations(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
            $validator = Validator::make($request->all(), [
                'resort_id' => 'required',
                'recommendation_id' => 'required',
                'run_id' => 'required'
            ]);
            if ($validator->fails()) {
                return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
            }
            else{
                $checkResort = Resort::where('id',$saveArray['resort_id'])->first();
                if ($checkResort) {
                    $checkRec = Recommended_items::where('user_id',$userExist['id'])
                    ->where('resort_id',$saveArray['resort_id'])
                    ->where('recommendation_id',$saveArray['recommendation_id'])
                    ->where('run_id',$saveArray['run_id'])->first();
                    if ($checkRec) {
                        return response()->json([
                            'status'=>'1',
                            'message'=>'You have already recommended this run'
                        ], $this->successStatus);
                    }
                    else {
                        $insertRec = [
                            'user_id' => $userExist['id'],
                            'run_id' => $saveArray['run_id'],
                            'resort_id' => $saveArray['resort_id'],
                            'recommendation_id' => $saveArray['recommendation_id'],
                            'created_at' => date('Y-m-d H:i:s')
                            ];
                            $insert = Recommended_items::insert($insertRec);

                            /*update rewards history with sf point for recommend run*/
                            $insertLikeReward = [
                                'user_id' => $userExist['id'],
                                'resort_id' => $saveArray['resort_id'],
                                'recommendation_id' => $saveArray['recommendation_id'],
                                'reward_for' => 'Run Recommendation',
                                'snowflake_rewards' => '1',
                                'reason' => 'Recommend A Run',
                                'reward_status' => '1',
                                'created_at' => date('Y-m-d H:i:s')
                            ];
                            $insertReward = Snowflake::insertGetId($insertLikeReward);
                            $get_Rpoints = Rewards_point::where('user_id',$userExist['id'])->select('total')->first();
                            $rewardedPoint = $get_Rpoints['total'] + 1;
                            $updateRData = [
                                'user_id'=> $userExist['id'],
                                'total'=> $rewardedPoint,
                                'updated_at'=>date('Y-m-d h:i:s'),
                            ];
                            $update = Rewards_point::where(['user_id'=>$userExist['id']])->update($updateRData);
                            if (empty($saveArray['recommendation_note'])) {
                                $insertRecNote = [
                                    'user_id' => $userExist['id'],
                                    'run_id' => $saveArray['run_id'],
                                    'resort_id' => $saveArray['resort_id'],
                                    'recommendation_id' => $saveArray['recommendation_id'],
                                    'thumbs_up' => '1',
                                    'recommendation_note' => '',
                                    'created_at' => date('Y-m-d H:i:s')
                                    ];
                                    $insertNote = Recommendation_notes::insert($insertRecNote);
                            }
                            if (!empty($saveArray['recommendation_note'])) {
                                $insertRecNote = [
                                    'user_id' => $userExist['id'],
                                    'run_id' => $saveArray['run_id'],
                                    'resort_id' => $saveArray['resort_id'],
                                    'recommendation_id' => $saveArray['recommendation_id'],
                                    'thumbs_up' => '1',
                                    'recommendation_note' => $saveArray['recommendation_note'],
                                    'created_at' => date('Y-m-d H:i:s')
                                    ];
                                    $insertNote = Recommendation_notes::insert($insertRecNote);
                            }
                            if($insert){
                                return response()->json([
                                    'status'=>'1',
                                    'message'=>'Recommended Successfully'
                                ], $this->successStatus);
                            }else{
                                return response()->json([
                                    'status'=>'0',
                                    'message'=>'error'
                                ], $this->successStatus);
                            }
                        }
                    }
                    else {
                        return response()->json([
                            'status'=>'1',
                            'message'=>'No resort found'
                        ], $this->successStatus);
                    }
                }
            }
            else{
                return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
            }
        }

    /*like for recommended run with optional note field
    note can be multiple but user can like once by this api
    */

    public function recommendedLike(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
            $validator = Validator::make($request->all(), [
                'resort_id' => 'required',
                'recommendation_id' => 'required',
                'run_id' => 'required',
                'like' => 'required',
            ]);
            if ($validator->fails()) {
                return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
            }
            else{
                $checkResort = Resort::where('id',$saveArray['resort_id'])->first();
                if ($checkResort) {
                    $checkLike = Recommendation_notes::where('user_id',$userExist['id'])->where('resort_id',$saveArray['resort_id'])->where('recommendation_id',$saveArray['recommendation_id'])->where('run_id',$saveArray['run_id'])->first();
                    if ($checkLike) {
                        $insertRec = [
                        'user_id' => $userExist['id'],
                        'run_id' => $saveArray['run_id'],
                        'resort_id' => $saveArray['resort_id'],
                        'recommendation_id' => $saveArray['recommendation_id'],
                        'thumbs_up' => '',
                        'recommendation_note' => $saveArray['recommendation_note'],
                        'created_at' => date('Y-m-d H:i:s')
                        ];
                        $insert = Recommendation_notes::insert($insertRec);
                        /*update rewards history with sf point for recommend run*/
                        $insert_rec_reward = [
                            'user_id' => $userExist['id'],
                            'resort_id' => $saveArray['resort_id'],
                            'recommendation_id' => $saveArray['recommendation_id'],
                            'reward_for' => 'Recommendation Note',
                            'snowflake_rewards' => '2',
                            'reason' => 'Added Note for Recommendation Run',
                            'reward_status' => '1',
                            'created_at' => date('Y-m-d H:i:s')
                        ];
                        $insertReward = Snowflake::insertGetId($insert_rec_reward);
                        $get_Rpoints = Rewards_point::where('user_id',$userExist['id'])->select('total')->first();
                        $rewardedPoint = $get_Rpoints['total'] + 2;
                        $updateRData = [
                            'total'=> $rewardedPoint,
                            'updated_at'=>date('Y-m-d h:i:s'),
                        ];
                        $update = Rewards_point::where(['user_id'=>$userExist['id']])->update($updateRData);
                        /*update rewards history with sf point for recommend run*/
                        if($insert){
                            return response()->json([
                                'status'=>'1',
                                'message'=>'Like Successfully'
                            ], $this->successStatus);
                        }else{
                            return response()->json([
                                'status'=>'0',
                                'message'=>'error'
                            ], $this->successStatus);
                        }
                    }else{
                    $insertRec = [
                        'user_id' => $userExist['id'],
                        'run_id' => $saveArray['run_id'],
                        'resort_id' => $saveArray['resort_id'],
                        'recommendation_id' => $saveArray['recommendation_id'],
                        'thumbs_up' => $saveArray['like'],
                        'recommendation_note' => $saveArray['recommendation_note']?$saveArray['recommendation_note']:'',
                        'created_at' => date('Y-m-d H:i:s')
                        ];
                        $insert = Recommendation_notes::insert($insertRec);
                        /*update rewards history with sf point for recommend run*/
                        $insert_rec_reward = [
                            'user_id' => $userExist['id'],
                            'resort_id' => $saveArray['resort_id'],
                            'recommendation_id' => $saveArray['recommendation_id'],
                            'reward_for' => 'Recommendation Note',
                            'snowflake_rewards' => '2',
                            'reason' => 'Added Note for Recommendation Run',
                            'reward_status' => '1',
                            'created_at' => date('Y-m-d H:i:s')
                        ];
                        $insertReward = Snowflake::insertGetId($insert_rec_reward);
                        $get_Rpoints = Rewards_point::where('user_id',$userExist['id'])->select('total')->first();
                        $rewardedPoint = $get_Rpoints['total'] + 2;
                        $updateRData = [
                            'total'=> $rewardedPoint,
                            'updated_at'=>date('Y-m-d h:i:s'),
                        ];
                        $update = Rewards_point::where(['user_id'=>$userExist['id']])->update($updateRData);
                        /*update rewards history with sf point for recommend run*/
                        if($insert){
                            return response()->json([
                                'status'=>'1',
                                'message'=>'Like Successfully'
                            ], $this->successStatus);
                        }else{
                            return response()->json([
                                'status'=>'0',
                                'message'=>'error'
                            ], $this->successStatus);
                        }
                    }    
                }
                else {
                    return response()->json([
                    'status'=>'1',
                    'message'=>'No resort found'
                  ], $this->successStatus);
                }
            }
        }
        else{
          return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }

    /*get recommended runs*/

    public function getRecommendedRun(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
            $validator = Validator::make($request->all(), [
                'resort_id' => 'required',
                'recommendation_id' => 'required',
            ]);
            if ($validator->fails()) {
                return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
            }
            else{
                $checkResort = Resort::where('id',$saveArray['resort_id'])->first();
                if ($checkResort) {
                    $getRecommendedrun = Recommended_items::where('resort_id',$saveArray['resort_id'])->where('recommendation_id',$saveArray['recommendation_id'])->get();
                    $detailArray = [];
                    if(count($getRecommendedrun)){
                        foreach($getRecommendedrun as $ls){
                            $likedByme = Recommendation_notes::where('resort_id',$ls['resort_id'])
                            ->where('recommendation_id',$ls['recommendation_id'])
                            ->where('run_id',$ls['run_id'])
                            ->where('thumbs_up','1')
                            ->where('user_id',$userExist['id'])
                            ->first();
                            if ($likedByme) {
                                $liked_by_me = 'Yes';
                            }else{
                                $liked_by_me = 'No';
                            }
                            $likecount = Recommendation_notes::where('resort_id',$ls['resort_id'])
                            ->where('recommendation_id',$ls['recommendation_id'])
                            ->where('run_id',$ls['run_id'])
                            ->where('thumbs_up','1')
                            ->get()
                            ->count();
                            $runname = Run::where(['id'=>$ls['run_id']])->first();
                            $noteArr = Recommendation_notes::where('resort_id',$ls['resort_id'])
                            ->where('recommendation_id',$ls['recommendation_id'])
                            ->where('run_id',$ls['run_id'])
                            ->where('recommendation_note','!=','')
                            ->select('id','recommendation_note','user_id','created_at')
                            ->get();
                            $noteArray = array();
                            foreach ($noteArr as $value) {
                                $username = User::where(['id'=>$value['user_id']])->first();
                                $note['recommendation_id'] = $value['id'];
                                $note['recommendation_note'] = $value['recommendation_note'];
                                $note['user_id'] = $value['user_id'];
                                $note['username'] = $username['name'];
                                $note['created_at'] = $value['created_at']->format('d-m-Y');
                                $noteArray[] = $note;
                            }
                            $info['id'] = $ls['id'];
                            $info['resort_id'] = $ls['resort_id'];
                            $info['recommendation_id'] = $ls['recommendation_id'];
                            $info['run_id'] = $ls['run_id'];
                            $info['run_name'] = $runname['run_name'];
                            $info['likecount'] = $likecount;
                            $info['liked_by_me'] = $liked_by_me;
                            $info['recommeded_at'] = $ls['created_at']->format('d-m-Y');
                            $info['note_array'] = $noteArray;
                            $detailArray[] = $info;
                        }
                        return response()->json([
                            'status'=>'1',
                            'message'=>'data fetched successfully',
                            'recommended_run'=>$detailArray
                        ], $this->successStatus);
                    }
                    else{
                        return response()->json([
                            'message'=>'No data',
                            'status'=>'0'
                        ], $this->successStatus);
                    }
                }
                else {
                    return response()->json([
                    'status'=>'1',
                    'message'=>'No resort found'
                  ], $this->successStatus);
                }
            }
        }
        else{
          return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }

    /**
     * Store a newly created Guide in storage.
     * POST /guides
     *
     * @param CreateGuideAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateGuideAPIRequest $request)
    {
        $input = $request->all();

        $guide = $this->guideRepository->create($input);

        return $this->sendResponse($guide->toArray(), 'Guide saved successfully');
    }

    /**
     * Display the specified Guide.
     * GET|HEAD /guides/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Guide $guide */
        $guide = $this->guideRepository->find($id);

        if (empty($guide)) {
            return $this->sendError('Guide not found');
        }

        return $this->sendResponse($guide->toArray(), 'Guide retrieved successfully');
    }

    /**
     * Update the specified Guide in storage.
     * PUT/PATCH /guides/{id}
     *
     * @param int $id
     * @param UpdateGuideAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateGuideAPIRequest $request)
    {
        $input = $request->all();

        /** @var Guide $guide */
        $guide = $this->guideRepository->find($id);

        if (empty($guide)) {
            return $this->sendError('Guide not found');
        }

        $guide = $this->guideRepository->update($input, $id);

        return $this->sendResponse($guide->toArray(), 'Guide updated successfully');
    }

    /**
     * Remove the specified Guide from storage.
     * DELETE /guides/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Guide $guide */
        $guide = $this->guideRepository->find($id);

        if (empty($guide)) {
            return $this->sendError('Guide not found');
        }

        $guide->delete();

        return $this->sendResponse($id, 'Guide deleted successfully');
    }
}
