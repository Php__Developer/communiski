<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateEventAPIRequest;
use App\Http\Requests\API\UpdateEventAPIRequest;
use App\Models\Event;
use App\Models\Resort;
use App\Models\User;
use App\Models\Event_attendee;
use App\Models\Friend;
use App\Models\Business;
use App\Repositories\EventRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use DB;
use Validator;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class EventController
 * @package App\Http\Controllers\API
 */

class EventAPIController extends AppBaseController
{
    /** @var  EventRepository */
    private $eventRepository;
    public $unauthorised = 401;
    public $successStatus = 200;
    public $badrequest = 400;

    public function __construct(EventRepository $eventRepo)
    {
        $this->eventRepository = $eventRepo;
    }

    /**
     * Display a listing of the Event.
     * GET|HEAD /events
     *
     * @param Request $request
     * @return Response
     */

    public function addEvent(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
            $validator = Validator::make($request->all(), [
                'event_title' => 'required',
                'resort_id' => 'required',
                'start_date' => 'required',
                // 'end_date' => 'required_with:start_date',
                // 'event_host_id' => 'required',
                // 'event_host_type' => 'required',
                'declaration' => 'required',
              ]);
              if ($validator->fails()) {
                return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
              }
              else{

                $eventtags = array($saveArray['tags']);
                $etags = implode(",", $eventtags);

                if($request->hasFile('event_image')){
                    $file = time().$request->event_image->getClientOriginalName();
                    $request->event_image->move(public_path('/event_images') . '/', $file);
                }
                else{
                    $file = 'eventDefault.jpg';
                }
                $resort_id = $saveArray['resort_id'];
                $event_name = $saveArray['event_title'];
                $start_date = $saveArray['start_date'];
                $end_date = $saveArray['end_date'];
                $event_location = $saveArray['event_location'];
                $latitude = $saveArray['latitude'];
                $longitude = $saveArray['longitude'];
                $event_description = $saveArray['event_description'];
                $tags = $etags;
                $event_host_id = $saveArray['event_host_id'];
                $event_host_type = $saveArray['event_host_type'];
                $event_host = $saveArray['event_host'];
                $event_link = $saveArray['event_link'];
                $event_contact_email = $saveArray['event_contact_email'];
                $event_contact_number = $saveArray['event_contact_number'];
                $event_price = $saveArray['event_price'];
                $declaration = $saveArray['declaration'];

                $addEvent = [
                    'resort_id'=>$resort_id?$resort_id:'',
                    'event_name'=>$event_name,
                    'start_date'=>$start_date,
                    'end_date'=>$end_date,
                    'event_location'=>$event_location?$event_location:'',
                    'latitude'=>$latitude,
                    'longitude'=>$longitude,
                    'event_description'=>$event_description?$event_description:'',
                    'tags'=>$tags?$tags:'',
                    'event_image'=>$file,
                    'event_host'=>$event_host?$event_host:'',
                    'event_host_id'=>$event_host_id?$event_host_id:'',
                    'event_host_type'=>$event_host_type?$event_host_type:'',
                    'event_link'=>$event_link?$event_link:'',
                    'event_contact_email'=>$event_contact_email?$event_contact_email:'',
                    'event_contact_number'=>$event_contact_number?$event_contact_number:'',
                    'event_price'=>$event_price,
                    'declaration'=>$declaration,
                    'created_at'=>date('Y-m-d h:i:s'),
                ];
                // print_r($addEvent);die();
                $event_id = Event::insertGetId($addEvent);
                if($event_id){
                    return response()->json([
                        'status'=>'1',
                        'message'=>'Event Added Successfully',
                    ], $this->successStatus);
                }
                else{
                    return response()->json([
                        'message'=>'Error',
                        'status'=>'0'
                    ], $this->badrequest);
                }
            }
        }
        else{
            return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }

    public function getEvents(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
          $validator = Validator::make($request->all(), [  
            'resort_id' => 'required'
          ]);
          if ($validator->fails()) { 
            return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);            
          }
          else{
            $resortDetail = Resort::where('id',$saveArray['resort_id'])->first();
            $start_date = date('Y-m-d');
            $end_date = date('Y-m-d', strtotime('+7 days'));

            $latitude = $resortDetail['latitude'];
            $longitude = $resortDetail['longitude'];

            $event = Event::selectRaw(" *, ( 6371000 * acos( cos( radians(?) ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians(?) ) + sin( radians(?) ) * sin( radians( latitude ) ) ) ) AS distance", [$latitude, $longitude, $latitude])
            ->whereBetween('created_at', [$start_date, $end_date])
            ->having("distance", "<", '5000')
            ->orderBy("distance",'asc')
            ->offset(0)
            ->limit(20)
            ->get();

            /*getbuddies*/
            $getBuddies = Friend::where(['sender_id'=>$userExist['id']])
            ->where(['status'=>'accepted'])
            ->orwhere(['reciever_id'=>$userExist['id']])
            ->where(['status'=>'accepted'])->get();
            $buddiesList = [];
            foreach($getBuddies as $ls){
                if($userExist['id'] == $ls['reciever_id']){
                    $loginUser = $ls['sender_id'];
                }elseif( $userExist['id'] == $ls['sender_id']){
                    $loginUser = $ls['reciever_id'];
                }
                $userDetails = User::where('id',$loginUser)
                ->select('id')
                ->first();
                $listing1['user_id'] = $userDetails['id'];
                $buddiesList[] = $listing1['user_id'];
            }
            /*getbuddies*/
            // print_r(json_encode($getBuddies));die();
            $events = json_decode(json_encode($event), true);
            $record = [];
            if(count($events)){
              foreach($events as $ls){
                if ($ls['event_host_type'] == 'business') {
                    $btype = "B";
                    $bdetail = Business::where('id',$ls['event_host_id'])->first();
                    $bid = $bdetail['id'];
                    $bname = $bdetail['business_name'];
                    $bdet = $bdetail['business_type'];
                }
                elseif ($ls['event_host_type'] == 'user') {
                    $btype = "U";
                    $bid = '';
                    $bname = '';
                    $bdetail = User::where('id',$ls['event_host_id'])->first();
                    $bdet = '';
                }
                elseif ($ls['event_host_type'] == 'resort') {
                    $btype = "R";
                    $bid = '';
                    $bname = '';
                    $bdetail = Resort::where('id',$ls['event_host_id'])->first();
                    $bdet = '';
                }
                else{
                    $btype = '';
                    $bid = '';
                    $bname = '';
                    $bdet = '';
                }
                $attendStatus = Event_attendee::where('event_id',$ls['id'])
                ->where('user_id',$userExist['id'])->first();
                if ($ls['declaration'] == '1') {
                    $dec = 'yes';
                }else{
                    $dec = 'no';
                }
                $getbuddsevents = Event_attendee::whereIn('user_id',$buddiesList)
                ->where('event_id',$ls['id'])
                ->get();
                $buddevent = [];
                foreach ($getbuddsevents as $budd) {
                    $username = User::where('id',$budd['user_id'])->first();
                    $image = url('/images',$username['image']);
                    $even['id'] = (String)$budd['id'];
                    $even['event_id'] = $budd['event_id'];
                    $even['user_id'] = $budd['user_id'];
                    $even['username'] = $username['name'];
                    $even['userImage'] = $image;
                    $even['attend_status'] = $budd['attend_status'];
                    $even['created_at'] = $budd['created_at']->format('Y-m-d H:i:s');
                    $buddevent[] = $even;
                }
                $eventimage = url('/event_images',$ls['event_image']);
                $listing['id'] = (String)$ls['id'];
                $listing['attend_status'] = $attendStatus['attend_status']?$attendStatus['attend_status']:'';
                $listing['type'] = $btype;
                $listing['business_id'] = $bid;
                $listing['business_name'] = $bname;
                $listing['business_type'] = $bdet?$bdet:'';
                $listing['event_host'] = $ls['event_host'];
                $listing['event_host_id'] = $ls['event_host_id']?$ls['event_host_id']:'';
                $listing['event_host_type'] = $ls['event_host_type']?$ls['event_host_type']:'';
                $listing['event_name'] = $ls['event_name'];
                $listing['start_date'] = $ls['start_date']?$ls['start_date']:'';
                $listing['end_date'] = $ls['end_date']?$ls['end_date']:'';
                $listing['event_location'] = $ls['event_location'];
                $listing['latitude'] = $ls['latitude'];
                $listing['longitude'] = $ls['longitude'];
                $listing['event_description'] = $ls['event_description'];
                $listing['tags'] = $ls['tags'];
                $listing['event_image'] = $eventimage;
                $listing['event_link'] = $ls['event_link'];
                $listing['event_contact_email'] = $ls['event_contact_email'];
                $listing['event_contact_number'] = $ls['event_contact_number'];
                $listing['event_price'] = $ls['event_price'];
                $listing['declaration'] = $ls['declaration'];
                $listing['declaration'] = $ls['declaration'];
                $listing['declaration_value'] = $dec;
                $listing['getbuddsevents']= $buddevent?$buddevent:'';
                $record[] = $listing;
              }
              return response()->json([
                'status'=>'1',
                'message'=>'Events fetched successfully',
                'eventListing'=>$record
              ], $this->successStatus);
            }
            else{
              return response()->json([
                'message'=>'No events Found',
                'status'=>'0'
              ], $this->successStatus);
            }
          }
        }
        else{
          return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }

    public function businessEvents(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
          $validator = Validator::make($request->all(), [  
            'id' => 'required',
            'type' => 'required'
          ]);
          if ($validator->fails()) { 
            return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);            
          }
          else{
            // $resortDetail = Resort::where('id',$saveArray['resort_id'])->first();
            $start_date = date('Y-m-d');
            $end_date = date('Y-m-d', strtotime('+7 days'));

            // $latitude = $resortDetail['latitude'];
            // $longitude = $resortDetail['longitude'];

            // $event = Event::selectRaw(" *, ( 6371000 * acos( cos( radians(?) ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians(?) ) + sin( radians(?) ) * sin( radians( latitude ) ) ) ) AS distance", [$latitude, $longitude, $latitude])
            // ->whereBetween('created_at', [$start_date, $end_date])
            // ->having("distance", "<", '5')
            // ->orderBy("distance",'asc')
            // ->offset(0)
            // ->limit(20)
            // ->get();

            // print_r(json_encode($event));die();
            /*getbuddies*/
            $getBuddies = Friend::where(['sender_id'=>$userExist['id']])
            ->where(['status'=>'accepted'])
            ->orwhere(['reciever_id'=>$userExist['id']])
            ->where(['status'=>'accepted'])->get();
            $buddiesList = [];
            foreach($getBuddies as $ls){
                if($userExist['id'] == $ls['reciever_id']){
                    $loginUser = $ls['sender_id'];
                }elseif( $userExist['id'] == $ls['sender_id']){
                    $loginUser = $ls['reciever_id'];
                }
                $userDetails = User::where('id',$loginUser)
                ->select('id')
                ->first();
                $listing1['user_id'] = $userDetails['id'];
                $buddiesList[] = $listing1['user_id'];
            }
            /*getbuddies*/
            $event = Event::where('event_host_id',$saveArray['id'])
            ->where('event_host_type',$saveArray['type'])
            ->whereBetween('created_at', [$start_date, $end_date])
            ->get();
            $events = json_decode(json_encode($event), true);
            $record = [];
            if(count($events)){
              foreach($events as $ls){
                if ($ls['event_host_type'] == 'business') {
                    $btype = "B";
                    $bdetail = Business::where('id',$ls['event_host_id'])->first();
                    $bid = $bdetail['id'];
                    $bname = $bdetail['business_name'];
                }
                if ($ls['event_host_type'] == 'user') {
                    $btype = "U";
                    $bid = '';
                    $bname = '';
                }
                if ($ls['event_host_type'] == 'resort') {
                    $btype = "R";
                    $bid = '';
                    $bname = '';
                }
                $attendStatus = Event_attendee::where('event_id',$ls['id'])
                ->where('user_id',$userExist['id'])->first();
                if ($ls['declaration'] == '1') {
                    $dec = 'yes';
                }else{
                    $dec = 'no';
                }
                $eventimage = url('/event_images',$ls['event_image']);
                $getbuddsevents = Event_attendee::whereIn('user_id',$buddiesList)
                ->where('event_id',$ls['id'])
                ->get();
                $buddevent = [];
                foreach ($getbuddsevents as $budd) {
                    $username = User::where('id',$budd['user_id'])->first();
                    $image = url('/images',$username['image']);
                    $even['id'] = (String)$budd['id'];
                    $even['event_id'] = $budd['event_id'];
                    $even['user_id'] = $budd['user_id'];
                    $even['username'] = $username['name'];
                    $even['userImage'] = $image;
                    $even['attend_status'] = $budd['attend_status'];
                    $even['created_at'] = $budd['created_at']->format('Y-m-d H:i:s');
                    $buddevent[] = $even;
                }
                $listing['attend_status'] = $attendStatus['attend_status']?$attendStatus['attend_status']:'';
                $listing['id'] = (String)$ls['id'];
                $listing['type'] = $btype;
                $listing['business_id'] = $bid;
                $listing['business_name'] = $bname;
                $listing['business_type'] = $bdetail['business_type']?$bdetail['business_type']:'';
                $listing['event_name'] = $ls['event_name'];
                $listing['start_date'] = $ls['start_date']?$ls['start_date']:'';
                $listing['end_date'] = $ls['end_date']?$ls['end_date']:'';
                $listing['event_location'] = $ls['event_location'];
                $listing['latitude'] = $ls['latitude'];
                $listing['longitude'] = $ls['longitude'];
                $listing['event_description'] = $ls['event_description'];
                $listing['tags'] = $ls['tags'];
                $listing['event_image'] = $eventimage;
                $listing['event_host'] = $ls['event_host'];
                $listing['event_host_id'] = $ls['event_host_id'];
                $listing['event_host_type'] = $ls['event_host_type'];
                $listing['event_link'] = $ls['event_link'];
                $listing['event_contact_email'] = $ls['event_contact_email'];
                $listing['event_contact_number'] = $ls['event_contact_number'];
                $listing['event_price'] = $ls['event_price'];
                $listing['declaration'] = $ls['declaration'];
                $listing['declaration'] = $ls['declaration'];
                $listing['declaration_value'] = $dec;
                $listing['getbuddsevents']= $buddevent?$buddevent:'';
                $record[] = $listing;
              }
              return response()->json([
                'status'=>'1',
                'message'=>'Events fetched successfully',
                'eventListing'=>$record
              ], $this->successStatus);
            }
            else{
              return response()->json([
                'message'=>'No events Found',
                'status'=>'0'
              ], $this->successStatus);
            }
          }
        }
        else{
          return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }

    public function buddysEvent(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
          $validator = Validator::make($request->all(), [  
            'friend_id' => 'required'
          ]);
          if ($validator->fails()) { 
            return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);            
          }
          else{
            $checkFriend = Friend::where(['sender_id'=>$userExist['id']])
            ->where(['reciever_id'=>$saveArray['friend_id']])
            ->where(['status'=>'accepted'])
            ->orwhere(['sender_id'=>$saveArray['friend_id']])
            ->where(['reciever_id'=>$userExist['id']])
            ->where(['status'=>'accepted'])
            ->first();
            // print_r($checkFriend);die();
            if ($checkFriend) {
                $goingDetails = Event_attendee::where('user_id',$saveArray['friend_id'])
                ->where('attend_status','=','1')
                ->get();
                $goingeventsData = json_decode($goingDetails, true);

                $maybeDetails = Event_attendee::where('user_id',$saveArray['friend_id'])
                ->where('attend_status','=','2')
                ->get();
                $maybeeventsData = json_decode($maybeDetails, true);

                $goingArray = [];
                $maybeArray = [];

                foreach($goingeventsData as $ls){
                    $eventDe = Event::where('id',$ls['event_id'])->first();
                    $eventDetail = json_decode($eventDe, true);
                    if ($eventDetail['event_host_type'] == 'business') {
                        $btype = "B";
                        $bdetail = Business::where('id',$eventDetail['event_host_id'])->first();
                        $bid = $bdetail['id'];
                        $bname = $bdetail['business_name'];
                        $bdet = $bdetail['business_type'];
                    }
                    elseif ($eventDetail['event_host_type'] == 'user') {
                        $btype = "U";
                        $bid = '';
                        $bname = '';
                        $bdetail = User::where('id',$eventDetail['event_host_id'])->first();
                        $bdet = '';
                    }
                    elseif ($eventDetail['event_host_type'] == 'resort') {
                        $btype = "R";
                        $bid = '';
                        $bname = '';
                        $bdetail = Resort::where('id',$eventDetail['event_host_id'])->first();
                        $bdet = '';
                    }
                    else{
                        $btype = '';
                        $bid = '';
                        $bname = '';
                        $bdet = '';
                    }
                    $attendStatus = Event_attendee::where('event_id',$ls['event_id'])
                    ->where('user_id',$saveArray['friend_id'])
                    ->first();
                    $myattendStatus = Event_attendee::where('event_id',$ls['event_id'])
                    ->where('user_id',$userExist['id'])
                    ->first();
                    $eventimage = url('/event_images',$eventDetail['event_image']);
                    $listing['type'] = $btype;
                    $listing['business_id'] = $bid;
                    $listing['business_name'] = $bname;
                    $listing['business_type'] = $bdet?$bdet:'';
                    $listing['my_attend_status'] = $myattendStatus['attend_status']?$myattendStatus['attend_status']:'';
                    $listing['attend_status'] = $attendStatus['attend_status']?$attendStatus['attend_status']:'';
                    $listing['event_id'] = (String)$ls['event_id'];
                    $listing['event_name'] = $eventDetail['event_name'];
                    $listing['start_date'] = $eventDetail['start_date']?$eventDetail['start_date']:'';
                    $listing['end_date'] = $eventDetail['end_date']?$eventDetail['end_date']:'';
                    $listing['event_location'] = $eventDetail['event_location'];
                    $listing['latitude'] = $eventDetail['latitude'];
                    $listing['longitude'] = $eventDetail['longitude'];
                    $listing['event_description'] = $eventDetail['event_description'];
                    $listing['tags'] = $eventDetail['tags'];
                    $listing['event_image'] = $eventimage;
                    $listing['event_host'] = $eventDetail['event_host'];
                    $listing['event_host_id'] = $eventDetail['event_host_id'];
                    $listing['event_host_type'] = $eventDetail['event_host_type'];
                    $listing['event_link'] = $eventDetail['event_link'];
                    $listing['event_contact_email'] = $eventDetail['event_contact_email'];
                    $listing['event_contact_number'] = $eventDetail['event_contact_number'];
                    $listing['event_price'] = $eventDetail['event_price'];
                    $listing['declaration'] = $eventDetail['declaration'];
                    $goingArray[] = $listing;
                  }
                  foreach($maybeeventsData as $ls1){
                    $eventDe1 = Event::where('id',$ls1['event_id'])->first();
                    $eventDetail1 = json_decode($eventDe1, true);
                    if ($eventDetail1['event_host_type'] == 'business') {
                        $btype = "B";
                        $bdetail = Business::where('id',$eventDetail1['event_host_id'])->first();
                        $bid = $bdetail['id'];
                        $bname = $bdetail['business_name'];
                        $bdet = $bdetail['business_type'];
                    }
                    elseif ($eventDetail1['event_host_type'] == 'user') {
                        $btype = "U";
                        $bid = '';
                        $bname = '';
                        $bdetail = User::where('id',$eventDetail1['event_host_id'])->first();
                        $bdet = '';
                    }
                    elseif ($eventDetail1['event_host_type'] == 'resort') {
                        $btype = "R";
                        $bid = '';
                        $bname = '';
                        $bdetail = Resort::where('id',$eventDetail1['event_host_id'])->first();
                        $bdet = '';
                    }
                    else{
                        $btype = '';
                        $bid = '';
                        $bname = '';
                        $bdet = '';
                    }
                    $attendStatus1 = Event_attendee::where('event_id',$ls1['event_id'])
                    ->where('user_id',$userExist['id'])
                    ->first();
                    $myattendStatus1 = Event_attendee::where('event_id',$ls1['event_id'])
                    ->where('user_id',$userExist['id'])
                    ->first();
                    $eventimage = url('/event_images',$eventDetail1['event_image']);
                    $listing1['type'] = $btype;
                    $listing1['business_id'] = $bid;
                    $listing1['business_name'] = $bname;
                    $listing1['business_type'] = $bdet?$bdet:'';
                    $listing1['my_attend_status'] = $myattendStatus1['attend_status']?$myattendStatus1['attend_status']:'';
                    $listing1['attend_status'] = $attendStatus1['attend_status']?$attendStatus1['attend_status']:'';
                    $listing1['event_id'] = (String)$ls1['event_id'];
                    $listing1['event_name'] = $eventDetail1['event_name'];
                    $listing1['start_date'] = $eventDetail1['start_date']?$eventDetail1['start_date']:'';
                    $listing1['end_date'] = $eventDetail1['end_date']?$eventDetail1['end_date']:'';
                    $listing1['event_location'] = $eventDetail1['event_location'];
                    $listing1['latitude'] = $eventDetail1['latitude'];
                    $listing1['longitude'] = $eventDetail1['longitude'];
                    $listing1['event_description'] = $eventDetail1['event_description'];
                    $listing1['tags'] = $eventDetail1['tags'];
                    $listing1['event_image'] = $eventimage;
                    $listing1['event_host'] = $eventDetail1['event_host'];
                    $listing1['event_host_id'] = $eventDetail1['event_host_id'];
                    $listing1['event_host_type'] = $eventDetail1['event_host_type'];
                    $listing1['event_link'] = $eventDetail1['event_link'];
                    $listing1['event_contact_email'] = $eventDetail1['event_contact_email'];
                    $listing1['event_contact_number'] = $eventDetail1['event_contact_number'];
                    $listing1['event_price'] = $eventDetail1['event_price'];
                    $listing1['declaration'] = $eventDetail1['declaration'];
                    $maybeArray[] = $listing1;
                  }
                  return response()->json([
                    'status'=>'1',
                    'message'=>'Buddies Events fetched successfully',
                    'goingArray'=>$goingArray,
                    'maybeArray'=>$maybeArray
                  ], $this->successStatus);
            }else{
                return response()->json([
                    'status'=>'1',
                    'message'=>'No events',
                    'goingArray'=>[],
                    'maybeArray'=>[]
                  ], $this->successStatus);
            }
            
          }
      }
        else{
          return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }

    public function myevents(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){

            $goingDetails = Event_attendee::where('user_id',$userExist['id'])
            ->where('attend_status','=','1')
            ->get();
            $goingeventsData = json_decode($goingDetails, true);

            $maybeDetails = Event_attendee::where('user_id',$userExist['id'])
            ->where('attend_status','=','2')
            ->get();
            $maybeeventsData = json_decode($maybeDetails, true);

            /*getbuddies*/
            $getBuddies = Friend::where(['sender_id'=>$userExist['id']])
            ->where(['status'=>'accepted'])
            ->orwhere(['reciever_id'=>$userExist['id']])
            ->where(['status'=>'accepted'])->get();
            $buddiesList = [];
            foreach($getBuddies as $ls){
                if($userExist['id'] == $ls['reciever_id']){
                    $loginUser = $ls['sender_id'];
                }elseif( $userExist['id'] == $ls['sender_id']){
                    $loginUser = $ls['reciever_id'];
                }
                $userDetails = User::where('id',$loginUser)
                ->select('id')
                ->first();
                $listing1['user_id'] = $userDetails['id'];
                $buddiesList[] = $listing1['user_id'];
            }
            /*getbuddies*/

            $goingArray = [];
            $maybeArray = [];

            foreach($goingeventsData as $ls){
                $getbuddsevents = Event_attendee::whereIn('user_id',$buddiesList)
                ->where('event_id',$ls['id'])
                ->get();
                $buddevent = [];
                foreach ($getbuddsevents as $budd) {
                    $username = User::where('id',$budd['user_id'])->first();
                    $image = url('/images',$username['image']);
                    $even['id'] = (String)$budd['id'];
                    $even['event_id'] = $budd['event_id'];
                    $even['user_id'] = $budd['user_id'];
                    $even['username'] = $username['name'];
                    $even['userImage'] = $image;
                    $even['attend_status'] = $budd['attend_status'];
                    $even['created_at'] = $budd['created_at']->format('Y-m-d H:i:s');
                    $buddevent[] = $even;
                }

                $eventD = Event::where('id',$ls['event_id'])->first();
                $eventDetail = json_decode($eventD, true);
                $attendStatus = Event_attendee::where('event_id',$ls['event_id'])
                ->where('user_id',$userExist['id'])
                ->first();
                if ($eventDetail['event_host_type'] == 'business') {
                    $btype = "B";
                    $bdetail = Business::where('id',$eventDetail['event_host_id'])->first();
                    $bid = $bdetail['id'];
                    $bname = $bdetail['business_name'];
                    $bdet = $bdetail['business_type'];
                }
                elseif ($eventDetail['event_host_type'] == 'user') {
                    $btype = "U";
                    $bid = '';
                    $bname = '';
                    $bdetail = User::where('id',$eventDetail['event_host_id'])->first();
                    $bdet = '';
                }
                elseif ($eventDetail['event_host_type'] == 'resort') {
                    $btype = "R";
                    $bid = '';
                    $bname = '';
                    $bdetail = Resort::where('id',$eventDetail['event_host_id'])->first();
                    $bdet = '';
                }
                else{
                    $btype = '';
                    $bid = '';
                    $bname = '';
                    $bdet = '';
                }
                $eventimage = url('/event_images',$eventDetail['event_image']);
                $listing['type'] = $btype;
                $listing['business_id'] = $bid;
                $listing['business_name'] = $bname;
                $listing['business_type'] = $bdet?$bdet:'';
                $listing['attend_status'] = $attendStatus['attend_status']?$attendStatus['attend_status']:'';
                $listing['event_id'] = (String)$ls['event_id'];
                $listing['event_name'] = $eventDetail['event_name'];
                $listing['start_date'] = $eventDetail['start_date']?$eventDetail['start_date']:'';
                $listing['end_date'] = $eventDetail['end_date']?$eventDetail['end_date']:'';
                $listing['event_location'] = $eventDetail['event_location'];
                $listing['latitude'] = $eventDetail['latitude'];
                $listing['longitude'] = $eventDetail['longitude'];
                $listing['event_description'] = $eventDetail['event_description'];
                $listing['tags'] = $eventDetail['tags'];
                $listing['event_image'] = $eventimage;
                $listing['event_host'] = $eventDetail['event_host'];
                $listing['event_host_id'] = $eventDetail['event_host_id'];
                $listing['event_host_type'] = $eventDetail['event_host_type'];
                $listing['event_link'] = $eventDetail['event_link'];
                $listing['event_contact_email'] = $eventDetail['event_contact_email'];
                $listing['event_contact_number'] = $eventDetail['event_contact_number'];
                $listing['event_price'] = $eventDetail['event_price'];
                $listing['declaration'] = $eventDetail['declaration'];
                $listing['getbuddsevents']= $buddevent?$buddevent:'';
                $goingArray[] = $listing;
              }
              foreach($maybeeventsData as $ls1){
                $getbuddsevents = Event_attendee::whereIn('user_id',$buddiesList)
                ->where('event_id',$ls1['id'])
                ->get();
                $buddevent = [];
                foreach ($getbuddsevents as $budd) {
                    $username = User::where('id',$budd['user_id'])->first();
                    $image = url('/images',$username['image']);
                    $even['id'] = (String)$budd['id'];
                    $even['event_id'] = $budd['event_id'];
                    $even['user_id'] = $budd['user_id'];
                    $even['username'] = $username['name'];
                    $even['userImage'] = $image;
                    $even['attend_status'] = $budd['attend_status'];
                    $even['created_at'] = $budd['created_at']->format('Y-m-d H:i:s');
                    $buddevent[] = $even;
                }
                $eventDetai = Event::where('id',$ls1['event_id'])->first();
                $eventDetail1 = json_decode($eventDetai, true);
                $attendStatus1 = Event_attendee::where('event_id',$ls1['event_id'])
                ->where('user_id',$userExist['id'])
                ->first();
                if ($eventDetail1['event_host_type'] == 'business') {
                    $btype = "B";
                    $bdetail = Business::where('id',$eventDetail1['event_host_id'])->first();
                    $bid = $bdetail['id'];
                    $bname = $bdetail['business_name'];
                    $bdet = $bdetail['business_type'];
                }
                elseif ($eventDetail1['event_host_type'] == 'user') {
                    $btype = "U";
                    $bid = '';
                    $bname = '';
                    $bdetail = User::where('id',$eventDetail1['event_host_id'])->first();
                    $bdet = '';
                }
                elseif ($eventDetail1['event_host_type'] == 'resort') {
                    $btype = "R";
                    $bid = '';
                    $bname = '';
                    $bdetail = Resort::where('id',$eventDetail1['event_host_id'])->first();
                    $bdet = '';
                }
                else{
                    $btype = '';
                    $bid = '';
                    $bname = '';
                    $bdet = '';
                }
                $eventimage = url('/event_images',$eventDetail1['event_image']);
                $listing1['type'] = $btype;
                $listing1['business_id'] = $bid;
                $listing1['business_name'] = $bname;
                $listing1['business_type'] = $bdet?$bdet:'';
                $listing1['attend_status'] = $attendStatus1['attend_status']?$attendStatus1['attend_status']:'';
                $listing1['event_id'] = (String)$ls1['event_id'];
                $listing1['event_name'] = $eventDetail1['event_name'];
                $listing1['start_date'] = $eventDetail1['start_date'];
                $listing1['end_date'] = $eventDetail1['end_date'];
                $listing1['event_location'] = $eventDetail1['event_location'];
                $listing1['latitude'] = $eventDetail1['latitude'];
                $listing1['longitude'] = $eventDetail1['longitude'];
                $listing1['event_description'] = $eventDetail1['event_description'];
                $listing1['tags'] = $eventDetail1['tags'];
                $listing1['event_image'] = $eventimage;
                $listing1['event_host'] = $eventDetail1['event_host'];
                $listing1['event_host_id'] = $eventDetail1['event_host_id'];
                $listing1['event_host_type'] = $eventDetail1['event_host_type'];
                $listing1['event_link'] = $eventDetail1['event_link'];
                $listing1['event_contact_email'] = $eventDetail1['event_contact_email'];
                $listing1['event_contact_number'] = $eventDetail1['event_contact_number'];
                $listing1['event_price'] = $eventDetail1['event_price'];
                $listing1['declaration'] = $eventDetail1['declaration'];
                $listing1['getbuddsevents']= $buddevent?$buddevent:'';
                $maybeArray[] = $listing1;
              }
              return response()->json([
                'status'=>'1',
                'message'=>'My events fetched successfully',
                'goingArray'=>$goingArray,
                'maybeArray'=>$maybeArray
              ], $this->successStatus);
            }
        else{
            return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }

    public function eventDetails(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
            $validator = Validator::make($request->all(), [
                'event_id' => 'required'
            ]);
            if ($validator->fails()) {
                return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
            }
            else{
                $event = Event::where('id',$saveArray['event_id'])->first();
                if ($event) {
                    /*getbuddies*/
                    $getBuddies = Friend::where(['sender_id'=>$userExist['id']])
                    ->where(['status'=>'accepted'])
                    ->orwhere(['reciever_id'=>$userExist['id']])
                    ->where(['status'=>'accepted'])->get();
                    $buddiesList = [];
                    foreach($getBuddies as $ls){
                        if($userExist['id'] == $ls['reciever_id']){
                            $loginUser = $ls['sender_id'];
                        }elseif( $userExist['id'] == $ls['sender_id']){
                            $loginUser = $ls['reciever_id'];
                        }
                        $userDetails = User::where('id',$loginUser)
                        ->select('id')
                        ->first();
                        $listing1['user_id'] = $userDetails['id'];
                        $buddiesList[] = $listing1['user_id'];
                    }
                    /*getbuddies*/
                    $getbuddsevents = Event_attendee::whereIn('user_id',$buddiesList)
                    ->where('event_id',$event['id'])
                    ->get();
                    $buddevent = [];
                    foreach ($getbuddsevents as $budd) {
                        $username = User::where('id',$budd['user_id'])->first();
                        $image = url('/images',$username['image']);
                        $even['id'] = (String)$budd['id'];
                        $even['event_id'] = $budd['event_id'];
                        $even['user_id'] = $budd['user_id'];
                        $even['username'] = $username['name'];
                        $even['userImage'] = $image;
                        $even['attend_status'] = $budd['attend_status'];
                        $even['created_at'] = $budd['created_at']->format('Y-m-d H:i:s');
                        $buddevent[] = $even;
                    }
                    $attendStatus = Event_attendee::where('event_id',$event['id'])
                    ->where('user_id',$userExist['id'])->first();
                    $image = url('/event_images',$event['event_image']);
                    $eventDetail = array(
                        'attend_status' => $attendStatus['attend_status']?$attendStatus['attend_status']:'',
                        'id'=>(String)$event['id'],
                        'event_name'=>$event['event_name'],
                        'start_date'=>$event['start_date']->format('Y-m-d H:i:s'),
                        'end_date'=>$event['end_date']->format('Y-m-d H:i:s'),
                        'event_location'=>$event['event_location']?$event['event_location']:'',
                        'event_description'=>$event['event_description']?$event['event_description']:'',
                        'tags'=>$event['tags']?$event['tags']:'',
                        'event_host'=>$event['event_host']?$event['event_host']:'',
                        'event_host_id'=>$event['event_host_id']?$event['event_host_id']:'',
                        'event_host_type'=>$event['event_host_type']?$event['event_host_type']:'',
                        'event_link'=>$event['event_link']?$event['event_link']:'',
                        'event_contact_email'=>$event['event_contact_email']?$event['event_contact_email']:'',
                        'event_contact_number'=>$event['event_contact_number']?$event['event_contact_number']:'',
                        'event_price'=>$event['event_price']?$event['event_price']:'',
                        'getbuddsevents'=> $buddevent?$buddevent:'',
                        'created_at'=>$event['created_at']->format('Y-m-d H:i:s'),
                    );
                    return response()->json(['status'=>'1','message'=>'Event fetched successfully','eventDetail'=>$eventDetail], $this->successStatus);
                }else{
                    return response()->json(['status'=>'1','message'=>'No Event Found'], $this->successStatus);
                }
            }
        }
        else{
            return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }

    public function attendEvent(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
            $validator = Validator::make($request->all(), [
                'event_id' => 'required',
                'attend_status' => 'required'
                ]);
            if ($validator->fails()) {
                return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
            }
            else{
                $checkattendee = Event_attendee::where('user_id',$userExist['id'])
                ->where('event_id',$saveArray['event_id'])
                ->first();
                if ($checkattendee) {
                    $updateData = [
                        'attend_status'=>$saveArray['attend_status'],
                        'updated_at'=>date('Y-m-d h:i:s'),
                    ];
                    $update = Event_attendee::where('user_id',$userExist['id'])
                    ->where('event_id',$saveArray['event_id'])
                    ->update($updateData);
                    if($update){
                        return response()->json([
                            'status'=>'1',
                            'message'=>'Status updated successfully',
                        ], $this->successStatus);
                    }
                    else{
                        return response()->json([
                            'status'=>'0',
                            'message'=>'error'
                        ], $this->badrequest);
                    }
                }else{
                    $attendStatus = [
                        'user_id'=>$userExist['id'],
                        'attend_status'=>$saveArray['attend_status'],
                        'event_id'=>$saveArray['event_id'],
                        'created_at'=>date('Y-m-d h:i:s'),
                    ];
                    $attendID = Event_attendee::insertGetId($attendStatus);
                    if($attendID){
                        return response()->json([
                            'status'=>'1',
                            'message'=>'Status saved successfully',
                        ], $this->successStatus);
                    }
                    else{
                        return response()->json([
                            'status'=>'0',
                            'message'=>'error'
                        ], $this->badrequest);
                    }
                }
            }
        }
        else{
            return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }

    public function searchEvent(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
            $validator = Validator::make($request->all(), [
                'resort_id'=>'required',
                // 'event_name_desc'=>'required_without_all:start_date,tags',
                // 'start_date' => 'required_without_all:event_name_desc,tags',
                // 'end_date' => 'required_without_all:start_date,event_name_desc,tags',
                // 'tags' => 'required_without_all:start_date,event_name_desc',
                ]);
            if ($validator->fails()) {
                return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
            }
            else{
                $start_date = date('Y-m-d');
                $end_date = date('Y-m-d', strtotime('+7 days'));
                /*DB::enableQueryLog();
                dd(DB::getQueryLog());

                /*getbuddies*/
                $getBuddies = Friend::where(['sender_id'=>$userExist['id']])
                ->where(['status'=>'accepted'])
                ->orwhere(['reciever_id'=>$userExist['id']])
                ->where(['status'=>'accepted'])->get();
                $buddiesList = [];
                foreach($getBuddies as $ls){
                    if($userExist['id'] == $ls['reciever_id']){
                        $loginUser = $ls['sender_id'];
                    }elseif( $userExist['id'] == $ls['sender_id']){
                        $loginUser = $ls['reciever_id'];
                    }
                    $userDetails = User::where('id',$loginUser)
                    ->select('id')
                    ->first();
                    $listing1['user_id'] = $userDetails['id'];
                    $buddiesList[] = $listing1['user_id'];
                }
                /*getbuddies*/

                if ( ( empty($saveArray['area']) && empty($saveArray['radius']) ) || !empty($saveArray['event_name_desc']) || !empty($saveArray['tags']) || !empty($saveArray['start_date']) || !empty($saveArray['end_date'])  ) 
                {

                $query = Event::query();
                
                if ( $saveArray['event_name_desc'] ) {
                    $query = $query->where('event_name','like','%'.$saveArray['event_name_desc'].'%');
                    $query = $query->orWhere('event_description','like','%'.$saveArray['event_name_desc'].'%');
                }
                if ( $saveArray['tags'] )  {
                    $tags = explode(',',$saveArray['tags']);
                    $query = $query->where(function($query) use($tags) {
                        foreach($tags as $tag) {
                            $query->orWhere('tags', 'like', "%$tag%");
                        };
                    })
                    ->inRandomOrder();
                }
                if ( $saveArray['start_date'] && empty($saveArray['end_date']) ){
                    $query = $query->where('start_date','>=',$saveArray['start_date']);
                }
                if ( $saveArray['start_date'] && $saveArray['end_date'] ) {
                    $start = $saveArray['start_date'];
                    $end = $saveArray['end_date'];
                    $query = $query->whereDate('start_date', '>=', $start)
                    ->whereDate('end_date', '<=', $end);
                }
                if ( empty($saveArray['area']) && empty($saveArray['radius']) && empty($saveArray['start_date']) && empty($saveArray['end_date']) && empty($saveArray['event_name_desc']) && empty($saveArray['tags']) )  {

                    $query = $query->where('start_date','>=',$start_date);
                }

                $events = $query->where('resort_id',$saveArray['resort_id'])->get();
                // print_r(json_encode($events));die();
                $event = json_decode(json_encode($events), true);
                $record = [];
                if(count($event)){
                  foreach($event as $ls){
                    if ($ls['event_host_type'] == 'business') {
                        $btype = "B";
                        $bdetail = Business::where('id',$ls['event_host_id'])->first();
                        $bid = $bdetail['id'];
                        $bname = $bdetail['business_name'];
                        $bdet = $bdetail['business_type'];
                    }
                    elseif ($ls['event_host_type'] == 'user') {
                        $btype = "U";
                        $bid = '';
                        $bname = '';
                        $bdetail = User::where('id',$ls['event_host_id'])->first();
                        $bdet = '';
                    }
                    elseif ($ls['event_host_type'] == 'resort') {
                        $btype = "R";
                        $bid = '';
                        $bname = '';
                        $bdetail = Resort::where('id',$ls['event_host_id'])->first();
                        $bdet = '';
                    }
                    else{
                        $btype = '';
                        $bid = '';
                        $bname = '';
                        $bdet = '';
                    }
                    $attendStatus = Event_attendee::where('event_id',$ls['id'])
                    ->where('user_id',$userExist['id'])->first();
                    if ($ls['declaration'] == '1') {
                        $dec = 'yes';
                    }else{
                        $dec = 'no';
                    }
                    $eventimage = url('/event_images',$ls['event_image']);
                    $getbuddsevents = Event_attendee::whereIn('user_id',$buddiesList)
                    ->where('event_id',$ls['id'])
                    ->get();
                    $buddevent = [];
                    foreach ($getbuddsevents as $budd) {
                        $username = User::where('id',$budd['user_id'])->first();
                        $image = url('/images',$username['image']);
                        $even['id'] = (String)$budd['id'];
                        $even['event_id'] = $budd['event_id'];
                        $even['user_id'] = $budd['user_id'];
                        $even['username'] = $username['name'];
                        $even['userImage'] = $image;
                        $even['attend_status'] = $budd['attend_status'];
                        $even['created_at'] = $budd['created_at']->format('Y-m-d H:i:s');
                        $buddevent[] = $even;
                    }
                    $listing['attend_status'] = $attendStatus['attend_status']?$attendStatus['attend_status']:'';
                    $listing['type'] = $btype;
                    $listing['business_id'] = $bid;
                    $listing['business_name'] = $bname;
                    $listing['business_type'] = $bdet?$bdet:'';
                    $listing['id'] = (String)$ls['id'];
                    $listing['event_name'] = $ls['event_name'];
                    $listing['start_date'] = $ls['start_date']?$ls['start_date']:'';
                    $listing['end_date'] = $ls['end_date']?$ls['end_date']:'';
                    $listing['event_location'] = $ls['event_location'];
                    $listing['latitude'] = $ls['latitude'];
                    $listing['longitude'] = $ls['longitude'];
                    $listing['event_description'] = $ls['event_description'];
                    $listing['tags'] = $ls['tags'];
                    $listing['event_image'] = $eventimage;
                    $listing['event_host'] = $ls['event_host'];
                    $listing['event_host_id'] = $ls['event_host_id']?$ls['event_host_id']:'';
                    $listing['event_host_type'] = $ls['event_host_type']?$ls['event_host_type']:'';
                    $listing['event_link'] = $ls['event_link'];
                    $listing['event_contact_email'] = $ls['event_contact_email'];
                    $listing['event_contact_number'] = $ls['event_contact_number'];
                    $listing['event_price'] = $ls['event_price'];
                    $listing['declaration'] = $ls['declaration']?$ls['declaration']:'';
                    $listing['declaration_value'] = $dec?$dec:'';
                    $listing['getbuddsevents']= $buddevent?$buddevent:'';
                    $record[] = $listing;
                  }
                  return response()->json([
                    'status'=>'1',
                    'message'=>'Events fetched successfully',
                    'eventListing'=>$record
                  ], $this->successStatus);
                }
                else{
                    return response()->json([
                        'message'=>'No events Found',
                        'status'=>'0'
                      ], $this->successStatus);
                    }
                }
                else{
                    $areas = explode('|',$saveArray['area']);
                    $mainArray = [];
                    $i = 0;
                    // $query = $query->where(function($query) use($areas) {
                    foreach($areas as $are) {
                        $query = Event::query();
                        $latlng = explode(',',$are);
                        $lat = $latlng[0];
                        $lng = $latlng[1];
                        $radius = $saveArray['radius']?$saveArray['radius']:'5';
                        $query = $query->selectRaw(" * ,( 3959 * acos( cos( radians(?) ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians(?)) + sin( radians(?) ) * sin( radians( latitude ) ))) AS distance", [$lat, $lng, $lat])
                            ->having("distance", "<", $radius)
                            ->orderBy("distance",'asc')
                            ->offset(0)
                            ->limit(20);

                            if ( $saveArray['event_name_desc'] ) {
                                $query = $query->where('event_name','like','%'.$saveArray['event_name_desc'].'%');
                                $query = $query->orWhere('event_description','like','%'.$saveArray['event_name_desc'].'%');
                            }

                            if ( $saveArray['tags'] )  {
                                $tags = explode(',',$saveArray['tags']);
                                $query = $query->where(function($query) use($tags) {
                                    foreach($tags as $tag) {
                                        $query->orWhere('tags', 'like', "%$tag%");
                                    };
                                })
                                ->inRandomOrder();
                            }

                            if ( $saveArray['start_date'] && empty($saveArray['end_date']) ) {
                                $query = $query->where('start_date','>=',$saveArray['start_date']);
                            }

                            if ( $saveArray['start_date'] && $saveArray['end_date'] ) {
                                $start = $saveArray['start_date'];
                                $end = $saveArray['end_date'];
                                $query = $query->whereDate('start_date', '>=', $start)
                                ->whereDate('end_date', '<=', $end);
                            }

                            $events = $query->where('resort_id',$saveArray['resort_id'])->get();
                            $event = json_decode(json_encode($events), true);
                            if(count($event)){
                            foreach ($event as $ls) {
                                if ($ls['event_host_type'] == 'business') {
                                    $btype = "B";
                                    $bdetail = Business::where('id',$ls['event_host_id'])->first();
                                    $bid = $bdetail['id'];
                                    $bname = $bdetail['business_name'];
                                    $bdet = $bdetail['business_type'];
                                }
                                if ($ls['event_host_type'] == 'user') {
                                    $btype = "U";
                                    $bid = '';
                                    $bname = '';
                                    $bdet = '';
                                }
                                if ($ls['event_host_type'] == 'resort') {
                                    $btype = "R";
                                    $bid = '';
                                    $bname = '';
                                    $bdet = '';
                                }
                                else{
                                    $btype = '';
                                    $bid = '';
                                    $bname = '';
                                    $bdet = '';
                                }
                                // $result[$i] = $ls;
                                $attendStatus = Event_attendee::where('event_id',$ls['id'])
                                ->where('user_id',$userExist['id'])->first();
                                if ($ls['declaration'] == '1') {
                                    $dec = 'yes';
                                }else{
                                    $dec = 'no';
                                }
                                $eventimage = url('/event_images',$ls['event_image']);
                                $getbuddsevents = Event_attendee::whereIn('user_id',$buddiesList)
                                ->where('event_id',$ls['id'])
                                ->get();
                                $buddevent = [];
                                foreach ($getbuddsevents as $budd) {
                                    $username = User::where('id',$budd['user_id'])->first();
                                    $image = url('/images',$username['image']);
                                    $even['id'] = (String)$budd['id'];
                                    $even['event_id'] = $budd['event_id'];
                                    $even['user_id'] = $budd['user_id'];
                                    $even['username'] = $username['name'];
                                    $even['userImage'] = $image;
                                    $even['attend_status'] = $budd['attend_status'];
                                    $even['created_at'] = $budd['created_at']->format('Y-m-d H:i:s');
                                    $buddevent[] = $even;
                                }
                                $result[$i]['attend_status'] = $attendStatus['attend_status']?$attendStatus['attend_status']:'';
                                $result[$i]['id'] = (string)$ls['id'];
                                $result[$i]['type'] = $btype;
                                $result[$i]['business_id'] = $bid;
                                $result[$i]['business_name'] = $bname;
                                $result[$i]['business_type'] = $bdet?$bdet:'';
                                $result[$i]['event_name'] = $ls['event_name'];
                                $result[$i]['start_date'] = $ls['start_date']?$ls['start_date']:'';
                                $result[$i]['end_date'] = $ls['end_date']?$ls['end_date']:'';
                                $result[$i]['event_location'] = $ls['event_location'];
                                $result[$i]['latitude'] = $ls['latitude'];
                                $result[$i]['longitude'] = $ls['longitude'];
                                $result[$i]['resort_id'] = $ls['resort_id'];
                                $result[$i]['event_description'] = $ls['event_description'];
                                $result[$i]['tags'] = $ls['tags'];
                                $result[$i]['event_image'] = $eventimage;
                                $result[$i]['event_host_id'] = (string)$ls['event_host_id']?$ls['event_host_id']:'';
                                $result[$i]['event_host_type'] = (string)$ls['event_host_type']?$ls['event_host_type']:'';
                                $result[$i]['event_link'] = $ls['event_link'];
                                $result[$i]['event_contact_email'] = $ls['event_contact_email'];
                                $result[$i]['event_contact_number'] = $ls['event_contact_number'];
                                $result[$i]['event_price'] = $ls['event_price'];
                                $result[$i]['declaration'] = $ls['declaration'];
                                $result[$i]['getbuddsevents']= $buddevent?$buddevent:'';
                                $result[$i]['created_at'] = $ls['created_at'];
                                $result[$i]['distance'] = (string)$ls['distance'];
                                $i++;
                            }
                        }
                        else{
                            return response()->json([
                                'message'=>'No events Found',
                                'status'=>'0'
                                ], $this->successStatus);
                            }

                        }
                        return response()->json([
                            'status'=>'1',
                            'message'=>'Events fetched successfully',
                            'eventListing'=>$result
                        ], $this->successStatus);
                    // });
                }
            }
        }
        else{
            return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }

    /*
      * Business Listing for Events
    */

    public function businessListing(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
          $validator = Validator::make($request->all(), [
            
          ]);
          if ($validator->fails()) { 
            return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);            
          }
          else{
            $business = Business::orderBy('id','DESC')->get();
            $record = [];
            if(count($business)){
              foreach($business as $ls){
                // $resortDetails = Resort::where('id',$saveArray['resort_id'])->first();
                $service_image = url('/service_images',$ls['service_image']);
                $listing['bcolor']= $ls['bcolor'];
                $listing['fcolor']= $ls['fcolor'];
                $listing['service_image'] = $service_image;
                $listing['id'] = (String)$ls['id'];
                $listing['business_type'] = $ls['business_type'];
                $listing['business_name'] = $ls['business_address'];
                $listing['latitude'] = $ls['latitude'];
                $listing['longitude'] = $ls['longitude'];
                $listing['features'] = $ls['features']?$ls['features']:'';
                $listing['cuisines'] = $ls['cuisines']?$ls['cuisines']:'';
                $listing['activities'] = $ls['activities']?$ls['activities']:'';
                $listing['services'] = $ls['services']?$ls['services']:'';
                $listing['price'] = $ls['price']?$ls['price']:'';
                $listing['star_rating'] = $ls['star_rating']?$ls['star_rating']:'';
                $listing['description'] = $ls['description'];
                $listing['website'] = $ls['website'];
                $listing['email'] = $ls['email'];
                $listing['phone_number'] = (String)$ls['phone_number'];
                $listing['self_working'] = (String)$ls['own_work'];
                $record[] = $listing;
              }
              return response()->json([
                'status'=>'1',
                'message'=>'Business fetched successfully',
                'businessListing'=>$record
              ], $this->successStatus);
            }
            else{
              return response()->json([
                'message'=>'No data Found',
                'status'=>'0'
              ], $this->successStatus);
            }
          }
        }
        else{
          return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }


    public function index(Request $request)
    {
        $events = $this->eventRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($events->toArray(), 'Events retrieved successfully');
    }

    /**
     * Store a newly created Event in storage.
     * POST /events
     *
     * @param CreateEventAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateEventAPIRequest $request)
    {
        $input = $request->all();

        $event = $this->eventRepository->create($input);

        return $this->sendResponse($event->toArray(), 'Event saved successfully');
    }

    /**
     * Display the specified Event.
     * GET|HEAD /events/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Event $event */
        $event = $this->eventRepository->find($id);

        if (empty($event)) {
            return $this->sendError('Event not found');
        }

        return $this->sendResponse($event->toArray(), 'Event retrieved successfully');
    }

    /**
     * Update the specified Event in storage.
     * PUT/PATCH /events/{id}
     *
     * @param int $id
     * @param UpdateEventAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateEventAPIRequest $request)
    {
        $input = $request->all();

        /** @var Event $event */
        $event = $this->eventRepository->find($id);

        if (empty($event)) {
            return $this->sendError('Event not found');
        }

        $event = $this->eventRepository->update($input, $id);

        return $this->sendResponse($event->toArray(), 'Event updated successfully');
    }

    /**
     * Remove the specified Event from storage.
     * DELETE /events/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Event $event */
        $event = $this->eventRepository->find($id);

        if (empty($event)) {
            return $this->sendError('Event not found');
        }

        $event->delete();

        return $this->sendSuccess('Event deleted successfully');
    }
}
