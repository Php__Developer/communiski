<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateTrackingAPIRequest;
use App\Http\Requests\API\UpdateTrackingAPIRequest;
use App\Models\Tracking;
use App\Models\Group;
use App\Models\Group_member;
use App\Models\User;
use App\Models\Weather;
use App\Models\Weather_history;
use App\Models\Friend;
use App\Models\Resort;
use App\Repositories\TrackingRepository;
use Illuminate\Http\Request;
use Validator;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class TrackingController
 * @package App\Http\Controllers\API
 */

class TrackingAPIController extends AppBaseController
{
    /** @var  TrackingRepository */
    private $trackingRepository;
    public $unauthorised = 401;
    public $successStatus = 200;
    public $badrequest = 400;

    public function __construct(TrackingRepository $trackingRepo)
    {
        $this->trackingRepository = $trackingRepo;
    }

    public function shareStatus(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
            $validator = Validator::make($request->all(), [
                'resort_id' => 'required',
                'latitude' => 'required',
                'longitude' => 'required',
                'last_seen_time' => 'required',
                'current_track' => 'required_without_all:future_track',
                'future_track' => 'required_without_all:current_track',
            ]);
            if ($validator->fails()) {
                return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->successStatus);
            }
            else{
                if ($saveArray['current_track'] != '') {
                    $current = $saveArray['current_track'];
                    $updateData = [
                        'resort_id'=> $saveArray['resort_id'],
                        'latitude'=> $saveArray['latitude'],
                        'longitude'=> $saveArray['longitude'],
                        'current_track'=> $current,
                        'date'=> $saveArray['last_seen_time'],
                        'updated_at'=>date('Y-m-d h:i:s'),
                    ];
                    $update = Tracking::where(['user_id'=>$userExist['id']])->update($updateData);
                    if ($update) {
                        return response()->json([
                            'status'=>'1',
                            'message'=>'status updated for current track',
                        ], $this->successStatus);
                    }else{
                        return response()->json([
                            'status'=>'0',
                            'message'=>'status not updated for current track',
                        ], $this->successStatus);
                    }
                }elseif ($saveArray['future_track'] != '') {
                    $future = $saveArray['future_track'];
                    $updateData = [
                        'resort_id'=> $saveArray['resort_id'],
                        'latitude'=> $saveArray['latitude'],
                        'longitude'=> $saveArray['longitude'],
                        'future_track'=> $future,
                        'current_track'=> $future,
                        'date'=> $saveArray['last_seen_time'],
                        'updated_at'=>date('Y-m-d h:i:s'),
                    ];
                    $update = Tracking::where(['user_id'=>$userExist['id']])->update($updateData);
                    /*user data share loc*/
                    $futureStatus = $saveArray['future_track'];
                    if ($future == '2') {
                        $future = 'false';
                    }elseif ($future == '1') {
                        $future = 'true';
                    }
                    $updateUserData = [
                        'share_location'=> $future,
                        'updated_at'=>date('Y-m-d h:i:s'),
                    ];
                    $update = User::where(['id'=>$userExist['id']])->update($updateUserData);
                    /*user data share loc*/
                    if ($update) {
                        return response()->json([
                            'status'=>'1',
                            'message'=>'status updated current and future track',
                        ], $this->successStatus);
                    }
                }
            }
        }else{
            return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }

    public function getStatus(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
            // $validator = Validator::make($request->all(), [
            //     'resort_id' => 'required',
            // ]);
            // if ($validator->fails()) {
            //     return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->successStatus);
            // }
            // else{
                $getStatus = Tracking::where('user_id',$userExist['id'])
                ->first();
                if ($getStatus) {
                    $getStatusArray = array(
                        'id'=>(String)$getStatus['id'],
                        'resort_id'=>'',
                        'current_track'=>(String)$getStatus['current_track'],
                        'future_track'=>(String)$getStatus['future_track'],
                        'created_at'=>$getStatus['created_at']->format('Y-m-d'),
                    );
                    return response()->json([
                        'status'=>'1',
                        'message'=>'detail fetched successfully',
                        'status_details'=>$getStatusArray
                    ], $this->successStatus);
                }else{
                    return response()->json([
                        'status'=>'1',
                        'message'=>'No data',
                    ], $this->successStatus);
                }
            //}
        }
        else{
            return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }

    public function sharedlocationFriends(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
            $validator = Validator::make($request->all(), [
                'resort_id' => 'required',
            ]);
            if ($validator->fails()) {
                return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->successStatus);
            }
            else{
                $checkResort = Resort::where('id',$saveArray['resort_id'])->first();
                if ($checkResort) {
                    //get list of all buddies --DONE
//with all group members at that resort onthat day where token user is member unique list --DONE
//only who have shared their location with in 60 minutes we will show them only --DONE
//get users who have update location with in 60 minutes

                /*get buddies*/
                $getBuddies = Friend::where(['sender_id'=>$userExist['id']])
                ->where(['status'=>'accepted'])
                ->orwhere(['reciever_id'=>$userExist['id']])
                ->where(['status'=>'accepted'])->get();
                $buddiesList = [];
                foreach($getBuddies as $ls){
                    if($userExist['id'] == $ls['reciever_id']){
                        $loginUser = $ls['sender_id'];
                    }elseif( $userExist['id'] == $ls['sender_id']){
                        $loginUser = $ls['reciever_id'];
                    }
                    $userDetails = User::where('id',$loginUser)
                    ->select('id')
                    ->first();
                    $listing['user_id'] = $userDetails['id'];
                    $buddiesList[] = $listing;
                }
                // print_r($buddiesList);die();
                /*get buddies*/
                /*group members*/
                $inGroupArray = [];
                $today = today()->format('Y-m-d');
                $groupsinME = Group_member::where(['user_id'=>$userExist['id']])
                ->where('location_resort_id',$saveArray['resort_id'])
                ->where(['request_status'=>'accepted'])
                // ->orwhere('message','=','Admin')
                ->where('created_at','>=', $today)
                ->select('group_id')
                ->orderBy('id','DESC')
                ->get();
                // print_r($groupsinME);die();
                foreach($groupsinME as $ls1){
                    $groupID = $ls1->group_id;
                    $groupDetails1 = Group::where(['id'=>$ls1['group_id']])->first();
                    $listing2['group_id'] = $groupDetails1['id'];
                    $inGroupArray[] = $listing2;
                }
                $getgroups = array_column($inGroupArray, 'group_id');
                $groupMembers = Group_member::whereIn('group_id', $getgroups)
                ->where('request_status','=', 'accepted')
                ->where('created_at','>=', $today)
                //->where('user_id','!=',$userExist['id'])
                ->select('user_id')
                ->get();
                $grpbuddiesList = [];
                foreach($groupMembers as $ls){
                    $listing5['user_id'] = $ls['user_id'];
                    $grpbuddiesList[] = $listing5;
                }
                $getGrpBudds = array_column($grpbuddiesList, 'user_id');
                // print_r($getGrpBudds);die();
                /*group members*/

                /*get buddies who have share location*/
                $getBudds = array_column($buddiesList, 'user_id');
                $mergedArray = array_merge($getGrpBudds,$getBudds);
                $uIDS = array_unique($mergedArray);
                // print_r($uIDS);die();
                $checkSharingLocation = Tracking::whereIn('user_id',$uIDS)
                ->where('resort_id',$saveArray['resort_id'])
                ->where('current_track','1')
                ->orwhere('future_track','1')
                ->whereIn('user_id',$uIDS)
                ->where('resort_id',$saveArray['resort_id'])
                ->get();
                $getBudd = [];
                foreach ($checkSharingLocation as $location) {
                    $userDetails = User::where('id',$location->user_id)
                    ->first();
                    if ($location['updated_at'] == '') {
                        $loc = $location['created_at']->format('d-m-Y H:i:s');
                    }else{
                        $loc = $location['updated_at']->format('d-m-Y H:i:s');
                    }
                    // $url ='http://3.19.27.66:7300/api/messages/getUsersChat?msgFromID='. $userDetails['id'] .'&msgTo='. $userExist['id'] .' &pageNo=1&pageSize=10';
                    $url ='http://3.19.27.66:7300/api/messages/getUsersChat?msgFromID='.$userDetails['id'].'&msgTo='.$userExist['id'].'&pageNo=1&pageSize=10';
                     // $url ='http://3.19.27.66:7300/api/messages/getUsersChat?msgFromID='. $location->user_id .'&msgTo='. $userExist['id'] .' &pageNo=1&pageSize=10';
                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, $url);
                    curl_setopt($ch, CURLOPT_POST, false);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    //curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
                    $res = curl_exec ($ch);
                    $result = json_decode($res);
                    $listing1['user_id'] = $userDetails['id'];
                    $listing1['user_name'] = $userDetails['name'];
                    $listing1['user_image'] = url('/images',$userDetails['image']);
                    $listing1['last_seen'] = $loc;
                    $listing1['latitude'] = $location['latitude']?$location['latitude']:'';
                    $listing1['longitude'] = $location['longitude']?$location['longitude']:'';
                    $listing1['last_seen_time'] = $location['date']?$location['date']:'';
                    $listing1['skier'] = $userDetails['skier']?$userDetails['skier']:'';
                    $listing1['skier_sky_level'] = $userDetails['skier_sky_level']?$userDetails['skier_sky_level']:'';
                    $listing1['skier_fav_tarrian'] = $userDetails['skier_fav_tarrian']?$userDetails['skier_fav_tarrian']:'';
                     $listing1['snowboarder'] = $userDetails['snowboarder']?$userDetails['snowboarder']:'';
                    $listing1['snowboarder_sky_level'] = $userDetails['snowboarder_sky_level']?$userDetails['snowboarder_sky_level']:'';
                    $listing1['snowboarder_fav_tarrian'] = $userDetails['snowboarder_fav_tarrian']?$userDetails['snowboarder_fav_tarrian']:'';
                    $listing1['unread_message'] = $result?$result:'';
                    $getBudd[] = $listing1;
                }
                /*get buddies who have share location*/

                return response()->json([
                    'status'=>'1',
                    'message'=>'Buddies fetched successfully',
                    'listing'=>$getBudd
                ], $this->successStatus);
                }else{
                    return response()->json([
                    'status'=>'1',
                    'message'=>'unknown resort found',
                    'listing'=>[]
                ], $this->successStatus);
                }
            }
        }else{
            return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }

    public function updateCurrentLoc(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
            $validator = Validator::make($request->all(), [
                'resort_id' => 'required',
                'latitude' => 'required',
                'longitude' => 'required',
                'last_seen_time'=> 'required',
            ]);
            if ($validator->fails()) {
                return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->successStatus);
            }
            else{
                $checkRecord = Tracking::where('user_id',$userExist['id'])->first();
                if ($checkRecord) {
                    $updateData = [
                        'resort_id'=> $saveArray['resort_id'],
                        'latitude'=> $saveArray['latitude'],
                        'longitude'=> $saveArray['longitude'],
                        'date'=> $saveArray['last_seen_time'],
                        'updated_at'=>date('Y-m-d h:i:s'),
                    ];
                    $update = Tracking::where(['user_id'=>$userExist['id']])->update($updateData);
                    if ($update) {
                        return response()->json([
                            'status'=>'1',
                            'message'=>'location updated',
                            ], $this->successStatus);
                      }else{
                        return response()->json([
                            'status'=>'1',
                            'message'=>'Error',
                        ], $this->successStatus);
                    }
                }else{
                    $insertData = [
                        'user_id'=>$userExist['id'],
                        'resort_id'=> $saveArray['resort_id'],
                        'latitude'=> $saveArray['latitude'],
                        'longitude'=> $saveArray['longitude'],
                        'date'=> $saveArray['last_seen_time'],
                        'created_at'=>date('Y-m-d h:i:s'),
                    ];
                    $insertedID = Tracking::insertGetId($insertData);
                    if ($insertedID) {
                        return response()->json([
                            'status'=>'1',
                            'message'=>'location saved',
                            ], $this->successStatus);
                      }else{
                        return response()->json([
                            'status'=>'1',
                            'message'=>'Error',
                        ], $this->successStatus);
                    }
                }
            }
        }
        else{
            return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }

    // public function checkmessage (Request $request){
    // $url ='http://3.19.27.66:7300/api/messages/getUsersChat?msgFromID=136&msgTo=165&pageNo=1&pageSize=10';
    //  // $url ='http://3.19.27.66:7300/api/messages/getUsersChat?msgFromID='. $location->user_id .'&msgTo='. $userExist['id'] .' &pageNo=1&pageSize=10';
    // $ch = curl_init();
    // curl_setopt($ch, CURLOPT_URL, $url);
    // curl_setopt($ch, CURLOPT_POST, false);
    // curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    // //curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
    // $result = curl_exec ($ch);
    // // curl_close ($ch);
    // print_r($result);
    // }

    // public function shareStatus_old(Request $request)
    // {
    //     $saveArray = $request->all();
    //     $token = $request->header('token');
    //     $userExist = User::where(['remember_token'=>$token])->first();
    //     if($userExist){
    //         $validator = Validator::make($request->all(), [
    //             'resort_id' => 'required',
    //             // 'date' => 'required',
    //             'current_track' => 'required_without_all:resort_id',
    //             'future_track' => 'required_without_all:resort_id',
    //         ]);
    //         if ($validator->fails()) {
    //             return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->successStatus);
    //         }
    //         else{
    //             $getshareLocation = User::where('id',$userExist['id'])->first();
    //             $checkSharing = Tracking::where('resort_id',$saveArray['resort_id'])
    //             ->where('user_id',$userExist['id'])
    //             ->first();
    //             if ($checkSharing) {
    //                 if ($saveArray['current_track'] != '') {
    //                     $current = $saveArray['current_track'];
    //                     $updateData = [
    //                     'current_track'=> $current,
    //                     'updated_at'=>date('Y-m-d h:i:s'),
    //                 ];
    //                 $update = Tracking::where(['resort_id'=>$saveArray['resort_id']])
    //                 ->where(['user_id'=>$userExist['id']])
    //                 ->update($updateData);
    //                 if ($update) {
    //                     return response()->json([
    //                         'status'=>'1',
    //                         'message'=>'data updated',
    //                         ], $this->successStatus);
    //                   }else{
    //                     return response()->json([
    //                         'status'=>'1',
    //                         'message'=>'Error',
    //                     ], $this->successStatus);
    //                 }
    //                 }else{
    //                     $future = $saveArray['future_track'];
    //                     $updateData = [
    //                     'future_track'=>$future,
    //                     'updated_at'=>date('Y-m-d h:i:s'),
    //                 ];
    //                 $update = Tracking::where(['resort_id'=>$saveArray['resort_id']])
    //                 ->where(['user_id'=>$userExist['id']])
    //                 ->update($updateData);
    //                 /*update profile data*/
    //                 if ($saveArray['future_track'] == '1') {
    //                     $share_location = 'true';
    //                 }elseif ($saveArray['future_track'] == '2') {
    //                    $share_location = 'false';
    //                 }
    //                 $updateData = [
    //                     'share_location'=>$share_location,
    //                     'updated_at'=>date('Y-m-d h:i:s'),
    //                 ];
    //                 $update = User::where(['id'=>$userExist['id']])
    //                 ->update($updateData);
    //                 /*update profile data*/
    //                 }
    //                 if ($update) {
    //                     return response()->json([
    //                         'status'=>'1',
    //                         'message'=>'data updated',
    //                         ], $this->successStatus);
    //                   }else{
    //                     return response()->json([
    //                         'status'=>'1',
    //                         'message'=>'Error',
    //                     ], $this->successStatus);
    //                 }
    //             }else{
    //                 if ($saveArray['current_track'] != '') {
    //                     $insertData = [
    //                     'user_id'=>$userExist['id'],
    //                     'resort_id'=> $saveArray['resort_id'],
    //                     'current_track'=>$saveArray['current_track'],
    //                     'created_at'=>date('Y-m-d h:i:s'),
    //                   ];
    //                   $insertedID = Tracking::insertGetId($insertData);
    //                   if ($insertedID) {
    //                     return response()->json([
    //                         'status'=>'1',
    //                         'message'=>'data inserted',
    //                         ], $this->successStatus);
    //                   }else{
    //                     return response()->json([
    //                         'status'=>'1',
    //                         'message'=>'Error',
    //                     ], $this->successStatus);
    //                 }
    //                 }else{
    //                     $insertData = [
    //                     'user_id'=>$userExist['id'],
    //                     'resort_id'=> $saveArray['resort_id'],
    //                     'future_track'=>$saveArray['future_track'],
    //                     'created_at'=>date('Y-m-d h:i:s'),
    //                   ];
    //                   $insertedID = Tracking::insertGetId($insertData);
    //                   /*update profile data*/
    //                  if ($saveArray['future_track'] == '1') {
    //                      $share_loc = 'true';
    //                  }elseif ($saveArray['future_track'] == '2') {
    //                      $share_loc = 'false';
    //                  }
    //                  $updateData = [
    //                      'share_location'=>$share_loc,
    //                      'updated_at'=>date('Y-m-d h:i:s'),
    //                  ];
    //                  $update = User::where(['id'=>$userExist['id']])
    //                  ->update($updateData);
    //                  /*update profile data*/
    //                   if ($insertedID) {
    //                     return response()->json([
    //                         'status'=>'1',
    //                         'message'=>'data inserted',
    //                         ], $this->successStatus);
    //                   }else{
    //                     return response()->json([
    //                         'status'=>'1',
    //                         'message'=>'Error',
    //                     ], $this->successStatus);
    //                 }
    //                 }
    //             }
    //         }
    //     }else{
    //         return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
    //     }
    // }

    /**
     * Display a listing of the Tracking.
     * GET|HEAD /trackings
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $trackings = $this->trackingRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($trackings->toArray(), 'Trackings retrieved successfully');
    }

    /**
     * Store a newly created Tracking in storage.
     * POST /trackings
     *
     * @param CreateTrackingAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateTrackingAPIRequest $request)
    {
        $input = $request->all();

        $tracking = $this->trackingRepository->create($input);

        return $this->sendResponse($tracking->toArray(), 'Tracking saved successfully');
    }

    /**
     * Display the specified Tracking.
     * GET|HEAD /trackings/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Tracking $tracking */
        $tracking = $this->trackingRepository->find($id);

        if (empty($tracking)) {
            return $this->sendError('Tracking not found');
        }

        return $this->sendResponse($tracking->toArray(), 'Tracking retrieved successfully');
    }

    /**
     * Update the specified Tracking in storage.
     * PUT/PATCH /trackings/{id}
     *
     * @param int $id
     * @param UpdateTrackingAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTrackingAPIRequest $request)
    {
        $input = $request->all();

        /** @var Tracking $tracking */
        $tracking = $this->trackingRepository->find($id);

        if (empty($tracking)) {
            return $this->sendError('Tracking not found');
        }

        $tracking = $this->trackingRepository->update($input, $id);

        return $this->sendResponse($tracking->toArray(), 'Tracking updated successfully');
    }

    /**
     * Remove the specified Tracking from storage.
     * DELETE /trackings/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Tracking $tracking */
        $tracking = $this->trackingRepository->find($id);

        if (empty($tracking)) {
            return $this->sendError('Tracking not found');
        }

        $tracking->delete();

        return $this->sendSuccess('Tracking deleted successfully');
    }
}
