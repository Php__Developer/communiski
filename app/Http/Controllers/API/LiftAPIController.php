<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateLiftAPIRequest;
use App\Http\Requests\API\UpdateLiftAPIRequest;
use App\Models\Lift;
use App\Models\Permission;
use App\Models\Lift_history;
use App\Models\User;
use App\Models\Resort;
use App\Models\Country;
use App\Models\State;
use App\Models\Snowflake;
use App\Models\Rewards_point;
use App\Repositories\LiftRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Validator;
use Response;

/**
 * Class LiftController
 * @package App\Http\Controllers\API
 */

class LiftAPIController extends AppBaseController
{
    /** @var  LiftRepository */
    private $liftRepository;
    public $unauthorised = 401;
    public $successStatus = 200;
    public $badrequest = 400;

    public function __construct(LiftRepository $liftRepo)
    {
        $this->liftRepository = $liftRepo;
    }

    /**
     * Display a listing of the Lift.
     * GET|HEAD /lifts
     *
     * @param Request $request
     * @return Response
     */

    public function addLift(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
            $validator = Validator::make($request->all(), [
                'country_id' => 'required',
                'state_id' => 'required',
                'resort_id' => 'required',
                'lift_name' => 'required_without_all:lift_number', 
                'lift_number' => 'required_without_all:lift_name',
                // 'description' => 'required',
                'lift_type' => 'required',
                // 'capacity' => 'required', 
                // 'manufacturer' => 'required',
                // 'installed_year' => 'required',
                // 'hourly_capacity' => 'required',
                // 'base_elevation' => 'required',
                // 'base_elevation_unit' => 'required',
                // 'top_elevation' => 'required',
                // 'top_elevation_unit' => 'required',
                // 'vertical_rise' => 'required',
                // 'vertical_rise_unit' => 'required',
                // 'length' => 'required',
                // 'length_unit' => 'required',
                // 'ride_time' => 'required',
                // 'opening_time' => 'required',
                // 'closing_time' => 'required',
            ]);
            if ($validator->fails()) {
                return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->successStatus);
            }
            else{
                $country_id = $saveArray['country_id'];
                $state_id = $saveArray['state_id'];
                $resort_id = $saveArray['resort_id'];
                $user_id = $userExist['id'];
                $lift_name = $saveArray['lift_name'];
                $lift_number = $saveArray['lift_number'];
                $description = $saveArray['description'];
                $lift_type = $saveArray['lift_type'];
                $capacity = $saveArray['capacity'];
                $manufacturer = $saveArray['manufacturer'];
                $installed_year = $saveArray['installed_year'];
                $hourly_capacity = $saveArray['hourly_capacity'];
                $base_elevation = $saveArray['base_elevation'];
                $base_elevation_unit = $saveArray['base_elevation_unit'];
                $top_elevation = $saveArray['top_elevation'];
                $top_elevation_unit = $saveArray['top_elevation_unit'];
                $vertical_rise = $saveArray['vertical_rise'];
                $vertical_rise_unit = $saveArray['vertical_rise_unit'];
                $length = $saveArray['length'];
                $length_unit = $saveArray['length_unit'];
                $ride_time = $saveArray['ride_time'];
                $opening_time = $saveArray['opening_time'];
                $closing_time = $saveArray['closing_time'];
                //print_r($closing_time);die();
                $insertData = [
                    'country_id'=>$country_id,
                    'state_id'=>$state_id,
                    'resort_id'=>$resort_id,
                    'user_id'=>$user_id,
                    'lift_name'=>$lift_name?$lift_name:'',
                    'lift_number'=>$lift_number?$lift_number:'',
                    'description'=>$description?$description:'',
                    'lift_type'=>$lift_type?$lift_type:'',
                    'capacity'=> $capacity?$capacity:'',
                    'manufacturer'=> $manufacturer?$manufacturer:'',
                    'installed_year'=> $installed_year?$installed_year:'0000',
                    'hourly_capacity'=> $hourly_capacity?$hourly_capacity:'',
                    'base_elevation'=> $base_elevation?$base_elevation:'',
                    'base_elevation_unit'=> $base_elevation_unit?$base_elevation_unit:'',
                    'top_elevation'=>$top_elevation?$top_elevation:'',
                    'top_elevation_unit'=>$top_elevation_unit?$top_elevation_unit:'',
                    'vertical_rise'=> $vertical_rise?$vertical_rise:'',
                    'vertical_rise_unit'=> $vertical_rise_unit?$vertical_rise_unit:'',
                    'length'=> $length?$length:'',
                    'length_unit'=> $length_unit?$length_unit:'',
                    'ride_time'=> $ride_time?$ride_time:'00',
                    'opening_time'=> $opening_time?$opening_time:'00',
                    'closing_time'=> $closing_time?$closing_time:'00',
                    'created_at'=>date('Y-m-d h:i:s'),
                ];
                $lift_id = Lift::insertGetId($insertData);

                $insertHistory = [
                    'lift_id'=>$lift_id,
                    'user_id'=>$user_id,
                    'note_for'=>'NULL',
                    'added_note'=>'NULL',
                    'lift_name'=>$lift_name,
                    'lift_number'=>$lift_number,
                    'description'=>$description,
                    'lift_type'=>$lift_type,
                    'capacity'=> $capacity,
                    'manufacturer'=> $manufacturer,
                    'installed_year'=> $installed_year,
                    'hourly_capacity'=> $hourly_capacity,
                    'base_elevation'=> $base_elevation,
                    'base_elevation_unit'=> $base_elevation_unit,
                    'top_elevation'=>$top_elevation,
                    'top_elevation_unit'=>$top_elevation_unit,
                    'vertical_rise'=> $vertical_rise,
                    'vertical_rise_unit'=> $vertical_rise_unit,
                    'length'=> $length,
                    'length_unit'=> $length_unit,
                    'ride_time'=> $ride_time,
                    'opening_time'=> $opening_time,
                    'closing_time'=> $closing_time,
                    'created_at'=>date('Y-m-d h:i:s'),
                ];
                $insert = Lift_history::insertGetId($insertHistory);

                /*snowflakes rewards and total points*/

                $getRun = Lift::where('id',$lift_id)->first();
                if ($saveArray['lift_name'] != '') {
                    $lift_name = $saveArray['lift_name'];
                }
                elseif ($saveArray['lift_name'] != '' && $saveArray['lift_number'] != '') {
                    $lift_name = $saveArray['lift_name'];
                }
                elseif ($saveArray['lift_number'] != '') {
                    $lift_name = $saveArray['lift_number'];
                }
                $resortName = Resort::where('id',$saveArray['resort_id'])->select('resort_name')->first(); 
                $concate_reason1 = 'Added a Lift at ';
                $concate_reason2 = $resortName['resort_name'];
                $space = ' : ';
                $concate_reason3 = $lift_name?$lift_name:'';
                $concate_reason = $concate_reason1.$concate_reason2.$space.$concate_reason3;
                if( ($getRun['lift_name'] !== '' || $getRun['lift_number'] !== '') && $getRun['lift_type'] !== '' && empty($getRun['description']) )
                {
                    $insertData = [
                        'lift_id' => $lift_id,
                        'user_id' => $userExist['id'],
                        'snowflake_rewards' => '10',
                        'reward_status' => '1',
                        'reason' => $concate_reason,
                        'created_at' => date('Y-m-d H:i:s')
                      ];
                      $insert = Snowflake::insert($insertData);
                      $getUser = Rewards_point::where('user_id',$userExist['id'])->select('total')->first();
                      $rewardedPoint = $getUser['total'] + 10;
                      $updateData = [
                        'user_id'=> $userExist['id'],
                        'total'=> $rewardedPoint,
                        'updated_at'=>date('Y-m-d h:i:s'),
                    ];
                    $update = Rewards_point::where(['user_id'=>$userExist['id']])->update($updateData);
                }
                elseif ( ($getRun['lift_name'] !== '' || $getRun['lift_number'] !== '') && $getRun['lift_type'] !== '' && $getRun['description'] !== '' && empty($getRun['capacity']) && empty($getRun['installed_year']) && empty($getRun['hourly_capacity'])) 
                {
                    $insertData = [
                        'lift_id' => $lift_id,
                        'user_id' => $userExist['id'],
                        'snowflake_rewards' => '20',
                        'reward_status' => '1',
                        'reason' => $concate_reason,
                        'created_at' => date('Y-m-d H:i:s')
                      ];
                      $insert = Snowflake::insert($insertData);
                      $getUser = Rewards_point::where('user_id',$userExist['id'])->select('total')->first();
                      $rewardedPoint = $getUser['total'] + 20;
                      $updateData = [
                        'user_id'=> $userExist['id'],
                        'total'=> $rewardedPoint,
                        'updated_at'=>date('Y-m-d h:i:s'),
                    ];
                    $update = Rewards_point::where(['user_id'=>$userExist['id']])->update($updateData);
                }
                elseif ( ($getRun['lift_name'] !== '' || $getRun['lift_number'] !== '') && $getRun['lift_type'] !== '' && $getRun['description'] !== '' && ( $getRun['capacity'] !== '' || $getRun['manufacturer'] !== '' || $getRun['installed_year'] !== '' || $getRun['hourly_capacity'] !== '' || $getRun['base_elevation'] !== '' || $getRun['base_elevation_unit'] !== '' || $getRun['top_elevation'] !== '' || $getRun['top_elevation_unit'] !== '' || $getRun['vertical_rise'] !== '' || $getRun['vertical_rise_unit'] !== '' || $getRun['length'] !== '' || $getRun['length_unit'] !== '' || $getRun['ride_time'] !== '' || $getRun['opening_time'] !== '' || $getRun['closing_time'] !== '') ) 
                {
                    $insertData = [
                        'lift_id' => $lift_id,
                        'user_id' => $userExist['id'],
                        'snowflake_rewards' => '40',
                        'reward_status' => '1',
                        'reason' => $concate_reason,
                        'created_at' => date('Y-m-d H:i:s')
                    ];
                    $insert = Snowflake::insert($insertData);
                    $getUser = Rewards_point::where('user_id',$userExist['id'])->select('total')->first();
                    $rewardedPoint = $getUser['total'] + 40;
                    $updateData = [
                        'user_id'=> $userExist['id'],
                        'total'=> $rewardedPoint,
                        'updated_at'=>date('Y-m-d h:i:s'),
                    ];
                    $update = Rewards_point::where(['user_id'=>$userExist['id']])->update($updateData);
                }

                /*snowflakes rewards and total points*/

                if($lift_id){
                    return response()->json([
                     'status'=>'1',
                     'message'=>'Lift Created Successfully',
                     'lift_id'=>(String)$lift_id
                 ], $this->successStatus);
                }
                else{
                    return response()->json([
                        'status'=>'0',
                        'message'=>'Lift Creation Failed'
                    ], $this->badrequest);
                }
            }
        }
        else{
            return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }

  public function getLift(Request $request)
  {
    $saveArray = $request->all();
    $token = $request->header('token');
    $userExist = User::where(['remember_token'=>$token])->first();
    if($userExist){
        $validator = Validator::make($request->all(), [
            'resort_id' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
        }
        else{
            $liftDetails = Lift::where(['resort_id'=>$saveArray['resort_id']])
            ->orderBy('id','DESC')
            ->get();
            $liftArray = [];
            if(count($liftDetails)){
              foreach($liftDetails as $lift){
                $userDetails = User::where(['id'=>$userExist['id']])->first();
                $resortDetails = Resort::where(['id'=>$lift['resort_id']])->first();
                $countryName = Country::where(['id'=>$lift['country_id']])->first();
                $stateName = State::where(['id'=>$lift['state_id']])->first();
                $listing['status'] = $lift['status']?$lift['status']:'';
                $listing['lift_id'] = (String)$lift['id'];
                $listing['resort_id'] = (String)$lift['resort_id'];
                $listing['resort_location'] = (String)$resortDetails['resort_name'];
                $listing['bcolor'] = $resortDetails['bcolor'];
                $listing['fcolor'] = $resortDetails['fcolor'];
                $listing['country_id'] = (String)$lift['country_id'];
                $listing['country_name'] = $countryName['name'];
                $listing['state_id'] = (String)$lift['state_id'];
                $listing['state_name'] = $stateName['name'];
                $listing['user_id'] = (String)$userExist['id'];
                $listing['user_name'] = $userDetails['name'];
                $listing['lift_name'] = (String)$lift['lift_name']?$lift['lift_name']:'';
                $listing['lift_number'] = $lift['lift_number']?$lift['lift_number']:'';
                $listing['description'] = $lift['description']?$lift['description']:'';
                $listing['lift_type'] = $lift['lift_type']?$lift['lift_type']:'';
                $listing['capacity'] = $lift['capacity']?$lift['capacity']:'';
                $listing['manufacturer'] = $lift['manufacturer']?$lift['manufacturer']:'';
                $listing['installed_year'] = $lift['installed_year']?$lift['installed_year']:'';
                $listing['hourly_capacity'] = $lift['hourly_capacity']?$lift['hourly_capacity']:'';
                $listing['base_elevation'] = $lift['base_elevation']?$lift['base_elevation']:'';
                $listing['base_elevation_unit'] = $lift['base_elevation_unit']?$lift['base_elevation_unit']:'';
                $listing['top_elevation'] = $lift['top_elevation']?$lift['top_elevation']:'';
                $listing['top_elevation_unit'] = $lift['top_elevation_unit']?$lift['top_elevation_unit']:'';
                $listing['vertical_rise'] = $lift['vertical_rise']?$lift['vertical_rise']:'';
                $listing['vertical_rise_unit'] = $lift['vertical_rise_unit']?$lift['vertical_rise_unit']:'';
                $listing['length_value'] = $lift['length']?$lift['length']:'';
                $listing['length_unit'] = $lift['length_unit']?$lift['length_unit']:'';
                $listing['ride_time'] = $lift['ride_time']?$lift['ride_time']:'';
                $listing['opening_time'] = $lift['opening_time']?$lift['opening_time']:'';
                $listing['closing_time'] = $lift['closing_time']?$lift['closing_time']:'';
                $listing['created_at'] = $lift['created_at']->format('d-m-Y');
                $liftArray[] = $listing;
            }
              return response()->json([
                'status'=>'1',
                'message'=>'Lifts fetched successfully',
                'lift_listing'=>$liftArray
              ], $this->successStatus);
            }
            else{
              return response()->json([
                'message'=>'No Lift Found',
                'status'=>'0',
              ], $this->successStatus);
            }
        }
    }
    else{
      return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
    }
  }

  public function editLift(Request $request)
  {
    $saveArray = $request->all();
    $token = $request->header('token');
    $userExist = User::where(['remember_token'=>$token])->first();
    if($userExist){
      $validator = Validator::make($request->all(), [
        'lift_id' => 'required',
        'post_anonymous' => 'required'
      ]);
      if ($validator->fails()) {
        return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
      }
      else{
        $liftDetail = Lift::where(['id'=>$saveArray['lift_id']])->first();
        $updateLift = [
          'lift_name'=>$saveArray['lift_name']?$saveArray['lift_name']:$liftDetail['lift_name'],
          'lift_number'=>$saveArray['lift_number']?$saveArray['lift_number']:$liftDetail['lift_number'],
          'lift_type'=>$saveArray['lift_type']?$saveArray['lift_type']:$liftDetail['lift_type'],
          'capacity'=>$saveArray['capacity']?$saveArray['capacity']:$liftDetail['capacity'],
          'description'=>$saveArray['description']?$saveArray['description']:$liftDetail['description'],
          'manufacturer'=>$saveArray['manufacturer']?$saveArray['manufacturer']:$liftDetail['manufacturer'],
          'installed_year'=>$saveArray['installed_year']?$saveArray['installed_year']:$liftDetail['installed_year'],
          'hourly_capacity'=>$saveArray['hourly_capacity']?$saveArray['hourly_capacity']:$liftDetail['hourly_capacity'],
          'base_elevation'=>$saveArray['base_elevation']?$saveArray['base_elevation']:$liftDetail['base_elevation'],
          'base_elevation_unit'=>$saveArray['base_elevation_unit']?$saveArray['base_elevation_unit']:$liftDetail['base_elevation_unit'],
          'top_elevation'=>$saveArray['top_elevation']?$saveArray['top_elevation']:$liftDetail['top_elevation'],
          'top_elevation_unit'=>$saveArray['top_elevation_unit']?$saveArray['top_elevation_unit']:$liftDetail['top_elevation_unit'],
          'vertical_rise'=>$saveArray['vertical_rise']?$saveArray['vertical_rise']:$liftDetail['vertical_rise'],
          'vertical_rise_unit'=>$saveArray['vertical_rise_unit']?$saveArray['vertical_rise_unit']:$liftDetail['vertical_rise_unit'],
          'length'=>$saveArray['length']?$saveArray['length']:$liftDetail['length'],
          'length_unit'=>$saveArray['length_unit']?$saveArray['length_unit']:$liftDetail['length_unit'],
          'ride_time'=>$saveArray['ride_time']?$saveArray['ride_time']:$liftDetail['ride_time'],
          'opening_time'=>$saveArray['opening_time']?$saveArray['opening_time']:$liftDetail['opening_time'],
          'closing_time'=>$saveArray['closing_time']?$saveArray['closing_time']:$liftDetail['closing_time'],
          'updated_at'=>date('Y-m-d h:i:s')
        ];

        /*update rewards on behalf first time and 2nd time insert update*/
        $givingReward = $this->giveRewards($saveArray,$userExist);
        /*update rewards on behalf first time and 2nd time insert update*/

        /*filtered existing records*/
        /*
        $existingRecord = Lift::where('id',$saveArray['lift_id'])->first()->toArray();
        $alreadyExistValue = (array_filter($existingRecord));
        $getUpdatedValue = (array_filter($updateLift));
        $currentValue = array_diff($getUpdatedValue, $alreadyExistValue);
        $cols = array_except($currentValue, ['base_elevation_unit','top_elevation_unit','vertical_rise_unit','length_unit','length_unit','updated_at',]);
        foreach($cols as $key=>$value)
        {
          $defaultValue =  $key;
        }
        $getField = Lift::where(['id'=>$saveArray['lift_id']])->select($defaultValue)->first();
        if (!empty($getField->$defaultValue)) {
            $concateReason = 'update data in Lifts for ';
            $getReason = $concateReason.$defaultValue;
            $insertData = [
                'user_id' => $userExist['id'],
                'lift_id' => $saveArray['lift_id'],
                'reward_for' => $defaultValue,
                'snowflake_rewards' => '5',
                'reward_status' => '0',
                'reason' => $getReason,
                'created_at' => date('Y-m-d H:i:s')
            ];
            $insert = Snowflake::insert($insertData);
        }
        else{
            $concateReason = 'Add data in Lifts for ';
            $getReason = $concateReason.$defaultValue;
            $insertData = [
                'user_id' => $userExist['id'],
                'lift_id' => $saveArray['lift_id'],
                'reward_for' => $defaultValue,
                'snowflake_rewards' => '10',
                'reward_status' => '0',
                'reason' => $getReason,
                'created_at' => date('Y-m-d H:i:s')
            ];
            $insert = Snowflake::insert($insertData);
        }
        */
        /*filtered existing records*/
        $update = Lift::where(['id'=>$saveArray['lift_id']])->update($updateLift);

        $insertHistroy = [
          'lift_id'=>$saveArray['lift_id'],
          'user_id'=>$userExist['id'],
          'lift_name'=> $saveArray['lift_name'],
          'lift_number'=> $saveArray['lift_number'],
          'lift_type'=> $saveArray['lift_type'],
          'capacity'=> $saveArray['capacity'],
          'description'=> $saveArray['description'],
          'manufacturer'=> $saveArray['manufacturer'],
          'installed_year'=> $saveArray['installed_year'],
          'hourly_capacity'=> $saveArray['hourly_capacity'],
          'base_elevation'=> $saveArray['base_elevation'],
          'base_elevation_unit'=> $saveArray['base_elevation_unit'],
          'top_elevation'=> $saveArray['top_elevation'],
          'top_elevation_unit'=> $saveArray['top_elevation_unit'],
          'vertical_rise'=> $saveArray['vertical_rise'],
          'vertical_rise_unit'=> $saveArray['vertical_rise_unit'],
          'length'=> $saveArray['length'],
          'length_unit'=> $saveArray['length_unit'],
          'ride_time'=> $saveArray['ride_time'],
          'opening_time'=> $saveArray['opening_time'],
          'closing_time'=> $saveArray['closing_time'],
          'post_anonymous'=>$saveArray['post_anonymous'],
          'created_at'=>date('Y-m-d h:i:s'),
        ];
        $history = Lift_history::insert($insertHistroy);

        if($update){
          return response()->json([
            'status'=>'1',
            'message'=>'Lift Updated Successfully'
          ], $this->successStatus);
        }
      }
    }
    else{
      return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
    }
  }

    public function giveRewards ($request,$userExist)
    {
        $saveArray = $request;
        $getUpdatedValue = (array_filter($saveArray));
        $cols = array_except($getUpdatedValue, ['lift_id','post_anonymous','base_elevation_unit','top_elevation_unit','vertical_rise_unit','length_unit']);
        $unit = '';
        if ($saveArray['base_elevation'] != '') {
            $unit = $saveArray['base_elevation_unit'];
        }
        if ($saveArray['top_elevation'] != '') {
            $unit = $saveArray['top_elevation_unit'];
        }
        if ($saveArray['vertical_rise'] != '') {
            $unit = $saveArray['vertical_rise_unit'];
        }
        if ($saveArray['length'] != '') {
            $unit = $saveArray['length_unit'];
        }
        foreach($cols as $key=>$value)
        {
          $defaultValue =  $key;
          $defaultValue1 = $value;
        }
        $output = str_replace('_', ' ', $defaultValue);
        $getField = Lift::where(['id'=>$saveArray['lift_id']])->select($defaultValue,'resort_id','lift_name')->first();
        $resortName = Resort::where('id',$getField['resort_id'])->select('resort_name')->first();
        if (!empty($getField->$defaultValue)) {
            // print_r($getField->$defaultValue);die();
            $concateReason = 'Updated '.$output.' for the Lift '.$getField->lift_name.' at '.$resortName->resort_name.' : '.$defaultValue1.''.$unit.'';

            // $concateReason = 'update data in Lifts for ';
            // $getReason = $concateReason.$defaultValue;
            $insertData = [
                'user_id' => $userExist['id'],
                'lift_id' => $saveArray['lift_id'],
                'reward_for' => $defaultValue,
                'snowflake_rewards' => '5',
                'reward_status' => '0',
                'reason' => $concateReason,
                'created_at' => date('Y-m-d H:i:s')
            ];
            $insert = Snowflake::insert($insertData);
        }
        else{
            // print_r($getField->$defaultValue);die();
            $concateReason = 'Added '.$output.' for the Lift '.$getField->lift_name.' at '.$resortName->resort_name.' : '.$defaultValue1.''.$unit.'';
            // $concateReason = 'Add data in Lifts for ';
            // $getReason = $concateReason.$defaultValue;
            $insertData = [
                'user_id' => $userExist['id'],
                'lift_id' => $saveArray['lift_id'],
                'reward_for' => $defaultValue,
                'snowflake_rewards' => '10',
                'reward_status' => '0',
                'reason' => $concateReason,
                'created_at' => date('Y-m-d H:i:s')
            ];
            $insert = Snowflake::insert($insertData);
        }
    }

  /*Lift status changed by this api*/
  
    public function changeLiftStatus(Request $request)
    {
      $saveArray = $request->all();
      $token = $request->header('token');
      $userExist = User::where(['remember_token'=>$token])->first();
      if($userExist){
        $validator = Validator::make($request->all(), [
                'lift_id' => 'required',
                'status' => 'required',
            ]);
            if ($validator->fails()) {
                return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
            }
            else{
              $checkPermission = Permission::where(['user_id'=>$userExist['id']])->first();
              if($checkPermission){
                $updateData = [
                'status'=>$saveArray['status'],
                'updated_at'=>date('Y-m-d h:i:s'),
              ];
              $update = Lift::where(['id'=>$saveArray['lift_id']])->update($updateData);
              if($update){
                $getStatus = Lift::where(['id'=>$saveArray['lift_id']])->select('status')->first();
                return response()->json([
                  'status'=>'1',
                  'message'=>'Status Updated Successfully',
                  'lift_status'=>$getStatus
                ], $this->successStatus);
              }else{
                return response()->json([
                  'status'=>'0',
                  'message'=>'error'
                ], $this->successStatus);
              }
              }
              else{
                return response()->json([
                  'status'=>'1',
                  'message'=>'You dont have permissions to change status'
                ], $this->successStatus);
              }
              /**/
            }
          }
          else{
              return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
          }
      }


  public function liftHistory(Request $request)
  {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
            $validator = Validator::make($request->all(), [
                'lift_id' => 'required',
                'type' => 'required',
            ]);
            if ($validator->fails()) {
                return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
            }
            else{
                $dynamicField = $saveArray['type'];
                $gethistory =Lift_history::where(['lift_id'=>$saveArray['lift_id']])
                    ->select('id','user_id','post_anonymous',$dynamicField)
                    ->where($dynamicField, '!=', 'Null')
                    ->orderBy('id','DESC')
                    ->get();
                    $detailArray = [];
                    if(count($gethistory)){
                        foreach($gethistory as $ls){
                            $username = User::where(['id'=>$ls['user_id']])->first();
                            $info['id'] = $ls['id'];
                            $info['post_anonymous'] = $ls['post_anonymous']?$ls['post_anonymous']:'';
                            $info['user_id'] = $ls['user_id'];
                            $info['user_name'] = $username['name'];
                            $info['type'] = $dynamicField;
                            $info['type_value'] = $ls->$dynamicField;
                            $info['created_at'] = date('Y-m-d h:i:s');
                            $detailArray[] = $info;
                        }
                        return response()->json([
                            'status'=>'1',
                            'message'=>'data fetched successfully',
                            'resort_history'=>$detailArray
                        ], $this->successStatus);
                    }
                    else{
                        return response()->json([
                            'message'=>'No data',
                            'status'=>'0'
                        ], $this->successStatus);
                    }
            }
        }
        else{
            return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }

    public function index(Request $request)
    {
        $lifts = $this->liftRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($lifts->toArray(), 'Lifts retrieved successfully');
    }

    /**
     * Store a newly created Lift in storage.
     * POST /lifts
     *
     * @param CreateLiftAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateLiftAPIRequest $request)
    {
        $input = $request->all();

        $lift = $this->liftRepository->create($input);

        return $this->sendResponse($lift->toArray(), 'Lift saved successfully');
    }

    /**
     * Display the specified Lift.
     * GET|HEAD /lifts/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Lift $lift */
        $lift = $this->liftRepository->find($id);

        if (empty($lift)) {
            return $this->sendError('Lift not found');
        }

        return $this->sendResponse($lift->toArray(), 'Lift retrieved successfully');
    }

    /**
     * Update the specified Lift in storage.
     * PUT/PATCH /lifts/{id}
     *
     * @param int $id
     * @param UpdateLiftAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateLiftAPIRequest $request)
    {
        $input = $request->all();

        /** @var Lift $lift */
        $lift = $this->liftRepository->find($id);

        if (empty($lift)) {
            return $this->sendError('Lift not found');
        }

        $lift = $this->liftRepository->update($input, $id);

        return $this->sendResponse($lift->toArray(), 'Lift updated successfully');
    }

    /**
     * Remove the specified Lift from storage.
     * DELETE /lifts/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Lift $lift */
        $lift = $this->liftRepository->find($id);

        if (empty($lift)) {
            return $this->sendError('Lift not found');
        }

        $lift->delete();

        return $this->sendResponse($id, 'Lift deleted successfully');
    }
}
