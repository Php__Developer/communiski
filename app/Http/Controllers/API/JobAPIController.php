<?php

namespace App\Http\Controllers\API;
use App\Traits\CommonFunctionTrait;

use App\Http\Requests\API\CreateJobAPIRequest;
use App\Http\Requests\API\UpdateJobAPIRequest;
use App\Models\Job;
use App\Models\Job_detail;
use App\Models\Job_Applicant;
use App\Models\User;
use App\Models\Resort;
use App\Models\Business;
use App\Models\State;
use App\Models\Country;
use App\Models\Fav_resort;
use App\Models\Permission;
use App\Models\Resume;
use App\Models\Resume_education;
use App\Models\Resume_work;
use Carbon\Carbon;
use App\Repositories\JobRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use DB;
use Validator;
use Stripe\Stripe;
use Response;

/**
 * Class JobController
 * @package App\Http\Controllers\API
 */

class JobAPIController extends AppBaseController
{
    /** @var  JobRepository */
    private $jobRepository;
    public $unauthorised = 401;
    public $successStatus = 200;
    public $badrequest = 400;
    use CommonFunctionTrait;

    public function __construct(JobRepository $jobRepo)
    {
        $this->jobRepository = $jobRepo;
    }

    /**
     * Display a listing of the Job.
     * GET|HEAD /jobs
     *
     * @param Request $request
     * @return Response
     */

    /*
      * Request for Job starting in a Business
    */

    public function requestJob(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
            $validator = Validator::make($request->all(), [
                'business_id' => 'required'
              ]);
              if ($validator->fails()) {
                return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
              }
              else{
                $check = $this->businessPermission($request ,$saveArray['business_id'],$userExist['id']);
                if($check == 1){
                    $checkRequest = Job::where('business_service_id',$saveArray['business_id'])->first();
                    if ($checkRequest) {
                        return response()->json(['status'=>'1','message'=>'you have already requested for job'], $this->successStatus);
                    }else{
                        $requestjob = [
                            'business_service_id'=>$saveArray['business_id'],
                            'job_request'=>'live',
                            'created_at'=>date('Y-m-d h:i:s'),
                        ];
                        $jobID = Job::insertGetId($requestjob);
                        if($jobID){
                            return response()->json(['status'=>'1','message'=>'Service started successfully',], $this->successStatus);
                        }
                        else{
                            return response()->json(['status'=>'0','message'=>'error'], $this->badrequest);
                        }
                    }
                }else{
                    return response()->json(['message'=>'you have not Admin Permission to do this','status'=>'0'], $this->badrequest);
                }
            }
        }
        else{
            return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }

/*admin apis*/

    /*
      * Post job by Business Admin
    */

    public function postJob(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
            $validator = Validator::make($request->all(), [
                'business_id' => 'required',
                'salary_guide' => 'required',
                'job_title' => 'required',
                'category' => 'required',
                'hours' => 'required',
                'benefits' => 'required',
                'duration' => 'required',
                'description' => 'required',
                'advert_size' => 'required'
              ]);
              if ($validator->fails()) {
                return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
              }
              else{
                $check = $this->businessPermission($request ,$saveArray['business_id'],$userExist['id']);
                if($check == 1){
                    $checkJob = Job::where('business_service_id',$saveArray['business_id'])->first();
                    if (empty($checkJob)) {
                        return response()->json(['message'=>'job service not started yet on this business','status'=>'1'], $this->successStatus);
                    }
                    else{
                        $qualification = array($saveArray['qualification']);
                        $multiqual = implode(",", $qualification);

                        $benefits = array($saveArray['benefits']);
                        $benefit = implode(",", $benefits);
                        $multibenefits = explode('$',$saveArray['benefits']);
                        $numlength = mb_strlen($multibenefits[1]);
                        
                        if ($saveArray['salary_guide'] == '1') {
                            $salary = '$';
                        }elseif ($saveArray['salary_guide'] == '2') {
                            $salary = '$$';
                        }elseif ($saveArray['salary_guide'] == '3') {
                            $salary = '$$$';
                        }elseif ($saveArray['salary_guide'] == '4') {
                            $salary = '$$$$';
                        }

                        if ($saveArray['save_later'] == 2) {
                            $status = 'Pending';
                        }else{
                            $status = 'live';
                        }

                        $other_benefits = array($saveArray['other_benefits']);
                        $multiOtherbenefits = implode(",", $other_benefits);

                        $insertJob = [
                            'business_id'=>$saveArray['business_id'],
                            'job_title'=>$saveArray['job_title'],
                            'category'=>$saveArray['category'],
                            'description'=>$saveArray['description'],
                            'hours'=>$saveArray['hours'],
                            'duration'=>$saveArray['duration'],
                            'qualification'=>$multiqual?$multiqual:'',
                            'benefits'=>$benefit,
                            'salary'=>$salary,
                            'other_benefits'=>$multiOtherbenefits,
                            'advert_size'=>$saveArray['advert_size'],
                            'pause_date'=>$saveArray['pause_date'],
                            'applicant_limit'=>$saveArray['applicant_limit'],
                            'save_later'=>$saveArray['save_later'],
                            'job_status'=>$status,
                            'created_at'=>date('Y-m-d h:i:s')
                        ];
                        $insert = Job_detail::insertGetId($insertJob);
                        if($insert){
                            return response()->json([
                                'status'=>'1',
                                'message'=>'Job Posted Successfully'
                            ], $this->successStatus);
                        }
                        else{
                            return response()->json([
                                'message'=>'you have not started job service yet',
                                'status'=>'0'
                            ], $this->badrequest);
                        }
                    }
                }else{
                    return response()->json(['message'=>'you have not Admin Permission to do this','status'=>'0'], $this->badrequest);
                }
            }
        }
        else{
            return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }

    /*
      * getting live and paused jobs from this api in admin saction
    */   

    public function adminJobListing(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
            $validator = Validator::make($request->all(), [
                'business_id' => 'required',
            ]);
            if ($validator->fails()) {
                return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
            }
            else{
                $check = $this->businessPermission($request ,$saveArray['business_id'],$userExist['id']);
                if($check == 1){
                    $adminJobs = Job_detail::where('business_id',$saveArray['business_id'])
                    ->where('job_status','!=','closed')
                    ->get();
                    $allJobs = json_decode(json_encode($adminJobs), true);
                    if ($allJobs) {
                        $jobArray = [];
                        foreach ($allJobs as $job) {
                            $appliedCount = Job_Applicant::where('job_id',$job['id'])
                            ->where('status','1')->get();
                            $archievedCount = Job_Applicant::where('job_id',$job['id'])
                            ->where('status','3')->get();
                            $shortlistedCount = Job_Applicant::where('job_id',$job['id'])
                            ->where('status','2')->get();
                            if (empty($job['qualification'])) {
                                $qualArray = '';
                            }else{
                                $qualification = explode(',',$job['qualification']);
                                $qualArray = array();
                                foreach ($qualification as $key ) {
                                    $multi_user_ordering = explode('$',$key);
                                    $qual['qualification'] = $multi_user_ordering[0];
                                    $qual['required/beneficial'] = $multi_user_ordering[1];
                                    $qualArray[] = $qual;
                                }
                            }
                            $jobs['appliedCount'] = count($appliedCount);
                            $jobs['archievedCount'] = count($archievedCount);
                            $jobs['shortlistedCount'] = count($shortlistedCount);
                            $jobs['salary_guide'] = (String)$job['salary'];
                            $jobs['job_id'] = $job['id'];
                            $jobs['job_title'] = $job['job_title'];
                            $jobs['category'] = $job['category'];
                            $jobs['description'] = $job['description'];
                            $jobs['hours'] = $job['hours'];
                            $jobs['duration'] = $job['duration'];
                            $jobs['qualification'] = $qualArray?$qualArray:[];
                            $jobs['benefits'] = $job['benefits'];
                            $jobs['other_benefits'] = $job['other_benefits'];
                            $jobs['advert_size'] = $job['advert_size'];
                            $jobs['pause_date'] = $job['pause_date'];
                            $jobs['applicant_limit'] = $job['applicant_limit'];
                            $jobs['job_status'] = $job['job_status'];
                            $jobs['save_later'] = $job['save_later'];
                            $jobs['created_at'] = $job['created_at'];
                            $jobArray[] = $jobs;
                        }
                        return response()->json([
                            'status'=>'1',
                            'message'=>'Jobs fetched successfully',
                            'jobsArray'=>$jobArray
                        ], $this->successStatus);
                    }else{
                        return response()->json(['message'=>'No jobs posted yet','status'=>'1'], $this->successStatus);
                    }
                }else{
                    return response()->json(['message'=>'you have not Admin Permission to do this','status'=>'0'], $this->badrequest);
                }
            }
        }else{
            return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }

    /*
      * getting only closed jobs from this api in admin saction
    */

    public function closedJobListing(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
            $validator = Validator::make($request->all(), [
                'business_id' => 'required',
            ]);
            if ($validator->fails()) {
                return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
            }
            else{
                $check = $this->businessPermission($request ,$saveArray['business_id'],$userExist['id']);
                if($check == 1){
                    $adminJobs = Job_detail::where('business_id',$saveArray['business_id'])
                    ->where('job_status','=','closed')
                    ->get();
                    $allJobs = json_decode(json_encode($adminJobs), true);
                    if ($allJobs) {
                        $jobArray = [];
                        foreach ($allJobs as $job) {
                            $appliedCount = Job_Applicant::where('job_id',$job['id'])
                            ->where('status','1')->get();
                            $archievedCount = Job_Applicant::where('job_id',$job['id'])
                            ->where('status','3')->get();
                            $shortlistedCount = Job_Applicant::where('job_id',$job['id'])
                            ->where('status','2')->get();
                            if (empty($job['qualification'])) {
                                $qualArray = '';
                            }else{
                                $qualification = explode(',',$job['qualification']);
                                $qualArray = array();
                                foreach ($qualification as $key ) {
                                    $multi_user_ordering = explode('$',$key);
                                    $qual['qualification'] = $multi_user_ordering[0];
                                    $qual['required/beneficial'] = $multi_user_ordering[1];
                                    $qualArray[] = $qual;
                                }
                            }
                            $jobs['appliedCount'] = count($appliedCount);
                            $jobs['archievedCount'] = count($archievedCount);
                            $jobs['shortlistedCount'] = count($shortlistedCount);
                            $jobs['salary_guide'] = (String)$job['salary'];
                            $jobs['job_id'] = $job['id'];
                            $jobs['job_title'] = $job['job_title'];
                            $jobs['category'] = $job['category'];
                            $jobs['description'] = $job['description'];
                            $jobs['hours'] = $job['hours'];
                            $jobs['duration'] = $job['duration'];
                            $jobs['qualification'] = $qualArray?$qualArray:[];
                            $jobs['benefits'] = $job['benefits'];
                            $jobs['other_benefits'] = $job['other_benefits'];
                            $jobs['advert_size'] = $job['advert_size'];
                            $jobs['pause_date'] = $job['pause_date'];
                            $jobs['applicant_limit'] = $job['applicant_limit'];
                            $jobs['job_status'] = $job['job_status'];
                            $jobs['save_later'] = $job['save_later'];
                            $jobs['created_at'] = $job['created_at'];
                            $jobArray[] = $jobs;
                        }
                        return response()->json([
                            'status'=>'1',
                            'message'=>'Jobs fetched successfully',
                            'jobsArray'=>$jobArray
                        ], $this->successStatus);
                    }else{
                        return response()->json(['message'=>'No jobs posted yet','status'=>'1'], $this->successStatus);
                    }
                }else{
                    return response()->json(['message'=>'you have not Admin Permission to do this','status'=>'0'], $this->badrequest);
                }
            }
        }else{
            return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }

    // public function closedJobListing(Request $request)
    // {
    //     $saveArray = $request->all();
    //     $token = $request->header('token');
    //     $userExist = User::where(['remember_token'=>$token])->first();
    //     if($userExist){
    //         $validator = Validator::make($request->all(), [
    //             'business_id' => 'required',
    //         ]);
    //         if ($validator->fails()) {
    //             return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
    //         }
    //         else{
    //             $adminJobs = Job_detail::where('business_service_id',$saveArray['business_id'])
    //             ->where('job_status','closed')
    //             ->get();
    //             $allJobs = json_decode(json_encode($adminJobs), true);
    //             if ($allJobs) {
    //                 $jobArray = [];
    //                 foreach ($allJobs as $job) {
    //                     $appliedCount = Job_Applicant::where('job_id',$job['id'])
    //                     ->where('status','1')->get();
    //                     $archievedCount = Job_Applicant::where('job_id',$job['id'])
    //                     ->where('status','3')->get();
    //                     $shortlistedCount = Job_Applicant::where('job_id',$job['id'])
    //                     ->where('status','2')->get();
    //                     if (empty($job['qualification'])) {
    //                         $qualArray = '';
    //                     }else{
    //                         $qualification = explode(',',$job['qualification']);
    //                         $qualArray = array();
    //                         foreach ($qualification as $key ) {
    //                             $multi_user_ordering = explode('$',$key);
    //                             $qual['qualification'] = $multi_user_ordering[0];
    //                             $qual['required/beneficial'] = $multi_user_ordering[1];
    //                             $qualArray[] = $qual;
    //                         }
    //                     }
    //                     $jobs['appliedCount'] = count($appliedCount);
    //                     $jobs['archievedCount'] = count($archievedCount);
    //                     $jobs['shortlistedCount'] = count($shortlistedCount);
    //                     $jobs['saylry_guide'] = (String)$job['salary'];
    //                     $jobs['job_id'] = $job['id'];
    //                     $jobs['job_title'] = $job['job_title'];
    //                     $jobs['category'] = $job['category'];
    //                     $jobs['description'] = $job['description'];
    //                     $jobs['hours'] = $job['hours'];
    //                     $jobs['duration'] = $job['duration'];
    //                     $jobs['qualification'] = $qualArray?$qualArray:[];
    //                     $jobs['benefits'] = $job['benefits'];
    //                     $jobs['other_benefits'] = $job['other_benefits'];
    //                     $jobs['advert_size'] = $job['advert_size'];
    //                     $jobs['pause_date'] = $job['pause_date'];
    //                     $jobs['applicant_limit'] = $job['applicant_limit'];
    //                     $jobs['save_later'] = $job['save_later'];
    //                     $jobs['job_status'] = $job['job_status'];
    //                     $jobs['created_at'] = $job['created_at'];
    //                     $jobArray[] = $jobs;
    //                 }
    //                 return response()->json([
    //                     'status'=>'1',
    //                     'message'=>'Jobs fetched successfully',
    //                     'jobsArray'=>$jobArray
    //                 ], $this->successStatus);
    //             }else{
    //                 return response()->json(['message'=>'No jobs posted yet','status'=>'1'], $this->successStatus);
    //             }

    //         }
    //     }else{
    //         return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
    //     }
    // }

    /*
      * Updating jobs status Pause live and closed manually in this API
    */

    public function updateJobStatus(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
            $validator = Validator::make($request->all(), [
                'job_id' => 'required',
                'for' => 'required',
            ]);
            if ($validator->fails()) {
                return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
            }
            else{
                $checkJob = Job_detail::where('id',$saveArray['job_id'])
                // ->where('job_status','live')
                // ->orWhere('job_status','pause')
                ->first();
                if ($checkJob) {
                    $check = $this->businessPermission($request ,$checkJob['business_id'],$userExist['id']);
                    if ($check == 1) {
                        if ($saveArray['for'] == 'P') {
                            $update_job = [
                                'job_status' => 'pause',
                                'updated_at' => date('Y-m-d h:i:s')
                            ];
                            $update = Job_detail::where(['id'=>$saveArray['job_id']])->update($update_job);
                            if ($update) {
                                return response()->json(['status'=>'1','message'=>'Jobs paused successfully',], $this->successStatus);
                            }
                        }elseif ($saveArray['for'] == 'C') {
                            $update_job = [
                                'job_status' => 'closed',
                                'updated_at' => date('Y-m-d h:i:s')
                            ];
                            $update = Job_detail::where(['id'=>$saveArray['job_id']])->update($update_job);
                            if ($update) {
                                return response()->json(['status'=>'1','message'=>'Jobs closed successfully',], $this->successStatus);
                            }
                        }elseif ($saveArray['for'] == 'L') {
                            $update_job = [
                                'job_status' => 'live',
                                'updated_at' => date('Y-m-d h:i:s')
                            ];
                            $update = Job_detail::where(['id'=>$saveArray['job_id']])->update($update_job);
                            if ($update) {
                                return response()->json(['status'=>'1','message'=>'Jobs resume successfully',], $this->successStatus);
                            }
                        }
                    }
                    else{
                        return response()->json(['message'=>'you have not Admin Permission to do this','status'=>'0'], $this->badrequest);
                    }
                }else{
                    return response()->json(['status'=>'1','message'=>'No Job found',], $this->successStatus);
                }
            }
        }else{
            return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }

/*admin apis*/
    

    /*
      * This is Test API for checking cron jobs query is working fine or not so this is not using anywhere in app i will delete this code after completion of jobs apis
    */
    // public function updatingjobs(Request $request)
    // {
    //     // $allJobs = Job_detail::where('job_status','live')
    //     // ->where('created_at', '<=', Carbon::now()->subDays(90)->toDateTimeString())
    //     // ->get();
    //     // $jobs = json_decode(json_encode($allJobs), true);
    //     // return $jobs;
    //     // foreach ($jobs as $job) {
    //         // print_r($job['pause_date']);die();
    //         // $start = strtotime($job['pause_date']);
    //         // $end = date("Y-m-d", strtotime("+1 month", $start));
    //         $jobStatus =[
    //             'job_status'=>'closed'
    //         ];
    //         $update = Job_detail::where('job_status','live')
    //         ->where('created_at', '<=', Carbon::now()->subDays(90)->toDateTimeString())->Update($jobStatus);
    //         if ($update) {
    //             return "yes updated";
    //         }
    //     // }
    // }

    public function jobsListing(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
            $validator = Validator::make($request->all(), [
                'country' => 'required_without_all:state,resort_id',
                'state' => 'required_without_all:country,resort_id',
                'resort_id' => 'required_without_all:country,state'
            ]);
            if ($validator->fails()) {
                return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
            }
            else{
                if(in_array($saveArray['country'], ['USA','Canada'], true ) ){
                    $country_id = Country::where(['name'=>$saveArray['country']])->select('id')->first();
                    $resorts = Resort::select('state')->groupBy('state')->get();
                    $state_id = $resorts;
                    $states = State::where(['country_id'=>$country_id['id']])
                    ->whereIn('id', $state_id)
                    ->select('id','name')->get();
                    $stateArray = [];
                    if(count($states)){
                        foreach($states as $state){
                            $listing['state_id'] = (String)$state['id'];
                            $listing['state_name'] = $state['name'];
                            $listing['created_at'] = date('Y-m-d h:i:s');
                            $stateArray[] = $listing;
                        }
                        return response()->json([
                            'status'=>'1',
                            'message'=>'States fetched successfully',
                            'stateListing'=>$stateArray
                        ], $this->successStatus);
                    }
                    else{
                        return response()->json([
                            'message'=>'No State Found',
                            'status'=>'0'
                        ], $this->successStatus);
                    }
                }
                elseif($saveArray['state']){
                    $checkState = State::where(['name'=>$saveArray['state']])->select('id','name')->first();
                    if ($checkState) {
                        $getResort = Resort::where(['state'=>$checkState['id']])->get();
                        $resortArray = [];
                        if(count($getResort)){
                            foreach($getResort as $ls){
                                $checkIsAdmin = Permission::where(['resort_id'=>$ls['id']])->where(['user_id'=>$userExist['id']])->where(['role_type'=>'ADM'])->first();
                                if ($checkIsAdmin) {
                                    $isAdmin = 'true';
                                }else{
                                    $isAdmin = 'false';
                                }
                                $checkIsFav = Fav_resort::where('resort_id',$ls['id'])
                                ->where('user_id',$userExist['id'])
                                ->where('favourite','=','1')
                                ->first();
                                if ($checkIsFav['favourite'] == 1) {
                                    $isFav = 'Yes';
                                }else{
                                    $isFav = 'No';
                                }
                                $image = url('/resort_images',$ls['image']);
                                $countryName = Country::where(['id'=>$ls['country']])->first();
                                $stateName = State::where(['id'=>$ls['state']])->first();
                                $listing['is_favourite'] = $isFav;
                                $listing['isAdmin'] = $isAdmin;
                                $listing['resort_id'] = (String)$ls['id'];
                                $listing['resort_name'] = $ls['resort_name'];
                                $listing['address'] = $ls['address'];
                                $listing['phone_no'] = $ls['phone_no'];
                                $listing['toplat'] = $ls['top_latitude']?$ls['top_latitude']:'';
                                $listing['toplong'] = $ls['top_longitude']?$ls['top_longitude']:'';
                                $listing['lat'] = $ls['latitude'];
                                $listing['long'] = $ls['longitude'];
                                $listing['country_id'] = $ls['country'];
                                $listing['country'] = $countryName['name'];
                                $listing['state_id'] = $ls['state'];
                                $listing['state'] = $stateName['name'];
                                $listing['bcolor'] = $ls['bcolor'];
                                $listing['fcolor'] = $ls['fcolor'];
                                $listing['image'] = $image;
                                $listing['lift_names'] = $ls['lift_names'];
                                $listing['lift_numbers'] = $ls['lift_numbers'];
                                $listing['run_names'] = $ls['run_names'];
                                $listing['run_numbers'] = $ls['run_numbers'];
                                $listing['created_at'] = date('Y-m-d h:i:s');
                                $resortArray[] = $listing;
                            }
                            return response()->json([
                                'status'=>'1',
                                'message'=>'Resorts fetched successfully',
                                'resortListing'=>$resortArray
                            ], $this->successStatus);
                        }
                        else{
                            return response()->json([
                                'message'=>'No resorts',
                                'status'=>'1',
                                'resortListing'=>$resortArray
                            ], $this->successStatus);
                        }
                    }
                    else{
                        return response()->json([
                                'message'=>'No state Found',
                                'status'=>'0'
                            ], $this->badrequest);
                    }
                }
                elseif($saveArray['resort_id']){
                    $getBusiness = Job_detail::where('job_status','live')->get();
                    $getLiveJobs = json_decode(json_encode($getBusiness), true);
                    if ($getLiveJobs) {
                        $jobArray = [];
                        foreach($getLiveJobs as $ls){

                            $query = Job_detail::query();

                            if ( $saveArray['category'] )  {
                                $category = explode(',',$saveArray['category']);
                                $query = $query->where(function($query) use($category) {
                                    foreach($category as $cat) {
                                        $query->orWhere('category', 'like', "%$cat%");
                                    };
                                });
                            }

                            if ( $saveArray['duration'] )  {
                                $duration = explode(',',$saveArray['duration']);
                                $query = $query->where(function($query) use($duration) {
                                    foreach($duration as $due) {
                                        $query->orWhere('duration', 'like', "%$due%");
                                    };
                                });
                            }

                            if ( $saveArray['hours'] )  {
                                $hours = explode(',',$saveArray['hours']);
                                $query = $query->where(function($query) use($hours) {
                                    foreach($hours as $hour) {
                                        $query->orWhere('hours', 'like', "%$hour%");
                                    };
                                });
                            }

                            if ( $saveArray['other_benefits'] )  {
                                $other_benefits = explode(',',$saveArray['other_benefits']);
                                $query = $query->where(function($query) use($other_benefits) {
                                    foreach($other_benefits as $benefit) {
                                        $query->orWhere('other_benefits', 'like', "%$benefit%");
                                    };
                                });
                            }

                            if ( $saveArray['salary'] )  {
                                $salary = explode(',',$saveArray['salary']);
                                $query = $query->where(function($query) use($salary) {
                                    foreach($salary as $sal) {
                                        $query->orWhere('salary', 'like', "%$sal%");
                                    };
                                });
                            }

                            $checkJob = $query->where('job_status','live')
                            ->get();
                            foreach ($checkJob as $job) {
                                $jobStatus = Job_Applicant::where('job_id',$job['id'])
                                ->where('user_id',$userExist['id'])
                                ->first();
                                $busines = Job_detail::where('id',$job['business_id'])->first();
                                $business_id = Job::where('id',$busines['job_id'])->first();
                                $businessName = Business::where('id',$business_id['business_service_id'])->first();
                                $resortDetail = Resort::where('id',$saveArray['resort_id'])->first();
                                $country = Country::where('id',$resortDetail['country'])->first();
                                $state = State::where('id',$resortDetail['state'])->first();
                                $qualification = explode(',',$job['qualification']);

                                $qualArray = array();
                                foreach ($qualification as $key ) {
                                    $multi_user_ordering = explode('$',$key);
                                    $qual['qualification'] = $multi_user_ordering[0];
                                    $qual['required/beneficial'] = $multi_user_ordering[1];
                                    $qualArray[] = $qual;
                                }
                                $jobs['application_status'] = $jobStatus['status']?$jobStatus['status']:'';
                                $jobs['resort_id'] = $resortDetail['id'];
                                $jobs['resort_name'] = $resortDetail['resort_name'];
                                $jobs['business_id'] = $business_id['business_service_id'];
                                $jobs['business_name'] = $businessName['business_name'];
                                $jobs['country_id'] = $resortDetail['country'];
                                $jobs['country'] = $country['name'];
                                $jobs['state_id'] = $resortDetail['state'];
                                $jobs['state'] = $state['name'];
                                $jobs['job_id'] = $job['id'];
                                $jobs['job_title'] = $job['job_title'];
                                $jobs['category'] = $job['category'];
                                $jobs['description'] = $job['description'];
                                $jobs['hours'] = $job['hours'];
                                $jobs['duration'] = $job['duration'];
                                $jobs['qualification'] = $qualArray;
                                $jobs['benefits'] = $job['benefits'];
                                $jobs['other_benefits'] = $job['other_benefits'];
                                $jobs['advert_size'] = $job['advert_size'];
                                $jobs['pause_date'] = $job['pause_date'];
                                $jobs['job_status'] = $job['job_status'];
                                $jobs['applicant_limit'] = $job['applicant_limit'];
                                $jobs['save_later'] = $job['save_later'];
                                $jobs['created_at'] = $job['created_at'];
                                $jobArray[] = $jobs;
                            }
                        }
                        return response()->json([
                            'status'=>'1',
                            'message'=>'Jobs fetched successfully',
                            'jobsArray'=>$jobArray
                        ], $this->successStatus);
                    }else{
                        return response()->json([
                            'status'=>'1',
                            'message'=>'No Jobs at this resort',
                            'jobsArray'=>[]
                        ], $this->successStatus);
                    }
                }
            }
        }else{
            return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }

    public function deleteResumeItems(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
            $validator = Validator::make($request->all(), [
                'type' => 'required', //Education.Work,Qualification
                'id' => 'required',
            ]);
            if ($validator->fails()) {
                return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
            }
            else{
                if ($saveArray['type'] == 'E') {
                    $getEdu = Resume_education::where('id',$saveArray['id'])
                    ->where('user_id',$userExist['id'])
                    ->first();
                    $delete_edu = Resume_education::where('id',$saveArray['id'])
                    ->where('user_id',$userExist['id'])
                    ->delete();
                    if ($delete_edu) {
                        return response()->json(['status'=>'1','message'=>'Education updated successfully'], $this->successStatus);
                    }else{
                        return response()->json(['status'=>'1','message'=>'Already updated'], $this->successStatus);
                    }
                }elseif ($saveArray['type'] == 'W') {
                    $getWork = Resume_work::where('id',$saveArray['id'])
                    ->where('user_id',$userExist['id'])
                    ->first();
                    $delete_work = Resume_work::where('id',$saveArray['id'])
                    ->where('user_id',$userExist['id'])
                    ->delete();
                    if ($delete_work) {
                        return response()->json(['status'=>'1','message'=>'Work updated successfully'], $this->successStatus);
                    }else{
                        return response()->json(['status'=>'1','message'=>'Already updated'], $this->successStatus);
                    }
                }elseif ($saveArray['type'] == 'Q') {
                    $getResume = Resume::where('id',$saveArray['id'])
                    ->where('user_id',$userExist['id'])
                    ->first();
                    $qualification = array($saveArray['qualification']);
                    $multiqual = implode(",", $qualification);
                    $qual =['qualification'=>$multiqual];
                    $update = Resume::where(['user_id'=>$userExist['id']])->Update($qual); 
                    if ($update) {
                        return response()->json(['status'=>'1','message'=>'Qualification updated successfully'], $this->successStatus);
                    }else{
                        return response()->json(['status'=>'1','message'=>'Already updated'], $this->successStatus);
                    }
                }else{
                    return response()->json(['status'=>'1','message'=>'invalid inputs'], $this->successStatus);
                }
            }
        }else{
            return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }

    public function update_status(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
            $validator = Validator::make($request->all(), [
                'job_id' => 'required',
                'type' => 'required',
            ]);
            if ($validator->fails()) {
                return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
            }
            else{
                if ($saveArray['type'] == 'W') {
                    $updateStatus = Job_Applicant::where('job_id',$saveArray['job_id'])
                    ->where('user_id',$userExist['id'])->first();
                    if ($updateStatus) {
                        $updateJob = [
                            'status'=>'4',
                            'updated_at'=>date('Y-m-d h:i:s'),
                        ];
                        $status = Job_Applicant::where('user_id',$userExist['id'])
                        ->where('user_id',$userExist['id'])
                        ->update($updateJob);
                        if ($status) {
                            return response()->json(['status'=>'1','message'=>'application withdrawn successfully'], $this->successStatus);
                        }else{
                            return response()->json(['status'=>'0','message'=>'error'], $this->badrequest);
                        }
                    }
                }elseif ($saveArray['type'] == 'A') {
                    $validator = Validator::make($request->all(), [
                        'job_id' => 'required',
                        'type' => 'required',
                        'reject_reason' => 'required',
                    ]);
                    if ($validator->fails()) {
                        return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
                    }else{
                        $updateStatus = Job_Applicant::where('job_id',$saveArray['job_id'])
                        ->where('user_id',$userExist['id'])
                        ->where('status','!=','3')
                        ->first();
                        if ($updateStatus) {
                            $updateJob = [
                                'status'=>'3',
                                'reject_reason'=>$saveArray['reject_reason'],
                                'updated_at'=>date('Y-m-d h:i:s'),
                            ];
                            $status = Job_Applicant::where('user_id',$userExist['id'])
                            ->where('user_id',$userExist['id'])
                            ->update($updateJob);
                            /*send notification to user*/
                            $user_exists = User::where('id', $userExist['id'])
                            ->where('device_id','!=','Null')
                            ->first();
                            $array =  $user_exists['device_id'];
                            $message = array(
                                'message'=>$saveArray['reject_reason'],
                                'type'=>'archieved',
                            );
                            JobAPIController::send_notification_android($array, $message);
                            /*send notification to user*/
                            if ($status) {
                                return response()->json(['status'=>'1','message'=>'application archieved successfully'], $this->successStatus);
                            }else{
                                return response()->json(['status'=>'0','message'=>'error'], $this->badrequest);
                            }
                        }else {
                            return response()->json(['status'=>'1','message'=>'Applicant already Archieved'], $this->badrequest);
                        }
                    }
                }elseif ($saveArray['type'] == 'S') {
                    $checkalimit = Job_detail::where('id',$saveArray['job_id'])->first();
                    if ($checkalimit['applicant_limit'] == 0) {
                        return response()->json(['status'=>'1','message'=>'You do not have any shortlists available for this job and it will cost 150 credits to shortlist this candidate'], $this->successStatus);
                    }else {
                        $updateStatus = Job_Applicant::where('job_id',$saveArray['job_id'])
                        ->where('user_id',$userExist['id'])
                        ->where('status','!=','2')
                        ->first();
                        if ($updateStatus) {
                            $updateStatus = [
                                'status'=>'2',
                                'updated_at'=>date('Y-m-d h:i:s'),
                            ];
                            $status = Job_Applicant::where('user_id',$userExist['id'])
                            ->where('user_id',$userExist['id'])
                            ->update($updateStatus);

                            $lim = $checkalimit['applicant_limit'] - 1;
                            $updateLimit = [
                                'applicant_limit'=>$lim,
                                'updated_at'=>date('Y-m-d h:i:s'),
                            ];
                            $status = Job_detail::where('id',$saveArray['job_id'])
                            ->update($updateLimit);
                            /*send notification to user*/
                            $user_exists = User::where('id', $userExist['id'])
                            ->where('device_id','!=','Null')
                            ->first();
                            $jobTitle = Job_detail::where('id',$saveArray['job_id'])->first();
                            $array =  $user_exists['device_id'];
                            $message = array(
                                'message'=>" Congratulations - you’ve been shortlisted for the job  ". $jobTitle['job_title'] ." ",
                                'type'=>'shortlisted',
                                'job_id'=>$saveArray['job_id'],
                            );
                            JobAPIController::send_notification_android($array, $message);
                            /*send notification to user*/
                            if ($status) {
                                return response()->json(['status'=>'1','message'=>'application shortlisted successfully'], $this->successStatus);
                            }else{
                                return response()->json(['status'=>'0','message'=>'error'], $this->badrequest);
                            }
                        }else {
                            return response()->json(['status'=>'1','message'=>'Applicant already shortlisted'], $this->badrequest);
                        }
                    }
                }else{
                    return response()->json(['status'=>'1','message'=>'invalid inputs'], $this->successStatus);
                }
            }
        }else{
            return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }

    public function myJobs(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
            $validator = Validator::make($request->all(), [
                'filter_by' => 'required',
            ]);
            if ($validator->fails()) {
                return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
            }
            else{
                $terms = explode(',',$saveArray['filter_by']);

                $query = Job_Applicant::where('user_id',$userExist['id'])
                ->where(function($query) use($terms) {
                    foreach($terms as $term) {
                        $query->orWhere('status', 'like', "%$term%");
                    };
                })
                // ->inRandomOrder()
                //->take(3)
                ->get();
                // return $query;
                $searchArray = [];
                if(count($query)){
                    foreach($query as $job){
                        $jobDetail = Job_detail::where('id',$job['job_id'])->first();
                        $getResort = Job::where('id',$jobDetail['job_id'])->first();
                        $resortName = Resort::where('id',$getResort['resort_id'])->first();
                        $country = Country::where('id',$resortName['country'])->first();
                        $state = State::where('id',$resortName['state'])->first();
                        // $breakqual = explode(",",$jobDetail['qualification']);
                        // foreach ($breakqual as $qual) {
                        //     $doller = explode("$",$qual);
                        //     print_r($doller);die();
                        // }
                        $qualification = explode(',',$jobDetail['qualification']);

                        $qualArray = array();
                        foreach ($qualification as $key ) {
                            $multi_user_ordering = explode('$',$key);
                            $qual['qualification'] = $multi_user_ordering[0];
                            $qual['required/beneficial'] = $multi_user_ordering[1];
                            $qualArray[] = $qual;
                        }
                        
                        $job_details = array(
                            'resort_name'=>$resortName['resort_name'],
                            'country'=>$country['name'],
                            'state'=>$state['name'],
                            'job_id'=>(String)$jobDetail['id'],
                            'job_title'=>$jobDetail['job_title'],
                            'category'=>$jobDetail['category'],
                            'description'=>$jobDetail['description'],
                            'hours'=>$jobDetail['hours'],
                            'duration'=>$jobDetail['duration'],
                            'qualification'=>$qualArray,
                            'benefits'=>$jobDetail['benefits'],
                            'other_benefits'=>$jobDetail['other_benefits'],
                            'advert_size'=>$jobDetail['advert_size'],
                            'pause_date'=>$jobDetail['pause_date']->format('Y-m-d'),
                            'applicant_limit'=>$jobDetail['applicant_limit'],
                            'created_at'=>$jobDetail['created_at']->format('Y-m-d'),
                          );
                        // $listing['id'] = $job['id'];
                        $listing['job_id'] = $job['job_id'];
                        $listing['user_id'] = $job['user_id'];
                        $listing['hiring_reason'] = $job['hiring_reason'];
                        $listing['status'] = $job['status'];
                        $listing['created_at'] = $job['created_at']->format('Y-m-d');
                        $listing['job_detail'] = $job_details;
                        $searchArray[] = $listing;
                    }
                    return response()->json([
                        'status'=>'1',
                        'message'=>'Jobs fetched successfully',
                        'myjobs'=>$searchArray
                    ], $this->successStatus);
                }else{
                    return response()->json(['message'=>'No Jobs','status'=>'0'], $this->successStatus);
                }
            }
        }else{
            return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }

    public function applyJob(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
            $validator = Validator::make($request->all(), [
                'job_id' => 'required',
                'why_you' => 'required',
            ]);
            if ($validator->fails()) {
                return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
            }
            else{
                $checkJob = Job_detail::where('id',$saveArray['job_id'])->first();
                if ($checkJob) {
                    $appliedJob = Job_Applicant::where('user_id',$userExist['id'])
                    ->where('job_id',$saveArray['job_id'])
                    ->first();
                    if ($appliedJob) {
                        return response()->json(['message'=>'you already applied for this job','status'=>'1'], $this->successStatus);
                    }else{
                        $applyJob = [
                            'user_id'=>$userExist['id'],
                            'job_id'=>$saveArray['job_id'],
                            'hiring_reason'=>$saveArray['why_you'],
                            'status'=>'1',
                            'created_at'=>date('Y-m-d h:i:s'),
                        ];
                        $apply = Job_Applicant::insertGetId($applyJob);
                        /*send notification to admins*/
                        $jobID = Job_detail::where('id',$saveArray['job_id'])->first();
                        $job_id = $jobID['job_id'];
                        $jobDetail = Job::where('id',$job_id)->first();
                        $jobadmin = $jobDetail['business_service_id'];
                        $businessAdmin = Business::where('id',$jobadmin)->first();
                        $owner = array($businessAdmin['owner_id']);
                        $admins = Permission::where('business_service_id',$jobadmin)
                        ->where('role_type','ADM')
                        ->select('user_id')
                        ->get();
                        $allAdm = json_decode(json_encode($admins), true);
                        $allowners = array_column($allAdm, 'user_id');
                        $both = array_merge($owner, $allowners);
                        $user_exists = User::whereIn('id', $both)
                        ->where('device_id','!=','Null')
                        ->get();
                        $array = array();
                        foreach($user_exists as $val){
                            $array[]    =  $val['device_id'];
                        }
                        $job_detail = Job_detail::where('id',$saveArray['job_id'])->first();
                        $job_name = $job_detail['job_title'];
                        $message = array(
                            'message'=>" You have a new application for the job ". $job_name ." ",
                            'type'=>'job',
                            'job_id'=>$saveArray['job_id'],
                        );
                        JobAPIController::android_send_notification($array, $message);
                        /*send notification to admins*/
                        return response()->json(['message'=>'Job application submitted','status'=>'1'], $this->successStatus);
                    }
                }else{
                    return response()->json(['message'=>'No Job found','status'=>'1'], $this->successStatus);
                }
            }
        }else{
            return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }

    public function createResume(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
            $checkUser = Resume::where('user_id',$userExist['id'])->first();
            if ($checkUser) {
                if($request->hasFile('photo')){
                    $file = time().$request->photo->getClientOriginalName();
                    $request->photo->move(public_path('/resume_images') . '/', $file);
                }
                else{
                    $file = 'default.jpg';
                }

                $qualification = array($saveArray['qualification']);
                $multiqual = implode(",", $qualification);

                $user_id = $userExist['id'];
                $name = $saveArray['name'];
                $dob = $saveArray['dob'];
                $phone_no = $saveArray['phone_no'];
                $email = $saveArray['email'];
                $personal_statement = $saveArray['personal_statement'];

                $updateResume = [
                    'user_id'=>$user_id,
                    'name'=>$name,
                    'dob'=>$dob,
                    'phone_no'=>$phone_no,
                    'email'=>$email,
                    'photo'=>$file,
                    'personal_statement'=>$personal_statement,
                    'qualification'=>$multiqual,
                    'updated_at'=>date('Y-m-d h:i:s'),
                ];
                $resume_id = Resume::where('user_id',$userExist['id'])
                ->update($updateResume);
                if($resume_id){
                    return response()->json([
                        'status'=>'1',
                        'message'=>'Resume Updated Successfully',
                    ], $this->successStatus);
                }
                else{
                    return response()->json([
                        'message'=>'Error',
                        'status'=>'0'
                    ], $this->badrequest);
                }
            }else{
                if($request->hasFile('photo')){
                    $file = time().$request->photo->getClientOriginalName();
                    $request->photo->move(public_path('/resume_images') . '/', $file);
                }
                else{
                    $file = 'default.jpg';
                }

                $qualification = array($saveArray['qualification']);
                $multiqual = implode(",", $qualification);

                $user_id = $userExist['id'];
                $name = $saveArray['name'];
                $dob = $saveArray['dob'];
                $phone_no = $saveArray['phone_no'];
                $email = $saveArray['email'];
                $personal_statement = $saveArray['personal_statement'];

                $addResume = [
                    'user_id'=>$user_id,
                    'name'=>$name,
                    'dob'=>$dob,
                    'phone_no'=>$phone_no,
                    'email'=>$email,
                    'photo'=>$file,
                    'personal_statement'=>$personal_statement,
                    'qualification'=>$multiqual,
                    'created_at'=>date('Y-m-d h:i:s'),
                ];
                $resumeID = Resume::insertGetId($addResume);
                if($resumeID){
                    return response()->json([
                        'status'=>'1',
                        'message'=>'Resume Created Successfully',
                    ], $this->successStatus);
                }
                else{
                    return response()->json([
                        'message'=>'Error',
                        'status'=>'0'
                    ], $this->badrequest);
                }
            }
        }else{
            return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }

    public function educationDetails(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
            $validator = Validator::make($request->all(), [
                'qualification_title' => 'required',
                'grade' => 'required',
                'institution' => 'required',
                'town_city' => 'required',
                'country' => 'required',
                // 'from_year' => 'required',
                // 'to_year' => 'required'
            ]);
            if ($validator->fails()) {
                return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
            }
            else{
                $addEducation = [
                    'user_id'=>$userExist['id'],
                    'qualification_title'=>$saveArray['qualification_title'],
                    'grade'=>$saveArray['grade'],
                    'institution'=>$saveArray['institution'],
                    'town_city'=>$saveArray['town_city'],
                    'country'=>$saveArray['country'],
                    'from_year'=>$saveArray['from_year']?$saveArray['from_year']:'',
                    'to_year'=>$saveArray['to_year']?$saveArray['to_year']:'',
                    'created_at'=>date('Y-m-d h:i:s'),
                ];
                $education = Resume_education::insertGetId($addEducation);
                if($education){
                    return response()->json([
                        'status'=>'1',
                        'message'=>'Education saved',
                    ], $this->successStatus);
                }
                else{
                    return response()->json([
                        'message'=>'Error',
                        'status'=>'0'
                    ], $this->badrequest);
                }
            }
        }else{
            return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }

    public function workHistory(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
            $validator = Validator::make($request->all(), [
                'job_title' => 'required',
                'organisation' => 'required',
                'town_city' => 'required',
                'country' => 'required',
                // 'from_year' => 'required',
                // 'to_year' => 'required',
                'roles_responsibilities' => 'required'
            ]);
            if ($validator->fails()) {
                return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
            }
            else{
                $workExp = [
                    'user_id'=>$userExist['id'],
                    'job_title'=>$saveArray['job_title'],
                    'organisation'=>$saveArray['organisation'],
                    'town_city'=>$saveArray['town_city'],
                    'country'=>$saveArray['country'],
                    'from_year'=>$saveArray['from_year']?$saveArray['from_year']:'',
                    'to_year'=>$saveArray['to_year']?$saveArray['to_year']:'',
                    'roles_responsibilities'=>$saveArray['roles_responsibilities'],
                    'created_at'=>date('Y-m-d h:i:s'),
                ];
                $work = Resume_work::insertGetId($workExp);
                if($work){
                    return response()->json([
                        'status'=>'1',
                        'message'=>'Work history saved',
                    ], $this->successStatus);
                }
                else{
                    return response()->json([
                        'message'=>'Error',
                        'status'=>'0'
                    ], $this->badrequest);
                }
            }
        }else{
            return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }

    public function getResume(Request $request)
    {

        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
            $validator = Validator::make($request->all(), [
                'user_id' => 'required'
            ]);
            if ($validator->fails()) {
                return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
            }
            else{
                $check = Resume::where('user_id',$saveArray['user_id'])->first();
                $checkResume = json_decode(json_encode($check), true);
                if ($checkResume) {
                    $image = url('/resume_images',$checkResume['photo']);
                    $education = Resume_education::where('user_id',$saveArray['user_id'])->get();
                    $work = Resume_work::where('user_id',$saveArray['user_id'])->get();
                    $qualification = explode(',',$checkResume['qualification']);
                    $qualArray = array();

                    foreach ($qualification as $key ) {
                        $qual['qualification'] = $key;
                        $qualArray[] = $qual;
                    }
                    $resume_details = array(
                        'id'=>(String)$checkResume['id'],
                        'name'=>$checkResume['name'],
                        'dob'=>$checkResume['dob']?$checkResume['dob']:'',
                        'phone_no'=>$checkResume['phone_no']?$checkResume['phone_no']:'',
                        'email'=>$checkResume['email']?$checkResume['email']:'',
                        'personal_statement'=>$checkResume['personal_statement']?$checkResume['personal_statement']:'',
                        'qualification'=>$checkResume['qualification']?$checkResume['qualification']:'',
                        'qualificationArray'=>$qualArray,
                        'photo'=>$image,
                        'education'=>$education,
                        'work_history'=>$work,
                        'created_at'=>$checkResume['created_at'],
                    );
                    return response()->json([
                        'status'=>'1',
                        'message'=>'Resume fetched successfully',
                        'resume_detail'=>$resume_details,
                    ], $this->successStatus);
                }else{
                    return response()->json([
                        'status'=>'1',
                        'message'=>'you have not created your resume yet',
                        'resume_detail'=>'',
                    ], $this->successStatus);
                }
            }
        }else{
            return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }

    public function candidatesListing(Request $request)
    {
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where(['remember_token'=>$token])->first();
        if($userExist){
            $validator = Validator::make($request->all(), [
                'job_id' => 'required',
                'filter_by' => 'required',
            ]);
            if ($validator->fails()) {
                return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
            }
            else{
                $terms = explode(',',$saveArray['filter_by']);
                $query = Job_Applicant::where('job_id',$saveArray['job_id'])
                ->where(function($query) use($terms) {
                    foreach($terms as $term) {
                        $query->orWhere('status', 'like', "%$term%");
                    };
                })
                ->get();

                $searchArray = [];
                if(count($query)){
                    foreach($query as $job){
                        $getQuali = Resume::where('user_id',$job['user_id'])->first();
                        $get_resume = json_decode(json_encode($getQuali), true);
                        
                        $image = url('/resume_images',$get_resume['photo']);
                        $education = Resume_education::where('user_id',$job['user_id'])->get();
                        $work = Resume_work::where('user_id',$job['user_id'])->get();
                        $qualification = explode(',',$get_resume['qualification']);
                        $qualArray = array();

                        foreach ($qualification as $key ) {
                            $qual['qualification'] = $key;
                            $qualArray[] = $qual;
                        }

                        $resume_details = array(
                            // 'id'=>(String)$getQuali['id'],
                            'name'=>$get_resume['name'],
                            'dob'=>$get_resume['dob']?$get_resume['dob']:'',
                            'phone_no'=>$get_resume['phone_no']?$get_resume['phone_no']:'',
                            'email'=>$get_resume['email']?$get_resume['email']:'',
                            'personal_statement'=>$get_resume['personal_statement']?$get_resume['personal_statement']:'',
                            'qualification'=>$get_resume['qualification']?$get_resume['qualification']:'',
                            'qualificationArray'=>$qualArray,
                            'photo'=>$image,
                            'education'=>$education,
                            'work_history'=>$work,
                            'created_at'=>$get_resume['created_at'],
                        );
                        /*counts*/
                        $idsCount = explode(',',$getQuali['qualification']);
                        $qualicount = count($idsCount);
                        $getEdu = Resume_education::where('user_id',$job['user_id'])->get();
                        $educount = $getEdu->count();
                        $getWork = Resume_work::where('user_id',$job['user_id'])->get();
                        $workcount = $getWork->count();
                        /*counts*/
                        /*job_detail*/
                        $job_det = Job_detail::where('id',$job['job_id'])->first();
                        /**/
                        $userDetail = User::where('id',$job['user_id'])->first();
                        // $image = url('/images',$userDetail['image']);
                        $image = url('/resume_images',$get_resume['photo']);
                        $listing['qualification_count'] = $qualicount;
                        $listing['education_count'] = $educount;
                        $listing['work_count'] = $workcount;
                        $listing['advert_size'] = $job_det['advert_size'];
                        $listing['applicant_limit'] = $job_det['applicant_limit'];
                        $listing['id'] = $job['id'];
                        $listing['job_id'] = $job['job_id'];
                        $listing['user_id'] = $job['user_id'];
                        $listing['user_name'] = $get_resume['name'];
                        $listing['image'] = $image?$image:'';
                        $listing['hiring_reason'] = $job['hiring_reason'];
                        $listing['status'] = $job['status'];
                        $listing['resume'] = $resume_details;
                        $listing['created_at'] = $job['created_at']->format('Y-m-d');
                        $searchArray[] = $listing;
                    }
                    return response()->json([
                        'status'=>'1',
                        'message'=>'Applicants fetched successfully',
                        'myjobs'=>$searchArray
                    ], $this->successStatus);
                }else{
                    return response()->json(['message'=>'No Applicants','status'=>'0'], $this->successStatus);
                }
            }
        }else{
            return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }

    /*notification for multi user notification*/

    public function android_send_notification($device_id,$message)
    {
        // print_r($message);die();
        $url = 'https://fcm.googleapis.com/fcm/send';
        // $url = 'https://android.googleapis.com/gcm/send';
        $fields = array(
            'registration_ids' => $device_id,
            'data' => $message,
        );
        if(!defined('GOOGLE_API_KEY')){
            $GOOGLE_API_KEY = 'AAAAYBmHz5g:APA91bHGee6_av7cgW_nA9eNQF_J0SEWlqcS_arCLAdnMStD69eFyjdDx5iDdCV5obWP_9sYD_D3eMHn06hHSKNSD0vSVkTP4-JTl7kOyjPSd8ubu4xgtpy_1nh_9mtBvWvyi6fMd-j4';
        }
        $headers = array(
            'Authorization: key='.$GOOGLE_API_KEY,
            'Content-Type: application/json'
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        if ($result === FALSE){
            die('Curl failed: ' . curl_error($ch));
        }
        curl_close($ch);
        return $result;
        // print_r($result);die;
    }

    /*notification for single user notification*/

    public function send_notification_android($device_id,$message)
    {
        // print_r($message);die();
        $url = 'https://fcm.googleapis.com/fcm/send';
        // $url = 'https://android.googleapis.com/gcm/send';
        $fields = array(
            'registration_ids' => array($device_id),
            'data' => $message,
        );
        if(!defined('GOOGLE_API_KEY')){
            $GOOGLE_API_KEY = 'AAAAYBmHz5g:APA91bHGee6_av7cgW_nA9eNQF_J0SEWlqcS_arCLAdnMStD69eFyjdDx5iDdCV5obWP_9sYD_D3eMHn06hHSKNSD0vSVkTP4-JTl7kOyjPSd8ubu4xgtpy_1nh_9mtBvWvyi6fMd-j4';
        }
        $headers = array(
            'Authorization: key='.$GOOGLE_API_KEY,
            'Content-Type: application/json'
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        if ($result === FALSE){
            die('Curl failed: ' . curl_error($ch));
        }
        curl_close($ch);
        return $result;
        // print_r($result);die;
    }

    public function addCard(Request $request){
        
        $saveArray = $request->all();
        $validator = Validator::make($request->all(), [ 
                'user_id'       => 'required',
            ]);
        if($validator->fails()) {           
            return response()->json(['message'=>'User Id is required','status'=>"0"],401);            
        }
        $saveArray = $request->all();
        $userExist = User::where(['id'=> $saveArray['user_id']])->first();
        if($userExist){
            $card = "";
            $user_id = $saveArray['user_id'];
            $card_holder_name = $saveArray['card_holder_name'];
            $card_number = $saveArray['card_number'];
            $exp_month = $saveArray['exp_month'];
            $exp_year = $saveArray['exp_year'];
            $cvc = $saveArray['cvc'];
            //$card_type = $saveArray['card_type'];

            \Stripe\Stripe::setApiKey("sk_test_vP2uSW8JEbW4czdnGePhMHpp00stTElYgy");
            $token = \Stripe\Token::create([
                'card' => [
                    'number'    => $card_number,
                    'exp_month' => $exp_month,
                    'exp_year'  => $exp_year,
                    'cvc'       => $cvc,
                    'name'=> $card_holder_name
                ],
            ]);
            // print_r($token);die();
            $existing_card = 0;
            if($userExist['stripe_customer_id'] != ""){
                $customer_id = $userExist['stripe_customer_id'];
                $cards = \Stripe\Customer::retrieve($customer_id)->sources->all(['object' => 'card']);
                if($cards['data']){
                    foreach($cards['data'] as $card){
                        if($card['fingerprint'] == $token['card']['fingerprint']){
                            $existing_card = 1;
                        }
                    }
                }
                if($existing_card == 0){
                    $get_customer = \Stripe\Customer::retrieve($customer_id);
                    $card = $get_customer->sources->create(["source" => $token['id']]);
                }
            }
            else{
                $customer = \Stripe\Customer::create([
                    "description" => $userExist['email']
                ]);

                $update = User::where(['id'=> $saveArray['user_id']])->update([
                    'stripe_customer_id' => $customer['id'],
                    'updated_at' => date('Y-m-d h:i:s')
                ]);
                $get_customer = \Stripe\Customer::retrieve($customer['id']);
                $card = $get_customer->sources->create(["source" => $token['id']]);
            }
            if($existing_card == 0){
                return Response::json([
                    'status'=>$card?'1':'0',
                    'message'=>($card)?'Card Added Successfully':'Error, Add Card'
                ]);
            }
            else{
                return response()->json(['message'=>'Card Already Exists','status'=>'0'], 401);
            }
            
        }
        else{
            return response()->json(['message'=>'User does not exist','status'=>'0'], 401); 
        }
    }

    public function getCard(Request $request){
        
        $saveArray = $request->all();
        $validator = Validator::make($request->all(), [ 
                'user_id'       => 'required',
            ]);
        if ($validator->fails()) {          
            return response()->json(['message'=>'User Id is required','status'=>"0"],401);
        }
        $userExist = User::where(['id'=> $saveArray['user_id']])->first() ;
        if($userExist){
            if($userExist['stripe_customer_id'] != ""){
                $stripe_customer_id = $userExist['stripe_customer_id'];
                \Stripe\Stripe::setApiKey("sk_test_vP2uSW8JEbW4czdnGePhMHpp00stTElYgy");

                //getting default card
                $customer = \Stripe\Customer::retrieve($stripe_customer_id);
                $default_card_id =$customer['default_source'];

                //getting all cards
                $cards = \Stripe\Customer::retrieve($stripe_customer_id)->sources->all(['object' => 'card']);
                
                if($cards['data']){
                    $card_list = array();
                    foreach($cards['data'] as $card){
                        $default_card = "N";
                        if($default_card_id == $card['id']){
                            $default_card = "Y";
                        }
                        $card_list[] = array(
                            'id' => $card['id'],
                            'object' => $card['object']?$card['object']:'',
                            'address_city' => $card['address_city']?$card['address_city']:'',
                            'address_country' => $card['address_country']?$card['address_country']:'',
                            'address_line1' => $card['address_line1']?$card['address_line1']:'',
                            'address_line1_check' => $card['address_line1_check']?$card['address_line1_check']:'',
                            'address_line2' => $card['address_line2']?$card['address_line2']:'',
                            'address_state' => $card['address_state']?$card['address_state']:'',
                            'address_zip' => $card['address_zip']?$card['address_zip']:'',
                            'address_zip_check' => $card['address_zip_check']?$card['address_zip_check']:'',
                            'brand' => $card['brand']?$card['brand']:'',
                            'country' => $card['country']?$card['country']:'',
                            'customer' => $card['customer']?$card['customer']:'',
                            'cvc_check' => $card['cvc_check']?$card['cvc_check']:'',
                            'dynamic_last4' => $card['dynamic_last4']?$card['dynamic_last4']:'',
                            'exp_month' => $card['exp_month']?$card['exp_month']:'',
                            'exp_year' => $card['exp_year']?$card['exp_year']:'',
                            'fingerprint' => $card['fingerprint']?$card['fingerprint']:'',
                            'funding' => $card['funding']?$card['funding']:'',
                            'last4' => $card['last4']?$card['last4']:'',
                            'metadata' => $card['metadata']?$card['metadata']:'',
                            'name' => $card['name']?$card['name']:'',
                            'tokenization_method' => $card['tokenization_method']?$card['tokenization_method']:'',
                            'default_card'=>$default_card
                        );
                    }
                    return response()->json(['status'=>'1','message'=>'Cards get successfully','cards'=>$card_list]);
                }
                else{
                    return response()->json(['message'=>'No Cards Found','status'=>'0'], 401);
                }
            }
            else{
                return response()->json(['message'=>'No Cards Found','status'=>'0'], 401);
            }
        }
        else{
            return response()->json(['message'=>'User does not exist','status'=>'0'], 401);
        }
    }

    // public function create_stripe_user(Request $request){
    //     $saveArray = $request->all();
    //     $validator = Validator::make($request->all(), [
    //         'user_id' => 'required'
    //     ]);

    //     if ($validator->fails()) {
    //         return response()->json(['message'=>'User ID is required','status'=>"0"],401);
    //     }else{
    //         $userExist = User::where('id',$saveArray['user_id'])->first();
    //         if($userExist){
    //             if (empty($userExist['stripe_id'])) {
    //                 // dd("yes");
    //                 \Stripe\Stripe::setApiKey("sk_test_vP2uSW8JEbW4czdnGePhMHpp00stTElYgy");
    //                 $account = \Stripe\Account::create([
    //                     'country' => $userExist['code'],
    //                     'type' => 'custom',
    //                     'capabilities' => [
    //                         'card_payments' => ['requested' => true],
    //                         'transfers' => ['requested' => true],
    //                     ],
    //                 ]);
    //                 if($account){
    //                     $updateData = [
    //                         'stripe_id' => $account['id'],
    //                         'stripe_verified'=>'N',
    //                         'updated_at' => date('Y-m-d H:i:s')
    //                     ];
    //                     $update = User::where(['id'=>$userExist->id])->update($updateData);
    //                     return response()->json(['status'=>'1','message'=>'Stripe User created successfully'], $this->successStatus);
    //                 }
    //             }else{
    //                 // dd("no");
    //                 return response()->json(['status'=>'1','message'=>'Stripe User Created Already'], $this->successStatus);
    //             }
    //         }else{
    //                 return response()->json(['status'=>'1','message'=>'User not found'], $this->successStatus);
    //             }
    //     }
    // }

    public function stripeConnect(Request $request){
        if (isset($request->code)) {
            $token_request_body = array(
                'grant_type' => 'authorization_code',
                'code' => $request->code,
                'client_secret' => 'sk_test_vP2uSW8JEbW4czdnGePhMHpp00stTElYgy',
            );
            $TOKEN_URI = "https://connect.stripe.com/oauth/token";
            $req = curl_init($TOKEN_URI);
            curl_setopt($req, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($req, CURLOPT_POST, true);
            curl_setopt($req, CURLOPT_POSTFIELDS, http_build_query($token_request_body));
            curl_setopt($req, CURLOPT_SSL_VERIFYPEER, false);
            $resp = json_decode(curl_exec($req), true);
            curl_close($req);

            if (!array_key_exists("error", $resp)) {
                return response()->json([
                    'status'=>'1',
                    'message'=>'Stripe Account Connected',
                    'code'=> $resp,
                ], $this->successStatus);
            }
        }
    }

    public function link_stripe(Request $request)
    {
        $saveArray = $request->all();
        $validator = Validator::make($request->all(), [ 
                'user_id' => 'required',
                'stripe_user_id' => 'required',
            ]);
        if ($validator->fails()) {          
            return response()->json(['message'=>'User ID is required','status'=>"0"],401);            
        }
        $userExist = User::where(['id'=> $saveArray['user_id']])->first();
        if($userExist){
            $datetime = date('Y-m-d H:i:s');
            $updateData = [
                'stripe_id' => $saveArray['stripe_user_id'],
                'stripe_verified' => "Y",
                'updated_at' => $datetime
            ];
            $update = User::where(['id'=>$saveArray['user_id']])->update($updateData);
            if($update){
                return response()->json(['status'=>'1','message'=>'Stripe ID saved successfully'], $this->successStatus);
            }
            else{
                return response()->json(['message'=>'Something went wrong','status'=>'0'], 401);
            }
        }
        else{
            return response()->json(['message'=>'User does not exist','status'=>'0'], 401); 
        }
    }

    public function removeCard(Request $request){
        
        $saveArray = $request->all();
        $validator = Validator::make($request->all(), [ 
                'user_id'       => 'required',
                'card_id'       => 'required',
            ]);
        if ($validator->fails()) {          
            return response()->json(['message'=>'User Id is required','status'=>"0"],401);
        }
        $userExist = User::where(['id'=> $saveArray['user_id']])->first() ;
        if($userExist){
            if($userExist['stripe_customer_id'] != ""){
                $stripe_customer_id = $userExist['stripe_customer_id'];
                \Stripe\Stripe::setApiKey("sk_test_vP2uSW8JEbW4czdnGePhMHpp00stTElYgy");
                $customer = \Stripe\Customer::retrieve($stripe_customer_id);
                $card_id = $saveArray['card_id'];
                $existing_card = 0;
                $cards = \Stripe\Customer::retrieve($stripe_customer_id)->sources->all(['object' => 'card']);
                if($cards['data']){
                    foreach($cards['data'] as $card){
                        if($card['id'] == $card_id){
                            $existing_card = 1;
                        }
                    }
                }
                if($existing_card == 1){
                    $delete = $customer->sources->retrieve($card_id)->delete();
                    if($delete){
                        return response()->json(['status'=>'1','message'=>'Card Deleted Successfully']);
                    }
                    else{
                        return response()->json(['message'=>'Something went wrong','status'=>'0'], 401);
                    }
                }
                else{
                    return response()->json(['message'=>'Card Already Deleted','status'=>'0'], 401);
                }
            }
            else{
                $user_id = $saveArray['user_id'];
                $cards= Card::where(['id'=>$saveArray['card_id']])->delete();
                if($cards){
                    return response()->json(['status'=>'1','message'=>'Card Deleted Successfully']);
                }
                else{
                    return response()->json(['message'=>'Something went wrong','status'=>'0'], 401);
                }
            }
        }
        else{
            return response()->json(['message'=>'User does not exist','status'=>'0'], 401);
        }
    }

    public function set_default_card(Request $request){
        $saveArray = $request->all();
        $validator = Validator::make($request->all(), [ 
                'user_id'       => 'required',
                'card_id'       => 'required',
            ]);
        if ($validator->fails()) {          
            return response()->json(['message'=>'User Id is required','status'=>"0"],401);
        }
        $userExist = User::where(['id'=> $saveArray['user_id']])->first();
        if($userExist){
            if($userExist['stripe_customer_id'] != ""){
                $stripe_customer_id = $userExist['stripe_customer_id'];
                \Stripe\Stripe::setApiKey("sk_test_vP2uSW8JEbW4czdnGePhMHpp00stTElYgy");
                $customer = \Stripe\Customer::retrieve($stripe_customer_id);
                $customer->default_source=$saveArray['card_id'];
                $customer->save();  
                return response()->json(['status'=>'1','message'=>'Card set default successfully']);
            }
            else{
                return response()->json(['status'=>'0','message'=>'Card not found']);
            }
        }
        else{
            return response()->json(['message'=>'User does not exist','status'=>'0'], 401);
        }
    }
    /*old method and manual method for connect stripe account*/
    // public function create_stripe_account(Request $request){
    //     try{
    //         $saveArray = $request->all();
    //         $validator = Validator::make($request->all(), [
    //             'user_id'=> 'required', 
    //             // 'country' => 'required',
    //             'first_name' => 'required',
    //             // 'business_tax_id' => 'required',
    //             'personal_id_number' => 'required',
    //             'dob' => 'required',
    //             'address_line' => 'required',
    //             'city' => 'required',
    //             'postal_code' => 'required',
    //             'state' => 'required',
    //             'country_of_bank' => 'required',
    //             'currency' => 'required',
    //             'account_number' => 'required',
    //             'routing_number' => 'required',
    //             'ip_address' => 'required'
    //         ]);
    //         if($validator->fails()) {           
    //             return response()->json(['message'=>$validator->errors()->first(),'status'=>"0"],401);
    //         }
    //         else{
    //             $userdetails = User::where(['id'=>$saveArray['user_id']])->select('email','stripe_id')->first();
    //             // dd($userdetails);
    //             if($userdetails){
    //                 $dob = $saveArray['dob'];
    //                 $orderdate = explode('-', $dob);
    //                 $day = $orderdate[0];
    //                 $month = $orderdate[1];
    //                 $year = $orderdate[2];
    //                 \Stripe\Stripe::setApiKey("sk_test_vP2uSW8JEbW4czdnGePhMHpp00stTElYgy");
    //                 $acct = \Stripe\Account::retrieve($userdetails['stripe_id']);
    //                 print_r(json_encode($acct));die();
    //                 $acct->email = $userdetails['email'];
    //                 $acct->business_profile = array(
    //                     'business_name' => $saveArray['first_name'],
    //                     // 'business_tax_id' => $saveArray['business_tax_id'],
    //                     'personal_id_number' => $saveArray['personal_id_number'],
    //                     "dob" => array(
    //                         "day" => $day,
    //                         "month" => $month,
    //                         "year" => $year
    //                     ),

    //                     "address" => array(
    //                         "city" => $saveArray['city'],
    //                         "line1" => $saveArray['address_line'],
    //                         "postal_code" => $saveArray['postal_code'],
    //                         "state"=>$saveArray['state']
    //                     ),

    //                     "business_type" => "individual",
    //                     "verification" => array(
    //                         "document" => null
    //                     )
    //                 );
    //                 $acct->external_account = array(
    //                   "country" => $saveArray['country_of_bank'],
    //                   "data" => array(
    //                         "day" => $day,
    //                         "month" => $month,
    //                         "year" => $year
    //                     ),
    //                   "currency" => $saveArray['currency'],
    //                   "account_number" => $saveArray['account_number'],
    //                   "object" => "bank_account",
    //                   "routing_number"=>$saveArray['routing_number']
    //                 );
    //                 $acct->tos_acceptance = array(
    //                   "date"=>time(),
    //                   "ip"=>$saveArray['ip_address']
    //                 );
    //                 // print_r($acct);die();
    //                 $acct->save();
    //                 $updateData = [
    //                     'stripe_verified' => "P",
    //                     'updated_at' => date('Y-m-d H:i:s')
    //                 ];
    //                 $update = User::where(['id'=>$saveArray['user_id']])->update($updateData);
    //                 if($update){
    //                     return response()->json(['status'=>'1','message'=>'Stripe account verified successfully'], $this->successStatus);
    //                 }
    //             }
    //         }
    //     } catch (\Stripe\Error\InvalidRequest $e){
    //         return Response::json(['status'=>0, 'message'=>$e->getMessage()]);
    //     } catch (NotFoundException $e) {
    //         return Response::json(['status'=>0, 'message'=>$e->getMessage()]);
    //     } catch (BadRequestException $e){
    //         return Response::json(['status'=>0, 'message'=>$e->getMessage()]);
    //     } catch (UnauthorizedException $e){
    //         return Response::json(['status'=>0, 'message'=>$e->getMessage()]);
    //     } catch (InvalidRequestException $e){
    //         return Response::json(['status'=>0, 'message'=>$e->getMessage()]);
    //     } catch (CardErrorException $e){
    //         return Response::json(['status'=>0, 'message'=>$e->getMessage()]);
    //     } catch (ServerErrorException $e){
    //         return Response::json(['status'=>0, 'message'=>$e->getMessage()]);
    //     }
    // }
    /*old method*/

    public function index(Request $request)
    {
        $jobs = $this->jobRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($jobs->toArray(), 'Jobs retrieved successfully');
    }

    /**
     * Store a newly created Job in storage.
     * POST /jobs
     *
     * @param CreateJobAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateJobAPIRequest $request)
    {
        $input = $request->all();

        $job = $this->jobRepository->create($input);

        return $this->sendResponse($job->toArray(), 'Job saved successfully');
    }

    /**
     * Display the specified Job.
     * GET|HEAD /jobs/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Job $job */
        $job = $this->jobRepository->find($id);

        if (empty($job)) {
            return $this->sendError('Job not found');
        }

        return $this->sendResponse($job->toArray(), 'Job retrieved successfully');
    }

    /**
     * Update the specified Job in storage.
     * PUT/PATCH /jobs/{id}
     *
     * @param int $id
     * @param UpdateJobAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateJobAPIRequest $request)
    {
        $input = $request->all();

        /** @var Job $job */
        $job = $this->jobRepository->find($id);

        if (empty($job)) {
            return $this->sendError('Job not found');
        }

        $job = $this->jobRepository->update($input, $id);

        return $this->sendResponse($job->toArray(), 'Job updated successfully');
    }

    /**
     * Remove the specified Job from storage.
     * DELETE /jobs/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Job $job */
        $job = $this->jobRepository->find($id);

        if (empty($job)) {
            return $this->sendError('Job not found');
        }

        $job->delete();

        return $this->sendSuccess('Job deleted successfully');
    }
}
