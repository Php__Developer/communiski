<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateGroupRequest;
use App\Http\Requests\UpdateGroupRequest;
use App\Repositories\GroupRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use App\Models\Group;
use App\Models\Group_member;
use App\Models\User;
use Flash;
use Response;

class GroupController extends AppBaseController
{
    /** @var  GroupRepository */
    private $groupRepository;

    public function __construct(GroupRepository $groupRepo)
    {
        $this->groupRepository = $groupRepo;
    }

    /**
     * Display a listing of the Group.
     *
     * @param Request $request
     *
     * @return Response
     */

    public function groupmembers(Request $request , $id)
    {
        $groupid = $id;
        $userids = array();
        $groupDetail = Group_member::where(['group_id'=> $groupid])->where(['request_status'=>'accepted'])->get();
        foreach ($groupDetail as $grp) {
        $userid    = $grp['user_id'];
        $userids[] = $userid;
    }
    $users = User::whereIn('id', $userids)->orderBy('id','ASC')->get();
    return view('groups.members')->with('groupid', $groupid)->with('users', $users);
    }

    public function index(Request $request)
    {
        $groups = Group::join('users', 'users.id', '=', 'groups.group_owner_id')
            ->orderBy('id', 'DESC')
            ->select('users.name as user_name','groups.*')
            ->paginate(10);


        //$groups = Group::orderBy('id', 'DESC')->paginate(10);
        return view('groups.index')->with('groups', $groups);
    }

    /**
     * Show the form for creating a new Group.
     *
     * @return Response
     */
    /*public function create()
    {
        return view('groups.create');
    }*/

    /**
     * Store a newly created Group in storage.
     *
     * @param CreateGroupRequest $request
     *
     * @return Response
     */
    public function store(CreateGroupRequest $request)
    {
        $input = $request->all();

        $group = $this->groupRepository->create($input);

        Flash::success('Group saved successfully.');

        return redirect(route('groups.index'));
    }

    /**
     * Display the specified Group.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        //$group = $this->groupRepository->find($id);
        $group = Group::where(['id'=>$id])->first();
        if (empty($group)) {
            Flash::error('Group not found');
            return redirect(route('groups.index'));
        }
        return view('groups.show')->with('group', $group);
    }

    /**
     * Show the form for editing the specified Group.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $group = $this->groupRepository->find($id);

        if (empty($group)) {
            Flash::error('Group not found');

            return redirect(route('groups.index'));
        }

        return view('groups.edit')->with('group', $group);
    }

    /**
     * Update the specified Group in storage.
     *
     * @param int $id
     * @param UpdateGroupRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateGroupRequest $request)
    {
        $group = $this->groupRepository->find($id);

        if (empty($group)) {
            Flash::error('Group not found');

            return redirect(route('groups.index'));
        }

        $group = $this->groupRepository->update($request->all(), $id);

        Flash::success('Group updated successfully.');

        return redirect(route('groups.index'));
    }

    /**
     * Remove the specified Group from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $group = $this->groupRepository->find($id);

        if (empty($group)) {
            Flash::error('Group not found');

            return redirect(route('groups.index'));
        }

        $this->groupRepository->delete($id);

        Flash::success('Group deleted successfully.');

        return redirect(route('groups.index'));
    }

    public function searchgroup(Request $request)
    {
        $search = $request->get('search');

            /*$groups = Group::join('users', 'users.id', '=', 'groups.group_owner_id')
            ->where('user_name','like','%'.$search.'%')
            ->orWhere('group_location','like','%'.$search.'%')
            ->orWhere('privacy_status','like','%'.$search.'%')
            ->orderBy('id','desc')
            ->select('users.name as user_name','groups.*')
            ->paginate(5);*/

            $groups = Group::join('users', 'users.id', '=', 'groups.group_owner_id')
            ->where('group_name','like','%'.$search.'%')
            ->orWhere('group_location','like','%'.$search.'%')
            ->orWhere('privacy_status','like','%'.$search.'%')
            ->orderBy('id', 'DESC')
            ->select('users.name as user_name','groups.*')
            ->paginate(10);
        //$groups = Group::where('group_name','like','%'.$search.'%')->orWhere('group_location','like','%'.$search.'%')->orWhere('privacy_status','like','%'.$search.'%')->orderBy('id','desc')->paginate(10);
        if($groups){
            return view('groups.index')->with('groups', $groups);
        }
    }
}
