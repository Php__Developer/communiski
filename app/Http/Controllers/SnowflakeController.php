<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateSnowflakeRequest;
use App\Http\Requests\UpdateSnowflakeRequest;
use App\Repositories\SnowflakeRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Snowflake;
use Flash;
use Response;

class SnowflakeController extends AppBaseController
{
    /** @var  SnowflakeRepository */
    private $snowflakeRepository;

    public function __construct(SnowflakeRepository $snowflakeRepo)
    {
        $this->snowflakeRepository = $snowflakeRepo;
    }

    /**
     * Display a listing of the Snowflake.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $snowflakes = Snowflake::join('users', 'users.id', '=', 'snowflakes.user_id')
        ->orderBy('id', 'DESC')
        ->select('users.name as user_name','snowflakes.*')
        ->paginate(10);
            //print_r($snowflakes);die();
        //$snowflakes = $this->snowflakeRepository->paginate(10);

        return view('snowflakes.index')
            ->with('snowflakes', $snowflakes);
    }

    /**
     * Show the form for creating a new Snowflake.
     *
     * @return Response
     */
    public function create()
    {
        return view('snowflakes.create');
    }

    /**
     * Store a newly created Snowflake in storage.
     *
     * @param CreateSnowflakeRequest $request
     *
     * @return Response
     */
    public function store(CreateSnowflakeRequest $request)
    {
        $input = $request->all();

        $snowflake = $this->snowflakeRepository->create($input);

        Flash::success('Snowflake saved successfully.');

        return redirect(route('snowflakes.index'));
    }

    /**
     * Display the specified Snowflake.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        //$snowflake = $this->snowflakeRepository->find($id);
        $snowflake = Snowflake::join('users', 'users.id', '=', 'snowflakes.user_id')
        ->select('users.name as user_name','snowflakes.*')
        ->where('snowflakes.id', '=', $id)
        ->first();

        if (empty($snowflake)) {
            Flash::error('Snowflake not found');
            return redirect(route('snowflakes.index'));
        }

        return view('snowflakes.show')->with('snowflake', $snowflake);
    }

    /**
     * Show the form for editing the specified Snowflake.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $snowflake = $this->snowflakeRepository->find($id);

        if (empty($snowflake)) {
            Flash::error('Snowflake not found');

            return redirect(route('snowflakes.index'));
        }

        return view('snowflakes.edit')->with('snowflake', $snowflake);
    }

    /**
     * Update the specified Snowflake in storage.
     *
     * @param int $id
     * @param UpdateSnowflakeRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSnowflakeRequest $request)
    {
        $snowflake = $this->snowflakeRepository->find($id);

        if (empty($snowflake)) {
            Flash::error('Snowflake not found');

            return redirect(route('snowflakes.index'));
        }

        $snowflake = $this->snowflakeRepository->update($request->all(), $id);

        Flash::success('Snowflake updated successfully.');

        return redirect(route('snowflakes.index'));
    }

    /**
     * Remove the specified Snowflake from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $snowflake = $this->snowflakeRepository->find($id);

        if (empty($snowflake)) {
            Flash::error('Snowflake not found');

            return redirect(route('snowflakes.index'));
        }

        $this->snowflakeRepository->delete($id);

        Flash::success('Snowflake deleted successfully.');

        return redirect(route('snowflakes.index'));
    }

    public function confirmed(Request $request , $id)
    {
        $snowflake = $this->snowflakeRepository->find($id);
        if (empty($snowflake)) {
            Flash::error('Snowflake not found');
            return redirect(route('snowflakes.index'));
        }
        $updateData = [
          'reward_status'=>'1',
          'updated_at'=>date('Y-m-d h:i:s'),
        ];
        $update = Snowflake::where(['id'=>$id])->update($updateData);
        Flash::success('Snowflake Accepted Successfully.');

        return redirect(route('snowflakes.index'));
    }

    public function reject(Request $request , $id)
    {
        $snowflake = $this->snowflakeRepository->find($id);
        if (empty($snowflake)) {
            Flash::error('Snowflake not found');
            return redirect(route('snowflakes.index'));
        }
        $updateData = [
          'reward_status'=>'2',
          'updated_at'=>date('Y-m-d h:i:s'),
        ];
        $update = Snowflake::where(['id'=>$id])->update($updateData);
        Flash::success('Snowflake Rejected.');
        return redirect(route('snowflakes.index'));
    }
}
