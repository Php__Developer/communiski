<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Repositories\UserRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use App\Models\User;
use Validator;
use Flash;
use Response;

class UserController extends AppBaseController
{
    /** @var  UserRepository */
    private $userRepository;

    public function __construct(UserRepository $userRepo)
    {
        $this->userRepository = $userRepo;
    }

    /**
     * Display a listing of the User.
     *
     * @param Request $request
     *
     * @return Response
     */

    public function adminprofile(Request $request)
    {
        $users = User::where('id','2')->first(); 
        return view('users.adminprofile')->with('users', $users);
    }

    public function updateadminprofile(Request $request)
    {
       $messages = [
             'name.required' => 'Name is required. ',
             'email.required' => 'Email is required',
             'phone_no.numeric' => 'Phone no. must be numeric',
            ];
            $validator = Validator::make($request->all(), [
            'name' => 'required',
            'phone_no' => 'required|numeric|min:12',
            'email' => 'required'
            ], $messages);
            if ($validator->fails()) {
                  return redirect()->back()->withErrors($validator)->withInput();
            } else {
                $id = $request->input('id');
                if($request->hasFile('image')){
                    $file = time().$request->image->getClientOriginalName();
                    $request->image->move(public_path('/images') . '/', $file);
                }
                else{
                    $get_image = User::where(['id'=>'2'])->select('image')->first();
                    $file = $get_image['image'];
                }
                $updateUser = [
                    'name' => $request->input('name'),
                    'phone_no' => $request->input('phone_no'),
                    'email' => $request->input('email'),
                    'image' => $file,
                    'updated_at'=> date('d-m-y h:i:s')
                ];
                $users =  User::where(['id'=>'2'])->update($updateUser);
                if ($users) {
                     return redirect('adminprofile')->with('su_status', 'Updated Sucessfully!');
                }
            }
    }

    public function index(Request $request)
    {
        $users = User::orderBy('id', 'DESC')->paginate(10); 

        return view('users.index')
            ->with('users', $users);
    }

    /**
     * Show the form for creating a new User.
     *
     * @return Response
     */
    public function create()
    {
        return view('users.create');
    }

    /**
     * Store a newly created User in storage.
     *
     * @param CreateUserRequest $request
     *
     * @return Response
     */
    public function store(CreateUserRequest $request)
    {
        $input = $request->all();

        $user = $this->userRepository->create($input);

        Flash::success('User saved successfully.');

        return redirect(route('users.index'));
    }

    /**
     * Display the specified User.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $user = $this->userRepository->find($id);

        $refferalcount = User::where('referral_by',$id)->get()->count();
        $referralusers = User::where('referral_by',$id)->select('id','name','image','created_at')->get();
        if (empty($user)) {
            Flash::error('User not found');

            return redirect(route('users.index'));
        }

        return view('users.show')->with('user', $user)->with('refferalcount', $refferalcount)->with('referralusers', $referralusers);
    }

    /**
     * Show the form for editing the specified User.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $user = $this->userRepository->find($id);
        if (empty($user)) {
            Flash::error('User not found');

            return redirect(route('users.index'));
        }

        return view('users.edit')->with('user', $user);
    }

    /**
     * Update the specified User in storage.
     *
     * @param int $id
     * @param UpdateUserRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateUserRequest $request)
    {
        $user = $this->userRepository->find($id);

        if (empty($user)) {
            Flash::error('User not found');

            return redirect(route('users.index'));
        }

        $user = $this->userRepository->update($request->all(), $id);

        Flash::success('User updated successfully.');

        return redirect(route('users.index'));
    }

    /**
     * Remove the specified User from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $user = $this->userRepository->find($id);

        if (empty($user)) {
            Flash::error('User not found');

            return redirect(route('users.index'));
        }

        $this->userRepository->delete($id);

        Flash::success('User deleted successfully.');

        return redirect(route('users.index'));
    }

    public function searchuser(Request $request)
    {
        $search = $request->get('search');
        $users = User::where('name','like','%'.$search.'%')->orWhere('email','like','%'.$search.'%')->orWhere('phone_no','like','%'.$search.'%')->orderBy('id','desc')->paginate(10);
        if($users){
            return view('users.index')->with('users', $users);
        }
    }
}
