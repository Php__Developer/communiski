<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateQARequest;
use App\Http\Requests\UpdateQARequest;
use App\Repositories\QARepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class QAController extends AppBaseController
{
    /** @var  QARepository */
    private $qARepository;

    public function __construct(QARepository $qARepo)
    {
        $this->qARepository = $qARepo;
    }

    /**
     * Display a listing of the QA.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $qAS = $this->qARepository->all();

        return view('q_a_s.index')
            ->with('qAS', $qAS);
    }

    /**
     * Show the form for creating a new QA.
     *
     * @return Response
     */
    public function create()
    {
        return view('q_a_s.create');
    }

    /**
     * Store a newly created QA in storage.
     *
     * @param CreateQARequest $request
     *
     * @return Response
     */
    public function store(CreateQARequest $request)
    {
        $input = $request->all();

        $qA = $this->qARepository->create($input);

        Flash::success('Q A saved successfully.');

        return redirect(route('qAS.index'));
    }

    /**
     * Display the specified QA.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $qA = $this->qARepository->find($id);

        if (empty($qA)) {
            Flash::error('Q A not found');

            return redirect(route('qAS.index'));
        }

        return view('q_a_s.show')->with('qA', $qA);
    }

    /**
     * Show the form for editing the specified QA.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $qA = $this->qARepository->find($id);

        if (empty($qA)) {
            Flash::error('Q A not found');

            return redirect(route('qAS.index'));
        }

        return view('q_a_s.edit')->with('qA', $qA);
    }

    /**
     * Update the specified QA in storage.
     *
     * @param int $id
     * @param UpdateQARequest $request
     *
     * @return Response
     */
    public function update($id, UpdateQARequest $request)
    {
        $qA = $this->qARepository->find($id);

        if (empty($qA)) {
            Flash::error('Q A not found');

            return redirect(route('qAS.index'));
        }

        $qA = $this->qARepository->update($request->all(), $id);

        Flash::success('Q A updated successfully.');

        return redirect(route('qAS.index'));
    }

    /**
     * Remove the specified QA from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $qA = $this->qARepository->find($id);

        if (empty($qA)) {
            Flash::error('Q A not found');

            return redirect(route('qAS.index'));
        }

        $this->qARepository->delete($id);

        Flash::success('Q A deleted successfully.');

        return redirect(route('qAS.index'));
    }
}
