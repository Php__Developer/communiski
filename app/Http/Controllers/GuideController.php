<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateGuideRequest;
use App\Http\Requests\UpdateGuideRequest;
use App\Repositories\GuideRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class GuideController extends AppBaseController
{
    /** @var  GuideRepository */
    private $guideRepository;

    public function __construct(GuideRepository $guideRepo)
    {
        $this->guideRepository = $guideRepo;
    }

    /**
     * Display a listing of the Guide.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $guides = $this->guideRepository->paginate(10);

        return view('guides.index')
            ->with('guides', $guides);
    }

    /**
     * Show the form for creating a new Guide.
     *
     * @return Response
     */
    public function create()
    {
        return view('guides.create');
    }

    /**
     * Store a newly created Guide in storage.
     *
     * @param CreateGuideRequest $request
     *
     * @return Response
     */
    public function store(CreateGuideRequest $request)
    {
        $input = $request->all();

        $guide = $this->guideRepository->create($input);

        Flash::success('Guide saved successfully.');

        return redirect(route('guides.index'));
    }

    /**
     * Display the specified Guide.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $guide = $this->guideRepository->find($id);

        if (empty($guide)) {
            Flash::error('Guide not found');

            return redirect(route('guides.index'));
        }

        return view('guides.show')->with('guide', $guide);
    }

    /**
     * Show the form for editing the specified Guide.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $guide = $this->guideRepository->find($id);

        if (empty($guide)) {
            Flash::error('Guide not found');

            return redirect(route('guides.index'));
        }

        return view('guides.edit')->with('guide', $guide);
    }

    /**
     * Update the specified Guide in storage.
     *
     * @param int $id
     * @param UpdateGuideRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateGuideRequest $request)
    {
        $guide = $this->guideRepository->find($id);

        if (empty($guide)) {
            Flash::error('Guide not found');

            return redirect(route('guides.index'));
        }

        $guide = $this->guideRepository->update($request->all(), $id);

        Flash::success('Guide updated successfully.');

        return redirect(route('guides.index'));
    }

    /**
     * Remove the specified Guide from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $guide = $this->guideRepository->find($id);

        if (empty($guide)) {
            Flash::error('Guide not found');

            return redirect(route('guides.index'));
        }

        $this->guideRepository->delete($id);

        Flash::success('Guide deleted successfully.');

        return redirect(route('guides.index'));
    }
}
