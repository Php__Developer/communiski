<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateResortRequest;
use App\Http\Requests\UpdateResortRequest;
use App\Repositories\ResortRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Models\Resort;
use App\Models\Import_item;
use App\Models\Resort_detail;
use App\Models\Resort_history;
use App\Models\Access_code;
use DB;
use Excel;
use CountryState;
use Flash;
use Response;

class ResortController extends AppBaseController
{
    /** @var  ResortRepository */
    private $resortRepository;

    public function __construct(ResortRepository $resortRepo)
    {
        $this->resortRepository = $resortRepo;
    }

    /**
     * Display a listing of the Resort.
     *
     * @param Request $request
     *
     * @return Response
     */



    public function index(Request $request)
    {
        $resorts = Resort::join('countries', 'countries.id', '=', 'resorts.country')
        ->join('states', 'states.id', '=', 'resorts.state')->orderBy('id', 'DESC')
        ->select('countries.name as country_name','states.name as state_name','resorts.*')
        ->paginate(5);
        return view('resorts.index')->with('resorts', $resorts);
    }

    /**
     * Show the form for creating a new Resort.
     *
     * @return Response
     */
    public function create()
    {
        $countries = DB::table("countries")->pluck("name","id");
        return view('resorts.create',compact('countries'));
    }

    public function getStateList(Request $request)
    {
        $states = DB::table("states")
        ->where("country_id",$request->country_id)
        ->pluck("name","id");
        return response()->json($states);
    }

    /**
     * Store a newly created Resort in storage.
     *
     * @param CreateResortRequest $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $name = $request->input('resort_name');
        $toplat = $request->input('top_latitude');
        $toplong = $request->input('top_longitude');
        $lat = $request->input('latitude');
        $long = $request->input('longitude');
        $address = $request->input('address');
        $country = $request->input('country');
        $phone_no = $request->input('phone_no');
        $state = $request->input('state');
        $image = $request->input('image');
        $lift_names = $request->input('lift_names');
        $lift_numbers = $request->input('lift_numbers');
        $run_names = $request->input('run_names');
        $run_numbers = $request->input('run_numbers');

        if ($request->hasFile('image')) {
            $file = time().$request->image->getClientOriginalName();
            $request->image->move(public_path('/resort_images') . '/', $file);
        } else {
            $file = 'default.jpg';
        }
        $resortdetail = [
            'website' => '',
            'resort_name' => $name,
            'top_latitude' => $toplat?$toplat:'',
            'top_longitude' => $toplong?$toplong:'',
            'latitude' => $lat,
            'longitude' => $long,
            'address' => $address,
            'phone_no' => $phone_no,
            'country' => $country,
            'state' => $state,
            'image' => $file,
            'lift_names' => $lift_names,
            'lift_numbers' => $lift_numbers,
            'run_names' => $run_names,
            'run_numbers' => $run_numbers,
            'bcolor' => '#10aec4',
            'fcolor' => '#ffa710',
            'run_grades' => 'Green,Blue,Black,Double Black',
            'created_at'=>date('Y-m-d h:i:s'),
        ];
        $rdetail =  Resort::insertGetId($resortdetail);
        /*insert resort details in resort_detail table for getting record in thumbs api*/
        $resortdetails = [
            'resort_id'=>$rdetail,
            'created_at'=>date('Y-m-d h:i:s'),
        ];
        $resdetail =  Resort_detail::insertGetId($resortdetails);
        /*insert resort details in Resort_history table for getting record in thumbs api*/
        $resortHistory= [
            'resort_id'=>$rdetail,
            'user_id'=>'2',
            'created_at'=>date('Y-m-d h:i:s'),
        ];
        $reshistory =  Resort_history::insertGetId($resortHistory);
        /*code generation*/
        $resort_id = $rdetail;

        $ADMrandomname = 'ADM';
        $ADMgetRstring = ResortController::ADMgenerateRandomString();
        $adminCode = $ADMrandomname.$ADMgetRstring;

        $EDIrandomname = 'EDI';
        $EDIgetRstring = ResortController::EDIgenerateRandomString();
        $editorCode = $EDIrandomname.$EDIgetRstring;

        $PATrandomname = 'PAT';
        $PATgetRstring = ResortController::EDIgenerateRandomString();
        $patrollerCode = $PATrandomname.$PATgetRstring;

        $LIFrandomname = 'LIF';
        $LIFgetRstring = ResortController::LIFgenerateRandomString();
        $liftyCode = $LIFrandomname.$LIFgetRstring;

        $gcodeData = [
            'resort_id'=> $resort_id,
            'resort_administrator'=> $adminCode,
            'resort_editor'=>$editorCode,
            'patroller'=>$patrollerCode,
            'lifty'=>$liftyCode,
            'ambassador'=>Null,
            'created_at'=>date('Y-m-d h:i:s'),
        ];
        $getcodes = Access_code::insertGetId($gcodeData);

        Flash::success('Resort saved successfully.');

        return redirect(route('resorts.index'));
    }

    public  function ADMgenerateRandomString($length = 7) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public  function EDIgenerateRandomString($length = 7) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public  function PATgenerateRandomString($length = 7) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public  function LIFgenerateRandomString($length = 7) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    /**
     * Display the specified Resort.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $resort = Resort::join('countries', 'countries.id', '=', 'resorts.country')
        ->join('states', 'states.id', '=', 'resorts.state')
        ->where('resorts.id', '=', $id)
        ->select('countries.name as country_name','states.name as state_name','resorts.*')
        ->first();

        $accessCode = Access_code::where('resort_id', '=', $id)->first();

        if (empty($resort)) {
            Flash::error('Resort not found');

            return redirect(route('resorts.index'));
        }

        return view('resorts.show')->with('resort', $resort)->with('accessCode', $accessCode);
    }

    /**
     * Show the form for editing the specified Resort.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $countries = DB::table("countries")->pluck("name","id");
        $states = DB::table("states")->pluck("name","id");
        $resort = $this->resortRepository->find($id);
        // print_r(json_encode($states));die();
        if (empty($resort)) {
            Flash::error('Resort not found');

            return redirect(route('resorts.index'));
        }
        return view('resorts.edit',compact('resort','countries','states'));
        //return view('resorts.edit')->with('resort', $resort);
    }

    /**
     * Update the specified Resort in storage.
     *
     * @param int $id
     * @param UpdateResortRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateResortRequest $request)
    {
        $resort = $this->resortRepository->find($id);
        $saveArray = $request->all();
        if ($request->hasFile('image')) {
            $file = time().$request->image->getClientOriginalName();
            $request->image->move(public_path('/resort_images') . '/', $file);
        } else {
            $get_image = Resort::where(['id'=>$id])->select('image')->first();
            $file = $get_image['image'];
        }
        $updateResort = [
            'resort_name' => $saveArray['resort_name'],
            'address' => $saveArray['address']?$saveArray['address']:'',
            'top_latitude' => $saveArray['top_latitude']?$saveArray['top_latitude']:$resort['top_latitude'],
            'top_longitude' => $saveArray['top_longitude']?$saveArray['top_longitude']:$resort['top_longitude'],
            'latitude' => $saveArray['latitude']?$saveArray['latitude']:$resort['latitude'],
            'longitude' => $saveArray['longitude']?$saveArray['longitude']:$resort['longitude'],
            'country' => $saveArray['country'],
            'state' => $saveArray['state'],
            'phone_no' => $saveArray['phone_no'],
            'image' => $file,
            'lift_names' => $saveArray['lift_names'],
            'lift_numbers' => $saveArray['lift_numbers'],
            'run_names' => $saveArray['run_names'],
            'run_numbers' => $saveArray['run_numbers'],
        ];
        // print_r($updateResort);die();
        $resort =  Resort::where(['id'=>$id])->update($updateResort);
        if ($resort) {

            Flash::success('Resort updated successfully.');

            return redirect(route('resorts.index'));
        }
    }

    public function importfile(Request $request)
    {
        return view('resorts.importfile');
    }

    public function importExcel1(Request $request)
    {  
        if(Input::hasFile('import_file')){
            $path = Input::file('import_file')->getRealPath();
            $data = Excel::load($path, function($reader){})->get()->toArray();
            // dd($data);
            if(!empty($data) && count($data)){
                foreach ($data as $key => $value) {
                    $insert[] = [
                        'resort_name' => $value['resort_name'], 
                        'country' => $value['country'],
                        'state' => $value['state']
                    ];
                }
                if(!empty($insert)){
                    DB::table('import_items')->insert($insert);
                    //  dd('Insert Record successfully.');
                }
            }
        }
        dd("no file");
        return back();
    }

    public function importExcel(Request $request)
    {
        if($request->hasFile('import_file'))
        {
            $path = $request->file('import_file')->getRealPath();
            $data = \Excel::load($path)->get();
            if($data->count()){
                foreach ($data as $value) {
                    foreach ($value as $value1) {
                        $arr[] = [
                            'resort_name' => $value1['resort_name'], 
                            'country' => $value1['country'],
                            'state' => $value1['state']
                        ];
                    }
                }
                if(!empty($arr)){
                    $lastId = Import_item::insert($arr);
                    // \DB::table('import_items')->insert($arr);
                    dd('Insert Record successfully.');
                }
            }
        }
        dd('Request data does not have any files to import.');      
    } 

    /**
     * Remove the specified Resort from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $resort = $this->resortRepository->find($id);

        if (empty($resort)) {
            Flash::error('Resort not found');

            return redirect(route('resorts.index'));
        }

        $this->resortRepository->delete($id);

        Flash::success('Resort deleted successfully.');

        return redirect(route('resorts.index'));
    }

    public function searchresort(Request $request)
    {
        $search = $request->get('search');
        // $resorts = Resort::where('resort_name','like','%'.$search.'%')
        $resorts = Resort::join('countries', 'countries.id', '=', 'resorts.country')
        ->join('states', 'states.id', '=', 'resorts.state')
        ->where('resort_name','like','%'.$search.'%')
        ->orderBy('id', 'DESC')
        ->select('countries.name as country_name','states.name as state_name','resorts.*')
        ->paginate(10);
        if($resorts){
            return view('resorts.index')->with('resorts', $resorts);
        }
    }
}
