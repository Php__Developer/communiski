<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateRunRequest;
use App\Http\Requests\UpdateRunRequest;
use App\Repositories\RunRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use App\Models\Resort;
use App\Models\Run;
use App\Models\Country;
use App\Models\State;
use Flash;
use DB;
use Response;

class RunController extends AppBaseController
{
    /** @var  RunRepository */
    private $runRepository;

    public function __construct(RunRepository $runRepo)
    {
        $this->runRepository = $runRepo;
    }

    /**
     * Display a listing of the Run.
     *
     * @param Request $request
     *
     * @return Response
     */

    public function index(Request $request)
    {
        $runs = Run::join('resorts', 'resorts.id', '=', 'runs.resort_id')
        ->join('states', 'states.id', '=', 'runs.state_id')
        ->join('countries', 'countries.id', '=', 'runs.country_id')
        ->join('users', 'users.id', '=', 'runs.user_id')
        ->orderBy('id', 'DESC')
        ->select('resorts.resort_name as resort_name','countries.name as country_name','states.name as state_name','users.name as user_name','runs.*')
        ->paginate(5);
        //print_r($runs);die();
        return view('runs.index')
            ->with('runs', $runs);
    }

    /**
     * Show the form for creating a new Run.
     *
     * @return Response
     */
    public function create()
    {
        return view('runs.create');
    }

    /**
     * Store a newly created Run in storage.
     *
     * @param CreateRunRequest $request
     *
     * @return Response
     */
    public function store(CreateRunRequest $request)
    {
        $input = $request->all();

        $run = $this->runRepository->create($input);

        Flash::success('Run saved successfully.');

        return redirect(route('runs.index'));
    }

    /**
     * Display the specified Run.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        //$run = $this->runRepository->find($id);
        $run = Run::join('resorts', 'resorts.id', '=', 'runs.resort_id')
        ->join('states', 'states.id', '=', 'runs.state_id')
        ->join('countries', 'countries.id', '=', 'runs.country_id')
        ->where('runs.id', '=', $id)
        ->orderBy('id', 'DESC')
        ->select('resorts.resort_name as resort_name','countries.name as country_name','states.name as state_name','runs.*')->first();

        if (empty($run)) {
            Flash::error('Run not found');

            return redirect(route('runs.index'));
        }

        return view('runs.show')->with('run', $run);
    }

    /**
     * Show the form for editing the specified Run.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $run = $this->runRepository->find($id);

        if (empty($run)) {
            Flash::error('Run not found');

            return redirect(route('runs.index'));
        }

        return view('runs.edit')->with('run', $run);
    }

    /**
     * Update the specified Run in storage.
     *
     * @param int $id
     * @param UpdateRunRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRunRequest $request)
    {
        $run = $this->runRepository->find($id);

        if (empty($run)) {
            Flash::error('Run not found');

            return redirect(route('runs.index'));
        }

        $run = $this->runRepository->update($request->all(), $id);

        Flash::success('Run updated successfully.');

        return redirect(route('runs.index'));
    }

    /**
     * Remove the specified Run from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $run = $this->runRepository->find($id);

        if (empty($run)) {
            Flash::error('Run not found');

            return redirect(route('runs.index'));
        }

        $this->runRepository->delete($id);

        Flash::success('Run deleted successfully.');

        return redirect(route('runs.index'));
    }

    public function test(Request $request)
    {
        return view('runs.test');
    }

    public function insert(Request $request)
    {
        $name = $request->input('name1');
        $items = $request->input('name');
        $item1 = implode(",",$items);
        if (empty($item1)) {
            $allitem = 0;
        }
        else{
            $allitem = $item1;
        }
        if (empty($name)) {
            $nam = 0;
        }
        else{
            $nam = $name;
        }
        $cartsave = array(
                'name' => $nam,
                'item1' => $allitem
            );
            DB::table('test')->insert($cartsave);
            if ($cartsave) {
                echo "saved";
            }
            else{
                echo "error";
            }
    }
}
