<?php
 
namespace App\Traits;

use App\Models\User;
use App\Models\Device;
use App\Models\Snowflake;
use App\Models\Rewards_point;
use App\Models\Permission;
use App\Snowflake_reward;
use Illuminate\Http\Request;

trait CommonFunctionTrait {
 
    public function rewardSnowflakes($amount, $reason, $id) {
        Snowflake_reward::insert(
                [
                    'user_id' => $id,
                    'amount' => $amount, 
                    'rewarded_spent' => 'rewarded',
                    'status' => 'confirmed',
                    'reward_for' => $reason,
                ]
            );
    }

    public function updateSnowflakeSum($amount, $id) {
        $oldPoints = \App\User::where('id', '=', $id)->select('snowflakes')->first();
        $newPoints = $oldPoints->snowflakes + $amount;
        \App\User::where('id', '=', $id)->update(['snowflakes' => $newPoints]);
    	// $getoldpoints = Rewards_point::where(['user_id'=>$data['user_id']])
    	// ->select('total')
    	// ->first();
    	// $total = $getoldpoints['total'] + $data['snow_reward'];
    	// $updatePoints = [
    	// 	'total'=>$total,
    	// 	'updated_at'=>date('Y-m-d h:i:s')
    	// ];
    	// $update = Rewards_point::where(['user_id'=>$data['user_id']])->update($updatePoints);
    }

    public function generateRandomString($length = 6) 
    {
      $characters = '23456789abcdefghjkmnpqrstuvwxyzABCDEFGHJKMNPQRSTUVWXYZ';
      $charactersLength = strlen($characters);
      $randomString = '';
      for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
      }
      return $randomString;
    }

    public function changeFlagStatus($id, $flag){
        $flag = \App\Flag::where('user_id', '=', $id)->where('flag', '=', $flag)->update(['flag_status' => 1]);
    }

	public function send_notification($id, $message)
    {
        //decides which device type
    	$devices = \App\Device::where('user_id', $id)->get();
        foreach($devices as $device){
            if($device->device_type == "A"){
                $this->android_send_notification($device->device_id, $message);
              }
            else if($device->device_type == "I"){
                $this->iphone_send_notifications($device->device_id, $message);
              }
        }

    }

    public function android_send_notification($deviceIdArr, $message)
    {
        $url = 'https://fcm.googleapis.com/fcm/send';
        $fields = array(
            'registration_ids' => $deviceIdArr,
            'data' => $message,
        );
        if(!defined('GOOGLE_API_KEY')){
            $GOOGLE_API_KEY = 'AAAAYBmHz5g:APA91bHGee6_av7cgW_nA9eNQF_J0SEWlqcS_arCLAdnMStD69eFyjdDx5iDdCV5obWP_9sYD_D3eMHn06hHSKNSD0vSVkTP4-JTl7kOyjPSd8ubu4xgtpy_1nh_9mtBvWvyi6fMd-j4';
        }
        $headers = array(
            'Authorization: key='.$GOOGLE_API_KEY,
            'Content-Type: application/json'
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        if ($result === FALSE){
            die('Curl failed: ' . curl_error($ch));
        }
        curl_close($ch);
        return $result;
    }

    public function iphone_send_notifications($deviceIdArr, $message){
        $result='';
        $body['aps'] = array(
            'alert' => [
            	"body" => $message['message'],
            	"title" => $message['title']
            ],
            'noti_for'=> $message,
            'sound' => 'default',
            'badge' => 0
        );
        $payload = json_encode($body);
        $ctx = stream_context_create();
        $passphrase = '';
        stream_context_set_option($ctx, 'ssl', 'local_cert', 'public/pem/nameOfFile.pem');
        stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
        $fp = stream_socket_client('ssl://gateway.push.apple.com:2195', $err, $errstr, 4, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);
    
        if($fp){
            $message = chr(0) . pack('n', 32) . pack('H*', $deviceIdArr[0]) . pack('n', strlen($payload)) . $payload;
            $result = fwrite($fp, $message, strlen($message)); 
            fclose($fp);
        }
        return $result;
    }

    /*
	   * Business Permission
    */

    public function businessPermission(Request $request ,$business_id,$userExist) 
    {
    	$checkPermission = Permission::where('entity_id',$business_id)
        ->where('entity_type','Business')
        ->where('user_id',$userExist)
        ->where(['permission_type'=>'ADM'])
        ->first();
    	if ($checkPermission) {
    		return 1;
    	}else{
    		return 0;
    	}
    }
}