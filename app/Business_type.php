<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Business_type extends Model
{
    protected $table = 'business_types';
    use SoftDeletes;
    
    public function businesses()
    {
        return $this->hasMany('App\Business');
    }
}
