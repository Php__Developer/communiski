<?php

namespace App\Repositories;

use App\Models\Lunch;
use App\Repositories\BaseRepository;

/**
 * Class LunchRepository
 * @package App\Repositories
 * @version May 29, 2020, 9:58 am UTC
*/

class LunchRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'business_service_id',
        'request_status',
        'active_status'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Lunch::class;
    }
}
