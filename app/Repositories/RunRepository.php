<?php

namespace App\Repositories;

use App\Models\Run;
use App\Repositories\BaseRepository;

/**
 * Class RunRepository
 * @package App\Repositories
 * @version August 30, 2019, 6:10 am UTC
*/

class RunRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'user_id',
        'run_name',
        'run_difficulty',
        'run_length_value',
        'measure_length_unit',
        'max_slope',
        'average_slope',
        'direction_run_faces',
        'accessed_from',
        'run_description'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Run::class;
    }
}
