<?php

namespace App\Repositories;

use App\Models\Lift;
use App\Repositories\BaseRepository;

/**
 * Class LiftRepository
 * @package App\Repositories
 * @version August 30, 2019, 10:36 am UTC
*/

class LiftRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'user_id',
        'lift_name',
        'lift_number',
        'description',
        'lift_type',
        'capacity',
        'manufacturer',
        'installed_year',
        'hourly_capacity',
        'base_elevation',
        'top_elevation',
        'vertical_rise',
        'length',
        'ride_time',
        'opening_time',
        'closing_time'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Lift::class;
    }
}
