<?php

namespace App\Repositories;

use App\Models\Snowflake;
use App\Repositories\BaseRepository;

/**
 * Class SnowflakeRepository
 * @package App\Repositories
 * @version August 21, 2019, 6:21 am UTC
*/

class SnowflakeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'user_id',
        'snowflake_rewards',
        'reason',
        'reward_status'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Snowflake::class;
    }
}
