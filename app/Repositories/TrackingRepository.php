<?php

namespace App\Repositories;

use App\Models\Tracking;
use App\Repositories\BaseRepository;

/**
 * Class TrackingRepository
 * @package App\Repositories
 * @version March 5, 2020, 9:39 am UTC
*/

class TrackingRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'resort_id',
        'track_id',
        'user_id',
        'date',
        'current_track',
        'future_track'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Tracking::class;
    }
}
