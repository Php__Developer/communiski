<?php

namespace App\Repositories;

use App\Models\Weather;
use App\Repositories\BaseRepository;

/**
 * Class WeatherRepository
 * @package App\Repositories
 * @version February 29, 2020, 8:23 am UTC
*/

class WeatherRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'resort_id',
        'report_date',
        'new_snow_top',
        'new_snow_bottom',
        'base_depth_top',
        'base_depth_bottom',
        'on_piste',
        'off_piste',
        'update_at'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Weather::class;
    }
}
