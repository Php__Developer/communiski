<?php

namespace App\Repositories;

use App\Models\QA;
use App\Repositories\BaseRepository;

/**
 * Class QARepository
 * @package App\Repositories
 * @version September 16, 2019, 10:47 am UTC
*/

class QARepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'user_id',
        'question',
        'answer',
        'question_type',
        'topic',
        'archive_status'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return QA::class;
    }
}
