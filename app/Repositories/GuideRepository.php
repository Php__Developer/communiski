<?php

namespace App\Repositories;

use App\Models\Guide;
use App\Repositories\BaseRepository;

/**
 * Class GuideRepository
 * @package App\Repositories
 * @version November 25, 2019, 7:37 am UTC
*/

class GuideRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'guide_title'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Guide::class;
    }
}
