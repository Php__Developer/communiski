<?php

namespace App\Repositories;

use App\Models\Event;
use App\Repositories\BaseRepository;

/**
 * Class EventRepository
 * @package App\Repositories
 * @version April 6, 2020, 1:40 pm UTC
*/

class EventRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'event_name',
        'start_date',
        'end_date',
        'event_location',
        'event_description',
        'tags',
        'event_image',
        'event_host_id',
        'event_link',
        'event_contact_email',
        'event_contact_number',
        'event_price',
        'declaration'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Event::class;
    }
}
