<?php

namespace App\Repositories;

use App\Models\Resort;
use App\Repositories\BaseRepository;

/**
 * Class ResortRepository
 * @package App\Repositories
 * @version August 23, 2019, 11:18 am UTC
*/

class ResortRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id',
        'resort_name',
        'country',
        'state',
        'image',
        'lift_names',
        'lift_numbers',
        'run_names',
        'run_numbers'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Resort::class;
    }
}
