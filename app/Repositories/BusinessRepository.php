<?php

namespace App\Repositories;

use App\Models\Business;
use App\Repositories\BaseRepository;

/**
 * Class BusinessRepository
 * @package App\Repositories
 * @version April 9, 2020, 6:58 am UTC
*/

class BusinessRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'owner_id',
        'business_type',
        'business_name',
        'cuisines_services',
        'price_star_rating',
        'facilities_activities',
        'service_image',
        'description',
        'website',
        'email',
        'phone_number',
        'own_declaration'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Business::class;
    }
}
