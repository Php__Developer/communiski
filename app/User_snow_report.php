<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User_snow_report extends Model
{
    protected $table ="user_snow_reports";

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function resort()
    {
        return $this->belongsTo('App\Resort');
    }
}
