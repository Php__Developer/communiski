<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Run extends Model
{
    protected $table = 'runs';
    use SoftDeletes;

    public function resort()
    {
        return $this->belongsTo('App\Resort');
    }
}
