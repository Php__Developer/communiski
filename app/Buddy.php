<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Buddy extends Model
{
    protected $table = 'buddies';

    function user(){
        return $this->belongsTo('App\User');
    }

}
