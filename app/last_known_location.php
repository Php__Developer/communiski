<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class last_known_location extends Model
{
    protected $table = 'last_known_locations'; 

    public function user()
    {
        return $this->hasOne('App\User');
    }
}
